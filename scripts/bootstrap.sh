#!/usr/bin/env bash

# Update the box
# --------------
# Downloads the package lists from the repositories
# and "updates" them to get information on the newest
# versions of packages and their dependencies
apt-get update
apt-get install -y language-pack-en

# Install Vim
apt-get install -y vim

# PHP 5.4
# -------
apt-get install -y libapache2-mod-php5
# Add add-apt-repository binary
apt-get install -y python-software-properties
# Install PHP 5.4
add-apt-repository ppa:ondrej/php5
# Update
apt-get update

# PHP stuff
# ---------
# Command-Line Interpreter
apt-get install -y php5-cli
# MySQL database connections directly from PHP
apt-get install -y php5-mysql
# cURL is a library for getting files from FTP, GOPHER, HTTP server
apt-get install -y php5-curl
# Module for MCrypt functions in PHP
apt-get install -y php5-mcrypt

echo -e "\n--- We definitly need to see the PHP errors, turning them on ---\n"
sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php5/apache2/php.ini
sed -i "s/display_errors = .*/display_errors = On/" /etc/php5/apache2/php.ini

# Apache
# ------
# Install
apt-get install -y apache2


echo -e "\n--- Allowing Apache override to all ---\n"
sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf


# Add ServerName to httpd.conf
echo "ServerName localhost" > /etc/apache2/httpd.conf

# setup hosts file
VHOST=$(cat <<EOF
<VirtualHost *:80>
    DocumentRoot "/var/www/uasdf/public"
    <Directory "/var/www/uasdf/public">
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
EOF
)
echo "${VHOST}" > /etc/apache2/sites-available/000-default.conf


# Enable mod_rewrite
echo -e "\n--- Enabling mod-rewrite ---\n"
a2enmod rewrite
# Restart apache
service apache2 restart

# cURL
# ----
apt-get install -y curl

# Mysql
# -----
# Ignore the post install questions
export DEBIAN_FRONTEND=noninteractive
# Install MySQL quietly
apt-get -q -y install mysql-server-5.5

# Git
# ---
apt-get install -y git-core

# Install Composer
# ----------------
echo -e "\n--- Installing Composer for PHP package management ---\n"
curl --silent https://getcomposer.org/installer | php > /dev/null 2>&1
mv composer.phar /usr/local/bin/composer


# Laravel stuff
# -------------
# Load Composer packages
cd /var/www/uasdf
composer install

rm -f .env
cp scripts/env_local .env

# Set up the database
echo "DROP DATABASE IF EXISTS uasdf_dev" | mysql;
echo "CREATE DATABASE uasdf_dev" | mysql
echo "GRANT ALL PRIVILEGES ON uasdf_dev.* TO 'uasdf'@'localhost' IDENTIFIED BY 'password'" | mysql

mysql uasdf_dev < /var/www/uasdf/scripts/backup/database_dump.sql

php artisan migrate


echo "----------------------------------------------------------------------------"
echo "| If you haven't done so yet, add the following entry on your hosts file:  |"
echo "| 192.168.2.233  uasdf.dev www.uasdf.dev                                   |"
echo "----------------------------------------------------------------------------"
