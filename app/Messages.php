<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model {

	protected $table = 'messages';

    protected $fillable = ['message'];

    public function from_user_data(){
        return $this->hasOne('App\User','id','from_user');
    }

    public function to_user_data(){
        return $this->hasOne('App\User','id','to_user');
    }
}
