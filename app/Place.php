<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model {

	protected $table = 'place';

    protected $fillable  = ['postal_code','establishment','formatted_address','sublocality_level_2','sublocality_level_1','sublocality','locality','administrative_area_level_1','country','lat','lng'];

    public $timestamps = false;
}
