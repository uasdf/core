<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract, SluggableInterface {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	//To generate slugs
    use SluggableTrait;

    protected $sluggable = array(
        'build_from' => 'name',
        'save_to'    => 'username',
        'unique'     => true
    );

    public function getFullnameAttribute() {
        return $this->name;
    }

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password', 'username', 'phone', 'activation_code'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['phone','email', 'password', 'remember_token', 'activation_code', 'email_verified', 'membership', 'created_at', 'updated_at', 'facebook', 'google', 'twitter'];

	public function places(){
		return $this->hasMany('App\Place','user_id')->where(['for' =>'user'])->orderBy('id','asc')->get();
	}

	public function primary_place(){
		return $this->hasMany('App\Place','user_id')->where(['primary' => 1])->first();
	}

	public function task_comments_count(){
		return $this->hasMany('App\TaskComments', 'user_id')->count();
	}

	public function skills(){
		return $this->hasMany('App\UserTags')->where('tag_type','=','skill')->orderBy('level','desc')->get();
	}

	public function learn(){
		return $this->hasMany('App\UserTags')->where('tag_type','=','learn')->get();
	}

	public function teach(){
		return $this->hasMany('App\UserTags')->where('tag_type','=','teach')->get();
	}

	public function user_resolved_tasks(){
		return $this->hasMany('App\Task')->where(['status' => 'resolved', 'deleted' => 0])->orderBy('created_at','desc')->get();
	}

	public function handling_tasks(){
		return $this->hasMany('App\TaskHandleRequest')->where(['deleted' => 0])->orderBy('created_at','desc')->get();
	}

	public function user_unresolved_tasks(){
		return $this->hasMany('App\Task')->where(['status' => 'unresolved', 'deleted' => 0])->orderBy('created_at','desc')->get();
	}

	public function notifications(){
		return $this->hasMany('App\Notifications')->orderBy('created_at','desc');
	}

	public function rating(){
		return $this->hasMany('App\UserRating','to_user')->orderBy('created_at','desc')->get();
	}

	public function languages(){
		return $this->hasMany('App\UserLanguages','user_id')->get();
	}

	public function newsletter(){
		return $this->hasOne('App\Newsletter')->first();
	}



}
