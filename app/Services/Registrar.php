<?php namespace App\Services;

use App\User;
use Validator;
use View;
use Mail;
use App\Helpers\Helper;
use App\Place;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;
use DB;
use Illuminate\Http\Request;
use App\Connections;
use App\ConnectionRequests;
use App\Invitations;
use App\Notifications;
use App\Ui_introduction;
use Image;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Registrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	private $data,$helper;
	public function __construct(Helper $helper, Request $request)
	{
		$this->helper = $helper;
		$this->request = $request;
	}

	public function validator(array $data)
	{
		$temp = $data;
		unset($temp['image']);
		setcookie('user_data', serialize($temp), time() + 3600, "/");
		$data['place'] = $this->helper->place($data['place']);

		return Validator::make($data, [
			'first_name' => 'required|max:30|alpha_spaces',
			'last_name'  => 'required|max:30|alpha_spaces',
			'email'      => 'required|email|max:50|unique:users',
			'phone'      => 'max:25|unique:users',
			'password'   => 'required|confirmed|min:6',
			'place'      => 'required',
			'image'      => 'mimes:jpeg,png'
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
		setcookie('user_data', '', time() - 3600, "/");

		$data['activation_code'] = str_random(30);

		$this->data = $data;
		$create =  User::create([
			'first_name'      => ucwords($data['first_name']),
			'last_name'       => ucwords($data['last_name']),
			'email'           => $data['email'],
			'password'        => bcrypt($data['password']),
			'phone'           => $data['phone'],
			'activation_code' => $data['activation_code']
			
		]);

		$place          = $this->helper->place($data['place']);
		$place          = Place::create($place);
		$place->for     = 'user';
		$place->primary = 1;
		$place->for_id  = $create->id;
		$place->user_id = $create->id;

		$place->save();
			Mail::send('emails.welcome', ['code' => $this->data['activation_code'], 'first_name' => $this->data['first_name'], 'last_name' => $this->data['last_name']], function ($message) {
				$data = $this->data;
				$message->to($data['email'], $data['first_name'] . ' ' . $data['last_name'])
					->subject('Welcome to Placerange! Please activate your account.');
			});

		Mail::send('emails.new-user', ['first_name' => $this->data['first_name'], 'last_name' => $this->data['last_name']], function ($message) {
			$data = $this->data;
			$message->to('placerange@gmail.com', $data['first_name'] . ' ' . $data['last_name'])
				->subject('New user has joined.');
		});

		//Check if this user has been invited by someone
		$invs = Invitations::where('email_id',$data['email'])->get();

		if(!$invs->isEmpty()){
			foreach($invs as $in){
				$cr            = new ConnectionRequests();
				$cr->from_user = $in->user_id;
				$cr->to_user   = $create->id;
				$cr->purpose   = 'friend';
				$cr->status    = 2;
				$cr->save();

				$con          = new Connections();
				$con->user1   = $in->user_id;
				$con->user2   = $create->id;
				$con->purpose = 'friend';
				$con->save();

				//Add notification
				$not             = new Notifications();
				$not->user_id    = $in->user_id;
				$not->type       = 'user_joined';
				$not->target_url = '/user/' . $create->username;
				$not->content    = '<strong>'.$create->first_name . ' ' . $create->last_name . "</strong> has now joined UASDF";
				$not->save();

				$in->delete();


			}
		}

		// Create a new record for UI Introduction
		$ui_intro	= new Ui_introduction();
		$ui_intro->user_id = $create->id;
		$ui_intro->save();


		if ($this->request->hasFile('image'))
		{
			$file = $this->request->file('image');
			if($file->getMimeType() == 'image/jpeg') $ext = '.jpeg';
			if($file->getMimeType() == 'image/png') $ext = '.png';
			if($ext) {
				$fname = $create->id . '_' . time() . $ext;
				Image::make($file)->save(public_path() . '/uploads/users/original/' . $fname);
				Image::make($file)->fit(250, 250)->save(public_path() . '/uploads/users/thumbnail/' . $fname);
				Image::make($file)->fit(50, 50)->save(public_path() . '/uploads/users/icon/' . $fname);
				Image::make($file)->fit(700, 700)->save(public_path() . '/uploads/users/medium/' . $fname);
				DB::table('users')->where('id', $create->id)->update(array('image_name' => $fname));
				$user = User::find($create->id);
				$user->image_name = $fname;
				$user->save();

			}

		}


		return $create;
	}

}
