<?php namespace App\Http\Controllers;

use App\ConnectionRequests;
use App\Connections;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Guard;
use Illuminate\Http\Request;
use App\Feedback;
use App\User;
use DB;
use App\Workshop;
use App\WorkshopAttendees;
use Validator;
use Image;
use Input;
use App\Helpers\Helper;
use Flash;
use App\Issue;
use Mail;
use App\Notifications;
use Auth;
use App\UserRating;


class UserController extends Controller {

    private $request,$helper,$uid,$auth;

    public function __construct(Guard $auth, Helper $helper){
        $this->auth   = $auth;
        $this->helper = $helper;
        $this->uid    = $this->helper->user_logged_in();
    }

    public function index()
    {
        return view('welcome');
    }

    public function register()
    {   
        $input['first_name'] = '';
        $input['last_name']  = '';
        $input['email']      = '';
        $input['phone']      = '';
        $input['username']   = '';
        $input['place']      = '';
        return view('users.register',compact('input'));
    }

    public function add_skills()
    {   
        $uid    = $this->uid;
        $user   = User::find($uid);
        $skills = $user->skills();
        if(count($skills) > 0 ){
            return redirect('/profile');
        }
        return view('users.add_skills');
    }

    public function add_want_to_learn()
    {   
        $uid = $this->uid;
        return view('users.add_want_to_learn');
    }

    public function upload_image()
    {
        return view('users.upload-image');
    }

    public function post_upload_image(Request $req, Guard $auth )
    {

        $this->request = $req;
        $uid = $this->uid;
        if ($this->request->hasFile('user_image'))
        {
            $file = $this->request->file('user_image');

            if($file->getMimeType() == 'image/jpeg') $ext = '.jpeg';
            if($file->getMimeType() == 'image/png') $ext = '.png';
            if($ext) {
                $fname = $uid.'_'.time().$ext;
                $previous_image = DB::table('users')->where('id', $uid)->pluck('image_name');

                if($previous_image) {
                    $original_url  = public_path() . '/uploads/users/original/' . $previous_image;
                    $medium_url    = public_path() . '/uploads/users/medium/' . $previous_image;
                    $thumbnail_url = public_path() . '/uploads/users/thumbnail/' . $previous_image;
                    $icon_url      = public_path() . '/uploads/users/icon/' . $previous_image;

                    // Check and remove the previous image file
                    if(file_exists ( $original_url )){
                        unlink($original_url);
                    }
                    if(file_exists ( $medium_url )){
                        unlink($medium_url);
                    }
                    if(file_exists ( $thumbnail_url )){
                        unlink($thumbnail_url);
                    }
                    if(file_exists ( $icon_url )){
                        unlink($icon_url);
                    }
                    
                }

                // Resize the uploaded image into various sizes and store them
                Image::make($file)->save(public_path().'/uploads/users/original/'.$fname);
                Image::make($file)->fit(250, 250)->save(public_path().'/uploads/users/thumbnail/'.$fname);
                Image::make($file)->fit(50, 50)->save(public_path().'/uploads/users/icon/'.$fname);
                Image::make($file)->fit(700, 700)->save(public_path().'/uploads/users/medium/'.$fname);

                // Update the databse with the new file name
                DB::table('users')->where('id', $uid)->update(array('image_name' => $fname));
                // Return the file name
                echo $fname;
            }else{
                echo 'error';
            }
        } else {
            echo 'error';
        }


    }

    public function login()
    {
        return view('users.login');
    }

    public function activate($code)
    {
        $uid = $this->uid;
        $activate = DB::table('users')->where('activation_code', $code)->update(array('email_verified' => 1));
        if($activate) {
            Flash::success('Your account has been activated!');
        }elseif(DB::table('users')->where('activation_code', $code)->where(array('email_verified' => 1))->first()) {
            Flash::error('Your account is already activated.');
        }else{
            Flash::error('There was some problem activating your account.');
        }
        if($uid) {
            return redirect('/profile');
        }else{
            return redirect('/login');
        }
    }

    public function user_profile($username){
        /*
        if(!Auth::user()){
            return redirect('/');
        }
        */
        $currentUid = $this->uid;
        //User details
        $user       = User::where('username',$username)->first();
        $uid        = $user->id;
        if(!$user->image_name) $user->image_name = 'default.png';

        //User address details
        $places                 = $user->places();
        $organising_workshops   = Workshop::where('user_id',$uid)->where('date',' >= ',date("Y-m-d"))->orderBy('created_at','desc')->get();
        $organised_workshops    = Workshop::where('user_id',$uid)->where('date',' <= ',date("Y-m-d"))->get();
        $attended_workshops     = WorkshopAttendees::where(['cancelled' => null, 'attended' => 1,'user_id' => $uid])->get();
        $attending_workshops    = WorkshopAttendees::where(['cancelled' => null, 'attended' => null,'user_id' => $uid])->get();
        $handling_issues        = Issue::whereHas('handle_requests', function($q) use($uid)
        {
            $q->where('user_id', $uid)->where('status',1);

        })->get();
        $connectionStatus = null;
        //Current user connection status
        $connection   = ConnectionRequests::whereIn('from_user', [$currentUid,$uid])->whereIn('to_user', [$uid,$currentUid])->first();
        $current_sent = false;
        $to_sent      = false;
        if(!$connection){
            $connectionStatus = null;
        }else{

            $connectionStatus = $connection->status;
            if($connection->from_user == $uid){
                $current_sent = true;
            }else{
                $to_sent = true;
            }
        }

        //User skills
        $skills = $user->skills();
        $learn  = $user->learn();
        $teach  = $user->teach();

        //User unresolved issues
        $my_issues = $user->user_unresolved_issues();

        //User resolved issues
        $resolved_issues = $user->user_resolved_issues();

        // Get the user rating
        $ratings = $user->rating();
        $rating_total = 0;
        $average_rating = 0;
        foreach ($ratings as $rating) {
            $rating_total = $rating_total + $rating->rating;
        }
        if(count($ratings) > 0){
            $average_rating = ROUND($rating_total / count($ratings), 1,1);
        }

        return view('users.profile', compact(
            'user',
            'uid',
            'places',
            'skills',
            'my_issues',
            'resolved_issues',
            'organising_workshops',
            'organised_workshops',
            'attending_workshops',
            'attended_workshops',
            'handling_issues',
            'connection',
            'connectionStatus',
            'currentUid',
            'learn',
            'teach',
            'current_sent',
            'to_sent',
            'ratings',
            'average_rating'
        ));

    }

    public function connect(Request $req){

        $from_user     = User::find($this->uid);
        $data          = $req->all();
        $to_user       = $data['to_user'];
        $to_user       = User::find($to_user);
        $cr            = new ConnectionRequests();
        $cr->from_user = $from_user->id;
        $cr->to_user   = $to_user->id;
        $cr->message   = $data['message'];
        $cr->purpose   = $data['purpose'];
        $cr->status    = 1;
        $cr->save();

        //Add notification
        $not             = new Notifications();
        $not->user_id    = $cr->to_user;
        $not->type       = 'user_connection_request';
        $not->target_url = '/user/'.$from_user->username;
        $not->content    = $from_user->first_name.' '.$from_user->last_name." has sent you a connection request.";
        $not->save();

        // Set it to default image if the user has no image
        if($from_user->image_name == ''){
            $from_user->image_name = 'default.png';
        }
        
        Mail::send('emails.connection-request', ['from_user' => $from_user, 'to_user' => $to_user, 'purpose' => $cr], function ($message) use ($to_user, $cr) {
            $message->to($to_user->email, $to_user->first_name . ' ' . $to_user->last_name)
                ->subject('Placerange - New '.$cr->purpose.' request');
        });
        return redirect('/user/'.$to_user->username);
    }

    public function post_connect(Request $req){

        $uid        = $this->uid;
        $data       = $req->all();
        $crID       = $data['cr_id'];
        $status     = $data['status'];
        $cr         = ConnectionRequests::find($crID);
        $cr->status = $status;
        $cr->save();
        if($status == 2) {

            //Add notification
            $not             = new Notifications();
            $not->user_id    = $cr->from_user;
            $not->type       = 'user_connection_accept';
            $not->target_url = '/user/'.$cr->receiver->username;
            $not->content    = $cr->receiver->first_name.' '.$cr->receiver->last_name." has accepted your connection request.";
            $not->save();

            $con          = new Connections();
            $con->user1   = $cr->from_user;
            $con->user2   = $cr->to_user;
            $con->purpose = $cr->purpose;
            $con->save();
        }
    }

    // Handle the rating
    public function post_rating(Request $req){

        $uid         = $this->uid;
        $data       = $req->all();
        $user = User::find($uid);
        // Make sure these two users are connected
        $check_connection_query = "Select * from connections where (`user1` = ".$uid." AND `user2` = ".$data['to_user']." ) OR (`user1` = ".$data['to_user']." AND `user2` = ".$uid.")";
        $connected = DB::select(DB::raw($check_connection_query));

        $data        = $req->all();
        $from_user   = $uid;
        $to_user     = $data['to_user'];

        $rating     = new UserRating();
        // Save the rating
        $rating->from_user = $from_user;
        $rating->to_user   = $to_user;
        $rating->rating    = $data['rating'];
        $rating->comment   = $data['comment'];
        $rating->save();

        //Add notification
        $notification             = new Notifications();
        $notification->user_id    = $rating->to_user;
        $notification->type       = 'user_rating';
        $notification->target_url = '#';
        $notification->content    = $user->first_name.' '.$user->last_name." has rated you with $rating->rating star(s).";
        $notification->save();

        return redirect('/user/'.$rating->reviewed_to->username);
        
    }


    public function feedback(){
        return view('users.feedback');
    }

    public function post_feedback(Request $req){
        $data      = $req->all();
        $validator = Validator::make(
            array(
                'content' => $data['content'],
            ),
            array(
                'content' => 'required',
            )
        );

        if ($validator->fails())
        {
            $errors = $validator->messages();
            return view('users.feedback',compact('errors'));
        }else{
            $fb          = new Feedback();
            $fb->content = $data['content'];
            $fb->save();
            $success = 1;

            Mail::send('emails.feedback', ['content' => $data['content']], function ($message) {
                $message->to('placerange@gmail.com','Placerange - Feedback')
                    ->subject('New Feedback received');
            });


            return view('users.feedback', compact('success'));

        }


    }

}
