<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Validator;
use Illuminate\Http\Request;
use Response;
use GuzzleHttp\Client;
use App\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use GuzzleHttp\Event\BeforeEvent;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use Hash;
use Config;
use Mail;
use App\Invitations;
use App\Notifications;
use App\UserTags;
use App\Tags;
use App\Helpers\Helper;
use App\Place;
use Image;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/


	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Helper $helper, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;
        $this->helper = $helper;

		// $this->middleware('guest', ['except' => 'getLogout']);

        $token = JWTAuth::getToken();
        if($token){
            $this->uid = JWTAuth::parseToken()->authenticate()->id;
        } else {
            $this->uid= null;
        }
	}



	public function register(Request $request)
    {
        $data = $request->all();

        $validator =  Validator::make($data, [
            'name' => 'required|max:100|alpha_spaces',
            'email'      => 'required|email|max:50|unique:users',
            'password'   => 'required|min:6'
        ]);

        if ($validator->fails()){
           $errors = $validator->messages();
            $validator_data = array(
                'errors' => $errors,
                'error' => 'error'
            );

            return Response::json($validator_data);
        }

        $data['activation_code'] = str_random(30);

        $this->data = $data;
        $create =  User::create([
            'name'            => ucwords($data['name']),
            'email'           => $data['email'],
            'password'        => bcrypt($data['password']),
            'activation_code' => $data['activation_code']
            
        ]);

        //Check if this user has been invited by someone
        $invs = Invitations::where('email_id',$data['email'])->get();

        if(!$invs->isEmpty()){
            foreach($invs as $in){
                $cr            = new ConnectionRequests();
                $cr->from_user = $in->user_id;
                $cr->to_user   = $create->id;
                $cr->purpose   = 'friend';
                $cr->status    = 2;
                $cr->save();

                $con          = new Connections();
                $con->user1   = $in->user_id;
                $con->user2   = $create->id;
                $con->purpose = 'friend';
                $con->save();

                //Add notification
                $not              = new Notifications();
                $not->user_id     = $in->user_id;
                $not->type        = 'user_joined';
                $not->target_url  = '/user/' . $create->username;
                $not->content     = '<strong>'.$create->name."</strong> has now joined Placerange";
                $not->target_user = $create->id;
                $not->save();

                $in->delete();


            }
        }


        // if no errors are encountered we can return a JWT
        return response()->json(['token' => JWTAuth::fromUser($create)]);

    }

    public function register2(Request $request){
        $data = $request->all();
        //Current user ID
        $uid         = $this->uid;

        $user = User::find($uid);

        $skills = $data['skills'];
        $learn  = $data['learn'];
        $place  = $data['place'];
        
        // Save the skills
        foreach($skills as $s){
            $ut = new UserTags;

            $ut->user_id  = $uid;
            $ut->tag_type = 'skill';
            $ut->tag      = $s['text'];
            $ut->save();

            $tag           = new Tags;
            $tag->tag      = $ut->tag;
            $tag->tag_type = 'skill';
            $tag->for      = 'user';
            $tag->for_id   = $uid;
            $tag->save();
        }

        // Save the learn
        foreach($learn as $s){
            $ut = new UserTags;

            $ut->user_id  = $uid;
            $ut->tag_type = 'learn';
            $ut->tag      = $s['text'];
            $ut->save();

            $tag           = new Tags;
            $tag->tag      = $ut->tag;
            $tag->tag_type = 'learn';
            $tag->for      = 'user';
            $tag->for_id   = $uid;
            $tag->save();
        }

        // save the place
        $related_id     = $this->uid;
        $place          = $this->helper->place($place['formatted_address']);
        $place          = Place::create($place);
        $place->for_id  = $related_id;
        $place->for     = 'user';
        $place->user_id = $related_id;
        $place->primary = 1;
        // Save the place if everything is fine
        $place->save();

        if(!$user->image_name) $user->image_name = 'default.png';
        $user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($user->image_name, 'icon');

        $response = array(
            'success' => 'success',
            'user'    => $user,
            'place'   => $place
        );

        return Response::json($response);

         
    }

    /**
     * Login with Google.
     */
    public function google(Request $request)
    {
        $client = new Client();
        $params = [
            'code'          => $request->input('code'),
            'client_id'     => $request->input('clientId'),
            'client_secret' => 'N71leel6zPNUdcRzDexIICrv',
            'redirect_uri'  => $request->input('redirectUri'),
            'grant_type'    => 'authorization_code',
        ];

        // Step 1. Exchange authorization code for access token.
        $accessTokenResponse = $client->post('https://www.googleapis.com/oauth2/v4/token', [
            'query' => $params
        ]

        );


        $accessToken = json_decode($accessTokenResponse->getBody(), true);
        // Step 2. Retrieve profile information about the current user.
        $profileResponse = $client->get('https://www.googleapis.com/plus/v1/people/me/openIdConnect', [
            'headers' => array('Authorization' => 'Bearer ' . $accessToken['access_token'])
        ]);
        $profile = json_decode($profileResponse->getBody(), true);


        // Step 3a. If user is already signed in then link accounts.
        if ($request->header('Authorization'))
        {
            $user = User::where('google', '=', $profile['sub']);
            if ($user->first())
            {
                return response()->json(['token' => JWTAuth::fromUser($user)]);
            }
            $token = explode(' ', $request->header('Authorization'))[1];
            $user = JWTAuth::authenticate($token);
            $user->google = $profile['sub'];
            $user->save();
            return response()->json(['token' => JWTAuth::fromUser($user), 'new_user' => true]);
        }
        // Step 3b. Create a new user account or return an existing one.
        else
        {
            $user = User::where('google', '=', $profile['sub']);
            if ($user->first())
            {
                return response()->json(['token' => JWTAuth::fromUser($user->first())]);
            }
            $user         = new User;
            $user->google = $profile['sub'];
            $user->name   = $profile['name'];
            $user->email  = $profile['email'];
            $user->save();
            $image_url = str_replace('sz=50', 'sz=2048', $profile['picture']);
            // Now get the image
            $fname = $user->id.'_'.time().'.jpg';
            // Resize the uploaded image into various sizes and store them
            Image::make($image_url)->save(public_path().'/uploads/users/original/'.$fname);
            Image::make($image_url)->fit(250, 250)->save(public_path().'/uploads/users/thumbnail/'.$fname);
            Image::make($image_url)->fit(50, 50)->save(public_path().'/uploads/users/icon/'.$fname);
            Image::make($image_url)->fit(700, 700)->save(public_path().'/uploads/users/medium/'.$fname);

            $user->image_name = $fname;
            $user->save();

            return response()->json(['token' => JWTAuth::fromUser($user), 'new_user' => true]);
        }
    }



    /**
     * Login with Facebook.
     */
    public function facebook(Request $request)
    {
        $client = new Client();
        $params = [
            'code'          => $request->input('code'),
            'client_id'     => $request->input('clientId'),
            'redirect_uri'  => $request->input('redirectUri'),
            'client_secret' => 'ff000f094156aecad7f2c4ca956291f9'
        ];


        // Step 1. Exchange authorization code for access token.
        $accessTokenResponse = $client->post('https://graph.facebook.com/v2.5/oauth/access_token', [
            'query' => $params
        ]);

        
        // return $accessTokenResponse;
        $accessToken = json_decode($accessTokenResponse->getBody(), true);

        // return $accessToken;
        // Step 2. Retrieve profile information about the current user.
        $fields = 'id,email,first_name,last_name,link,name';
        $profileResponse = $client->get('https://graph.facebook.com/me', [
            'query' => [
                'access_token' => $accessToken['access_token'],
                'fields' => $fields
            ]
        ]);
        $profile = json_decode($profileResponse->getBody(), true);
        // Step 3a. If user is already signed in then,picture link accounts.
        if ($request->header('Authorization'))
        {   		
            $user = User::where('facebook', '=', $profile['id']);
            if ($user->first())
            {
                return response()->json(['token' => JWTAuth::fromUser($user->first())]);
            }
            $token          = explode(' ', $request->header('Authorization'))[1];
            $user           = JWTAuth::authenticate($token);
            $user->facebook = $profile['id'];
            $user->save();
            return response()->json(['token' => JWTAuth::fromUser($user), 'new_user' => true]);
        }
        // Step 3b. Create a new user account or return an existing one.
        else
        {
            $user = User::where('facebook', '=', $profile['id']);
            if ($user->first())
            {
                return response()->json(['token' => JWTAuth::fromUser($user->first())]);
            }
            $user           = new User;
            $user->facebook = $profile['id'];
            $user->email    = $profile['email'];
            $user->name     = $profile['first_name'];
            $user->save();
            // Now get the image
            $fname = $user->id.'_'.time().'.jpg';
            // Resize the uploaded image into various sizes and store them
            Image::make('https://graph.facebook.com/'.$user->facebook.'/picture?type=large')->save(public_path().'/uploads/users/original/'.$fname);
            Image::make('https://graph.facebook.com/'.$user->facebook.'/picture?type=large')->fit(250, 250)->save(public_path().'/uploads/users/thumbnail/'.$fname);
            Image::make('https://graph.facebook.com/'.$user->facebook.'/picture?type=large')->fit(50, 50)->save(public_path().'/uploads/users/icon/'.$fname);
            Image::make('https://graph.facebook.com/'.$user->facebook.'/picture?type=large')->fit(700, 700)->save(public_path().'/uploads/users/medium/'.$fname);

            $user->image_name = $fname;
            $user->save();
            return response()->json(['token' => JWTAuth::fromUser($user), 'new_user' => true]);
        }
    }


    /**
     * Login with Twitter.
     */
    public function twitter(Request $request)
    {
	
        // Part 1 of 2: Initial request from Satellizer.
        if (!$request->input('oauth_token') || !$request->input('oauth_verifier'))
        {   

            $client = new Client();
         
            $params = [
                'consumer_key'    => 'OgTq0d1lObGg0uEFZCGWQ0plH',
                'consumer_secret' => '4M3swfMj9qInT68MthEyoJd9qgRCKOLH7fnn23F9sdOaIgd7eJ',
                'token'           => '',
                'verifier'        => '',
                'token_secret'    => '',
                'auth'          => 'Oauth'
            ];

            // Step 1. Obtain request token for the authorization popup.
            $requestTokenResponse = $client->post('https://api.twitter.com/oauth/request_token', [
                'query' => $params
            ]);
            $oauthToken = array();
            parse_str($requestTokenResponse->getBody(), $oauthToken);
            // Step 2. Send OAuth token back to open the authorization screen.
            return response()->json($oauthToken);
        }
        // Part 2 of 2: Second request after Authorize app is clicked.
        else
        {       
	        $client = new Client();
	        $params = [
	            'consumer_key'	  => 'OgTq0d1lObGg0uEFZCGWQ0plH',
	            'consumer_secret' => '4M3swfMj9qInT68MthEyoJd9qgRCKOLH7fnn23F9sdOaIgd7eJ',
	            'token'           => $request->input('oauth_token'),
	            'verifier'        => $request->input('oauth_verifier'),
	            'token_secret'    => ''
	        ];

	        // Step 3. Exchange oauth token and oauth verifier for access token.
	        $accessTokenResponse = $client->post('https://api.twitter.com/oauth/access_token', [
	            'query' => $params
	        ]);

	        // return $accessTokenResponse;
	        $accessToken = json_decode($accessTokenResponse->getBody(), true);

	        // Step 4. Retrieve profile information about the current user.
	        $profileResponse = $client->get('https://api.twitter.com/1.1/users/show.json?screen_name=' . $accessToken['screen_name'], [
	            'auth' => 'oauth'
	        ]);
	        $profile = json_decode($profileResponse->getBody(), true);
	        // Step 5a. Link user accounts.
	        if ($request->header('Authorization'))
	        {
	            $user = User::where('twitter', '=', $profile['id']);
	            if ($user->first())
	            {
	                return response()->json(['message' => 'There is already a Twitter account that belongs to you'], 409);
	            }
	            $token          = explode(' ', $request->header('Authorization'))[1];
	            $user           = JWTAuth::authenticate($token);
	            $user->facebook = $profile['id'];
	            $user->save();
	            return response()->json(['token' => JWTAuth::fromUser($user)]);
	        }
	        // Step 5b. Create a new user account or return an existing one.
	        else
	        {
	            $user = User::where('twitter', '=', $profile['id']);
	            if ($user->first())
	            {
	                return response()->json(['token' => JWTAuth::fromUser($user->first())]);
	            }
	            $user          = new User;
	            $user->twitter = $profile['id'];
	            $user->email   = $profile['email'];
	            $user->name    = $profile['screen_name'];
	            $user->save();
	            return response()->json(['token' => JWTAuth::fromUser($user)]);
	        }
        }

    }





}
