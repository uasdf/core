<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\GeneralTags;
use Illuminate\Http\Request;
use Illuminate\Auth\Guard;
use App\Helpers\Helper;


class AutofillController extends Controller {

    private $auth, $helper, $req, $uid;

    public function __construct(Guard $auth, Helper $helper, Request $req)
    {
        $this->auth   = $auth;
        $this->helper = $helper;
        $this->req    = $req;
        $this->uid    = $this->helper->user_logged_in();
    }

    public function skills(){

        $data = $this->req->all();
        $skills_db = GeneralTags::where('type','skill')->where('tag','like','%'.$data['query'].'%')->get();
        $i         = 0;
        $skills    = array();
        foreach($skills_db as $s){
            $skills[$i] = array(
                    'text' => $s->tag
                );
            $i++;
        }
        return $skills;
    }

}
