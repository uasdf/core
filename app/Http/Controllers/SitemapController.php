<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App;

class SitemapController extends Controller {

	function generate_sitemap(){

	    // create new sitemap object
	    $sitemap = App::make("sitemap");

	    // get all issues from db
	    $issues = DB::table('issues')->orderBy('created_at', 'desc')->get();

	    // add every issue to the sitemap
	    foreach ($issues as $issue)
	    {
	        $sitemap->add('https://'.$_SERVER['SERVER_NAME'] .'#/issue/'. $issue->slug, $issue->updated_at, '0.9', 'weekly');
	    }

	    // get all users from db
	    $users = DB::table('users')->orderBy('created_at', 'desc')->get();

	    // add every users to the sitemap
	    foreach ($users as $user)
	    {
	        $sitemap->add('https://'.$_SERVER['SERVER_NAME'] .'#/user/'. $user->username, $user->updated_at, '0.8', 'weekly');
	    }


	    // add items to the sitemap (url, date, priority, freq)
	    // Near me page
	    $sitemap->add('https://'.$_SERVER['SERVER_NAME'] . '#/near-me', date('Y-m-d'), '0.3', 'monthly');

	    $sitemap->add('https://'.$_SERVER['SERVER_NAME'] . '#/messages', date('Y-m-d'), '0.3', 'monthly');

	    // $sitemap->add('https://'.$_SERVER['SERVER_NAME'] . '/search', date('Y-m-d'), '0.7', 'monthly');
	    
	    $sitemap->add('https://'.$_SERVER['SERVER_NAME'] . '#/notifications', date('Y-m-d'), '0.3', 'monthly');
	    
	    $sitemap->add('https://'.$_SERVER['SERVER_NAME'] . '#/profile', date('Y-m-d'), '0.3', 'monthly');
	    
	    // $sitemap->add('https://'.$_SERVER['SERVER_NAME'] . '/issues', date('Y-m-d'), '0.3', 'monthly');

	    // $sitemap->add('https://'.$_SERVER['SERVER_NAME'] . '/programs', date('Y-m-d'), '0.3', 'monthly');
	    
	    // $sitemap->add('https://'.$_SERVER['SERVER_NAME'] . '/feedback', date('Y-m-d'), '0.3', 'monthly');

	    $sitemap->add('https://'.$_SERVER['SERVER_NAME'] . '#/messages', date('Y-m-d'), '0.3', 'monthly');
	    
	    
	    // generate your sitemap (format, filename)
	    $sitemap->store('xml', 'sitemap');
	    // this will generate file mysitemap.xml to your public folder


	}

}
