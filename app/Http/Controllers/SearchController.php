<?php namespace App\Http\Controllers;

use App\GeneralTags;
use App\IssueHandleRequest;
use App\User;
use App\Tags;
use App\WorkshopAttendees;
use Illuminate\Contracts\Auth\Guard;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use PhpSpec\Exception\Exception;
use Validator;
use App\UserTags;
use App\Issue;
use Geocoder;
use App\Place;
use App\Workshop;
use DB;
use nlpWP;
use nlpMN;
use NlpTools\Tokenizers\WhitespaceAndPunctuationTokenizer;
use NlpTools\Models\FeatureBasedNB;
use NlpTools\Documents\TrainingSet;
use NlpTools\Documents\TokensDocument;
use NlpTools\FeatureFactories\DataAsFeatures;
use NlpTools\Classifiers\MultinomialNBClassifier;
use NlpTools\Stemmers\PorterStemmer;
use NlpTools\FeatureFactories\FunctionFeatures;

use App\Search;

class SearchController extends Controller
{

    public function __construct(Guard $auth, Helper $helper, Request $req)
    {
        $this->auth   = $auth;
        $this->helper = $helper;
        $this->req    = $req;
        $this->uid    = $this->helper->user_logged_in();
    }

    public function not_found()
    {
        return view('errors.404');
    }

    public function index()
    {   
        if (!$this->auth->check()) {
            return redirect('/login')->send();
        }

        $uid    = $this->uid;
        $user   = User::find($uid);
        $places = $user->places();
        return view('search.index',compact('places'));
    }

    public function welcomeSearch(Request $req){
        $data         = $req->all();
        $skill_actual = $data['search_string'];
        $ss           = strtolower($skill_actual);
        $tags_list1   = array();
        $search       = Search::where('type','skill')->get();
        
        foreach($search as $tag){
            $tags_list1[] = $tag->actual;
        }

        //Get all the tags and store it in a list
        $tags = Tags::all();
        foreach($tags as $tag){
            $tags_list2[] = strtolower($tag->tag);
        }
        $general_tags = GeneralTags::all();
        foreach($general_tags as $tag){
            $tags_list3[] = strtolower($tag->tag);
        }
        $tags_list = array_merge($tags_list1,$tags_list2, $tags_list3);

        $tags_list = array_unique($tags_list);

        $known_tag = false;

        // check if it is a known tag
        if(in_array($ss,$tags_list)){
            $known_tag = true;
        }

        // Get the user count
        $user_query_knows = "SELECT COUNT(1) AS 'total', for_id FROM place" . '
                     LEFT JOIN `user_tags` on `place`.`for_id` = `user_tags`.`user_id`
                     LEFT JOIN `users` on `place`.`for_id` = `users`.`id`
                     WHERE `user_tags`.`tag` = "' . $ss . '" AND `user_tags`.`tag_type` = "skill" AND `for` = "user"';
        $user_query_knows = "SELECT * FROM (".$user_query_knows.") a GROUP BY for_id";

        $user_result_teach = DB::select(DB::raw($user_query_knows));

        // Get the user count
        $user_query_learn = "SELECT COUNT(1) AS 'total', for_id FROM place" . '
                     LEFT JOIN `user_tags` on `place`.`for_id` = `user_tags`.`user_id`
                     LEFT JOIN `users` on `place`.`for_id` = `users`.`id`
                     WHERE `user_tags`.`tag` = "' . $ss . '" AND `user_tags`.`tag_type` = "learn" AND `for` = "user"';
        $user_query_learn = "SELECT * FROM (".$user_query_learn.") a GROUP BY for_id";

        $user_result_learn = DB::select(DB::raw($user_query_learn));

        // Get the program count
        $program_query = "SELECT COUNT(1) AS 'total', for_id FROM place" . '
                     LEFT JOIN `workshop_tags` on `place`.`for_id` = `workshop_tags`.`workshop_id`
                     LEFT JOIN `workshops` on `place`.`for_id` = `workshops`.`id`
                     WHERE `workshop_tags`.`tag` = "' . $ss . '" AND `for` = "workshop"';
        $program_query = "SELECT * FROM (".$program_query.") a GROUP BY for_id";

        $program_result = DB::select(DB::raw($program_query));

        // Get the issue count
        $issue_query = "SELECT COUNT(1) AS 'total', for_id FROM place" . '
                     LEFT JOIN `issue_tags` on `place`.`for_id` = `issue_tags`.`issue_id`
                     LEFT JOIN `issues` on `place`.`for_id` = `issues`.`id`
                     WHERE `issue_tags`.`tag` = "' . $ss . '" AND `for` = "issue"';
        $issue_query = "SELECT * FROM (".$issue_query.") a GROUP BY for_id";

        $issue_result = DB::select(DB::raw($issue_query));


        $response = 
        '<table class="table-hover"><tbody>
            <tr><td><h1>Number of users who knows ' . $skill_actual . ': </h1></td><td><h1>'          . $user_result_teach[0]->total . '</h1></td></tr>
            <tr><td><h1 style="margin-right:20px;">Number of users who wants to learn ' . $skill_actual . ': </h1></td><td><h1>' . $user_result_learn[0]->total . '</h1></td></tr>
            <tr><td><h1>Number of programs tagged with ' . $skill_actual . ': </h1></td><td><h1>'     . $program_result[0]->total . '</h1></td></tr>
            <tr><td><h1>Number of issues tagged with ' . $skill_actual . ': </h1></td><td><h1>'       . $issue_result[0]->total . '</h1></td></tr>
        </tbody></table>'

        ;

        echo $response;
    }


    public function postSearch(Request $req){

        if (!$this->auth->check()) {
            return redirect('/login')->send();
        }
     
        $uid         = $this->uid;
        $user        = User::find($uid);
        $skills      = array();
        $level       = '';
        $for         = '';
        $type        = '';
        $i           = 0;
        $connections = array();
        //Tokenize and stem the given search string
        $tokenizer = new WhitespaceAndPunctuationTokenizer();
        $stemmer   = new PorterStemmer();
        $data      = $req->all();
        $ss        = $data['search_string'];
        $ss        = strtolower($ss);
        $ssArray   = $tokenizer->tokenize($ss);

        //Set all the flags to false by default
        $friend_flag = false;
        $type_flag   = false;
        $for_flag    = false;
        $level_flag  = false;
        $users_match = false;
        $location    = array();



        $tokens           = array();
        $tags_list1       = array();
        $for_set1         = array();
        $for_set2         = array();
        $for_set3         = array();
        $for_set4         = array();
        $type_set1        = array();
        $type_set2        = array();
        $level_set1       = array();
        $level_set2       = array();
        $general_filters  = array();
        $tags_list        = array();
        $location_filters = array();

        $search = Search::all();
        foreach($search as $s){
            if($s->type=='for_set1'){
                $for_set1[] = $s->stemmed;
            }elseif($s->type=='for_set2'){
                $for_set2[] = $s->stemmed;
            }elseif($s->type=='for_set3'){
                $for_set3[] = $s->stemmed;
            }elseif($s->type=='for_set4'){
                $for_set4[] = $s->stemmed;
            }elseif($s->type=='type_set1'){
                $type_set1[] = $s->stemmed;
            }elseif($s->type=='type_set2'){
                $type_set2[] = $s->stemmed;
            }elseif($s->type=='level_set1'){
                $level_set1[] = $s->stemmed;
            }elseif($s->type=='level_set2'){
                $level_set2[] = $s->stemmed;
            }elseif($s->type=='general'){
                $general_filters[] = $s->stemmed;
            }elseif($s->type=='skill'){
                $tags_list1[] = $s->actual;
            }elseif($s->type=='location'){
                $location_filters[] = $s->stemmed;
            }
        }
        $for_entities   = array_merge($for_set1,$for_set3,$for_set4);
        $type_entities  = array_merge($type_set1,$type_set2);
        $level_entities = array_merge($level_set1,$level_set2);


        foreach($ssArray as $sa){
            $tokens[$i]['original'] = $sa;
            $tokens[$i]['stemmed'] = $stemmer->stem($sa);
            $i++;
        }

        //Get all the tags and store it in a list
        $tags = Tags::all();
        foreach($tags as $tag){
            $tags_list2[] = strtolower($tag->tag);
        }
        $general_tags = GeneralTags::all();
        foreach($general_tags as $tag){
            $tags_list3[] = strtolower($tag->tag);
        }
        $tags_list = array_merge($tags_list1,$tags_list2, $tags_list3);

        $tags_list = array_unique($tags_list);



        foreach($tokens as $key => $token){

            $tok      = $token['stemmed'];
            $tok_o    = $token['original'];
            $tok_prev = '';
            $tok_next = '';

            if(isset($tokens[$key-1])){
                $tok_prev = $tokens[$key-1]['original'];
            }
            if(isset($tokens[$key+1])){
                $tok_next = $tokens[$key+1]['original'];
            }

            if(in_array($tok, $general_filters)){
                continue;
            }

            //Check for skill entities
            if(in_array(strtolower($tok_o),$tags_list) || in_array(strtolower($tok_prev.' '.$tok_o),$tags_list) || in_array(strtolower($tok_o.' '.$tok_next),$tags_list)){
                $skills[] = $tok;
                continue;
            }else

            if(strlen($tok_o) > 2) {

                //Check for users
                $users = User::whereRaw("CONCAT(`first_name`,`last_name`) LIKE '%$tok_o%'")->get();

                foreach ($users as $usr) {
                    $users_match[] = $usr->id;
                }
                if ($users_match) {
                    continue;
                }
            }else

            echo '';
            //check for 'For' entities
            if(in_array($tok,$for_entities) && $for_flag) {
            continue;
            }else

            if(in_array($tok,$for_entities)) {
                $for_flag = true;
                $tokens[$key]['type'] = 'for';
                if (in_array($tok, $for_set1)) {
                    $for[] = 'user';
                } elseif (in_array($tok, $for_set3)) {
                    $for[] = 'issue';

                } elseif (in_array($tok, $for_set4)) {
                    $for[] = 'workshop';
                }
                continue;

            } elseif (in_array($tok, $for_set2)) {
                $for[] = 'user';

                $friend_flag = true;
                $Query = "Select * from connections where `user1` = $uid or `user2` = $uid";
                $allConnections = DB::select(DB::raw($Query));
                foreach($allConnections as $con){
                    if($con->user1 == $uid){
                        $connections[] = $con->user2;
                    }else{
                        $connections[] = $con->user1;
                    }
                }
                continue;
            }else

            //check for 'Type' entities
            if(in_array($tok,$type_entities)){
                $type_flag = true;
                $tokens[$key]['type'] = 'type';
                if(in_array($tok,$type_set1)){
                    $type = 'skill';
                }elseif(in_array($tok,$type_set2)){

                    if($tok == 'help'){
                        if(isset($tokens[$key--])) {
                            //n-gram analyzer
                            if (in_array($tokens[$key--], ['need', 'want'])) {
                                $type = 'learn';
                            } else {
                                $type = 'skill';
                            }
                        }else{
                            $type = 'skill';
                        }
                    }else{
                        $type = 'learn';
                    }
                }
                continue;
            }else

            //check for 'Level' entities
            if(in_array($tok,$level_entities)){
                $level_flag = true;
                $tokens[$key]['type'] = 'level';
                if(in_array($tok,$level_set1)){
                    $level = $tok;
                }elseif(in_array($tok,$level_set2)){
                    $level = 'intermediate';
                }
                continue;
            }else

            //Location filters
            if(in_array($tok,$location_filters)){
                $location[] = $tok;
                continue;
            }else

            //Consider the remaining tags as Location and storing them in the database for learning purpose
            if(!in_array($tok,$general_filters)){
                $location[] = $tok;
                if(strlen($tok) > 2) {
                    if(!Search::where('stemmed',$tok)->first()) {
                        $new_tag          = new Search();
                        $new_tag->stemmed = $tok;
                        $new_tag->actual  = $tok_o;
                        $new_tag->save();
                    }
                }
            }

        }//End of token loop

        $join_for_query = '';
        $for_query = '';
        if(!$for){
            $for = ['user','issue','workshop'];
        }
        if($for) {
            foreach ($for as $f) {
                $join_for_query .=  " LEFT JOIN `" . $f . "_tags` on `place`.`".$f."_id` = `" . $f . "_tags`.`" . $f . "_id` LEFT JOIN `" . $f . "s` on `place`.`".$f."_id` = `" . $f . "s`.`id` ";
            }
            $for_query = " AND `place`.`for` IN ('" . implode($for, "','") ."')";
        }
        $connection_query = '';
        if($friend_flag) {
            $connection_query = ' AND `users`.`id` IN (\'' . implode($connections, "','") . '\')';
        }

        if($users_match) {
            $connection_query = ' AND `users`.`id` IN (\'' . implode($users_match, "','") . '\')';
        }

        $level_value = 0;
        $level_query = '';
        if($level) {
            if($level == 'expert'){
                $level_value = 4;
            }elseif($level == 'intermediate'){
                $level_value = 3;
            }elseif($level == 'novice'){
                $level_value = 2;
            }elseif($level == 'beginner'){
                $level_value = 0;
            }
            foreach ($for as $f) {
                $level_query .= " OR `" . $f . "_tags`.`level` >= " . $level_value;
            }
        }

        $type_query = '';
        if($type) {
            foreach ($for as $f) {

                $type_query .= " AND `" . $f . "_tags`.`tag_type` = '" . $type . "'";
                if ($type == 'learn') {
                    if ($level_value == 4) {
                        $level_value = 3;
                    }
                } else {
                    if ($level_value == 0) {
                        $level_value = 2;
                    }
                }
                foreach ($for as $f) {
                    $level_query .= " OR `" . $f . "_tags`.`level` >= " . $level_value;
                }
            }
        }

        if($level_query){
            $level_query = "AND (".ltrim($level_query," OR").") ";
        }

        $skill_query  = '';
        $skill_query1 = '';
        $skill_query2 = '';

        if($skills) {
            $skill_query1 = 'AND (';
            foreach ($for as $f) {
                $skill_query2 .= ' `' . $f . '_tags`.`tag` IN (\'' . implode($skills, "','") . '\') OR';
            }
            $skill_query .= $skill_query1.rtrim($skill_query2,'OR').') ';
        }

        //Location query
        $location_query = '';
        $p = null;
        if(isset($location[0])) {
            $p = $this->helper->place(implode(' ', $location));
            if ($p) {
                $location_query = ',( 3959 * acos( cos( radians(' . $p['lat'] . " ) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(" . $p['lng'] . ") ) + sin( radians(" . $p['lat'] . ") ) * sin( radians( lat ) ) ) ) AS distance";
            }
        }

        if(!$p){
                $p = Place::Where(['for'=> 'user','for_id'=>$uid,'primary' => 1])->first();
                $location_query = ',( 3959 * acos( cos( radians('.$p->lat ." ) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(" . $p->lng . ") ) + sin( radians(" . $p->lat . ") ) * sin( radians( lat ) ) ) ) AS distance";
        }
        $location_distance = '';
        if($location_query){
            $location_distance = ' ORDER BY distance asc';
        }


        $u_for = false;
        $w_for = false;
        $i_for = false;

        if(in_array('user',$for)){
            $u_for = "`users`.`id` as uID,";
        }
        if(in_array('issue',$for)){
            $i_for ="`issues`.`id` as iID,";
        }
        if(in_array('workshop',$for)){
            $w_for= "`workshops`.`id` as wID,";
        }
        $all_for = rtrim($u_for.$i_for.$w_for,',');

        $subQuery ="SELECT `place`.*,
        $all_for
        ".$location_query." FROM place" .
                    $join_for_query.'
					 WHERE 1 = 1 '.$skill_query.$level_query.$type_query. $for_query.$connection_query.(in_array('user',$for) ? ' AND `users`.`id` !='.$uid : '').'
					 LIMIT 0, 100';
        $Query = "SELECT * FROM (".$subQuery.") a GROUP BY user_id, issue_id, workshop_id".$location_distance;


        $result = DB::select(DB::raw($Query));



        if ($result) {?>

            <?php  foreach ($result as $ts) {
                if($ts->for == 'user'){
                    if($ts->uID == $uid){
                        continue;
                    }

                    $user = User::find($ts->uID);
                    ?>
                    <a href="/user/<?= $user->username ?>" class="col-md-12 user_list">
                        <img src="<?= user_avatar($user->image_name) ?>" class="user_image">

                        <div class="content">
                            <span class="user_name"><?= $user->first_name . ' ' . $user->last_name ?></span>
                                <span class="skills"><?php  $skill = '';
                                    foreach ($user->skills() as $s) {
                                        $skill .= $s->tag . ', ';
                                    }
                                    echo rtrim($skill, ', '); ?></span>
                        </div>
                        <?php if(isset($ts->distance)){?><span class="distance"><?= round($ts->distance, 2) ?> miles</span><?php }?>
                    </a>

                <?php }elseif($ts->for == 'issue'){
                $issue = Issue::find($ts->iID);
                    if(!$issue) continue;
                ?>
                <a href="/issue/<?= $issue->slug ?>" class="col-md-12 user_list">
                    <img src="/img/support-icon.png" class="user_image">

                    <div class="content">
                        <span class="user_name"><?= $issue->title ?></span>
                                <span class="skills"><?php  $skill = '';
                                    foreach ($issue->tags() as $s) {
                                        $skill .= $s->tag . ', ';
                                    }

                                    echo rtrim($skill, ', '); ?></span>
                    </div>
                    <?php if(isset($ts->distance)){?><span class="distance"><?= round($ts->distance, 2) ?> miles</span><?php }?>
                </a>
                <?php }else{

                    $workshop = Workshop::find($ts->wID);
                    if(!$workshop) continue;

                    ?>
                    <a href="/program/<?= $workshop->slug ?>" class="col-md-12 user_list">
                        <img src="/img/learn-icon.png" class="user_image">

                        <div class="content">
                            <span class="user_name"><?= $workshop->title ?></span>
                                <span class="skills"><?php  $skill = '';
                                    foreach ($workshop->tags() as $s) {
                                        $skill .= $s->tag . ', ';
                                    }
                                    echo rtrim($skill, ', '); ?></span>
                        </div>
                        <?php if(isset($ts->distance)){?><span class="distance"><?= round($ts->distance, 2) ?> miles</span><?php }?>
                    </a>
                <?php }?>
            <?php }?>
        <?php } else {
            ?>
            No result found
        <?php 
        }
    }


    public function postSearchDefault(Request $req)
    {
        //Current user ID
        $uid  = $this->uid;
        $data = $req->all();

        //User details
        $user = User::find($uid);
        $levelText = 'Expert' ;
        if($data['level'] == 1 ) $levelText = 'expert';
        elseif($data['level'] == 2) $levelText = 'intermediate';
        elseif($data['level'] == 3) $levelText = 'beginner';
        elseif($data['level'] == 0) $levelText = 'any';



        //User address details
        $p            = Place::find($data['location']);
        $resultHeader = 'Results for '.$data['search_for'].' nearby '.place_min($p,true).($data['search_string'] ? ' matching skills '.$data['search_string'].' and in '.$levelText.' level.' : '');
        $skills       = explode(',', $data['search_string']);

        //People who can teach
        $teachQuery     = '';
        $results        = null;
        $i              = 0;
        $search_for     = $data['search_for'];
        if ($search_for == 'people') {
            $distance = 2;
            do {
                $result = '';
                $Query =
                    "SELECT `place`.*, `user_tags`.`tag`,`users`.`id` as uID, ( 3959 * acos( cos( radians(" . $p->lat . ") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(" . $p->lng . ") ) + sin( radians(" . $p->lat . ") ) * sin( radians( lat ) ) ) ) AS distance FROM place" . '
					 LEFT JOIN `user_tags` on `place`.`for_id` = `user_tags`.`user_id`
					 LEFT JOIN `users` on `place`.`for_id` = `users`.`id`
					 WHERE '.( !count($skills) ? '`user_tags`.`tag` IN (\'' . implode($skills, "','") . '\') AND `user_tags`.`level` >= ' . $data['level'] . ' AND `user_tags`.`tag_type` IN ("skill","teach") AND' : '').' `for` = "user" AND `for_id` != ' . $uid . '
					 HAVING distance < ' . $distance . '
					 Order BY distance ASC
					 LIMIT 0, 20';
                $Query = "SELECT * FROM (".$Query.") a GROUP BY for_id";

                $result = DB::select(DB::raw($Query));
                $distance++;
            } while (!$result && $distance < 10);

            if ($result) {
                $results[$i] = $result;
            } else {
                $results[$i] = '';
            }
            $i++;

            ?>
            <h4 class="grey-bottom"><?=$resultHeader?></h4>
            <?php 
            $i = 0;
            ?>
            <h4><span class="glyphicon glyphicon-map-marker"></span> <?= place_min($p, true) ?></h4>
            <?php if ($results[$i] != null) {
                ?>
                <?php  foreach ($results[$i] as $ts) {
                    $user = User::find($ts->uID);
                    ?>
                    <a href="/user/<?= $user->username ?>" class="col-md-12 user_list">
                        <img src="<?= user_avatar($user->image_name) ?>" class="user_image">

                        <div class="content">
                            <span class="user_name"><?= $user->first_name . ' ' . $user->last_name ?></span>
                                <span class="skills"><?php  $skill = '';
                                    foreach ($user->skills() as $s) {
                                        $skill .= $s->tag . ', ';
                                    }
                                    echo rtrim($skill, ', '); ?></span>
                        </div>
                        <span class="distance"><?= round($ts->distance, 2) ?> miles</span>
                    </a>
                <?php 
                } ?>
            <?php 
            } else {
                ?>
                No one found
            <?php 
            }
            $i++;
        } elseif ($search_for == 'issue') {
            $distance = 2;
            do {
                $result = '';
                $Query =
                    "SELECT `place`.*, `issue_tags`.`tag`,`issues`.`id` as iID, ( 3959 * acos( cos( radians(" . $p->lat . ") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(" . $p->lng . ") ) + sin( radians(" . $p->lat . ") ) * sin( radians( lat ) ) ) ) AS distance FROM place" . '
					 LEFT JOIN `issue_tags` on `place`.`for_id` = `issue_tags`.`issue_id`
					 LEFT JOIN `issues` on `place`.`for_id` = `issues`.`id`
					 WHERE '.( !count($skills) ? '`issue_tags`.`tag` IN (\'' . implode($skills, "','") . '\') AND `issue_tags`.`level` >= ' . $data['level'] . ' AND `issue_tags`.`tag_type` = "skill" AND ' : '').' `place`.`for` = "issue"
					 HAVING distance < ' . $distance . '
					 LIMIT 0, 20';
                $Query = "SELECT * FROM (".$Query.") a GROUP BY for_id";

                $result = DB::select(DB::raw($Query));
                $distance++;
            } while (!$result && $distance < 10);

            if ($result) {
                $results[$i] = $result;
            } else {
                $results[$i] = '';
            }
            $i++;
            ?>
            <h4 class="grey-bottom"><?=$resultHeader?></h4>
            <?php 
            $i = 0;
            ?>
            <h4><span class="glyphicon glyphicon-map-marker"></span> <?= place_min($p, true) ?></h4>
            <?php if ($results[$i] != null) {
                ?>

                <?php  foreach ($results[$i] as $ts) {
                    $issue = Issue::find($ts->iID);
                    ?>
                    <a href="/issue/<?= $issue->slug ?>" class="col-md-12 user_list">
                        <img src="<?= user_avatar($issue->user->image_name) ?>" class="user_image">

                        <div class="content">
                            <span class="user_name"><?= $issue->title ?></span>
                                <span class="skills"><?php  $skill = '';
                                    foreach ($issue->tags() as $s) {
                                        $skill .= $s->tag . ', ';
                                    }
                                    echo rtrim($skill, ', '); ?></span>
                        </div>
                        <span class="distance"><?= round($ts->distance, 2) ?> miles</span>
                    </a>
                <?php 
                } ?>
            <?php 
            } else {
                ?>
                No issues found
            <?php 
            }
            $i++;

        } elseif ($search_for == 'workshop') {
            $distance = 2;
            do {
                $result = '';
                $Query = "SELECT `place`.*, `workshop_tags`.`tag`,`workshops`.`id` as wID, ( 3959 * acos( cos( radians(" . $p->lat . ") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(" . $p->lng . ") ) + sin( radians(" . $p->lat . ") ) * sin( radians( lat ) ) ) ) AS distance FROM place" . '
                     LEFT JOIN `workshop_tags` on `place`.`for_id` = `workshop_tags`.`workshop_id`
                     LEFT JOIN `workshops` on `place`.`for_id` = `workshops`.`id`
                     WHERE '.( !count($skills) ? '`workshop_tags`.`tag` IN (\'' . implode($skills, "','") . '\') AND `workshop_tags`.`level` >= ' . $data['level'] . ' AND `workshop_tags`.`tag_type` = "skill"  AND' : '').'  `place`.`for` = "workshop"
                     HAVING distance < ' . 2 . '
                     LIMIT 0, 20';
                $Query = "SELECT * FROM (".$Query.") a GROUP BY for_id";

                $result = DB::select(DB::raw($Query));
                $distance++;
            } while (!$result && $distance < 2);

            if ($result) {
                $results[$i] = $result;
            } else {
                $results[$i] = '';
            }
            $i++;

            ?>
            <h4 class="grey-bottom"><?=$resultHeader?></h4>
            <?php 
            $i = 0;
            ?>
            <h4><span class="glyphicon glyphicon-map-marker"></span> <?= place_min($p, true) ?></h4>
            <?php if ($results[$i] != null) {
                ?>

                <?php  foreach ($results[$i] as $ts) {
                    $workshop = Workshop::find($ts->wID);
                    ?>
                    <a href="/program/<?= $workshop->slug ?>" class="col-md-12 user_list">
                        <img src="<?= user_avatar($workshop->user->image_name) ?>" class="user_image">

                        <div class="content">
                            <span class="user_name"><?= $workshop->title ?></span>
                                <span class="skills"><?php  $skill = '';
                                    //var_dump($issue->tags());
                                    foreach ($workshop->tags() as $s) {
                                        $skill .= $s->tag . ', ';
                                    }
                                    echo rtrim($skill, ', '); ?></span>
                        </div>
                        <span class="distance"><?= round($ts->distance, 2) ?> miles</span>
                    </a>
                <?php 
                } ?>
            <?php 
            } else {
                ?>
                No issues found
            <?php 
            }
            $i++;

        }
    }



}
