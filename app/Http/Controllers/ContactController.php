<?php namespace App\Http\Controllers;

use App\ConnectionRequests;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use OAuth;
use Illuminate\Http\Request;
use App\Invitations;
use App\Helpers\Helper;
use Illuminate\Auth\Guard;
use App\User;
use App\Notifications;
use Mail;
use Illuminate\Support\Facades\Session;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Response;

class ContactController extends Controller {

    private $auth, $helper, $req, $uid;

    public function __construct(Guard $auth, Helper $helper, Request $req)
    {
        $this->auth   = $auth;
        $this->helper = $helper;
        $this->req    = $req;
        $token = JWTAuth::getToken();

        if($token){
            $this->uid = JWTAuth::parseToken()->authenticate()->id;
        } else {
            $this->uid= null;
        }
    }
    public function inviteFriends(Request $request)
    {
        $uid             = $this->uid;
        $previousInvites = Invitations::where('user_id',$uid)->get();
    

        $invite_data = array(
                'previousInvites' => $previousInvites
            );
        return Response::json($invite_data);
          
    }


    public function importGoogleContact(Request $request)
    {
        // get data from request
        $code    = $request->get('code');
        if($code == Session::get('google_code')){
            $code = null;
        }else{
            Session::put('google_code', $code);
        }
        $uid             = $this->uid;
        $current_user    = User::find($this->uid);
        $connections     = $this->helper->connections();
        $connections[]   = $uid;
        $pendingRequests = ConnectionRequests::where(['from_user' => $uid, 'status' => 1])->get();
        if($pendingRequests){
            foreach($pendingRequests as $pr){
                $connections[] = $pr->to_user;
            }
        }

        // get google service
        $googleService = \OAuth::consumer('Google');

        // check if code is valid

        // if code is provided get user data and sign in
        if ( ! is_null($code)) {
            // This was a callback request from google, get the token
            $token = $googleService->requestAccessToken($code);

            // Send a request with it
            $result = json_decode($googleService->request('https://www.google.com/m8/feeds/contacts/default/full?alt=json&max-results=400'), true);

            // Going through the array to clear it and create a new clean array with only the email addresses
            $emails = []; // initialize the new array
            foreach ($result['feed']['entry'] as $contact) {
                if (isset($contact['gd$email'])) { // Sometimes, a contact doesn't have email address
                    $emails[] = $contact['gd$email'][0]['address'];
                }
            }

            $existing_users    = array();
            $to_invite         = array();
            $already_connected = array();


            foreach($emails as $e){

                $user = User::where(['email' => $e])->first();

                if(!$user) {
                    $inv = Invitations::where(['email_id' => $e, 'user_id' => $this->uid])->first();
                    if (!$inv) {
                        $to_invite[] = $e;
                    } else {
                    }
                }else {

                    //Check for connections
                    if(in_array($user->id, $connections)) {
                        $already_connected[] = $user;

                    } else {
                        if($user->id != $uid) {
                            $existing_users[] = $user;
                        }
                    }
                }
            }

            return view('invite.result',compact(
                'uid',
                'to_invite',
                'existing_users',
                'already_connected'
                )
            );

        }

        // if not ask for permission first
        else {
            // get googleService authorization
            $url = $googleService->getAuthorizationUri();

            // return to google login url
            return redirect((string)$url);
        }
    }

    public function send_connect(){

        $from_user     = User::find($this->uid);
        $data          = $this->req->all();
        $to_user       = $data['to_user'];
        $to_user       = User::find($to_user);
        $cr            = new ConnectionRequests();
        $cr->from_user = $from_user->id;
        $cr->to_user   = $to_user->id;
        $cr->message   = 'I\'d like to connect';
        $cr->purpose   = 'friend';
        $cr->status    = 1;
        $cr->save();

        //Add notification
        $not             = new Notifications();
        $not->user_id    = $cr->to_user;
        $not->type       = 'user_connection_request';
        $not->target_url = '/user/'.$from_user->username;
        $not->content    = $from_user->first_name.' '.$from_user->last_name." has sent you a connection request.";
        $not->save();

        // Set it to default image if the user has no image
        if($from_user->image_name == ''){
            $from_user->image_name = 'default.png';
        }

        Mail::send('emails.connection-request', ['from_user' => $from_user, 'to_user' => $to_user, 'purpose' => $cr], function ($message) use ($to_user, $cr) {
            $message->to($to_user->email, $to_user->first_name . ' ' . $to_user->last_name)
                ->subject('Placerange - New '.$cr->purpose.' request');
        });
        return 'success';

    }

    public function send_invite(){

        $from_user = User::find($this->uid);
        $data      = $this->req->all();
        $email     = $data['email'];
        $inv       = Invitations::where(['email_id' => $email, 'user_id' => $this->uid])->first();
        $user      = User::where(['email' => $email])->first();
        if($user){
        return $user->username;
        }

        if($inv){
            return 'already';
        }

        $inv           = new Invitations();
        $inv->user_id  = $this->uid;
        $inv->email_id = $email;
        // $inv->save();

        // ----------------
        // Test Email    
        // ----------------
        $email = 'hello@placerange.com';

        Mail::send('emails.invite-to-placerange', ['from_user' => $from_user], function ($message) use ($email) {
            $message->to($email,'')
                ->subject('Invitation to join Placerange');
        });

        return 'success';

    }

}
