<?php namespace App\Http\Controllers\APIv1;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Place;
use Flash;
use App\TaskCommentHelpful;
use App\TaskComments;
use App\TaskCommentReply;
use App\TaskHandleRequest;
use Illuminate\Contracts\Auth\Guard;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Validator;
use App\UserTags;
use App\TaskTags;
use App\User;
use App\Task;
use App\Tags;
use DB;
use Mail;
use Geocoder;
use App\Notifications;
use NlpTools\Tokenizers\WhitespaceAndPunctuationTokenizer;
use App\GeneralTags;
use JWTAuth;
use Pusher;
use Response;
use Illuminate\Routing\Route;
use Tymon\JWTAuth\Exceptions\JWTException;

class groupAPIController extends Controller {

	private $request,$helper,$uid,$auth;

    public function __construct(Guard $auth, Helper $helper, Route $route, Request $req){
        $this->auth   = $auth;
        $this->helper = $helper;
        $this->req    = $req;
        $token = JWTAuth::getToken();

        if($token){
            $this->uid = JWTAuth::parseToken()->authenticate()->id;
        } else {
            $this->uid= null;
        }

    }

    public function groups_home(){
        
        // Current user ID
        $uid           = $this->uid;
        // User details
        $user          = User::find($uid);
        // current user place      
        $place        = $user->primary_place();
        // Current user skills
        $skills       = $user->skills();

        $groups = [];
        foreach($skills as $skill){

            $total_members = $this->helper->get_nearby_members_for_group_count( $place, $skill );

            $total_tasks   = $this->helper->get_nearby_tasks_for_group_count(   $place, $skill );

            $groups[] = [$skill->tag, $skill->id, $total_members, $total_tasks];
        }

        $groups_data = array(
            'uid'     => $uid,
            'place'   => $place,
            'groups'  => $groups
        );

        return Response::json($groups_data);
    }

    public function group_publicpage_data(){
        
        $data = $this->req->all();
        $slug = $data['slug'];

        $skill_id = explode('-', $slug)[1];
        $place_id = explode('-', $slug)[0];


        $group_place = Place::find($place_id);
        $group_skill = UserTags::find($skill_id);

        $uid   = $this->uid;
        
        $members_nearby = $this->helper->get_nearby_members_for_group( $group_place, $group_skill );

        $nearbytasks    = $this->helper->get_nearby_tasks_for_group($group_place, $group_skill);

		$group_data = array(
			'uid'     => $uid,
            'place'	  => $group_place,
            'skill'	  => $group_skill,
            'members' => $members_nearby,
            'tasks'   => $nearbytasks
		);

		return Response::json($group_data);
    }


}
