<?php namespace App\Http\Controllers\APIv1;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ConnectionRequests;
use App\Connections;
use Guard;
use App\Feedback;
use App\User;
use DB;
use Validator;
use Image;
use Input;
use App\Helpers\Helper;
use Flash;
use App\Task;
use Mail;
use App\Notifications;
use Auth;
use App\UserRating;
use Response;
use App\Place;
use App\Tags;
use Illuminate\Routing\Route;
use App\UserTags;
use App\UserLanguages;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\MessageThreads;
use App\Messages;

class publicProfileAPIController extends Controller {

	private $request,$helper,$uid,$auth;

    public function __construct(Guard $auth, Helper $helper, Route $route, Request $req){
        $this->auth   = $auth;
        $this->helper = $helper;
        $this->req    = $req;

        $token = JWTAuth::getToken();
        if($token){
            $this->uid = JWTAuth::parseToken()->authenticate()->id;
        } else {
            $this->uid= null;
        }

    }


    public function user_publicpage_data(Request $req){
       
        $data = $req->all();
        //Current user ID
        $current_user_id = $this->uid;

        //User details
        $user = User::Where('username',$data['username'])->first();

        $currentUid = $this->uid;
        //User details
        $user       = User::where('username',$data['username'])->first();
        $uid        = $user->id;
        if(!$user->image_name) $user->image_name = 'default.png';
        $user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($user->image_name, 'thumbnail');
        $user->original_image = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($user->image_name, 'original');


        //User address details
        $places                 = $user->places();
        foreach ($places as $p) {
            $p->address = place_min($p);
        }
        $primary_place = place_min($user->primary_place(), true);
    
        $handling_tasks        = Task::whereHas('handle_requests', function($q) use($uid)
        {
            $q->where('user_id', $uid)->where('status',1);

        })->get();
        $connectionStatus = null;
        //Current user connection status
        $connection   = ConnectionRequests::whereIn('from_user', [$currentUid,$uid])->whereIn('to_user', [$uid,$currentUid])->first();
        $current_sent = false;
        $to_sent      = false;
        if(!$connection){
            $connectionStatus = null;
        }else{

            $connectionStatus = $connection->status;
            if($connection->from_user == $uid){
                // Current user has received request
                $to_sent = true;
            }else{
                // Current user has sent request
                
                $current_sent = true;
            }
        }
        $connections = get_user_connections($uid);
        $connections_ids = [];
        foreach ($connections as $friend) {
            if(!$friend->image_name) $friend->image_name = 'default.png';
            $friend->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($friend->image_name, 'medium');
            $connections_ids[] = $friend->id;
        }
        //User skills
        $skills = $user->skills();
        $learns  = $user->learn();
        $teach  = $user->teach();

        //User unresolved tasks
        $my_tasks = $user->user_unresolved_tasks();

        //User resolved tasks
        $resolved_tasks = $user->user_resolved_tasks();

        // Get the user rating
        $ratings = $user->rating();
        $rating_total = 0;
        $average_rating = 0;
        $current_user_reviewed = false;
        foreach ($ratings as $rating) {
            $rating_total = $rating_total + $rating->rating;
            
            // Set the user image
            if(!$rating->reviewed_by->image_name) $rating->reviewed_by->image_name = 'default.png';
            $rating->reviewed_by->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($rating->reviewed_by->image_name, 'medium');

            // Check if the current user has reviewed
            if($currentUid  == $rating->reviewed_by->id){
                $current_user_reviewed = true;
            }
        }
        if(count($ratings) > 0){
            $average_rating = ROUND($rating_total / count($ratings), 1,1);
        }

        $connections_ids[] = $currentUid;
        // Nearby members
        $distance = 2;
        do {
            $result = '';
            $Query =
                "SELECT `place`.*, `user_tags`.`tag`,`users`.`id` as uID," .$this->helper->location_query($p->lat, $p->lng) . '
                     LEFT JOIN `user_tags` on `place`.`for_id` = `user_tags`.`user_id`
                     LEFT JOIN `users` on `place`.`for_id` = `users`.`id`
                     WHERE `for` = "user" AND `place`.`user_id` NOT IN (\'' . implode($connections_ids, "','") . '\')
                     HAVING distance < ' . $distance . '
                     Order BY distance ASC
                     LIMIT 0, 10';
            $Query = "SELECT * FROM (".$Query.") a GROUP BY for_id";

            $result = DB::select(DB::raw($Query));
            $distance = $distance + 10;
        } while (!$result);

        $members_nearby = array();

        foreach($result as $mn){
            $skill = '';
            $member = User::find($mn->uID);
            if(!$member->image_name) $member->image_name = 'default.png';
            $member->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($member->image_name, 'icon');

            foreach ($member->skills() as $s) {
                $skill .= $s->tag . ', ';
            }
            $member->skill_set = rtrim($skill, ', ');
            $members_nearby[] = $member;
        }

        $user_data =  array(
            'user'                  => $user,
            'uid'                   => $uid,
            'places'                => $places,
            'primary_place'         => $primary_place,
            'skills'                => $skills,
            'my_tasks'              => $my_tasks,
            'resolved_tasks'        => $resolved_tasks,
            'handling_tasks'        => $handling_tasks,
            'connection'            => $connection,
            'connectionStatus'      => $connectionStatus,
            'currentUid'            => $currentUid,
            'learns'                => $learns,
            'teach'                 => $teach,
            'current_sent'          => $current_sent,
            'to_sent'               => $to_sent,
            'ratings'               => $ratings,
            'average_rating'        => $average_rating,
            'connections'           => $connections,
            'current_user_reviewed' => $current_user_reviewed,
            'members_nearby'        => $members_nearby
        );

        return Response::json($user_data);
    }

    public function send_connection_request(Request $req){

        $from_user     = User::find($this->uid);
        $data          = $req->all();
        $to_user       = $data['to_user'];
        $to_user       = User::find($to_user);
        $cr            = new ConnectionRequests();
        $cr->from_user = $from_user->id;
        $cr->to_user   = $to_user->id;
        $cr->message   = $data['message'];
        $cr->purpose   = $data['purpose'];
        $cr->status    = 1;
        $cr->save();

        //Add notification
        $not              = new Notifications();
        $not->user_id     = $cr->to_user;
        $not->target_user = $cr->from_user;
        $not->type        = 'user_connection_request';
        $not->target_url  = '/user/'.$from_user->username;
        $not->content     = $from_user->name." has sent you a connection request.";
        $not->save();

        // Set it to default image if the user has no image
        if($from_user->image_name == ''){
            $from_user->image_name = 'default.png';
        }

        $receiver = $to_user;
        $title    = 'You have received a new connection request!';
        $content  = $cr->message;
        $user = $from_user;
        $user->image_url = user_avatar($user->image_name, 'thumbnail');
        Mail::send('emails.user.connection-request', ['sender' => $user, 'receiver' => $receiver, 'content' => $content, 'title' => $title], function ($message) use ($receiver) {
         $message->to($receiver->email, $receiver->name)
             ->subject('Placerange - New Connection Request');
        });


        return 'success';
    }

    public function user_respond_connect(Request $req){

        $uid        = $this->uid;
        $data       = $req->all();
        $crID       = $data['request_id'];
        $status     = $data['status'];
        $cr         = ConnectionRequests::find($crID);
        $cr->status = $status;
        $cr->save();
        if($status == 2) {

            //Add notification
            $not              = new Notifications();
            $not->user_id     = $cr->from_user;
            $not->target_user = $cr->to_user;
            $not->type        = 'user_connection_accept';
            $not->target_url  = '/user/'.$cr->receiver->username;
            $not->content     = $cr->receiver->name." has accepted your connection request.";
            $not->save();

            $con          = new Connections();
            $con->user1   = $cr->from_user;
            $con->user2   = $cr->to_user;
            $con->purpose = $cr->purpose;
            $con->save();

            $receiver        = User::find($cr->to_user);
            $title           = 'You connection request has been accepted!';
            $content         = '';
            $user            = User::find($cr->from_user);
            $user->image_url = user_avatar($user->image_name, 'thumbnail');

            Mail::send('emails.user.connection-accepted', ['sender' => $user, 'receiver' => $receiver, 'content' => $content, 'title' => $title], function ($message) use ($receiver) {
             $message->to($receiver->email, $receiver->name)
                 ->subject('Placerange - Connection Request Approved');
            });


            return 2;
        } else {
            return 3;
        }
    }

    // Handle the rating
    public function user_post_rating(Request $req){

        $uid  = $this->uid;
        $data = $req->all();
        $user = User::find($uid);
        // Make sure these two users are connected
        $check_connection_query = "Select * from connections where (`user1` = ".$uid." AND `user2` = ".$data['to_user']." ) OR (`user1` = ".$data['to_user']." AND `user2` = ".$uid.")";
        $connected = DB::select(DB::raw($check_connection_query));

        $data        = $req->all();
        $from_user   = $uid;
        $to_user     = $data['to_user'];

        $rating     = new UserRating();
        // Save the rating
        $rating->from_user = $from_user;
        $rating->to_user   = $to_user;
        $rating->rating    = $data['rating'];
        $rating->comment   = $data['comment'];
        $rating->save();

        //Add notification
        $notification              = new Notifications();
        $notification->user_id     = $rating->to_user;
        $notification->target_user = $rating->from_user;
        $notification->type        = 'user_rating';
        $notification->target_url  = '#';
        $notification->content     = $user->name." has rated you with $rating->rating star(s).";
        $notification->save();

        $receiver        = User::find($to_user);
        $title           = 'You have received a Rating!';
        $content         = 'Has given you Rating of '.$rating->rating.'<p>'. $rating->comment.'<p>';
        $user->image_url = user_avatar($user->image_name, 'thumbnail');

        Mail::send('emails.user.rating', ['sender' => $user, 'receiver' => $receiver, 'content' => $content, 'title' => $title], function ($message) use ($receiver) {
         $message->to($receiver->email, $receiver->name)
             ->subject('Placerange - Rating Received');
        });

        // Get the user rating
        $rated_user = User::find($to_user);
        $ratings = $rated_user->rating();
        $rating_total = 0;
        $average_rating = 0;
        foreach ($ratings as $rating) {
            $rating_total = $rating_total + $rating->rating;

            if(!$rating->reviewed_by->image_name) $rating->reviewed_by->image_name = 'default.png';
            $rating->reviewed_by->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($rating->reviewed_by->image_name, 'medium');
        }
        if(count($ratings) > 0){
            $average_rating = ROUND($rating_total / count($ratings), 1,1);
        }

        $rating_data =  array(
            'ratings'               => $ratings,
            'average_rating'        => $average_rating,
            'current_user_reviewed' => true
        );

        return Response::json($rating_data);
        
    }

    public function user_send_message(){

        $data = $this->req->all();
        $from_user = User::find($this->uid);
        $to_user = User::find($data['to_user']);

        $thread_exist = MessageThreads::whereIn('user1', [$from_user->id,$to_user->id])->whereIn('user2', [$from_user->id,$to_user->id])->first();
        $msg = new Messages();

        if(!$thread_exist){
            $thread        = new MessageThreads();
            $thread->user1 = $from_user->id;
            $thread->user2 = $to_user->id;
            $thread->save();

            $msg->from_user = $from_user->id;
            $msg->to_user   = $to_user->id;
            $msg->thread_id = $thread->id;
            $msg->message   = $data['message'];
            $msg->save();

        }else{
            $thread_exist->touch();
            $msg->from_user = $from_user->id;
            $msg->to_user   = $to_user->id;
            $msg->thread_id = $thread_exist->id;
            $msg->message   = $data['message'];
            $msg->save();
        }

        $receiver        = $to_user;
        $title           = 'You have received a new Message!';
        $content         = 'Has sent you a message.'.'<p>'. $msg->message.'<p>';
        $user            = $from_user;
        $user->image_url = user_avatar($user->image_name, 'thumbnail');

        Mail::send('emails.user.new-message', ['sender' => $user, 'receiver' => $receiver, 'content' => $content, 'title' => $title], function ($message) use ($receiver) {
         $message->to($receiver->email, $receiver->name)
             ->subject('Placerange - New Message Received');
        });

        $success_data =  array(
            'success'               => 'success'
        );

        return Response::json($success_data);

    }
}
