<?php namespace App\Http\Controllers\APIv1;

use App\Place;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;
use App\User;
use App\Task;
use App\Notifications;
use NlpTools\Tokenizers\WhitespaceAndPunctuationTokenizer;
use JWTAuth;
use Response;
use Illuminate\Routing\Route;
use Tymon\JWTAuth\Exceptions\JWTException;

class notificationAPIController extends Controller {

	private $request,$helper,$uid,$auth;

    public function __construct(Guard $auth, Helper $helper, Route $route, Request $req){
        $this->auth   = $auth;
        $this->helper = $helper;
        $this->req    = $req;
        $token = JWTAuth::getToken();

        if($token){
            $this->uid = JWTAuth::parseToken()->authenticate()->id;
        } else {
            $this->uid= null;
        }
        
    }

    public function notifications(){
		$uid           = $this->uid;
		$user          = User::find($uid);
		$notifications_raw = $user->notifications;    
        $notifications = [];
        $unread_count = 0;
		foreach($notifications_raw as $notification){
		
            $notification->user        = User::find($notification->user_id);
            if(!$notification->user->image_name) $notification->user->image_name = 'default.png';
            $notification->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($notification->user->image_name, 'thumbnail');
            
            if($notification->target_user){
                $notification->receiver = User::find($notification->target_user);
                if(!$notification->receiver->image_name) $notification->receiver->image_name = 'default.png';
                $notification->receiver->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($notification->receiver->image_name, 'thumbnail');
            }

            $notification->target_task = Task::find($notification->target_task);

            $notifications[] = $notification;

            if($notification->read != 1){
                $unread_count++;
            }
		}

		$response =  array(
            'notifications'     => $notifications,
            'unread_count'      => $unread_count
      
        );

        return Response::json($response);

	}


    public function notification_mark_read(){

        $data = $this->req->all();
        $notification = Notifications::find($data['id']);
        if(!$notification->read){
            $notification->read = 1;
            $notification->save();
        }
    }
}
