<?php namespace App\Http\Controllers\APIv1;

use App\Place;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use Illuminate\Contracts\Auth\Guard;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;
use App\User;

use DB;
use NlpTools\Tokenizers\WhitespaceAndPunctuationTokenizer;
use JWTAuth;
use Response;
use Illuminate\Routing\Route;
use Tymon\JWTAuth\Exceptions\JWTException;

use Pusher;
use App\Messages;
use App\MessageThreads;

class messageAPIController extends Controller {

	private $auth, $helper, $req, $uid;
    public function __construct(Guard $auth, Helper $helper, Request $req)
    {
        $this->auth   = $auth;
        $this->helper = $helper;
        $this->req = $req;
        
        $token = JWTAuth::getToken();

        if($token){
            $this->uid = JWTAuth::parseToken()->authenticate()->id;
        } else {
            $this->uid= null;
        }
    }


	public function messages(){
		$uid            = $this->uid;
		$threads_users        = MessageThreads::whereIn('user1', [$uid])->orwhereIn('user2', [$uid])->orderBy('updated_at','desc')->get();
		$unread         = array();
		$users          = array();
		$i              = 0;
		$start_messages = null;
		$start_user     = null;
		$threads        = array();
		foreach($threads_users as $th){
			foreach($th->UnreadMessages as $um){
				if($um->to_user == $uid){
					$unread[$th->id] = true;
				}

			}

			if(!isset($unread[$th->id])){
				$unread[$th->id] = false;
			}

			if($i==0){
				$start_messages = $th->messages;

				foreach($start_messages as $sm){
					if($sm->from_user == $uid){
					}else{
						//Message read by receiver
						$sm->read = 1;
						$sm->save();
					}
				}
				if($th->user1 != $uid) {
					$start_user = $th->user1;
				}else{
					$start_user = $th->user2;

				}
			}
			if($th->user1 != $uid){
				$this_user = User::find($th->user1);
				if(!$this_user->image_name) $this_user->image_name = 'default.png';
                $this_user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($this_user->image_name, 'icon');

                $th->user = $this_user;
			}else{
				$this_user = User::find($th->user2);
				if(!$this_user->image_name) $this_user->image_name = 'default.png';
                $this_user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($this_user->image_name, 'icon');

                $th->user = $this_user;
			}

			$threads[] = $th;
			$i++;
		}

		$connections_data = get_user_connections($uid);
		$connections = [];
		foreach($connections_data as $con){

        	if(!$con->image_name) $con->image_name = 'default.png';
            $con->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($con->image_name, 'icon');

            $connections[] = $con;
        }	

		$messages_data = array(
            'threads'        => $threads,
            'start_messages' => $start_messages,
            'uid'            => $uid,
            'start_user'     => $start_user,
            'unread'         => $unread,
            'connections'    => $connections
        );

        return Response::json($messages_data);
	}

	public function post_new_message(){
		$data = $this->req->all();
		$from_user = User::find($this->uid);
		$to_user = User::find($data['to_user']);

		$thread_exist = MessageThreads::whereIn('user1', [$from_user->id,$to_user->id])->whereIn('user2', [$from_user->id,$to_user->id])->first();
		$msg = new Messages();

		if(!$thread_exist){
			$thread        = new MessageThreads();
			$thread->user1 = $from_user->id;
			$thread->user2 = $to_user->id;
			$thread->save();

			$msg->from_user = $from_user->id;
			$msg->to_user   = $to_user->id;
			$msg->thread_id = $thread->id;
			$msg->message   = $data['message'];
			$msg->save();

		}else{
			$thread_exist->touch();
			$msg->from_user = $from_user->id;
			$msg->to_user   = $to_user->id;
			$msg->thread_id = $thread_exist->id;
			$msg->message   = $data['message'];
			$msg->save();
		}

		Mail::send('emails.message', ['from_user' => $from_user, 'to_user' => $to_user, 'msg' => $msg], function ($message) use ($to_user) {
			$message->to($to_user->email, $to_user->first_name . ' ' . $to_user->last_name)
				->subject('Placerange - New message');
		});

	    

		$uid            = $this->uid;
		$threads_users  = MessageThreads::whereIn('user1', [$uid])->orwhereIn('user2', [$uid])->orderBy('updated_at','desc')->get();
		$unread         = array();
		$users          = array();
		$i              = 0;
		$start_messages = null;
		$start_user     = null;
		$threads        = array();
		foreach($threads_users as $th){
			foreach($th->UnreadMessages as $um){
				if($um->to_user == $uid){
					$unread[$th->id] = true;
				}

			}

			if(!isset($unread[$th->id])){
				$unread[$th->id] = false;
			}

			if($i==0){
				$start_messages = $th->messages;

				foreach($start_messages as $sm){
					if($sm->from_user == $uid){
					}else{
						//Message read by receiver
						$sm->read = 1;
						$sm->save();
					}
				}
				if($th->user1 != $uid) {
					$start_user = $th->user1;
				}else{
					$start_user = $th->user2;

				}
			}
			if($th->user1 != $uid){
				$this_user = User::find($th->user1);
				if(!$this_user->image_name) $this_user->image_name = 'default.png';
                $this_user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($this_user->image_name, 'icon');

                $th->user = $this_user;
			}else{
				$this_user = User::find($th->user2);
				if(!$this_user->image_name) $this_user->image_name = 'default.png';
                $this_user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($this_user->image_name, 'icon');

                $th->user = $this_user;

			}
			$threads[] = $th;
			$i++;
		}

		$connections_data = get_user_connections($uid);
		$connections = [];
		foreach($connections_data as $con){

        	if(!$con->image_name) $con->image_name = 'default.png';
            $con->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($con->image_name, 'icon');

            $connections[] = $con;
        }	

		$messages_data = array(
            'threads'        => $threads,
            'start_messages' => $start_messages,
            'uid'            => $uid,
            'start_user'     => $start_user,
            'unread'         => $unread,
            'connections'    => $connections
        );

        return Response::json($messages_data);

	}

	public function ajax_post_message(){
		$data      = $this->req->all();
		$uid       = $this->uid;
		$from_user = User::find($this->uid);
		$to_user   = User::find($data['to_user']);
		$thread    = MessageThreads::whereIn('user1', [$from_user->id,$to_user->id])->whereIn('user2', [$from_user->id,$to_user->id])->first();
		$msg       = new Messages();
		$thread->touch();
		$msg->from_user = $from_user->id;
		$msg->to_user   = $to_user->id;
		$msg->thread_id = $thread->id;
		$msg->message   = $data['message'];
		$msg->save();
		// Mail::send('emails.message', ['from_user' => $from_user, 'to_user' => $to_user, 'msg' => $msg], function ($message) use ($to_user) {
		// 	$message->to($to_user->email, $to_user->first_name . ' ' . $to_user->last_name)
		// 		->subject('Placerange - New message');
		// });

		$messages = $thread->messages;
		//Return block
		foreach($messages as $m){
			if($m->from_user == $uid){
				$m->sender = 1;

			}else{
				
				//Message read by receiver
				$m->read = 1;
				$m->save();
				$m->sender = 0;
			}

			$m->message_from = User::find($m->from_user);
			if(!$m->message_from->image_name) $m->message_from->image_name = 'default.png';
            $m->message_from->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($m->message_from->image_name, 'icon');

		}

		if($msg->from_user == $uid){
				$msg->sender = 1;

			}else{
				
				//Message read by receiver
				$msg->read = 1;
				$msg->save();
				$msg->sender = 0;
			}

			$msg->message_from = User::find($msg->from_user);
			if(!$msg->message_from->image_name) $msg->message_from->image_name = 'default.png';
            $msg->message_from->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($msg->message_from->image_name, 'icon');


		$pusher = new Pusher( '3cf39c63f99ff76f1249','1118e34b7e164f6bb7ea','196448' );
	    $pusher_data['message'] = $msg;
    	$pusher->trigger('thread_'.$thread->id , 'messages', $pusher_data);
	}

	public function load_messages(){
		$uid      = $this->uid;
		$data     = $this->req->all();
		$for_user = $data['for_user'];
		$thread   = MessageThreads::whereIn('user1', [$uid,$for_user])->whereIn('user2', [$uid,$for_user])->first();
		$messages = $thread->messages;
		//Return block
		foreach($messages as $m){
			if($m->from_user == $uid){
				$m->sender = 1;

			}else{
				
				//Message read by receiver
				$m->read = 1;
				$m->save();
				$m->sender = 0;
			}

			$m->message_from = User::find($m->from_user);
			if(!$m->message_from->image_name) $m->message_from->image_name = 'default.png';
                $m->message_from->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($m->message_from->image_name, 'icon');

		}

		//End return block
		$messages_data = array(
            'thread'   => $thread,
            'messages' => $messages
        );

        return Response::json($messages_data);
	}

	public function message_autoload(){
		$uid      = $this->uid;
		$data     = $this->req->all();
		$for_user = $data['to_user'];
		$thread   = MessageThreads::whereIn('user1', [$uid,$for_user])->whereIn('user2', [$uid,$for_user])->first();
		$messages = Messages::where('thread_id', $thread->id)->where('id','>',$data['count'])->orderBy('id','DESC')->get();

		//Return block
		foreach($messages as $m){
			if($m->from_user == $uid){
				$sender = 1;
			}else{
				$sender = 0;

				//Message read by receiver
				$m->read = 1;
				$m->save();
			}
		}
		//End return block
		$messages_data = array(
            'thread'   => $thread,
            'messages' => $messages
        );

        return Response::json($messages_data);
	}



}
