<?php namespace App\Http\Controllers\APIv1;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ConnectionRequests;
use App\Connections;
use Guard;
use App\Feedback;
use App\User;
use DB;
use Validator;
use Image;
use Input;
use App\Helpers\Helper;
use App\Task;
use Mail;
use App\Notifications;
use Auth;
use App\UserRating;
use Response;
use App\Place;
use App\Tags;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Routing\Route;
use App\UserTags;
use App\UserLanguages;
use App\Invitations;

class PlacerangeAPI extends Controller {

	private $request,$helper,$uid,$auth;

    public function __construct(Guard $auth, Helper $helper, Route $route, Request $req){
        $this->auth   = $auth;
        $this->helper = $helper;
        $this->req    = $req; 
        $token = JWTAuth::getToken();

        if($token){
            $this->uid = JWTAuth::parseToken()->authenticate()->id;
        } else {
            $this->uid= null;
        }
        
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            $customClaims = ['exp' => strtotime("+10 days", time())];
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials, $customClaims)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // if no errors are encountered we can return a JWT
        return response()->json(compact('token'));
    }

    public function get_authenticated_user()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        if(!$user->image_name) $user->image_name = 'default.png';
        $user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($user->image_name, 'icon');
        // the token is valid and we have found the user via the sub claim
        $place = $user->primary_place();
        $response = array(
            'user'    => $user,
            'place'   => $place,
            'token'   => JWTAuth::getToken()
        );

        return Response::json($response);
    }

    // Landing PAge Search
    function landing_search(){

        $data         = $this->req->all();
        $distance     = $data['distance'];
        $place        = $this->helper->place($data['place']);
        $current_date = date('Y-m-d G:i:s');

        $tasks = [];
        $Query = "SELECT `users`.first_name, `users`.last_name, `users`.username, `users`.image_name,`place`.*,`tasks`.title,`tasks`.slug,`tasks`.created_at, `tasks`.updated_at, `tasks`.`id` as wID,".
                        $this->helper->location_query($place['lat'], $place['lng']). '
                         JOIN `tasks` on `place`.`for_id` = `tasks`.`id`
                         JOIN `users` on `tasks`.`user_id` = `users`.`id`
                         WHERE `tasks`.`deleted` != 1 AND `place`.`for` = "task" AND `tasks`.`title` LIKE "%' . $data['search_text'] . '%"
                         HAVING distance < ' . $distance . "
                         LIMIT 0, 20 ";
            $Query = "SELECT * FROM (" . $Query . ") a GROUP BY task_id";

            $nearbytasks = DB::select(DB::raw($Query));

            foreach ($nearbytasks as $r) {
                $task = task::find($r->task_id);
                $task_data['title']     = $task->title;
                $task_data['slug']      = $task->slug;
                $task_data['summary']   = $task->summary;
                $task_data['date']      = $task->created_at;
                $task_data['skills']    = $task->tags()->get()->toArray();
                $task_data['comments']  = $task->comments->toArray(); 
                $task_data['days_past'] = 'Raised ' . ((isset($task->created_at)) ? abs(floor((strtotime($task->created_at) - time())/60/60/24)) : 0) . ' days ago.' ;
                $tasks[] = $task_data;
            }


            //tasks created near current user
            $Query = "SELECT `place`.user_id AS 'user_idss', `users`.first_name, `users`.last_name, `users`.username, `users`.image_name, `users`.created_at,`place`.*," . 
            $this->helper->location_query($place['lat'], $place['lng']) . '
                 JOIN `users` on `place`.`user_id` = `users`.`id`
                 HAVING distance < ' . $distance . "
                 ORDER BY `users`.`id` DESC
                 LIMIT 0, 40";
            $Query = "SELECT * FROM (" . $Query . ") a GROUP BY user_id";

            $newUsers = DB::select(DB::raw($Query));
            $users = [];
            foreach ($newUsers as $wa) {

                if(!$wa->image_name) $wa->image_name = 'default.png';
                $wa->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($wa->image_name, 'thumbnail');

                $feed_data['image']    = $wa->image_url;
                $feed_data['name']     = $wa->first_name . ' ' . $wa->last_name;
                $feed_data['username'] = $wa->username;
                $feed_data['title']    = $wa->first_name . ' ' . $wa->last_name;
                $feed_data['slug']     = '#/user/' . $wa->username;

                
                $feed_data['thumbnail_url'] = $wa->image_url;

                $this_user = User::find($wa->user_idss);
                //User skills
                $feed_data['skills']   = $this_user->skills()->toArray();
                $feed_data['learns']    = $this_user->learn()->toArray();
                $feed_data['distance'] = round($wa->distance, 2);
                $feed_data['date']     = date("d M - h:i A",strtotime($wa->created_at));
                $feed_data['top_text'] = '<a ng-href="'.$feed_data['slug'].'">'.$wa->first_name . ' ' . $wa->last_name . '</a> has joined Placerange from ' . ($wa->locality ? $wa->locality : $wa->country). '.';
                $users[]               = $feed_data;
            }

        $search_data =  array(
            'users' => $users,
            'tasks' => $tasks
        );

        return Response::json($search_data);

    }

    public function post_feedback(Request $req){
        $data      = $req->all();
        $uid = $this->uid;
        $validator = Validator::make(
            array(
                'content' => $data['content'],
            ),
            array(
                'content' => 'required',
            )
        );

        if ($validator->fails())
        {
            $errors = $validator->messages();
            return Response::json($errors);
        }else{
            $fb          = new Feedback();
            $fb->content = $data['content'];
            $fb->user_id = $uid;
            $fb->save();
            $success = 1;

            Mail::send('emails.feedback', ['content' => $data['content']], function ($message) {
                $message->to('placerange@gmail.com','Placerange - Feedback')
                    ->subject('New Feedback received');
            });

            return 'success';

        }


    }
    
    public function user_update_profile(Request $req)
    {
        $this->request = $req;
        $data          = $req->all();

        //Current user ID
        $uid  = $data['id'];

        //User details
        $user = User::find($uid);

        // Available to meet
        if($data['available_to_meet'] == true){
            $data['available_to_meet'] = 1;
        }else{
            $data['available_to_meet'] = 0;
        }

        $validator = Validator::make(
            array(
                'name' => $data['name']
            ),
            array(
                'name' => 'required'
            )
        );

        if ($validator->fails())
        {
            $errors = $validator->messages();
            $validator_data = array(
                'errors' => $errors,
                'error' => 'error'
            );

            return Response::json($validator_data);
        }else{
            $user->name              = $data['name'];
            $user->phone             = $data['phone'];
            $user->newsletter        = $data['newsletter'];
            $user->status            = $data['status'];
            $user->available_to_meet = $data['available_to_meet'];
            $user->save();
            return 'success';

        }


    }

    // function to update the given user skill
    public function user_update_language(Request $req)
    {
        //Current user ID
        $uid  = $this->uid;
        $data = $req->all();

        $ul           = UserLanguages::find($data['pk']);
        if( $data['language'] == '' || $data['language'] == null){
            $ul->delete();
        } else {
            $ul->language = $data['language'];
            $ul->save();
        }

        return 'success';
    }

    public function user_add_skill(){

        //Current user ID
        $uid         = $this->uid;
        $user        = User::find($uid);
        $data        = $this->req->all();

        $validator   = Validator::make(
            array(
                'skill' => $data['skill'],
            ),
            array(
                'skill' => 'required',
            )
        );

        $ut = new UserTags;

        $ut->user_id  = $uid;
        $ut->tag_type = $data['type'];
        $ut->tag      = $data['skill'];
        $ut->save();

        $tag           = new Tags;
        $tag->tag      = $ut->tag;
        $tag->tag_type = $ut->tag_type;
        $tag->for      = 'user';
        $tag->for_id   = $uid;
        $tag->user_id  = $uid;
        $tag->save();

        // Return the new set of skills
        if($data['type'] == 'skill'){
            return Response::json($user->skills());
        } else{
            return Response::json($user->learn());
        }
    }

    public function user_remove_skill(){

        //Current user ID
        $uid  = $this->uid;

        $data = $this->req->all();
        $user = User::find($uid);
        $ut   = UserTags::find($data['skill_id']);
        $tag  = $ut->tag;
        $type = $ut->tag_type;
        $ut->delete();

        $tag  = Tags::where(['for'=>'user', 'tag' => $tag, 'for_id' => $uid, 'tag_type' => $type])->first();
        $tag->delete();

        // Return the new set of skills
        if($type == 'skill'){
            return Response::json($user->skills());
        } else{
            return Response::json($user->learn());
        }
    }

    public function user_add_place(){
        //Current user ID
        $data       = $this->req->all();
        $related_id = $this->uid;
        $user      = User::find($related_id);
        $place      = $this->helper->place($data['place']);
        $validator  = Validator::make(
            array(
                'place' => $place,

            ),
            array(
                'place' => 'required',
            )
        );

        if ($validator->fails())
        {
            $errors = $validator->messages();
            if (count($errors) > 0){
                // Get the place data again and return with an error
                $places = $user->places();
                foreach ($places as $p) {
                    $p->address = place_min($p);
                }

                $return_data = array(
                    'places' => $places,
                    'error'  => 'invalid_place'
                );

                return Response::json($return_data);
            }
        }else{
            $place          = Place::create($place);
            $place->for_id  = $related_id;
            $place->for     = 'user';
            $place->user_id = $related_id;
            // Save the place if everything is fine
            $place->save();

            // Get the place data again and return if success
            $places = $user->places();
            foreach ($places as $p) {
                $p->address = place_min($p);
            }

            $place_data = array(
                'places'        => $places
            );

            return Response::json($place_data);
         }
    }


    public function user_update_place()
    {
        $data      = $this->req->all();
        $uid       = $this->uid;
        $user      = User::find($uid);
        $old_place = Place::find($data['place_id']);
        $new_place = $this->helper->place($data['place']);
        $validator = Validator::make(
            array(
                'place' => $new_place,

            ),
            array(
                'place' => 'required',
            )
        );
        $invalid_message = '';
        if ($validator->fails()) {
            $errors = $validator->messages();
            $invalid_message = 'invalid_place';
        } else {
            // Update the place
            $place_keys = ['establishment', 'formatted_address', 'postal_code', 'sublocality_level_2', 'sublocality_level_1', 'sublocality', 'locality', 'administrative_area_level_1', 'country'];
            foreach ($place_keys as $key) {
                if (!isset($new_place[$key])) {
                    $new_place[$key] = null;
                }
            }

            // Update the place (Performs the actual update)
            Place::find($old_place->id)->update($new_place);
        }



        $primary_place = $user->primary_place();

        $place_data = array(
            'error'         => $invalid_message,
            'primary_place' => $primary_place
        );

        return Response::json($place_data);
    }

    // Remove place
    public function user_remove_place(){
        $data  = $this->req->all();
        $uid   = $this->uid;
        $user      = User::find($uid);
        $place = Place::find($data['place_id']);
        if($place->primary){
            return;
        }else{
            // If this is not a primary place, delete it
            $place->delete();
        }

        // Get the place data again and return if success
        $places = $user->places();
        foreach ($places as $p) {
            $p->address = place_min($p);
        }

        $place_data = array(
            'places'        => $places
        );

        return Response::json($place_data);
    }


    public function user_upload_image()
    {
        $token = $this->req->all()['token'];

        if($token){
            $uid = JWTAuth::authenticate($token)->id;
        } else {
            return 'Invalid User';
        }       
        if (!empty( $_FILES ))
        {
            $file = $_FILES[ 'file' ];
            if($file['type'] == 'image/jpeg') $ext = '.jpeg';
            if($file['type'] == 'image/png') $ext = '.png';
            if($ext) {
                $fname = $uid.'_'.time().$ext;
                $previous_image = DB::table('users')->where('id', $uid)->pluck('image_name');

                if($previous_image) {
                    $original_url  = public_path() . '/uploads/users/original/' . $previous_image;
                    $medium_url    = public_path() . '/uploads/users/medium/' . $previous_image;
                    $thumbnail_url = public_path() . '/uploads/users/thumbnail/' . $previous_image;
                    $icon_url      = public_path() . '/uploads/users/icon/' . $previous_image;

                    // Check and remove the previous image file
                    if(file_exists ( $original_url )){
                        unlink($original_url);
                    }
                    if(file_exists ( $medium_url )){
                        unlink($medium_url);
                    }
                    if(file_exists ( $thumbnail_url )){
                        unlink($thumbnail_url);
                    }
                    if(file_exists ( $icon_url )){
                        unlink($icon_url);
                    }
                    
                }

                // Resize the uploaded image into various sizes and store them
                Image::make($file['tmp_name'])->save(public_path().'/uploads/users/original/'.$fname);
                Image::make($file['tmp_name'])->fit(250, 250)->save(public_path().'/uploads/users/thumbnail/'.$fname);
                Image::make($file['tmp_name'])->fit(50, 50)->save(public_path().'/uploads/users/icon/'.$fname);
                Image::make($file['tmp_name'])->fit(700, 700)->save(public_path().'/uploads/users/medium/'.$fname);

                // Update the databse with the new file name
                DB::table('users')->where('id', $uid)->update(array('image_name' => $fname));
                // Return the file name
                $image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($fname, 'medium');

                 $image_data = array(
                        'image_url' => $image_url
                    );

                    return Response::json($image_data);
            }else{
                return 'invalid_image';
            }
        } else {
            return 'no_image';
        }
    }

    
}
