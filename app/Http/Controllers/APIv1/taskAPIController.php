<?php namespace App\Http\Controllers\APIv1;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Place;
use Flash;
use App\TaskCommentHelpful;
use App\TaskComments;
use App\TaskCommentReply;
use App\TaskHandleRequest;
use Illuminate\Contracts\Auth\Guard;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Validator;
use App\UserTags;
use App\TaskTags;
use App\User;
use App\Task;
use App\Tags;
use DB;
use Geocoder;
use App\Notifications;
use NlpTools\Tokenizers\WhitespaceAndPunctuationTokenizer;
use App\GeneralTags;
use JWTAuth;
use Response;
use Illuminate\Routing\Route;
use Tymon\JWTAuth\Exceptions\JWTException;

class taskAPIController extends Controller {

	private $req,$helper,$uid,$auth;

    public function __construct(Guard $auth, Helper $helper, Route $route, Request $req){
        $this->auth   = $auth;
        $this->helper = $helper;
        $this->req    = $req;
        $token = JWTAuth::getToken();

        if($token){
            $this->uid = JWTAuth::parseToken()->authenticate()->id;
        } else {
            $this->uid= null;
        }
   
    }

    public function task_create_data(){

        if(!$this->uid){
            return 'error';
        }

        $uid   = $this->uid;
        $user  = User::find($uid);
        $place = place_min($user->primary_place());

        $task_data = array(
                'uid'   => $uid,
                'user'  => $user,
                'place' => $place,
            );

        return Response::json($task_data);

    }

    public function task_create(){

        if(!$this->uid){
            return 'error';
        }

        //Current user ID
        $uid           = $this->uid;
        $user          = User::find($uid);
        $data          = $this->req->all();
        $data['place'] = $this->helper->place($user->primary_place()->formatted_address);

        if($data['assistance'] === true){
            $data['assistance'] = 1;
        } else{
            $data['assistance'] = null;
        }
        
        $validator = Validator::make(
            array(
                'title'   => $data['title'],
                'summary' => $data['summary']

            ),
            array(
                'title'   => 'required|min:5',
                'summary' => 'required|min:5'
            )
        );

        if ($validator->fails())
        {

            $errors = $validator->messages();
            $validator_data = array(
                'errors' => $errors,
                'error' => 'error'
            );

            return Response::json($validator_data);
        }else{

            $task             = new task;
            $task->title      = $data['title'];
            $task->user_id    = $uid;
            $task->assistance = $data['assistance'];
            $task->summary    = $data['summary'];
            $task->save();
            $place           = $data['place'];
            $place           = Place::create($place);
            $place->for      = 'task';
            $place->primary  = 1;
            $place->for_id   = $task->id;
            $place->task_id = $task->id;
            $place->save();

            //Autoset skills
            $tokenizer = new WhitespaceAndPunctuationTokenizer();
            $ss        = $task->title. ' ' . $task->summary;
            $tokens    = $tokenizer->tokenize($ss);

                //Get all the tags and store it in a list
                $tags = Tags::all();
                foreach($tags as $tag){
                    $tags_list2[] = strtolower($tag->tag);
                }
                $general_tags = GeneralTags::all();
                foreach($general_tags as $tag){
                    $tags_list3[] = strtolower($tag->tag);
                }
            $tags_list = array_unique(array_merge($tags_list2, $tags_list3));

            foreach($tokens as $key => $token) {

                $tok_prev = '';
                $tok_next = '';

                if (isset($tokens[$key - 1])) {
                    $tok_prev = $tokens[$key - 1];
                }
                if (isset($tokens[$key + 1])) {
                    $tok_next = $tokens[$key + 1];
                }
                if(in_array(strtolower($token),$tags_list) || in_array(strtolower($tok_prev.' '.$token),$tags_list) || in_array(strtolower($token.' '.$tok_next),$tags_list)){
                    $ut             = new TaskTags;
                    $ut->task_id   = $task->id;
                    $ut->tag_type   = 'skill';
                    $ut->tag        = $token;
                    $ut->level      = 3;
                    $ut->timestamps = false;
                    $ut->save();

                    $tag           = new Tags;
                    $tag->tag      = $token;
                    $tag->tag_type = 'skill';
                    $tag->for      = 'task';
                    $tag->for_id   = $task->id;
                    $tag->task_id = $task->id;
                    $tag->level    = 3;
                    $tag->save();
                }
            }

            $task_data = array(
                'success' => 'success',
                'slug' => $task->slug
            );

            return Response::json($task_data);

        }
    }
    
    public function task_data(){
        $data = $this->req->all();
    	
        $task = Task::findBySlug($data['slug']);
        $uid   = $this->uid;
        if(!$task){
            return '404';
        }
        $user            = $task->user;
        if(!$user->image_name) $user->image_name = 'default.png';
        $user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($user->image_name, 'thumbnail');

        $place = $task->primary_place();
        
        $solved_by       = $task->solved_by_user;
        $skills          = $task->tags()->get();
        $comments        = $task->comments;

        foreach ($comments as $c) {
            if(!$c->user->image_name) $c->user->image_name = 'default.png';
            $c->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($c->user->image_name, 'thumbnail');
            $c->replies = $c->replies()->get();

            foreach ($c->replies as $r) {
                $r->user = $r->user()->first();
                if(!$r->user->image_name) $r->user->image_name = 'default.png';
                $r->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($r->user->image_name, 'thumbnail');
            }
        }

        $handle_requests = $task->handle_requests;

        foreach ($handle_requests as $hr) {
            if(!$hr->user->image_name) $hr->user->image_name = 'default.png';
            $hr->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($hr->user->image_name, 'thumbnail');
        }

        if($task->assistance == 1){
            $task->assistance = true;
        } else{
            $task->assistance = false;
        }

        $handle_approved = false;
        $awating_approval = false;
        $handle_cancelled = false;
        if(!$handle_requests->isEmpty()){
            foreach($handle_requests as $hr){
                if($uid == $hr->user_id){
                    if($hr->status == null){
                        $awating_approval = true;
                    }elseif($hr->status == 1){
                        $handle_approved = true;
                    }elseif($hr->status > 1){
                        $handle_cancelled = true;
                    }
                }
            }
        }

        

		$task_data = array(
			'task'            => $task,
			'user'            => $user,
			'uid'             => $uid,
			'skills'          => $skills,
			'comments'        => $comments,
			'handle_requests' => $handle_requests,
			'solved_by'       => $solved_by,
			'place'           => $place,
            'handle_approved'  => $handle_approved,
            'awating_approval' => $awating_approval,
            'handle_cancelled' => $handle_cancelled


		);

		return Response::json($task_data);
    }

    public function task_update()
    {
        $data      = $this->req->all();

        //Current user ID
        $task_id  = $data['id'];
        if($data['assistance'] === true){
            $data['assistance'] = 1;
        } else{
            $data['assistance'] = null;
        }
        //Current user ID
        $uid       = $this->uid;
        $data      = $this->req->all();
        $task     = Task::find($task_id);
        $validator = Validator::make(
            array(
                'title'   => $data['title'],
                'summary' => $data['summary'],

            ),
            array(
                'title'   => 'required|min:5',
                'summary' => 'required|min:10',
            )
        );

        if ($validator->fails())
        {
            $errors = $validator->messages();
            return  'error';
        }else{

            $task->title      = $data['title'];
            $task->user_id    = $uid;
            $task->assistance = $data['assistance'];
            $task->summary    = $data['summary'];
            $task->save();
            return 'success';
        }


    }


    public function task_delete(){

        if(!$this->uid){
            return 'error';
        }

        $data = $this->req->all();

        // Find the task and set the deleted flag
        $task = Task::find($data['id']);
        $task->deleted = 1;
        $task->save();

        return 'success';

    }
    // function to update the given task skill
    public function task_update_skill_level()
    {
        //Current user ID
        $data = $this->req->all();

        $ut        = TaskTags::find($data['id']);
        $ut->level = $data['skill_level'];
        $ut->save();

        $tagu       = Tags::where(['tag' => $ut->tag, 'task_id' => $ut->task_id ])->first();
        $tag        = Tags::find($tagu->id);
        $tag->level = $data['skill_level'];
        $tag->save();

        return 'success';
    }

    public function task_add_skill(){

        //Current user ID
        $data     = $this->req->all();
		$task_id = $data['task_id'];
        $task     = Task::find($task_id);
        $validator = Validator::make(
            array(
                'skill' => $data['skill'],
            ),
            array(
                'skill' => 'required',
            )
        );

        $ut = new TaskTags;

        $ut->task_id  = $task_id;
        $ut->tag_type = 'skill';
        $ut->tag      = $data['skill'];
        $ut->save();

        $tag           = new Tags;
        $tag->tag      = $ut->tag;
        $tag->tag_type = $ut->tag_type;
        $tag->for      = 'task';
        $tag->for_id   = $task_id;
        $tag->task_id = $task_id;
        $tag->save();

        // Return the new set of skills
        return Response::json($task->tags()->get());

    }


    public function task_remove_skill(){

        $data     = $this->req->all();
		$task_id = $data['task_id'];
        $task = Task::find($task_id);
        $ut   = TaskTags::find($data['skill_id']);
        $tag  = $ut->tag;
        $type = $ut->tag_type;
        $ut->delete();

        $tag  = Tags::where(['for'=>'task', 'tag' => $tag, 'task_id' => $task_id])->first();
        $tag->delete();

        // Return the new set of skills
        return Response::json($task->tags()->get());
    }

    public function task_add_place(){
        //Current task ID
        $data      = $this->req->all();
        $task_id  = $data['task_id'];
        $task     = Task::find($task_id);
        $place     = $this->helper->place($data['place']);
        $validator = Validator::make(
            array(
                'place' => $place,

            ),
            array(
                'place' => 'required',
            )
        );

        if ($validator->fails())
        {
            $errors = $validator->messages();
            if (count($errors) > 0){
                // Get the place data again and return with an error
                $places = $task->places();
                foreach ($places as $p) {
                    $p->address = place_min($p);
                }

                $return_data = array(
                    'places' => $places,
                    'error'  => 'invalid_place'
                );

                return Response::json($return_data);
            }
        }else{
            $place           = Place::create($place);
            $place->for_id   = $task_id;
            $place->for      = 'task';
            $place->task_id = $task_id;
            // Save the place if everything is fine
            $place->save();

            // Get the place data again and return if success
            $places = $task->places();
            foreach ($places as $p) {
                $p->address = place_min($p);
            }

            $place_data = array(
                'places'        => $places
            );

            return Response::json($place_data);
         }
    }


    public function task_update_place()
    {
        $data      = $this->req->all();
        $task_id  = $data['task_id'];
        $task     = Task::find($task_id);
        $old_place = Place::find($data['place_id']);
        $new_place = $this->helper->place($data['place']);
        $validator = Validator::make(
            array(
                'place' => $new_place,

            ),
            array(
                'place' => 'required',
            )
        );
        $invalid_message = '';
        if ($validator->fails()) {
            $errors          = $validator->messages();
            $invalid_message = 'invalid_place';
        } else {
            // Update the place
            $place_keys = ['establishment', 'formatted_address', 'postal_code', 'sublocality_level_2', 'sublocality_level_1', 'sublocality', 'locality', 'administrative_area_level_1', 'country'];
            foreach ($place_keys as $key) {
                if (!isset($new_place[$key])) {
                    $new_place[$key] = null;
                }
            }

            // Update the place (Performs the actual update)
            Place::find($old_place->id)->update($new_place);
        }

        // Get the place data again and return if success
        $places = $task->places();
        foreach ($places as $p) {
            $p->address = place_min($p);
        }

        $primary_place = place_min($task->primary_place(), true);

        $place_data = array(
            'error'         => $invalid_message,
            'places'        => $places, 
            'primary_place' => $primary_place
        );

        return Response::json($place_data);
    }

    // Remove place
    public function task_remove_place(){
        $data     = $this->req->all();
        $task_id  = $data['task_id'];
        $task     = Task::find($task_id);
        $place    = Place::find($data['place_id']);
        if($place->primary){
            return;
        }else{
            // If this is not a primary place, delete it
            $place->delete();
        }

        // Get the place data again and return if success
        $places = $task->places();
        foreach ($places as $p) {
            $p->address = place_min($p);
        }

        $place_data = array(
            'places'        => $places
        );

        return Response::json($place_data);
    }
}
