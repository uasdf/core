<?php namespace App\Http\Controllers\APIv1;

use App\Place;
use App\Http\Controllers\Controller;
use App\Tags;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Laracasts\Flash\Flash;
use Validator;
use App\User;
use App\Task;
use Geocoder;
use DB;
use App\Search;
use App\Notifications;
use App\GeneralTags;
use NlpTools\Tokenizers\WhitespaceAndPunctuationTokenizer;
use NlpTools\Stemmers\PorterStemmer;
use JWTAuth;
use Response;
use Illuminate\Routing\Route;
use Tymon\JWTAuth\Exceptions\JWTException;


class searchAPIController extends Controller {

	private $req,$helper,$uid,$auth;

    public function __construct(Guard $auth, Helper $helper, Route $route, Request $req){
        $this->auth   = $auth;
        $this->helper = $helper;
        $this->req    = $req;
        $token = JWTAuth::getToken();

        if($token){
            $this->uid = JWTAuth::parseToken()->authenticate()->id;
        } else {
            $this->uid= null;
        }
   
    }


	public function search_near_me(){

        $uid    = $this->uid;
		$user   = User::find($uid);
		$places = $user->places();

		foreach ($places as $p) {
            $p->address = place_min($p);
        }

        $user_skills = $user->skills();

        $primary_place = $user->primary_place();
		$search_data = array(
                'uid'           => $uid,
                'user'          => $user,
                'places'        => $places,
                'primary_place' => $primary_place
            );

        return Response::json($search_data);

    }

    public function search_post_near_me()
    {
        //Current user ID
        $uid  = $this->uid;
        $data = $this->req->all();

        //User details
        $user                 = User::find($uid);
        
        //User address details
        $p            = Place::find($data['selected_place']);
        $skills_string = '';
        foreach ($data['skills'] as $key => $s) {
            $s = (array)$s;
            $skills_string = $skills_string . $s['text'] . ',';
        }

        
        $result_header = 'Results for '.$data['selected_for'].' nearby '.place_min($p,true).($skills_string ? ' matching skills '.$skills_string : '');

        $skills = explode(',', $skills_string);
        $distance = $data['distance'];
        //People who can teach
        $teachQuery       = '';
        $users            = null;
        $programs         = null;
        $tasks          = null;
        $i                = 0;
        $selected_for     = $data['selected_for'];
        if ($selected_for == 'users') {

            $Query =
                "SELECT `place`.*, ". $this->helper->location_query($p->lat, $p->lng ) . '
                 LEFT JOIN `user_tags` on `place`.`for_id` = `user_tags`.`user_id`
                 LEFT JOIN `users` on `place`.`for_id` = `users`.`id`
                 WHERE '.( $skills ? '`user_tags`.`tag` IN (\'' . implode($skills, "','") . '\') AND  `user_tags`.`tag_type` IN ("skill","teach") AND' : '').' `for` = "user" AND `for_id` != ' . $uid . '
                 HAVING distance < ' . $distance . '
                 Order BY distance ASC
                 LIMIT 0, 20';
            $Query = "SELECT * FROM (".$Query.") a GROUP BY for_id ORDER BY distance ASC ";

            $users_result = DB::select(DB::raw($Query));

            $users = [];
            foreach ($users_result as $user) {
                $dist = $user->distance;
                
                $user = User::find($user->user_id);

                if(!$user->image_name) $user->image_name = 'default.png';
                $user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($user->image_name, 'thumbnail');

                $skill_set = '';
                foreach ($user->skills() as $s) {
                    $skill_set .= $s->tag . ', ';
                }
                $user->skill_set = rtrim($skill_set, ', ');
                $user->address = place_min($user->primary_place());
                $user->distance = round($dist, 2);
                $users[] = $user;
            }


        } elseif ($selected_for == 'tasks') {

            $Query = "SELECT `place`.*, ". $this->helper->location_query($p->lat, $p->lng ) . '
                 LEFT JOIN `task_tags` on `place`.`for_id` = `task_tags`.`task_id`
                 LEFT JOIN `tasks on `place`.`for_id` = `tasks.`id`
                 WHERE `tasks`.`deleted` != 1 AND '.( $skills ? '`task_tags`.`tag` IN (\'' . implode($skills, "','") . '\') AND `task_tags`.`tag_type` = "skill" AND ' : '').' `place`.`for` = "task"
                 HAVING distance < ' . $distance . '
                 LIMIT 0, 20';
            $Query = "SELECT * FROM (".$Query.") a GROUP BY for_id ORDER BY distance ASC ";

            $task_result = DB::select(DB::raw($Query));
            $tasks= [];

            foreach ($task_result as $task) {
                $dist = $task->distance;
                $task = Task::find($task->task_id);

                $skill_set = '';
                foreach ($task->tags()->get() as $s) {
                    $skill_set .= $s->tag . ', ';
                }
                $task->skill_set = rtrim($skill_set, ', ');
                if(!$task->user->image_name) $task->user->image_name = 'default.png';
                $task->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($task->user->image_name, 'thumbnail');

                $task->image_url = $task->user->image_url;
                
                $task->address = place_min($task->primary_place());
                $task->distance = round($dist, 2);
                $tasks[] = $task;
            }


         } 


        $search_result = array(
                'users'         => $users,
                'tasks'        => $tasks,
                'result_header' => $result_header
            );

        return Response::json($search_result);

    }



    public function postSearch(Request $req){
     
        $uid         = $this->uid;
        $user        = User::find($uid);
        $skills      = array();
        $level       = '';
        $for         = '';
        $type        = '';
        $i           = 0;
        $connections = array();
        //Tokenize and stem the given search string
        $tokenizer = new WhitespaceAndPunctuationTokenizer();
        $stemmer   = new PorterStemmer();
        $data      = $req->all();
        $ss        = $data['search_string'];
        $ss        = strtolower($ss);
        $ssArray   = $tokenizer->tokenize($ss);

        //Set all the flags to false by default
        $friend_flag = false;
        $type_flag   = false;
        $for_flag    = false;
        $level_flag  = false;
        $users_match = false;
        $location    = array();



        $tokens           = array();
        $tags_list1       = array();
        $for_set1         = array();
        $for_set2         = array();
        $for_set3         = array();
        $type_set1        = array();
        $type_set2        = array();
        $level_set1       = array();
        $level_set2       = array();
        $general_filters  = array();
        $tags_list        = array();
        $location_filters = array();

        $search = Search::all();
        foreach($search as $s){
            if($s->type=='for_set1'){
                $for_set1[] = $s->stemmed;
            }elseif($s->type=='for_set2'){
                $for_set2[] = $s->stemmed;
            }elseif($s->type=='for_set3'){
                $for_set3[] = $s->stemmed;
            }elseif($s->type=='type_set1'){
                $type_set1[] = $s->stemmed;
            }elseif($s->type=='type_set2'){
                $type_set2[] = $s->stemmed;
            }elseif($s->type=='level_set1'){
                $level_set1[] = $s->stemmed;
            }elseif($s->type=='level_set2'){
                $level_set2[] = $s->stemmed;
            }elseif($s->type=='general'){
                $general_filters[] = $s->stemmed;
            }elseif($s->type=='skill'){
                $tags_list1[] = $s->actual;
            }elseif($s->type=='location'){
                $location_filters[] = $s->stemmed;
            }
        }
        $for_entities   = array_merge($for_set1,$for_set3);
        $type_entities  = array_merge($type_set1,$type_set2);
        $level_entities = array_merge($level_set1,$level_set2);


        foreach($ssArray as $sa){
            $tokens[$i]['original'] = $sa;
            $tokens[$i]['stemmed'] = $stemmer->stem($sa);
            $i++;
        }

        //Get all the tags and store it in a list
        $tags = Tags::all();
        foreach($tags as $tag){
            $tags_list2[] = strtolower($tag->tag);
        }
        $general_tags = GeneralTags::all();
        foreach($general_tags as $tag){
            $tags_list3[] = strtolower($tag->tag);
        }
        $tags_list = array_merge($tags_list1,$tags_list2, $tags_list3);

        $tags_list = array_unique($tags_list);



        foreach($tokens as $key => $token){

            $tok      = $token['stemmed'];
            $tok_o    = $token['original'];
            $tok_prev = '';
            $tok_next = '';

            if(isset($tokens[$key-1])){
                $tok_prev = $tokens[$key-1]['original'];
            }
            if(isset($tokens[$key+1])){
                $tok_next = $tokens[$key+1]['original'];
            }

            if(in_array($tok, $general_filters)){
                continue;
            }

            //Check for skill entities
            if(in_array(strtolower($tok_o),$tags_list) || in_array(strtolower($tok_prev.' '.$tok_o),$tags_list) || in_array(strtolower($tok_o.' '.$tok_next),$tags_list)){
                $skills[] = $tok;
                continue;
            }else

            if(strlen($tok_o) > 2) {

                //Check for users
                $users = User::whereRaw("`name` LIKE '%$tok_o%'")->get();

                foreach ($users as $usr) {
                    $users_match[] = $usr->id;
                }
                if ($users_match) {
                    continue;
                }
            }else

            //check for 'For' entities
            if(in_array($tok,$for_entities) && $for_flag) {
            continue;
            }else

            if(in_array($tok,$for_entities)) {
                $for_flag = true;
                $tokens[$key]['type'] = 'for';
                if (in_array($tok, $for_set1)) {
                    $for[] = 'user';
                } elseif (in_array($tok, $for_set3)) {
                    $for[] = 'task';

                } 
                continue;

            } elseif (in_array($tok, $for_set2)) {
                $for[] = 'user';

                $friend_flag = true;
                $Query = "Select * from connections where `user1` = $uid or `user2` = $uid";
                $allConnections = DB::select(DB::raw($Query));
                foreach($allConnections as $con){
                    if($con->user1 == $uid){
                        $connections[] = $con->user2;
                    }else{
                        $connections[] = $con->user1;
                    }
                }
                continue;
            }else

            //check for 'Type' entities
            if(in_array($tok,$type_entities)){
                $type_flag = true;
                $tokens[$key]['type'] = 'type';
                if(in_array($tok,$type_set1)){
                    $type = 'skill';
                }elseif(in_array($tok,$type_set2)){

                    if($tok == 'help'){
                        if(isset($tokens[$key--])) {
                            //n-gram analyzer
                            if (in_array($tokens[$key--], ['need', 'want'])) {
                                $type = 'learn';
                            } else {
                                $type = 'skill';
                            }
                        }else{
                            $type = 'skill';
                        }
                    }else{
                        $type = 'learn';
                    }
                }
                continue;
            }else

            //check for 'Level' entities
            if(in_array($tok,$level_entities)){
                $level_flag = true;
                $tokens[$key]['type'] = 'level';
                if(in_array($tok,$level_set1)){
                    $level = $tok;
                }elseif(in_array($tok,$level_set2)){
                    $level = 'intermediate';
                }
                continue;
            }else

            //Location filters
            if(in_array($tok,$location_filters)){
                $location[] = $tok;
                continue;
            }else

            //Consider the remaining tags as Location and storing them in the database for learning purpose
            if(!in_array($tok,$general_filters)){
                $location[] = $tok;
                if(strlen($tok) > 2) {
                    if(!Search::where('stemmed',$tok)->first()) {
                        $new_tag          = new Search();
                        $new_tag->stemmed = $tok;
                        $new_tag->actual  = $tok_o;
                        $new_tag->save();
                    }
                }
            }

        }//End of token loop

        $join_for_query = '';
        $for_query = '';
        if(!$for){
            $for = ['user','task'];
        }
        if($for) {
            foreach ($for as $f) {
                $join_for_query .=  " LEFT JOIN `" . $f . "_tags` on `place`.`".$f."_id` = `" . $f . "_tags`.`" . $f . "_id` LEFT JOIN `" . $f . "s` on `place`.`".$f."_id` = `" . $f . "s`.`id` ";
            }
            $for_query = " AND `place`.`for` IN ('" . implode($for, "','") ."')";
        }
        $connection_query = '';
        if($friend_flag) {
            $connection_query = ' AND `users`.`id` IN (\'' . implode($connections, "','") . '\')';
        }

        if($users_match) {
            $connection_query = ' AND `users`.`id` IN (\'' . implode($users_match, "','") . '\')';
        }

        $level_value = 0;
        $level_query = '';
        if($level) {
            if($level == 'expert'){
                $level_value = 4;
            }elseif($level == 'intermediate'){
                $level_value = 3;
            }elseif($level == 'novice'){
                $level_value = 2;
            }elseif($level == 'beginner'){
                $level_value = 0;
            }
            foreach ($for as $f) {
                $level_query .= " OR `" . $f . "_tags`.`level` >= " . $level_value;
            }
        }

        $type_query = '';
        if($type) {
            foreach ($for as $f) {

                $type_query .= " AND `" . $f . "_tags`.`tag_type` = '" . $type . "'";
                if ($type == 'learn') {
                    if ($level_value == 4) {
                        $level_value = 3;
                    }
                } else {
                    if ($level_value == 0) {
                        $level_value = 2;
                    }
                }
                foreach ($for as $f) {
                    $level_query .= " OR `" . $f . "_tags`.`level` >= " . $level_value;
                }
            }
        }

        if($level_query){
            $level_query = "AND (".ltrim($level_query," OR").") ";
        }

        $skill_query  = '';
        $skill_query1 = '';
        $skill_query2 = '';

        if($skills) {
            $skill_query1 = 'AND (';
            foreach ($for as $f) {
                $skill_query2 .= ' `' . $f . '_tags`.`tag` IN (\'' . implode($skills, "','") . '\') OR';
            }
            $skill_query .= $skill_query1.rtrim($skill_query2,'OR').') ';
        }

        //Location query
        $location_query = '';
        $p = null;
        if(isset($location[0])) {
            $p = $this->helper->place(implode(' ', $location));
            if ($p) {
                $location_query = ',( 3959 * acos( cos( radians(' . $p['lat'] . " ) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(" . $p['lng'] . ") ) + sin( radians(" . $p['lat'] . ") ) * sin( radians( lat ) ) ) ) AS distance";
            }
        }

        if(!$p){
                $p = Place::Where(['for'=> 'user','for_id'=>$uid,'primary' => 1])->first();
                $location_query = ',( 3959 * acos( cos( radians('.$p->lat ." ) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(" . $p->lng . ") ) + sin( radians(" . $p->lat . ") ) * sin( radians( lat ) ) ) ) AS distance";
        }
        $location_distance = '';
        if($location_query){
            $location_distance = ' ORDER BY distance asc';
        }


        $u_for = false;
        $i_for = false;

        if(in_array('user',$for)){
            $u_for = "`users`.`id` as uID,";
        }
        if(in_array('task',$for)){
            $i_for ="`tasks`.`id` as iID,";
        }
        
        $all_for = rtrim($u_for.$i_for,',');

        $subQuery ="SELECT `place`.*,
        $all_for
        ".$location_query." FROM place" .
                    $join_for_query.'
                     WHERE 1 = 1 '.$skill_query.$level_query.$type_query. $for_query.$connection_query.(in_array('user',$for) ? ' AND `users`.`id` !='.$uid : '').'
                     LIMIT 0, 100';
        $Query = "SELECT * FROM (".$subQuery.") a GROUP BY user_id, task_id".$location_distance;


        $result = DB::select(DB::raw($Query));


        $processed_result = [];
        $temp_data;
        if ($result) {
            foreach ($result as $ts) {
                $temp_data = [];
                if($ts->for = 'user'){
                    $user = User::find($ts->uID);
                    if(!$user->image_name) $user->image_name = 'default.png';
                    $user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($user->image_name, 'thumbnail');
            
                    $temp_data['image_url']  = $user->image_url;
                    $temp_data['title']      = $user->name;
                    $temp_data['target_url'] = '/app/user/'.$user->username;
                } else{

                    $task = Task::find($ts->iID);
                    $user = User::find($task->user_id);
                    if(!$user->image_name) $user->image_name = 'default.png';
                    $user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($user->image_name, 'thumbnail');
            
                    $temp_data['image_url'] = $user->image_url;
                    $temp_data['title']     = $task->title;
                    $temp_data['target_url'] = '/app/task/'.$task->slug;

                }

                $processed_result[] = $temp_data;

            }
            return array('results' => $processed_result);

            ?>

            <?php  foreach ($result as $ts) {
                if($ts->for == 'user'){
                    if($ts->uID == $uid){
                        continue;
                    }

                    $user = User::find($ts->uID);
                    ?>
                    <a href="/user/<?= $user->username ?>" class="col-md-12 user_list">
                        <img src="<?= user_avatar($user->image_name) ?>" class="user_image">

                        <div class="content">
                            <span class="user_name"><?= $user->first_name . ' ' . $user->last_name ?></span>
                                <span class="skills"><?php  $skill = '';
                                    foreach ($user->skills() as $s) {
                                        $skill .= $s->tag . ', ';
                                    }
                                    echo rtrim($skill, ', '); ?></span>
                        </div>
                        <?php if(isset($ts->distance)){?><span class="distance"><?= round($ts->distance, 2) ?> miles</span><?php }?>
                    </a>

                <?php }elseif($ts->for == 'task'){
                $task = Task::find($ts->iID);
                    if(!$task) continue;
                ?>
                <a href="/task/<?= $task->slug ?>" class="col-md-12 user_list">
                    <img src="/img/support-icon.png" class="user_image">

                    <div class="content">
                        <span class="user_name"><?= $task->title ?></span>
                                <span class="skills"><?php  $skill = '';
                                    foreach ($task->tags() as $s) {
                                        $skill .= $s->tag . ', ';
                                    }

                                    echo rtrim($skill, ', '); ?></span>
                    </div>
                    <?php if(isset($ts->distance)){?><span class="distance"><?= round($ts->distance, 2) ?> miles</span><?php }?>
                </a>
                <?php }?>
            <?php }?>
        <?php } else {
            ?>
            No result found
        <?php 
        }
    }

}
