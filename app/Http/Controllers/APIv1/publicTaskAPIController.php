<?php namespace App\Http\Controllers\APIv1;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Place;
use Flash;
use App\TaskCommentHelpful;
use App\TaskComments;
use App\TaskCommentReply;
use App\TaskHandleRequest;
use Illuminate\Contracts\Auth\Guard;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Validator;
use App\UserTags;
use App\TaskTags;
use App\User;
use App\Task;
use App\Tags;
use DB;
use Mail;
use Geocoder;
use App\Notifications;
use NlpTools\Tokenizers\WhitespaceAndPunctuationTokenizer;
use App\GeneralTags;
use JWTAuth;
use Pusher;
use Response;
use Illuminate\Routing\Route;
use Tymon\JWTAuth\Exceptions\JWTException;

class publicTaskAPIController extends Controller {

	private $request,$helper,$uid,$auth;

    public function __construct(Guard $auth, Helper $helper, Route $route, Request $req){
        $this->auth   = $auth;
        $this->helper = $helper;
        $this->req    = $req;
        $token = JWTAuth::getToken();

        if($token){
            $this->uid = JWTAuth::parseToken()->authenticate()->id;
        } else {
            $this->uid= null;
        }

        
    }


    
    public function task_publicpage_data(){
        $data = $this->req->all();
    	
        $task = Task::findBySlug($data['slug']);
        $uid   = $this->uid;
        if(!$task){
            return '404';
        }
        $user                                    = $task->user;
        $user->place                             = $user->primary_place();
        if(!$user->image_name) $user->image_name = 'default.png';
        $user->image_url                         = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($user->image_name, 'thumbnail');

        $solved_by       = $task->solved_by_user;
        $skills          = $task->tags()->get();
        $comments        = $task->comments;


        foreach ($comments as $c) {
            $helpful_count                                 = 0;
            $current_user_set_helpful                      = false;
            if(!$c->user->image_name) $c->user->image_name = 'default.png';
            $c->user->image_url                            = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($c->user->image_name, 'thumbnail');
            $c->replies                                    = $c->replies()->get();

            foreach ($c->replies as $r) {
                $r->user                                       = $r->user()->first();
                if(!$r->user->image_name) $r->user->image_name = 'default.png';
                $r->user->image_url                            = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($r->user->image_name, 'thumbnail');
            }

            foreach($c->helpful as $ch){
                $helpful_count++;
                if($uid == $ch->user_id){
                    $current_user_set_helpful = true;
                }
            }
            $c->helpful_count            = $helpful_count;
            $c->current_user_set_helpful = $current_user_set_helpful;

        }

        $handle_requests = $task->handle_requests;

        foreach ($handle_requests as $hr) {
            if(!$hr->user->image_name) $hr->user->image_name = 'default.png';
            $hr->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($hr->user->image_name, 'thumbnail');
            // get the number of user comments
            $hr->user->task_comments = $hr->user->task_comments_count();

            // Get the user rating
            $ratings = $hr->user->rating();
            $rating_total = 0;
            $average_rating = 0;
            $current_user_reviewed = false;
            foreach ($ratings as $rating) {
                $rating_total = $rating_total + $rating->rating;
            }
            if(count($ratings) > 0){
                $average_rating = ROUND($rating_total / count($ratings), 1,1);
            }
            $hr->user->rating = $average_rating;

            // Handling tasks
            $hr->user->total_tasks_count = $hr->user->handling_tasks()->count();
            $hr->user->solved_tasks_count = $hr->user->user_resolved_tasks()->count();
            $hr->user->solved_percent = ($hr->user->solved_tasks_count / $hr->user->total_tasks_count) * 100;
        }

        if($task->assistance == 1){
            $task->assistance = true;
        } else{
            $task->assistance = false;
        }

        $handle_approved = false;
        $awating_approval = false;
        $handle_cancelled = false;
        if(!$handle_requests->isEmpty()){
            foreach($handle_requests as $hr){
                if($uid == $hr->user_id){
                    if($hr->status == null){
                        $awating_approval = true;
                    }elseif($hr->status == 1){
                        $handle_approved = true;
                    }elseif($hr->status > 1){
                        $handle_cancelled = true;
                    }
                }
            }
        }

        $place = $task->primary_place();

        $skill_string = '';
        foreach ($skills as $s) {
            $skill_string = $skill_string . "'". $s['tag'] . '\', ';
        }
        $skill_string= rtrim($skill_string,', ');
        // Get the similar tasks
        $similar_tasks_query = 'select `tasks`.`title`, `tasks`.`slug`, `tasks`.`id`, `place`.`formatted_address`, `place`.`task_id`, '. $this->helper->location_query($place->lat, $place->lng ) .
            ' JOIN `tasks` ON `place`.`task_id` = `tasks`.`id`'.
            ' LEFT JOIN `task_tags` on `place`.`task_id` = `task_tags`.`task_id`'.
            ' WHERE `tasks`.`deleted` != 1 AND '.( $skill_string ? '`task_tags`.`tag` IN (' . $skill_string . ') AND ' : '').' `place`.`for` = "task"'.
            ' AND `tasks`.`user_id` != '. $uid. ' AND `tasks`.`id` != '. $task->id.
            ' HAVING distance < 5'.
            ' LIMIT 0, 5';

        $Query = "SELECT * FROM (".$similar_tasks_query.") a GROUP BY task_id ORDER BY distance ASC ";

        $similar_task_result = DB::select(DB::raw($Query));
        $similar_tasks= [];

        foreach ($similar_task_result as $similar_task) {
            $dist = $similar_task->distance;
            $similar_task = Task::find($similar_task->task_id);

            $skill_set = '';
            foreach ($similar_task->tags()->get() as $s) {
                $skill_set .= $s->tag . ', ';
            }
            $similar_task->skill_set = rtrim($skill_set, ', ');
            if(!$similar_task->user->image_name) $similar_task->user->image_name = 'default.png';
            $similar_task->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($similar_task->user->image_name, 'thumbnail');

            $similar_task->image_url = $similar_task->user->image_url;
            
            $similar_task->address = place_min($similar_task->primary_place());
            $similar_task->distance = round($dist, 2);
            $similar_tasks[] = $similar_task;
        }
		$task_data = array(
			'task'            => $task,
			'user'            => $user,
			'uid'             => $uid,
			'skills'          => $skills,
			'comments'        => $comments,
			'handle_requests' => $handle_requests,
			'solved_by'       => $solved_by,
			'place'           => $place,
            'handle_approved'  => $handle_approved,
            'awating_approval' => $awating_approval,
            'handle_cancelled' => $handle_cancelled,
            'similar_tasks'    => $similar_tasks
		);

		return Response::json($task_data);
    }


    public function task_reply_to_comment(){

        $uid                       = $this->uid;
        $user                      = User::find($uid);
        $data                      = $this->req->all();
        $comment_id                = $data['comment_id'];
        $comment                   = TaskComments::find($comment_id);
        $task                      = Task::find($comment->task_id);
        $task_owner                = User::find($task->user_id);
        $comment_reply             = new TaskCommentReply;
        $comment_reply->comment    = $data['reply'];
        $comment_reply->user_id    = $uid;
        $comment_reply->comment_id = $comment_id;
        $comment->replies()->save($comment_reply);

        if($comment->user_id != $uid){
            //Add notification
            $not              = new Notifications();
            $not->user_id     = $comment->user_id;
            $not->target_task = $comment->task_id;
            $not->target_user = $user->id;
            $not->type        = 'task_comment_reply';
            $not->target_url  = '/task/' . $task->slug;
            $not->content     = '<strong>'.$user->name. "</strong> has replied to your comment on the task <strong>".$task->title."</strong>";
            $not->save();

            $this->helper->push_notification($not);

            $user->image_url = user_avatar($user->image_name, 'thumbnail');

            $comment_owner = User::find($comment->user_id);

            Mail::send('emails.task.new-reply', ['user' => $user, 'receiver' => $comment_owner, 'task' => $task, 'comment' => $comment], function ($message) use ($comment_owner) {
             $message->to($comment_owner->email, $comment_owner->name)
                 ->subject('Placerange - New Reply Received');
            });
        }

        // Get the task comments
        $comments        = $task->comments;

        foreach ($comments as $c) {
            if(!$c->user->image_name) $c->user->image_name = 'default.png';
            $c->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($c->user->image_name, 'thumbnail');
            $c->replies = $c->replies()->get();

            foreach ($c->replies as $r) {
                $r->user = $r->user()->first();
                if(!$r->user->image_name) $r->user->image_name = 'default.png';
                $r->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($r->user->image_name, 'thumbnail');
            }
        }


        $comments_data = array(
            "comments" => $comments
            );
        return Response::json($comments_data);
        
    }

    public function task_add_new_comment(){

        $data  = $this->req->all();
        $task_id = $data['task_id'];

        $uid        = $this->uid;
        $task       = Task::find($task_id);
        $task_owner = User::find($task->user_id);
        $user       = User::find($uid);
        
        $comment           = new TaskComments;
        $comment->comment  = $data['comment'];
        $comment->user_id  = $uid;
        $comment->task_id = $task_id;
        $task->comments()->save($comment);

        if($task->user_id != $uid){
            //Add notification
            $not              = new Notifications();
            $not->user_id     = $task->user_id;
            $not->target_user = $comment->user_id;
            $not->target_task = $comment->task_id;
            $not->type        = 'task_comment_new';
            $not->target_url  = '/task/' . $task->slug;
            $not->content     = '<strong>'.$user->name. "</strong> has commented on your task <strong>".$task->title."</strong>";
            $not->save();

            $this->helper->push_notification($not);
            
        }

        if($user->id != $task_owner->user_id){
            $user->image_url = user_avatar($user->image_name, 'thumbnail');
            Mail::send('emails.task.new-comment', ['sender' => $user, 'task_owner' => $task_owner, 'task' => $task, 'comment' => $comment], function ($message) use ($task_owner) {
             $message->to($task_owner->email, $task_owner->name)
                 ->subject('Placerange - New Comment Received');
            });
        }
        // Get the task comments
        $comments = $task->comments;

        foreach ($comments as $c) {
            if(!$c->user->image_name) $c->user->image_name = 'default.png';
            $c->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($c->user->image_name, 'thumbnail');
            $c->replies = $c->replies()->get();

            foreach ($c->replies as $r) {
                $r->user = $r->user()->first();
                if(!$r->user->image_name) $r->user->image_name = 'default.png';
                $r->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($r->user->image_name, 'thumbnail');
            }
        }


        $comments_data = array(
            "comments" => $comments
            );
        return Response::json($comments_data);
    }

    public function task_edit_reply(){

        $uid      = $this->uid;
        $data     = $this->req->all();
        $reply_id = $data['reply_id'];
        $reply    = TaskCommentReply::find($reply_id);
        $comment  = TaskComments::find($reply->comment_id);
        $task     = Task::find($comment->task_id);

        $reply->comment = $data['reply'];
        $reply->save();
        
        // Get the task comments
        $comments = $task->comments;

        foreach ($comments as $c) {
            if(!$c->user->image_name) $c->user->image_name = 'default.png';
            $c->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($c->user->image_name, 'thumbnail');
            $c->replies = $c->replies()->get();

            foreach ($c->replies as $r) {
                $r->user = $r->user()->first();
                if(!$r->user->image_name) $r->user->image_name = 'default.png';
                $r->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($r->user->image_name, 'thumbnail');
            }
        }

        $comments_data = array(
            "comments" => $comments
            );
        return Response::json($comments_data);
    }

    public function task_edit_comment(){

        $uid        = $this->uid;
        $data       = $this->req->all();
        $comment_id = $data['comment_id'];
        $comment    = TaskComments::find($comment_id);
        $task      = Task::find($comment->task_id);
        
        $comment->comment = $data['comment'];
        $comment->save();
        
        // Get the task comments
        $comments = $task->comments;

        foreach ($comments as $c) {
            if(!$c->user->image_name) $c->user->image_name = 'default.png';
            $c->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($c->user->image_name, 'thumbnail');
            $c->replies = $c->replies()->get();

            foreach ($c->replies as $r) {
                $r->user = $r->user()->first();
                if(!$r->user->image_name) $r->user->image_name = 'default.png';
                $r->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($r->user->image_name, 'thumbnail');
            }
        }

        $comments_data = array(
            "comments" => $comments
            );
        return Response::json($comments_data);
    }

    public function task_comment_helpful(){

        $uid        = $this->uid;
        $data       = $this->req->all();
        $comment_id = $data['comment_id'];
        $user       = User::find($uid);
        $comment    = TaskComments::find($comment_id);
        $task      = Task::find($comment->task_id);
        if($uid     == $comment->user_id){
            return 'same_user';
        }else{
            $helpful             = new TaskCommentHelpful();
            $helpful->user_id    = $uid;
            $helpful->comment_id = $comment_id;
            $helpful->save();

            //Add notification
            $not              = new Notifications();
            $not->user_id     = $comment->user_id;
            $not->target_task = $comment->task_id;
            $not->target_user = $user->id;
            $not->type        = 'task_comment_helpful';
            $not->target_url  = '/task/' . $task->slug;
            $not->content     = '<strong>'.$user->name. "</strong> has thanked you for your comment on the task <strong>".$task->title."</strong>";
            $not->save();

            $this->helper->push_notification($not);

            // Get the task comments
            $comments = $task->comments;

            foreach ($comments as $c) {
                $helpful_count                                 = 0;
                $current_user_set_helpful                      = false;
           
                if(!$c->user->image_name) $c->user->image_name = 'default.png';
                $c->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($c->user->image_name, 'thumbnail');
                $c->replies = $c->replies()->get();

                foreach ($c->replies as $r) {
                    $r->user = $r->user()->first();
                    if(!$r->user->image_name) $r->user->image_name = 'default.png';
                    $r->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($r->user->image_name, 'thumbnail');
                }
             
                foreach($c->helpful as $ch){
                    $helpful_count++;
                    if($uid == $ch->user_id){
                        $current_user_set_helpful = true;
                    }
                }
                $c->helpful_count            = $helpful_count;
                $c->current_user_set_helpful = $current_user_set_helpful;
            }

            $receiver = $comment->user;
            if($helpful->user_id != $comment->user_id){
                $user->image_url = user_avatar($user->image_name, 'thumbnail');
                Mail::send('emails.task.comment-helpful', ['sender' => $user, 'receiver' => $receiver, 'task' => $task, 'comment' => $comment], function ($message) use ($receiver) {
                 $message->to($receiver->email, $receiver->name)
                     ->subject('Placerange - Your comment was marked as helpful!');
                });
            }


            $comments_data = array(
                "comments" => $comments
                );
            return Response::json($comments_data);
        }

    }

    public function task_remove_comment(){

        //Current user ID
        $uid        = $this->uid;
        $data       = $this->req->all();
        $comment_id = $data['comment_id'];
        $wc         = TaskComments::find($comment_id);
        $task      = Task::find($wc->task_id);
        $wc->delete();
        
        // Get the task comments
        $comments = $task->comments;

        foreach ($comments as $c) {
            if(!$c->user->image_name) $c->user->image_name = 'default.png';
            $c->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($c->user->image_name, 'thumbnail');
            $c->replies = $c->replies()->get();

            foreach ($c->replies as $r) {
                $r->user = $r->user()->first();
                if(!$r->user->image_name) $r->user->image_name = 'default.png';
                $r->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($r->user->image_name, 'thumbnail');
            }
        }

        $comments_data = array(
            "comments" => $comments
            );
        return Response::json($comments_data);
    }

    public function task_remove_reply(){

        //Current user ID
        $uid      = $this->uid;
        $data     = $this->req->all();
        $reply_id = $data['reply_id'];
        $wc       = TaskCommentReply::find($reply_id);
        $ic       = TaskComments::find($wc->comment_id);
        $task    = Task::find($ic->task_id);
        $wc->delete();
        
        // Get the task comments
        $comments = $task->comments;

        foreach ($comments as $c) {
            if(!$c->user->image_name) $c->user->image_name = 'default.png';
            $c->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($c->user->image_name, 'thumbnail');
            $c->replies = $c->replies()->get();

            foreach ($c->replies as $r) {
                $r->user = $r->user()->first();
                if(!$r->user->image_name) $r->user->image_name = 'default.png';
                $r->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($r->user->image_name, 'thumbnail');
            }
        }

        $comments_data = array(
            "comments" => $comments
            );
        return Response::json($comments_data);
    }

    public function task_send_handle_request(){

        //Current user ID
        $uid       = $this->uid;
        $user      = User::find($uid);

        $data     = $this->req->all();
        $task_id = $data['task_id'];

        $task     = Task::find($task_id);
        $validator = Validator::make(
            array(
                'message' => $data['message'],
            ),
            array(
                'message' => 'required',
            )
        );

        if ($validator->fails())
        {
            return 'error';
        }else{
            $ia           = new TaskHandleRequest;
            $ia->task_id = $task->id;
            $ia->user_id  = $uid;
            $ia->message  = $data['message'];
            $ia->save();

            //Add notification
            $not              = new Notifications();
            $not->user_id     = $task->user_id;
            $not->target_task = $task_id;
            $not->target_user = $user->id;
            $not->type        = 'task_handle_received';
            $not->target_url  = '/task/' . $task->slug;
            $not->content     = '<strong>'.$user->name. "</strong> has sent a request to handle your task <strong>".$task->title."</strong>";
            $not->save();
            $this->helper->push_notification($not);

            $handle_requests  = $task->handle_requests;
            foreach ($handle_requests as $hr) {
                if(!$hr->user->image_name) $hr->user->image_name = 'default.png';
                $hr->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($hr->user->image_name, 'thumbnail');

            }
            $handle_approved  = false;
            $awating_approval = false;
            $handle_cancelled = false;
            if(!$handle_requests->isEmpty()){
                foreach($handle_requests as $hr){
                    if($uid == $hr->user_id){
                        if($hr->status == null){
                            $awating_approval = true;
                        }elseif($hr->status == 1){
                            $handle_approved = true;
                        }elseif($hr->status > 1){
                            $handle_cancelled = true;
                        }
                    }
                }
            }
            $receiver = User::find($task->user_id);
            $title    = 'You have received a new handle request!';
            $content  = 'Has send you a request to handle your Task "'. $task->title .'".';
            $user->image_url = user_avatar($user->image_name, 'thumbnail');
            Mail::send('emails.task.handle-request-received', ['sender' => $user, 'receiver' => $receiver, 'task' => $task, 'content' => $content, 'title' => $title], function ($message) use ($receiver) {
             $message->to($receiver->email, $receiver->name)
                 ->subject('Placerange - Handle Request Received');
            });

            $handle_data = array(
                "handle_requests" => $handle_requests,
                "awating_approval" => $awating_approval
            );
            return Response::json($handle_data);
        }

    }

    // Cancelling an handle
    public function task_cancel_handle(){

        //Current user ID
        $uid           = $this->uid;
        $user          = User::find($uid);
        $data          = $this->req->all();
        $task_id      = $data['task_id'];
        $cancel_reason = $data['cancel_reason'];
        $task         = Task::find($task_id);

        // GEt the handle request record and add the cancel reason
        $task_request = TaskHandleRequest::where(['user_id' => $uid, 'task_id' => $task_id])->first();
        $task_request->status = 4;
        $task_request->cancel_reason = $cancel_reason;
        $task_request->save();


        //Add notification
        $not              = new Notifications();
        $not->user_id     = $task->user_id;
        $not->target_task = $task_id;
        $not->target_user = $user->id;
        $not->type        = 'task_handle_awaiting_cancelled';
        $not->target_url  = '/task/' . $task->slug;
        $not->content     = '<strong>'.$user->name. "</strong> has cancelled the request to handle your task <strong>".$task->title."</strong>";
        $not->save();
        $this->helper->push_notification($not);

        $handle_requests = $task->handle_requests;

        $handle_data = array(
            "handle_requests" => $handle_requests
        );

        return Response::json($handle_data);

    }


    public function task_cancel_awaiting_handle_request(){

        //Current user ID
        $uid     = $this->uid;
        $user    = User::find($uid);
        $data    = $this->req->all();
        $task_id = $data['task_id'];
        $task    = Task::task_id($task_id);

        // GEt the handle request record and delete it
        $ia = TaskHandleRequest::where(['user_id' => $uid, 'task_id' => $task_id])->first();
        $ia->delete();

        //Add notification
        $not              = new Notifications();
        $not->user_id     = $task->user_id;
        $not->target_user = $user->id;
        $not->target_task = $task_id;
        $not->type        = 'task_handle_awaiting_cancelled';
        $not->target_url  = '/task/' . $task->slug;
        $not->content     = '<strong>'.$user->name. "</strong> has cancelled the request to handle your task <strong>".$task->title."</strong>";
        $not->save();
        $this->helper->push_notification($not);

        $handle_requests = $task->handle_requests;

        $handle_data = array(
            "handle_requests" => $handle_requests
        );
        return Response::json($handle_data);

    }

    public function task_accept_handle_request(){

        //Current user ID
        $uid           = $this->uid;
        $data          = $this->req->all();
        $handle_id     = $data['handle_id'];
        $task_request = TaskHandleRequest::find($handle_id);
        $task         = Task::find($task_request->task_id);

        if($uid != $task->user_id) return 'error';


        $task_request->status = 1;
        $task_request->save();

        //Add notification
        $not              = new Notifications();
        $not->user_id     = $task_request->user_id;
        $not->target_task = $task->id;
        $not->target_user = $uid;
        $not->type        = 'task_handle_approved';
        $not->target_url  = '/task/' . $task->slug;
        $not->content     = "Your handle request has been accepted for the task <strong>".$task->title."</strong>";
        $not->save();
        $this->helper->push_notification($not);

        $handle_requests = $task->handle_requests;
        foreach ($handle_requests as $hr) {
            if(!$hr->user->image_name) $hr->user->image_name = 'default.png';
            $hr->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($hr->user->image_name, 'thumbnail');
            // get the number of user comments
            $hr->user->task_comments = $hr->user->task_comments_count();

            // Get the user rating
            $ratings = $hr->user->rating();
            $rating_total = 0;
            $average_rating = 0;
            $current_user_reviewed = false;
            foreach ($ratings as $rating) {
                $rating_total = $rating_total + $rating->rating;
            }
            if(count($ratings) > 0){
                $average_rating = ROUND($rating_total / count($ratings), 1,1);
            }
            $hr->user->rating = $average_rating;

            // Handling tasks
            $hr->user->total_tasks_count = $hr->user->handling_tasks()->count();
            $hr->user->solved_tasks_count = $hr->user->user_resolved_tasks()->count();
            $hr->user->solved_percent = ($hr->user->solved_tasks_count / $hr->user->total_tasks_count) * 100;
        }

        $receiver = User::find($task_request->user_id);
        $user     = User::find($uid);
        $user->image_url = user_avatar($user->image_name, 'thumbnail');
        $content  = 'Has accepted your Handle request for the task "'. $task->title.'".';
        $title    = 'Your handle request has been accepted!';
        
        Mail::send('emails.task.handle-accepted', ['sender' => $user, 'receiver' => $receiver, 'task' => $task, 'content' => $content, 'title' => $title], function ($message) use ($receiver) {
         $message->to($receiver->email, $receiver->name)
             ->subject('Placerange - Handle Request Accepted');
        });


        $handle_data = array(
            "handle_requests" => $handle_requests
        );
        return Response::json($handle_data);
    }

    public function task_reject_handle_request(){

        //Current user ID
        $uid           = $this->uid;
        $data          = $this->req->all();
        $handle_id     = $data['handle_id'];
        $task_request = TaskHandleRequest::find($handle_id);
        $task         = Task::find($task_request->task_id);

        if($uid != $task_request->user_id) return 'error';

        $task_request->status = 2;
        $task_request->save();

        //Add notification
        $not              = new Notifications();
        $not->user_id     = $uid;
        $not->target_task = $task_id;
        $not->target_user = $task_request->user_id;
        $not->type        = 'task_handle_rejected';
        $not->target_url  = '/task/' . $task->slug;
        $not->content     = "Your handle request has been rejected for the task <strong>".$task->title."</strong>";
        $not->save();
        $this->helper->push_notification($not);
        
        $handle_requests = $task->handle_requests;
        foreach ($handle_requests as $hr) {
            if(!$hr->user->image_name) $hr->user->image_name = 'default.png';
            $hr->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($hr->user->image_name, 'thumbnail');
        }

        $handle_data = array(
            "handle_requests" => $handle_requests
        );
        return Response::json($handle_data);
    }

    // Owner cancelling the handle request
    public function task_owner_cancel_handle_request(){

        //Current user ID
        $uid           = $this->uid;
        $data          = $this->req->all();
        $handle_id     = $data['handle_id'];
        $task_request = TaskHandleRequest::find($handle_id);
        $task         = Task::find($task_request->task_id);

        if($uid != $task->user_id) return 'error';

        $task_request->status = 3;
        $task_request->save();

        //Add notification
        $not              = new Notifications();
        $not->user_id     = $uid;
        $not->target_task = $task_id;
        $not->target_user = $task_request->user_id;
        $not->type        = 'task_handle_cancelled';
        $not->target_url  = '/task/' . $task->slug;
        $not->content     = "Your handle has been canclled for the task <strong>".$task->title."</strong>";
        $not->save();
        $this->helper->push_notification($not);
        
        $handle_requests = $task->handle_requests;
        foreach ($handle_requests as $hr) {
            if(!$hr->user->image_name) $hr->user->image_name = 'default.png';
            $hr->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($hr->user->image_name, 'thumbnail');
        }
        
        $handle_data = array(
            "handle_requests" => $handle_requests
        );
        return Response::json($handle_data);
    }


}
