<?php namespace App\Http\Controllers\APIv1;

use App\Place;
use App\Http\Controllers\Controller;
use App\Tags;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Laracasts\Flash\Flash;
use Validator;
use App\User;
use App\Task;
use Geocoder;
use DB;
use App\Notifications;
use App\GeneralTags;
use NlpTools\Tokenizers\WhitespaceAndPunctuationTokenizer;
use JWTAuth;
use Response;
use Illuminate\Routing\Route;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\ConnectionRequests;

class homeAPIController extends Controller {

	private $request,$helper,$uid,$auth;

    public function __construct(Guard $auth, Helper $helper, Route $route, Request $req){
        $this->auth   = $auth;
        $this->helper = $helper;
        $this->req    = $req;
        $token = JWTAuth::getToken();

        if($token){
            $this->uid = JWTAuth::parseToken()->authenticate()->id;
        } else {
            $this->uid= null;
        }

        
    }

    // Dashboard Search
    function dashboard_search(){

        $data         = $this->req->all();
        $distance     = $data['distance'];
        $place        = $this->helper->place($data['place']);
        $current_date = date('Y-m-d G:i:s');

        $tasks = [];
        $Query = "SELECT `users`.name, `users`.username, `users`.image_name,`place`.*,`tasks`.title,`tasks`.slug,`tasks`.created_at, `tasks`.updated_at, `tasks`.`id` as wID,".
                        $this->helper->location_query($place['lat'], $place['lng']). '
                         JOIN `tasks` on `place`.`for_id` = `tasks`.`id`
                         JOIN `users` on `tasks`.`user_id` = `users`.`id`
                         WHERE `tasks`.`deleted` != 1 AND `place`.`for` = "task" AND `tasks`.`title` LIKE "%' . $data['search_text'] . '%" AND `tasks`.`user_id` != ' . $this->uid. '
                         HAVING distance < ' . $distance . "
                         LIMIT 0, 20 ";
            $Query = "SELECT * FROM (" . $Query . ") a GROUP BY task_id";

            $nearbytasks = DB::select(DB::raw($Query));

            foreach ($nearbytasks as $r) {
                $task = Task::find($r->task_id);
                $task_data['title']     = $task->title;
                $task_data['slug']      = $task->slug;
                $task_data['summary']   = $task->summary;
                $task_data['date']      = $task->created_at;
                $task_data['skills']    = $task->tags()->get()->toArray();
                $task_data['comments']  = $task->comments->toArray(); 
                $task_data['days_past'] = 'Raised ' . ((isset($task->created_at)) ? abs(floor((strtotime($task->created_at) - time())/60/60/24)) : 0) . ' days ago.' ;
                $tasks[] = $task_data;
            }


            //tasks created near current user
            $Query = "SELECT `place`.user_id AS 'user_idss', `users`.name, `users`.username, `users`.image_name, `users`.created_at,`place`.*," . 
            $this->helper->location_query($place['lat'], $place['lng']) . '
                 JOIN `users` on `place`.`user_id` = `users`.`id`
                 WHERE `users`.id != ' .$this->uid. '
                 HAVING distance < ' . $distance . "
                 ORDER BY `users`.`id` DESC
                 LIMIT 0, 40";
            $Query = "SELECT * FROM (" . $Query . ") a GROUP BY user_id";

            $newUsers = DB::select(DB::raw($Query));
            $users = [];
            foreach ($newUsers as $wa) {

                if(!$wa->image_name) $wa->image_name = 'default.png';
                $wa->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($wa->image_name, 'thumbnail');

                $feed_data['image']    = $wa->image_url;
                $feed_data['name']     = $wa->name;
                $feed_data['username'] = $wa->username;
                $feed_data['title']    = $wa->name;
                $feed_data['slug']     = '#/user/' . $wa->username;

                
                $feed_data['thumbnail_url'] = $wa->image_url;

                $this_user = User::find($wa->user_idss);
                //User skills
                $feed_data['skills']   = $this_user->skills()->toArray();
                $feed_data['learns']    = $this_user->learn()->toArray();
                $feed_data['distance'] = round($wa->distance, 2);
                $feed_data['date']     = date("d M - h:i A",strtotime($wa->created_at));
                $feed_data['top_text'] = '<a ng-href="'.$feed_data['slug'].'">'.$wa->name . '</a> has joined Placerange from ' . ($wa->locality ? $wa->locality : $wa->country). '.';
                $users[]               = $feed_data;
            }

        $search_data =  array(
            'users' => $users,
            'tasks' => $tasks
        );

        return Response::json($search_data);

    }

    public function user_home()
    {

        //Current user ID
        $uid           = $this->uid;
        //User details
        $user          = User::find($uid);
        //User address details
        $p             = $user->primary_place();

        $connections   = get_user_connections($uid);
        $connections_ids = [];
        foreach ($connections as $friend) {
            $connections_ids[] = $friend->id;
        }

        $connections_ids[] = $uid;

		// Nearby members
        $distance = 2;
        do {
            $result = '';
            $Query =
                "SELECT `place`.*, `user_tags`.`tag`,`users`.`id` as uID," .$this->helper->location_query($p->lat, $p->lng) . '
					 LEFT JOIN `user_tags` on `place`.`for_id` = `user_tags`.`user_id`
					 LEFT JOIN `users` on `place`.`for_id` = `users`.`id`
					 WHERE `for` = "user" AND `place`.`user_id` NOT IN (\'' . implode($connections_ids, "','") . '\')
					 HAVING distance < ' . $distance . '
					 Order BY distance ASC
					 LIMIT 0, 10';
            $Query = "SELECT * FROM (".$Query.") a GROUP BY for_id";

            $result = DB::select(DB::raw($Query));
            $distance = $distance + 10;
        } while (!$result);

        $members_nearby = array();

        foreach($result as $mn){
            $skill = '';
            $member = User::find($mn->uID);
            if(!$member->image_name) $member->image_name = 'default.png';
            $member->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($member->image_name, 'icon');

            foreach ($member->skills() as $s) {
                $skill .= $s->tag . ', ';
            }
            $member->skill_set = rtrim($skill, ', ');
            $members_nearby[] = $member;
        }
        //Notifications
        $notifications = $user->notifications->take(5);

        if(!$user->image_name) $user->image_name = 'default.png';
        $user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($user->image_name, 'icon');
        //User address details
        $places                 = $user->places();
        foreach ($places as $p) {
            $p->address = place_min($p);
        }
        foreach($notifications as $n){
            if(!$n->read){
                $n->read = 1;
                $n->save();
            }
        }

        // Current user skills
        $skills             = $user->skills();

        $user_data =  array(
            'user'           => $user,
            'uid'            => $uid,
            'places'         => $places,
            'notifications'  => $notifications,
            'members_nearby' => $members_nearby,
            'skills'         => $skills
      
        );

        return Response::json($user_data);
    }


    // Get the user Homepage data
    function user_homepage_data(){
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
        //Current user ID
        $uid = $this->uid;

        //User details
        $user = User::find($uid);
        // Available to meet
        if($user->available_to_meet == 1){
            $user->available_to_meet = true;
        }else{
            $user->available_to_meet = false;
        }

        // Newsletter
        if($user->newsletter == 1){
            $user->newsletter = true;
        }else{
            $user->newsletter = false;
        }
        if(!$user->image_name) $user->image_name = 'default.png';
        $user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($user->image_name, 'medium');
        $user->original_image = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($user->image_name, 'original');
        $distance = 5;
        $place = $user->primary_place();
        $current_date = date('Y-m-d G:i:s');

        $tasks = [];
        $Query = "SELECT `users`.name, `users`.username, `users`.image_name,`place`.*,`tasks`.title,`tasks`.slug,`tasks`.created_at, `tasks`.updated_at, `tasks`.`id` as wID,".
                        $this->helper->location_query($place->lat, $place->lng). '
                         JOIN `tasks` on `place`.`for_id` = `tasks`.`id`
                         JOIN `users` on `tasks`.`user_id` = `users`.`id`
                         WHERE `tasks`.`deleted` != 1 AND `place`.`for` = "task" AND `tasks`.`user_id` != ' . $uid . " 
                         HAVING distance < " . $distance . "
                         LIMIT 0, 20";
            $Query = "SELECT * FROM (" . $Query . ") a GROUP BY task_id";

            $nearbytasks = DB::select(DB::raw($Query));

            foreach ($nearbytasks as $r) {
                $task = Task::find($r->task_id);
                $task_data['title']     = $task->title;
                $task_data['slug']      = $task->slug;
                $task_data['summary']   = $task->summary;
                $task_data['date']      = $task->created_at;
                $task_data['skills']    = $task->tags()->get()->toArray();
                $task_data['comments']  = $task->comments->toArray(); 
                $task_data['days_past'] = 'Raised ' . ((isset($task->created_at)) ? abs(floor((strtotime($task->created_at) - time())/60/60/24)) : 0) . ' days ago.' ;
                $tasks[] = $task_data;
            }

        //User address details
        $places                 = $user->places();
        foreach ($places as $p) {
            $p->address = place_min($p);
        }
        //$handling_tasks      = taskHandleRequest::Where('user_id',$uid)->Where('status',1)->get();
        $handling_tasks        = Task::whereHas('handle_requests', function($q) use($uid)
        {
            $q->where('user_id', $uid)->where('status',1);

        })->get();

        //User skills
        $skills             = $user->skills();
        $learn              = $user->learn();
        $teach              = $user->teach();
        $connectionRequests = ConnectionRequests::where(['to_user'=> $uid, 'status'=>1])->get();

        //User unresolved tasks
        $my_tasks          = $user->user_unresolved_tasks();

        //User resolved tasks
        $resolved_tasks    = $user->user_resolved_tasks();

        $teach_suggestions = [];

        $connections = get_user_connections($uid);
        $connections_ids = [];
        foreach ($connections as $friend) {
            if(!$friend->image_name) $friend->image_name = 'default.png';
            $friend->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($friend->image_name, 'medium');
            $connections_ids[] = $friend->id;
        }

        $connections_ids[] = $uid;

        // GEt the awating connection requests to this user
        $connection_requests = get_user_awating_connections($uid);

        // Get the user rating
        $ratings = $user->rating();
        $rating_total = 0;
        $average_rating = 0;
        foreach ($ratings as $rating) {
            $rating_total = $rating_total + $rating->rating;
            $rating->from_user = User::find($rating->from_user);
            if(!$rating->from_user->image_name) $rating->from_user->image_name = 'default.png';
            $rating->from_user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($rating->from_user->image_name, 'medium');
        }
        if(count($ratings) > 0){
            $average_rating = ROUND($rating_total / count($ratings), 1,1);
        }

        // Nearby members
        $distance = 2;
        do {
            $result = '';
            $Query =
                "SELECT `place`.*, `user_tags`.`tag`,`users`.`id` as uID," .$this->helper->location_query($p->lat, $p->lng) . '
                     LEFT JOIN `user_tags` on `place`.`for_id` = `user_tags`.`user_id`
                     LEFT JOIN `users` on `place`.`for_id` = `users`.`id`
                     WHERE `for` = "user" AND `place`.`user_id` NOT IN (\'' . implode($connections_ids, "','") . '\')
                     HAVING distance < ' . $distance . '
                     Order BY distance ASC
                     LIMIT 0, 10';
            $Query = "SELECT * FROM (".$Query.") a GROUP BY for_id";

            $result = DB::select(DB::raw($Query));
            $distance = $distance + 10;
        } while (!$result);

        $members_nearby = array();

        foreach($result as $mn){
            $skill = '';
            $member = User::find($mn->uID);
            if(!$member->image_name) $member->image_name = 'default.png';
            $member->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($member->image_name, 'icon');

            foreach ($member->skills() as $s) {
                $skill .= $s->tag . ', ';
            }
            $member->skill_set = rtrim($skill, ', ');
            $members_nearby[] = $member;
        }

        $tasks_nearby = $this->helper->get_nearby_tasks( $uid );

        //  Get the user languages
        $languages = $user->languages();
        $user_homepage_data =  array(
            'user'                => $user,
            'uid'                 => $uid,
            'places'              => $places,
            'primary_place'       => $place,
            'skills'              => $skills,
            'learns'              => $learn,
            'teach'               => $teach,
            'teach_suggestions'   => $teach_suggestions,
            'my_tasks'            => $my_tasks,
            'resolved_tasks'      => $resolved_tasks,
            'handling_tasks'      => $handling_tasks,
            'connectionRequests'  => $connectionRequests,
            'tasks'               => $tasks,
            'connections'         => $connections,
            'ratings'             => $ratings,
            'average_rating'      => $average_rating,
            'languages'           => $languages,
            'members_nearby'      => $members_nearby,
            'tasks_nearby'        => $tasks_nearby,
            'connection_requests' => $connection_requests
        );


        return Response::json($user_homepage_data);
    }


    public function user_wall_data()
    {
        $data            = $this->req->all();
        $date            = $data['start_date'];
        //Current user ID
        $uid             = $this->uid;
        //User details
        $user            = User::find($uid);
        $connections     = $this->helper->connections();
        $place           = $user->primary_place();
        $feeds           = array();
        $distance        = 5;
        $no_feed         = false;
        $iteration_count = 0;
        if ($date        == '') {
            $current_date = date('Y-m-d G:i:s');
        } else {
            $current_date = date('Y-m-d G:i:s', $date);
        }

        do{

            $oneDayBack = date('Y-m-d G:i:s', (strtotime($current_date) - ( 86400 * 5 )));

            $task_near_ids = array();

            //tasks created by friends of current user
            $connectiontasks = DB::table('tasks')
                ->leftJoin('users', 'tasks.user_id', '=', 'users.id')
                ->select('users.*', 'tasks.*')
                ->whereRaw("`tasks`.`deleted` != 1 AND `tasks`.`user_id` IN (' " . implode($connections, "','") . " ') AND (`tasks`.`updated_at` BETWEEN '$oneDayBack' AND '$current_date') ")
                ->get();
            foreach ($connectiontasks as $wa) {
                $feed_data['root_type']     = 'task';
                $feed_data['type']           = 'task_user_connections';
                if(!$wa->image_name) $wa->image_name = 'default.png';
                $wa->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($wa->image_name, 'icon');
                $feed_data['user_image']     = $wa->image_url;
                $feed_data['user_full_name'] = $wa->name;
                $feed_data['username']       = $wa->username;
                $feed_data['title']          = $wa->title;
                $feed_data['summary']        = $wa->summary;
                $feed_data['slug']           = '#/task/' . $wa->slug;
                $feed_data['thumbnail_url']  = "img/menu/SuperAticLABS.png";
                $feed_data['distance']       = '';
                $feed_data['date']           = date("d M - h:i A",strtotime($wa->updated_at));
                $feed_data['top_text']       = '<a href="#/app/user/' . $wa->username . '">' . $feed_data['user_full_name'] . '</a> has raised an task.';
                
                $this_task = Task::find($wa->id);
                // task skills
                $skills = $this_task->tags()->get()->toArray();
                $skill_learn = '';

                foreach ($skills as $s) {
                    $skill_learn = $skill_learn . $s['tag'] . ', ';
                }
                $feed_data['skills-learn'] = rtrim($skill_learn,', ');


                //To avoid duplicates
                $task_near_ids[] = $wa->id;
                $feeds[] = $feed_data;
            }

            //tasks created near current user
            $Query = "SELECT `users`.name, `users`.username, `users`.image_name,`place`.*,`tasks`.title, `tasks`.summary,`tasks`.slug,`tasks`.created_at, `tasks`.updated_at, `tasks`.`id` as wID,".
                         $this->helper->location_query($place->lat, $place->lng) . '
                         JOIN `tasks` on `place`.`for_id` = `tasks`.`id`
                         JOIN `users` on `tasks`.`user_id` = `users`.`id`
                         WHERE `tasks`.`deleted` != 1 AND `place`.`for` = "task" AND `tasks`.`id` NOT IN (\' ' . implode($task_near_ids, "','") . ' \') AND `tasks`.`user_id` != ' . $uid . " AND  (`tasks`.`updated_at` BETWEEN '$oneDayBack' AND '$current_date')
                         HAVING distance < " . $distance . "
                         LIMIT 0, 20";
            $Query = "SELECT * FROM (" . $Query . ") a GROUP BY task_id";

            $nearbytask = DB::select(DB::raw($Query));
            foreach ($nearbytask as $wa) {
            	$feed_data['root_type'] = 'task';
                $feed_data['type']           = 'tasks_near_user';
                if(!$wa->image_name) $wa->image_name = 'default.png';
                $wa->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($wa->image_name, 'icon');
                $feed_data['user_image']     = $wa->image_url;
                $feed_data['user_full_name'] = $wa->name;
                $feed_data['username']       = $wa->username;
                $feed_data['title']          = $wa->title;
                $feed_data['summary']        = $wa->summary;
                $feed_data['slug']          = '#/app/task/' . $wa->slug;
                $feed_data['thumbnail_url'] = "img/menu/SuperAticLABS.png";
                $feed_data['distance']      = round($wa->distance, 2);
                $feed_data['date']          = date("d M - h:i A",strtotime($wa->updated_at));
                $feed_data['top_text']      = '<a href="#/app/user/' . $wa->username . '">' . $feed_data['user_full_name'] . '</a> has raised an task near ' . place_min($place, true). '.';
                
                $this_task = Task::find($wa->wID);
                // task skills
                $skills      = $this_task->tags()->get()->toArray();
                $skill_learn = '';

                foreach ($skills as $s) {
                    $skill_learn = $skill_learn . $s['tag'] . ', ';
                }
                $feed_data['skills-learn'] = rtrim($skill_learn,', ');

                $feeds[] = $feed_data;
            }

      
            $distance     = $distance + 10;
            $current_date = $oneDayBack;
            $iteration_count++;
            if($iteration_count == 10 && !$feeds){
                $no_feed  = true;
                $distance = 5000;
                // Users near current user
                $Query = "SELECT `place`.user_id AS 'user_idss', `users`.name, `users`.username, `users`.image_name, `users`.created_at,`place`.*," . 
                $this->helper->location_query($place->lat, $place->lng) . '
                     JOIN `users` on `place`.`user_id` = `users`.`id`
                     HAVING distance < ' . $distance . "
                     ORDER BY `users`.`id` DESC
                     LIMIT 0, 40";
                $Query = "SELECT * FROM (" . $Query . ") a GROUP BY user_id";

                $newUsers = DB::select(DB::raw($Query));

                foreach ($newUsers as $wa) {
                    if($wa->user_id == $uid){
                        continue;
                    }
                    $feed_data['root_type']           = 'user';
                    $feed_data['type']           = 'new_users';
                    if(!$wa->image_name) $wa->image_name = 'default.png';
			        $wa->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($wa->image_name, 'thumbnail');

			        $feed_data['user_image']     = $wa->image_url;
                    $feed_data['user_full_name'] = $wa->name;
                    $feed_data['username']       = $wa->username;
                    $feed_data['title']          = $wa->name;
                    $feed_data['slug']           = '#/app/user/' . $wa->username;

                    
                    $feed_data['thumbnail_url'] = $wa->image_url;

                    $this_user = User::find($wa->user_idss);
                    //User skills
                    $skills      = $this_user->skills()->toArray();
                    $learn       = $this_user->learn();
                    $skill_learn = '';
                    $skill_teach = '';
                    foreach ($skills as $s) {
                        $skill_teach = $skill_teach . $s['tag'] . ', ';
                    }

                    foreach ($learn as $s) {
                        $skill_learn = $skill_learn . $s['tag'] . ', ';
                    }

                    $feed_data['skills-learn'] = rtrim($skill_learn,', ');
                    $feed_data['skills-teach'] = rtrim($skill_teach,', ');
                    $feed_data['distance']     = round($wa->distance, 2);
                    $feed_data['date']         = date("d M - h:i A",strtotime($wa->created_at));
                    $feed_data['top_text']     = '<a href="'.$feed_data['slug'].'">'.$wa->name . '</a> has joined Placerange from ' . ($wa->locality ? $wa->locality : $wa->country). '.';
                    $feeds[]                   = $feed_data;
                }

            }


        } while(count($feeds) < 10 && $iteration_count < 300);

        // Sort the feed data with respect to the Date
        // The most recent post comes first
        usort($feeds, function ($a, $b) {
            $t1 = strtotime($a['date']);
            $t2 = strtotime($b['date']);
            return ($t1 < $t2) ? 1 : -1;

        });

        $wall_data =  array(
            'start_date'     => strtotime($current_date),
            'no_feed'        => $no_feed,
            'feeds'          => $feeds
      
        );

        return Response::json($wall_data);

    }

}
