<?php namespace App\Http\Controllers;


use App\ConnectionRequests;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Issue;
use App\Workshop;
use OAuth;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use Illuminate\Auth\Guard;
use App\User;
use Mail;
use DB;
use App\Newsletter;
use Response;

class EmailController extends Controller {

    private $auth, $helper, $req, $uid;

    public function __construct(Guard $auth, Helper $helper, Request $req)
    {
        $this->auth   = $auth;
        $this->helper = $helper;
        $this->req    = $req;
        $this->uid    = $this->helper->user_logged_in();
    }

    public function weekly()
    {
        //Current user ID
        $uid          = $this->uid;
        $current_user = User::find($uid);
        $current_date = date('Y-m-d G:i:s');
        $oneWeekBack  = date('Y-m-d G:i:s', (strtotime($current_date) - (86400*7)));
        $i            = 0;
        //User details
        $users        = User::all();

        foreach($users as $user) {
            $users     = array();
            $issues    = array();
            $workshops = array();
            $i++;
            if($i == 5){
                break;
            }
            $p                 = $user->primary_place();
            $connections       = array();
            $connections_query = "Select * from connections where `user1` = $uid or `user2` = $uid";
            $allConnections    = DB::select(DB::raw($connections_query));
            foreach ($allConnections as $con) {
                if ($con->user1 == $uid) {
                    $connections[] = $con->user2;
                } else {
                    $connections[] = $con->user1;
                }
                }
                $connections[] = $uid;


            //Recently joined users and not in connections with skills
            $result = '';
            $usersQuery =
                "SELECT `place`.*, ( 3959 * acos( cos( radians(" . $p->lat . ") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(" . $p->lng . ") ) + sin( radians(" . $p->lat . ") ) * sin( radians( lat ) ) ) ) AS distance FROM place" . '
					 LEFT JOIN `users` on `place`.`user_id` = `users`.`id`
					 WHERE  `for` = "user" AND `place`.`user_id` NOT IN (\'' . implode($connections, "','") . '\')'." AND  (`users`.`created_at` BETWEEN '$oneWeekBack' AND '$current_date')
					 HAVING distance < 5
					 Order BY distance ASC
					 LIMIT 0, 10";
            $usersQuery1 = "SELECT * FROM (" . $usersQuery . ") a GROUP BY user_id";
            $usersResult = DB::select(DB::raw($usersQuery1));
            foreach($usersResult as $us){
                $users[] = User::find($us->user_id);
            }

            //Issues created nearby

            $issuesQuery =
                "SELECT `place`.*, ( 3959 * acos( cos( radians(" . $p->lat . ") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(" . $p->lng . ") ) + sin( radians(" . $p->lat . ") ) * sin( radians( lat ) ) ) ) AS distance FROM place" . '
					 LEFT JOIN `issues` on `place`.`issue_id` = `issues`.`id`
					 LEFT JOIN `users` on `issues`.`user_id` = `users`.`id`
					 WHERE `place`.`for` = "issue"'." AND  (`issues`.`created_at` BETWEEN '$oneWeekBack' AND '$current_date')
					 HAVING distance < 5
					 Order BY distance ASC
					 LIMIT 0, 10";
            $issuesQuery1 = "SELECT * FROM (" . $issuesQuery . ") a GROUP BY issue_id";
            $issuesResult = DB::select(DB::raw($issuesQuery1));
            foreach($issuesResult as $is){
                $issues[] = Issue::find($is->id);
            }



            Mail::send('emails.weekly-newsletter', compact('users', 'issues', 'uid', 'user'), function ($message) use ($user) {
                $message->to('placerange@gmail.com', $user->name)
                    ->subject('UASDF - Weekly');
            });
            echo 'Sent to '.$user->name.'<br>';
        }


        //If no issues, psot your issues
        //If no workshop, Organize a workshop

    }

    public function update_subscription(){
        $data              = $this->req->all();
        $token             = $data['token'];
        $email_id          = decrypt($token, 'AllowtheminPR1');
        $user              = User::where('email',$email_id)->first();
        $user_subscribtion = Newsletter::find($user->id);

        $user_subscribtion->weekly_newsletter   = $data['weekly_newsletter'];
        $user_subscribtion->new_message         = $data['new_message'];
        $user_subscribtion->task_comment        = $data['task_comment'];
        $user_subscribtion->task_handle_request = $data['task_handle_request'];
        $user_subscribtion->profile_connection  = $data['profile_connection'];
        $user_subscribtion->profile_rating      = $data['profile_rating'];
        $user_subscribtion->save();
        return 'success';

    }

    
    public function get_subscribtion(){
        
        $data     = $this->req->all();
        $token    = $data['token'];
        $email_id = decrypt($token, 'AllowtheminPR1');
        $user     = User::where('email',$email_id)->first();

        $user_subscribtion = $user->newsletter();

        return Response::json($user_subscribtion);


    }

}
