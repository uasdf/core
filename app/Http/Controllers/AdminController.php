<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Search;
use App\Messages;
use App\MessageThreads;
use App\User;
use Auth;
use Illuminate\Contracts\Auth\Guard;
use App\Helpers\Helper;
use Laracasts\Flash\Flash;
use PhpSpec\Exception\Exception;
use Validator;
use App\UserTags;
use App\Issue;
use Geocoder;
use App\Place;
use App\Tags;
use DB;
use App\ConnectionRequests;
use Mail;
use DateTime;

class AdminController extends Controller {
	private $auth, $helper, $req, $uid;

	public function __construct(Request $req,Helper $helper)
	{
		$this->req    = $req;
		$this->helper = $helper;

		if(Auth::user()->role != 'admin'){
			return redirect('/')->send();
		}
		$this->uid= $this->helper->user_logged_in();
	}

	public function index()
	{
		$user 	   = User::find($this->uid);
		//Total users
		$users     = User::all()->toArray();
		//Total issues
		$issues    = Issue::all()->toArray();


		$date = date("Y-m-d 00:10",strtotime('tomorrow -1 month'));


		$users_days = User::select(array(
			DB::raw('DATE(`created_at`) as `date`'),
			DB::raw('COUNT(*) as `count`')
		))
			->where('created_at', '>', $date)
			->groupBy('date')
			->orderBy('date', 'DESC')
			->get();

		$issues_days = Issue::select(array(
			DB::raw('DATE(`created_at`) as `date`'),
			DB::raw('COUNT(*) as `count`')
		))
			->where('created_at', '>', $date)
			->groupBy('date')
			->orderBy('date', 'DESC')
			->get();


		$start_date = date("Y-m-d",strtotime('tomorrow'));
		for($i=0; $i < 30; $i ++){

			foreach ($users_days as $ud) {
				if($start_date == $ud['date'])
					$count[$start_date]['users'] = $ud['count'];
			}

			foreach ($issues_days as $id) {
				if($start_date == $id['date'])
					$count[$start_date]['issues'] = $id['count'];
			}


			if(!isset($count[$start_date]['users'])){
				$count[$start_date]['users'] = 0;
			}

			if(!isset($count[$start_date]['issues'])){
				$count[$start_date]['issues'] = 0;
			}


		$start_date = date("Y-m-d",strtotime('tomorrow -'.$i.' day'));

		}

		$new_users = User::orderBy('created_at','desc')->take(10)->get();

		return view('admin.index', compact(
			'user',
			'users',
			'issues',
			'count',
			'new_users'
		));
	}

	public function tags()
	{
		$user 	   = User::find($this->uid);
		//Total users
		$users     = User::all()->toArray();
		//Total issues
		$issues    = Issue::all()->toArray();


		$date = date("Y-m-d 00:10",strtotime('tomorrow -1 month'));


		$users_days = User::select(array(
			DB::raw('DATE(`created_at`) as `date`'),
			DB::raw('COUNT(*) as `count`')
		))
			->where('created_at', '>', $date)
			->groupBy('date')
			->orderBy('date', 'DESC')
			->get();

		$issues_days = Issue::select(array(
			DB::raw('DATE(`created_at`) as `date`'),
			DB::raw('COUNT(*) as `count`')
		))
			->where('created_at', '>', $date)
			->groupBy('date')
			->orderBy('date', 'DESC')
			->get();

		$start_date = date("Y-m-d",strtotime('tomorrow'));
		for($i=0; $i < 30; $i ++){

			foreach ($users_days as $ud) {
				if($start_date == $ud['date'])
					$count[$start_date]['users'] = $ud['count'];
			}

			foreach ($issues_days as $id) {
				if($start_date == $id['date'])
					$count[$start_date]['issues'] = $id['count'];
			}

			if(!isset($count[$start_date]['users'])){
				$count[$start_date]['users'] = 0;
			}

			if(!isset($count[$start_date]['issues'])){
				$count[$start_date]['issues'] = 0;
			}

		$start_date = date("Y-m-d",strtotime('tomorrow -'.$i.' day'));

		}

		$new_users = User::orderBy('created_at','desc')->take(10)->get();

		return view('admin.blank-page', compact(
			'user',
			'users',
			'issues',
			'count',
			'new_users'
		));
	}

}
