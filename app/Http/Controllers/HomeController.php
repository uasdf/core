<?php namespace App\Http\Controllers;
use App\IssueHandleRequest;
use App\Messages;
use App\MessageThreads;
use App\User;
use App\WorkshopAttendees;
use Illuminate\Contracts\Auth\Guard;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use PhpSpec\Exception\Exception;
use Validator;
use App\UserTags;
use App\Issue;
use Geocoder;
use App\Place;
use App\Workshop;
use App\Tags;
use App\Ui_introduction;
use DB;
use App\ConnectionRequests;
use Mail;

class HomeController extends Controller {

	private $auth, $helper, $req, $uid;

	public function __construct(Guard $auth, Helper $helper, Request $req)
	{
		$this->auth   = $auth;
		$this->helper = $helper;
		$this->req    = $req;
		if(!$this->auth->check()){
			return redirect('/login')->send();
		}
		$this->uid= $this->helper->user_logged_in();
		
	}

	public function home()
	{

		//Current user ID
		$uid = $this->uid;
		//User details
		$user = User::find($uid);
		if(!$user->image_name) $user->image_name = 'default.png';

		//User address details

		$places               = $user->places();
		$organising_workshops = Workshop::where('date',' >= ',date("Y-m-d"))->where('user_id',$uid)->get();
		$organised_workshops  = Workshop::where('date','<',date("Y-m-d"))->where('user_id',$uid)->get();
		$attended_workshops   = WorkshopAttendees::where(['cancelled' => null, 'attended' => 1])->get();
		$attending_workshops = WorkshopAttendees::select('workshop_attendees.workshop_id')
        			->join('workshops','workshops.id','=','workshop_attendees.workshop_id')
        			->where('workshops.date','>=',date("Y-m-d"))
        			->where(['workshop_attendees.cancelled' => null, 'workshop_attendees.attended' => null])
        			->where('workshop_attendees.user_id', $uid)
        			->get();

		//User skills
		$skills 			  = $user->skills();

		//User unresolved issues
		$my_issues 			  = Issue::Where('user_id',$uid)->Where('status','unresolved')->get();

		//User resolved issues
		$resolved_issues 	  = Issue::Where('user_id',$uid)->Where('status','resolved')->get();
		$solved_issues   	  = Issue::Where('solved_by',$uid)->Where('status','resolved')->get();

		//Connection requests
		$connectionRequests   = ConnectionRequests::where(['to_user'=> $uid, 'status'=>1])->get();


		return view('home.home', compact(
			'user',
			'uid',
			'places',
			'skills',
			'my_issues',
            'resolved_issues',
            'organising_workshops',
            'organised_workshops',
            'attending_workshops',
            'attended_workshops',
            'handling_issues',
			'connectionRequests'
		));
	}

	public function issues()
	{

		//Current user ID
		$uid  = $this->uid;
		//User details
		$user = User::find($uid);
		if(!$user->image_name) $user->image_name = 'default.png';

		//User address details

		$places = $user->places();
		//User skills
		$skills = $user->skills();

		//User unresolved issues
		$my_issues 		 = Issue::Where('user_id',$uid)->Where('status','unresolved')->get();
		//User resolved issues
		$resolved_issues = Issue::Where('user_id',$uid)->Where('status','resolved')->get();
		$solved_issues   = Issue::Where('solved_by',$uid)->Where('status','resolved')->get();

		//Currently handling issues
		$handling_issues = IssueHandleRequest::Where('user_id',$uid)->Where('status',1)->get();

		return view('home.issues', compact(
			'user',
			'uid',
			'places',
			'skills',
			'my_issues',
			'resolved_issues',
			'solved_issues',
			'handling_issues'
		));
	}

	public function workshops()
	{

		//Current user ID
		$uid  = $this->uid;
		//User details
		$user = User::find($uid);
		if(!$user->image_name) $user->image_name = 'default.png';

		//User address details

		$places                    = $user->places();
		$organising_workshops      = Workshop::where('user_id',$uid)->whereRaw("(`date` >= '".date('Y-m-d',time())."' OR `date` = null)")->orderBy('created_at','desc')->get();
		$organised_workshops       = Workshop::where('user_id',$uid)->where('date','<',date("Y-m-d"))->get();
		$attended_workshops_query  ="SELECT `workshops`.* FROM workshop_attendees LEFT JOIN `workshops` ON `workshop_attendees`.`workshop_id` = `workshops`.`id` WHERE `workshop_attendees`.`user_id` = $uid AND `workshops`.`date` <= '".date("Y-m-d")."'";
		$attended_workshops        = DB::select(DB::raw($attended_workshops_query));
		$attending_workshops_query = "SELECT `workshops`.* FROM workshop_attendees LEFT JOIN `workshops` ON `workshop_attendees`.`workshop_id` = `workshops`.`id` WHERE `workshop_attendees`.`user_id` = $uid AND `workshops`.`date` > '".date("Y-m-d")."'";
		$attending_workshops       = DB::select(DB::raw($attending_workshops_query));

		//User skills
		$skills = $user->skills();

		return view('home.workshops', compact(
			'user',
			'uid',
			'places',
			'skills',
			'organising_workshops',
			'organised_workshops',
			'attending_workshops',
			'attended_workshops'
		));
	}

	public function profile()
	{

		//Current user ID
		$uid = $this->uid;
		//User details
		$user = User::find($uid);
		if(!$user->image_name) $user->image_name = 'default.png';

		$distance = 5;
		$place = $user->primary_place();

		$current_date = date('Y-m-d G:i:s');
		$programs = [];
		$Query = "SELECT `users`.first_name, `users`.last_name, `users`.username, `users`.image_name,`place`.*,`workshops`.title,`workshops`.slug,`workshops`.created_at, `workshops`.updated_at, `workshops`.`id` as wID,".
                        $this->helper->location_query($place->lat, $place->lng). '
                         JOIN `workshops` on `place`.`for_id` = `workshops`.`id`
                         JOIN `users` on `workshops`.`user_id` = `users`.`id`
                         WHERE `place`.`for` = "workshop" AND `workshops`.`user_id` != ' . $uid . " AND `workshops`.`date` = '$current_date'
                         HAVING distance < " . $distance . "
                         LIMIT 0, 20";
            $Query = "SELECT * FROM (" . $Query . ") a GROUP BY workshop_id";

            $nearbyPrograms = DB::select(DB::raw($Query));

            foreach ($nearbyPrograms as $r) {
	        	$program = Workshop::find($r->workshop_id);
	        	$program_data['title'] 	   = $program->title;
	        	$program_data['slug']      = $program->slug;
	        	$program_data['summary']   = $program->summary;
	        	$program_data['date']      = $program->date;
	        	$program_data['skills']    = $program->tags()->toArray();
	        	$program_data['attendees'] = $program->attendees->toArray();
	        	$program_data['comments']  = $program->comments->toArray(); 
	        	$program_data['days_left'] = 'Days left: ' . (isset($program->date)) ? floor((strtotime($program->date) - time())/60/60/24) : FALSE;
	        	$programs[] = $program_data;
        	}

    	$issues = [];
		$Query = "SELECT `users`.first_name, `users`.last_name, `users`.username, `users`.image_name,`place`.*,`issues`.title,`issues`.slug,`issues`.created_at, `issues`.updated_at, `issues`.`id` as wID,".
                        $this->helper->location_query($place->lat, $place->lng). '
                         JOIN `issues` on `place`.`for_id` = `issues`.`id`
                         JOIN `users` on `issues`.`user_id` = `users`.`id`
                         WHERE `place`.`for` = "issue" AND `issues`.`user_id` != ' . $uid . " 
                         HAVING distance < " . $distance . "
                         LIMIT 0, 20";
            $Query = "SELECT * FROM (" . $Query . ") a GROUP BY issue_id";

            $nearbyIssues = DB::select(DB::raw($Query));

            foreach ($nearbyIssues as $r) {
	        	$issue = Issue::find($r->issue_id);
	        	$issue_data['title'] 	 = $issue->title;
	        	$issue_data['slug']      = $issue->slug;
	        	$issue_data['summary']   = $issue->summary;
	        	$issue_data['date']      = $issue->created_at;
	        	$issue_data['skills']    = $issue->tags()->get()->toArray();
	        	$issue_data['comments']  = $issue->comments->toArray(); 
	        	$issue_data['days_past'] = 'Raised ' . ((isset($issue->created_at)) ? abs(floor((strtotime($issue->created_at) - time())/60/60/24)) : 0) . ' days ago.' ;
	        	$issues[] = $issue_data;
        	}

		//User address details
		$places                 = $user->places();
		$organising_workshops   = Workshop::where('user_id',$uid)->where('date',' >= ',date("Y-m-d"))->orderBy('created_at','desc')->get();
		$organised_workshops    = Workshop::where('user_id',$uid)->where('date',' <= ',date("Y-m-d"))->get();
		$attended_workshops     = WorkshopAttendees::where(['cancelled' => null, 'attended' => 1,'user_id' => $uid])->get();
		
		$attending_workshops = WorkshopAttendees::select('workshop_attendees.workshop_id')
        			->join('workshops','workshops.id','=','workshop_attendees.workshop_id')
        			->where('workshops.date','>=',date("Y-m-d"))
        			->where(['workshop_attendees.cancelled' => null, 'workshop_attendees.attended' => null])
        			->where('workshop_attendees.user_id', $uid)
        			->get();
		//$handling_issues      = IssueHandleRequest::Where('user_id',$uid)->Where('status',1)->get();
		$handling_issues        = Issue::whereHas('handle_requests', function($q) use($uid)
		{
			$q->where('user_id', $uid)->where('status',1);

		})->get();

		//User skills
		$skills             = $user->skills();
		$learn              = $user->learn();
		$teach              = $user->teach();
		$connectionRequests = ConnectionRequests::where(['to_user'=> $uid, 'status'=>1])->get();

		//User unresolved issues
		$my_issues 			= $user->user_unresolved_issues();

		//User resolved issues
		$resolved_issues    = $user->user_resolved_issues();

		if($skills->isEmpty()){
			// Flash::error('Please add some skills to your profile to make use of all the features of UASDF.');
		}

		// Get and update the UI introduction flag
		// This flag is 
		$ui_introduction = Ui_introduction::where('user_id',$uid)->first();
		$dashboard_intro = $ui_introduction->dashboard;
		$ui_introduction->dashboard = 1;
		$ui_introduction->save();

		return view('home.profile', compact(
			'user',
			'uid',
			'places',
			'skills',
			'learn',
			'teach',
			'teach_suggestions',
			'my_issues',
			'resolved_issues',
			'organising_workshops',
			'organised_workshops',
			'attending_workshops',
			'attended_workshops',
			'handling_issues',
			'connectionRequests',
			'programs',
			'issues',
			'dashboard_intro'
		));
	}

	public function who_can_teach(){

		//Current user ID
		$uid  = $this->uid;
		//User details
		$user = User::find($uid);
		if(!$user->image_name) $user->image_name = 'default.png';

		//User address details
		$places 	= $user->places();

		//User skills
		$skills     = $user->skills();
		$learn      = $user->learn();
		$learnArray = array();
		$i          = 1;
		$level      = 1;
			foreach ($learn as $le) {
				$learnArray[] = $le->tag;
				if ($i == 1) {
					$level = $le->level;
				}
				$i++;
			}
			//People who can teach
			$teachQuery = '';
			$teach_suggestions = null;
			$i = 0;
			foreach ($places as $p) {
				$distance = 2;
				do {
					$result = '';
					$teachQuery =
						"SELECT *, ( 3959 * acos( cos( radians(" . $p->lat . ") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(" . $p->lng . ") ) + sin( radians(" . $p->lat . ") ) * sin( radians( lat ) ) ) ) AS distance FROM place" . '
			 LEFT JOIN `user_tags` on `place`.`for_id` = `user_tags`.`user_id`
			 LEFT JOIN `users` on `place`.`for_id` = `users`.`id`
			 WHERE `user_tags`.`tag` = "' . $learnArray[0] . '" AND `user_tags`.`level` > ' . $level . ' AND for_id != ' . $uid . '
			 GROUP BY `for_id`
			 HAVING distance < ' . $distance . '
			 LIMIT 0, 20';
					$result = DB::select(DB::raw($teachQuery));
					$distance++;
				} while (!$result && $distance < 10);

				if ($result) {
					$teach_suggestions[$i] = $result;
				} else {
					$teach_suggestions[$i] = '';
				}
				$i++;
			}


		return view('home.who-can-teach', compact(
			'user',
			'uid',
			'places',
			'skills',
			'teach',
			'learn',
			'teach_suggestions',
			'learnArray'

		));

	}

	public function teach_suggestions(Request $req){
		//Current user ID
		$uid    = $this->uid;
		$data   = $req->all();

		//User details
		$user   = User::find($uid);

		//User address details
		$places = $user->places();

		//People who can teach
		$teachQuery        = '';
		$teach_suggestions = null;
		$i                 = 0;
		foreach($places as $p){
			$distance = 2;
			do{
				$result = '';
				$teachQuery =
					"SELECT *, ( 3959 * acos( cos( radians(".$p->lat.") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(".$p->lng.") ) + sin( radians(".$p->lat.") ) * sin( radians( lat ) ) ) ) AS distance FROM place".'
					 LEFT JOIN `user_tags` on `place`.`for_id` = `user_tags`.`user_id`
					 LEFT JOIN `users` on `place`.`for_id` = `users`.`id`
					 WHERE `user_tags`.`tag` = "'.$data['skill'].'" AND `user_tags`.`level` > '.$data['level'].' AND `user_tags`.`tag_type` IN ("skill","teach") AND for_id != '.$uid.'
					 GROUP BY `for_id`
					 HAVING distance < '.$distance.'
					 LIMIT 0, 20';
				$result = DB::select( DB::raw($teachQuery) );
				$distance++;
			}while(!$result && $distance < 10);

			if($result){
				$teach_suggestions[$i] = $result;
			}else{
				$teach_suggestions[$i] = '';
			}
			$i++;
		}
		?>
			<h4 class="grey-bottom">People who can teach <?=$data['skill']?> near you</h4>
			<?php 
			$i = 0;
			foreach($places as $p){
				?>
				<h4><span class="glyphicon glyphicon-map-marker"></span> <?=place_min($p, true)?></h4>
				<?php if($teach_suggestions[$i] != null){?>

					<?php foreach($teach_suggestions[$i] as $ts){?>
						<a href="/user/<?=$ts->username?>" class="col-md-12 user_list">
							<img src="<?=user_avatar($ts->image_name)?>" class="user_image">
							<span class="user_name"><?=$ts->first_name.' '.$ts->last_name?></span>
							<span class="distance"><?=round($ts->distance,2)?> miles</span>
						</a>
					<?php }?>
				<?php }else{?>
					No one found
				<?php }
				$i++;
			}
	}

	public function who_can_learn(){

		//Current user ID
		$uid    = $this->uid;
		//User details
		$user   = User::find($uid);
		if(!$user->image_name) $user->image_name = 'default.png';

		//User address details
		$places = $user->places();

		//User skills
		$skills = $user->skills();
		$teachArray = array();
		$i = 1;
		$level = 1;
		foreach($skills as $le){
			$teachArray[] = $le->tag;
			if($i == 1){
				$level = $le->level;
			}
			$i++;
		}

		//People who can teach
		$learnQuery        = '';
		$learn_suggestions = null;
		$i                 = 0;
		if($teachArray) {
			foreach ($places as $p) {
				$distance = 2;
				do {
					$result = '';
					$learnQuery =
						"SELECT `place`.*, `user_tags`.`tag`,`users`.`id` as uID, ( 3959 * acos( cos( radians(" . $p->lat . ") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(" . $p->lng . ") ) + sin( radians(" . $p->lat . ") ) * sin( radians( lat ) ) ) ) AS distance FROM place" . '
			 LEFT JOIN `user_tags` on `place`.`for_id` = `user_tags`.`user_id`
			 LEFT JOIN `users` on `place`.`for_id` = `users`.`id`
			 WHERE `user_tags`.`tag` = "' . $teachArray[0] . '" AND `user_tags`.`tag_type` = "learn" AND `user_tags`.`level` < ' . $level . ' AND for_id != ' . $uid . '
			 HAVING distance < ' . $distance . '
			 LIMIT 0, 20';
					$learnQuery = "SELECT * FROM (" . $learnQuery . ") a GROUP BY for_id";

					$result = DB::select(DB::raw($learnQuery));
					$distance++;
				} while (!$result && $distance < 10);

				if ($result) {
					$learn_suggestions[$i] = $result;
				} else {
					$learn_suggestions[$i] = '';
				}
				$i++;
			}
		}


		return view('home.who-can-learn', compact(
			'user',
			'uid',
			'places',
			'skills',
			'teach',
			'learn',
			'learn_suggestions',
			'teachArray'

		));

	}

	public function learn_suggestions(Request $req){
		//Current user ID
		$uid    = $this->uid;
		$data   = $req->all();

		//User details
		$user   = User::find($uid);

		//User address details
		$places = $user->places();

		//People who can teach
		$learnQuery        = '';
		$learn_suggestions = null;
		$i                 = 0;
		foreach($places as $p){
			$distance = 2;
			do{
				$result = '';
				$learnQuery =
					"SELECT `place`.*, `user_tags`.`tag`,`users`.`id` as uID, ( 3959 * acos( cos( radians(".$p->lat.") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(".$p->lng.") ) + sin( radians(".$p->lat.") ) * sin( radians( lat ) ) ) ) AS distance FROM place".'
					 LEFT JOIN `user_tags` on `place`.`for_id` = `user_tags`.`user_id`
					 LEFT JOIN `users` on `place`.`for_id` = `users`.`id`
					 WHERE `user_tags`.`tag` = "'.$data['skill'].'" AND `user_tags`.`level` < '.$data['level'].' AND `user_tags`.`tag_type` = "learn" AND for_id != '.$uid.'
					 HAVING distance < '.$distance.'
					 LIMIT 0, 20';
				$learnQuery = "SELECT * FROM (".$learnQuery.") a GROUP BY for_id";
				$result = DB::select( DB::raw($learnQuery) );
				$distance++;
			}while(!$result && $distance < 10);

			if($result){
				$learn_suggestions[$i] = $result;
			}else{
				$learn_suggestions[$i] = '';
			}
			$i++;
		}
		?>
		<h4 class="grey-bottom">People who wants to learn <?=$data['skill']?> near you</h4>
		<?php 
		$i = 0;
		foreach($places as $p){
			?>
			<h4><span class="glyphicon glyphicon-map-marker"></span> <?=place_min($p, true)?></h4>
			<?php if($learn_suggestions[$i] != null){?>

				<?php foreach($learn_suggestions[$i] as $ts){
					$usr = User::find($ts->uID);
					?>

					<a href="/user/<?=$usr->username?>" class="col-md-12 user_list">
						<img src="<?=user_avatar($usr->image_name)?>" class="user_image">
						<span class="user_name"><?=$usr->first_name.' '.$usr->last_name?></span>
						<span class="distance"><?=round($ts->distance,2)?> miles</span>
					</a>
				<?php }?>
			<?php }else{?>
				No one found
			<?php }
			$i++;
		}
	}

	public function edit_profile()
	{
		//Current user ID
		$uid  = $this->uid;
		//User details
		$user = User::find($uid);

		return view('home.edit-profile', compact('user','uid'));
	}

	public function post_edit_profile(Request $req)
	{
		$this->request = $req;
		$data          = $req->all();

		//Current user ID
		$uid  = $this->uid;

		//User details
		$user = User::find($uid);

		$validator = Validator::make(
			array(
				'first_name' => $data['first_name'],
				'last_name'  => $data['last_name']
			),
			array(
				'first_name' => 'required',
				'last_name'  => 'required'
			)
		);

		if ($validator->fails())
		{
			$errors = $validator->messages();
			return view('home.edit-profile',compact('errors'));
		}else{
			$user->first_name = $data['first_name'];
			$user->last_name  = $data['last_name'];
			$user->phone      = $data['phone'];
			$user->newsletter = $data['newsletter'];
			$user->save();
			return redirect('profile');

		}


	}

	public function add_skill(){

		//Current user ID
		$uid         = $this->uid;
		$data        = $this->req->all();
		$skill_exist = UserTags::where(['user_id'=>$uid, 'tag' => $data['skill']])->first();
		if($skill_exist){
			Flash::error('Same skill cannot be added twice.');
			return redirect('profile');

		}
		$validator = Validator::make(
			array(
				'skill' => $data['skill'],
			),
			array(
				'skill' => 'required',
			)
		);

		if ($validator->fails())
		{
			return redirect('profile');
		}else{
			$ut = new UserTags;

			$ut->user_id  = $uid;
			$ut->tag_type = $data['type'];
			$ut->tag      = $data['skill'];
			$ut->level    = $data['level'];
			$ut->save();

			$tag           = new Tags;
			$tag->tag      = $ut->tag;
			$tag->tag_type = $ut->tag_type;
			$tag->for      = 'user';
			$tag->for_id   = $uid;
			$tag->level    = $data['level'];
			$tag->save();

			return redirect('profile');

		}

	}

	public function update_skill(){

		//Current user ID
		$uid  = $this->uid;
		$data = $this->req->all();

			$ut        = UserTags::find($data['pk']);
			$ut->level = $data['value'];
			$ut->save();

			$tagu 		= Tags::where(['tag' => $ut->tag, 'user_id' => $uid,'tag_type' =>$ut->tag_type ])->first();
			$tag 		= Tags::find($tagu->id);
			$tag->level = $data['value'];
			$tag->save();

			return;

	}



	public function messages(){
		$uid            = $this->uid;
		$threads        = MessageThreads::whereIn('user1', [$uid])->orwhereIn('user2', [$uid])->orderBy('updated_at','desc')->get();
		$unread         = array();
		$users          = array();
		$i              = 0;
		$start_messages = null;
		$start_user     = null;
		foreach($threads as $th){
			foreach($th->UnreadMessages as $um){
				if($um->to_user == $uid){
					$unread[$th->id] = true;
				}

			}

			if(!isset($unread[$th->id])){
				$unread[$th->id] = false;
			}

			if($i==0){
				$start_messages = $th->messages;

				foreach($start_messages as $sm){
					if($sm->from_user == $uid){
					}else{
						//Message read by receiver
						$sm->read = 1;
						$sm->save();
					}
				}
				if($th->user1 != $uid) {
					$start_user = $th->user1;
				}else{
					$start_user = $th->user2;

				}
			}
			if($th->user1 != $uid){
				$users[$th->id] = User::find($th->user1);
			}else{
				$users[$th->id] = User::find($th->user2);
			}
			$i++;
		}

		return view('home.messages',compact(
			'threads',
			'users',
			'start_messages',
			'uid',
			'start_user',
			'unread'
			)
		);
	}

	public function new_message(){
		$uid = $this->uid;
		$connections = get_user_connections($uid);

		return view('home.new-message',compact('connections'));
	}
	public function post_message(){
		$data = $this->req->all();
		$from_user = User::find($this->uid);
		$to_user = User::find($data['to_user']);

		$thread_exist = MessageThreads::whereIn('user1', [$from_user->id,$to_user->id])->whereIn('user2', [$from_user->id,$to_user->id])->first();
		$msg = new Messages();

		if(!$thread_exist){
			$thread        = new MessageThreads();
			$thread->user1 = $from_user->id;
			$thread->user2 = $to_user->id;
			$thread->save();

			$msg->from_user = $from_user->id;
			$msg->to_user   = $to_user->id;
			$msg->thread_id = $thread->id;
			$msg->message   = $data['message'];
			$msg->save();

		}else{
			$thread_exist->touch();
			$msg->from_user = $from_user->id;
			$msg->to_user   = $to_user->id;
			$msg->thread_id = $thread_exist->id;
			$msg->message   = $data['message'];
			$msg->save();
		}

		Mail::send('emails.message', ['from_user' => $from_user, 'to_user' => $to_user, 'msg' => $msg], function ($message) use ($to_user) {
			$message->to($to_user->email, $to_user->first_name . ' ' . $to_user->last_name)
				->subject('Placerange - New message');
		});

		return redirect('/messages');
	}

	public function ajax_post_message(){
		$data      = $this->req->all();
		$from_user = User::find($this->uid);
		$to_user   = User::find($data['to_user']);
		$thread    = MessageThreads::whereIn('user1', [$from_user->id,$to_user->id])->whereIn('user2', [$from_user->id,$to_user->id])->first();
		$msg       = new Messages();
		$thread->touch();
		$msg->from_user = $from_user->id;
		$msg->to_user   = $to_user->id;
		$msg->thread_id = $thread->id;
		$msg->message   = $data['message'];
		$msg->save();
		Mail::send('emails.message', ['from_user' => $from_user, 'to_user' => $to_user, 'msg' => $msg], function ($message) use ($to_user) {
			$message->to($to_user->email, $to_user->first_name . ' ' . $to_user->last_name)
				->subject('Placerange - New message');
		});

		//Return block
		?>
			<div id="<?=$msg->id?>" class="media row  message-sender  pull-right clear" style="width: 70%;">

				<div class="media-right pull-right">
					<a href="#">
						<img class="media-object small" src="<?=user_avatar($msg->from_user_data->image_name)?>" alt="...">
					</a>
				</div>
				<div class="media-body pull-right">
					<?=$msg->message?>
				</div>
			</div>
		<?php 

		//End return block
	}

	public function load_messages(){
		$uid      = $this->uid;
		$data     = $this->req->all();
		$for_user = $data['for_user'];
		$thread   = MessageThreads::whereIn('user1', [$uid,$for_user])->whereIn('user2', [$uid,$for_user])->first();
		$messages = $thread->messages;
		//Return block
		foreach($messages as $m){
		if($m->from_user == $uid){
			$sender = 1;
		}else{
			$sender = 0;

			//Message read by receiver
			$m->read = 1;
			$m->save();
		}
		?>
		<?php if($sender){?>
			<div id="<?=$m->id?>" class="media row pull-right message-sender clear" style="width: 70%;">

				<div class="media-right pull-right">
					<a href="#">
						<img class="media-object small" src="<?=user_avatar($m->from_user_data->image_name)?>" alt="...">
					</a>
				</div>
				<div class="media-body pull-right">
					<?=$m->message?>
				</div>
			</div>
		<?php }else{?>
			<div id="<?=$m->id?>" class="media pull-left message-receiver row clear" style="width: 70%;">
				<div class="media-left ">
					<a href="/user/<?php $m->from_user_data->username?>">
						<img class="media-object small" src="<?=user_avatar($m->from_user_data->image_name)?>" alt="...">
					</a>
				</div>
				<div class="media-body">
					<?=$m->message?>
				</div>
			</div>
		<?php }?>
		<?php }

		//End return block

	}

	public function message_autoload(){
		$uid      = $this->uid;
		$data     = $this->req->all();
		$for_user = $data['to_user'];
		$thread   = MessageThreads::whereIn('user1', [$uid,$for_user])->whereIn('user2', [$uid,$for_user])->first();
		$messages = Messages::where('thread_id', $thread->id)->where('id','>',$data['count'])->orderBy('id','DESC')->get();
//		$messages = Messages::where('thread_id', $thread->id)->where('read','=',null)->get();
		//Return block
		foreach($messages as $m){
			if($m->from_user == $uid){
				$sender = 1;
			}else{
				$sender = 0;

				//Message read by receiver
				$m->read = 1;
				$m->save();
			}
			?>
			<?php if($sender){?>
				<div id="<?=$m->id?>" class="media row message-sender pull-right clear"  style="width: 70%;">

					<div class="media-right pull-right">
						<a href="#">
							<img class="media-object small" src="<?=user_avatar($m->from_user_data->image_name)?>" alt="...">
						</a>
					</div>
					<div class="media-body pull-right">
						<?=$m->message?>
					</div>
				</div>
			<?php }else{?>
				<div id="<?=$m->id?>" class="media pull-left message-receiver row clear"  style="width: 70%;">
					<div class="media-left ">
						<a href="/user/<?php $m->from_user_data->username?>">
							<img class="media-object small" src="<?=user_avatar($m->from_user_data->image_name)?>" alt="...">
						</a>
					</div>
					<div class="media-body">
						<?=$m->message?>
					</div>
				</div>
			<?php }?>
		<?php }

		//End return block

	}

	public function nearMe()
	{
		$uid    = $this->uid;
		$user   = User::find($uid);
		$places = $user->places();
		return view('general.near-me',compact('places'));
	}

	public function postNearMe(Request $req)
	{
		//Current user ID
		$uid  = $this->uid;
		$data = $req->all();

		//User details
		$user                 = User::find($uid);
		$levelText            = 'Expert' ;
		if($data['level']     == 1) $levelText = 'expert';
		elseif($data['level'] == 2) $levelText = 'intermediate';
		elseif($data['level'] == 3) $levelText = 'beginner';
		elseif($data['level'] == 0) $levelText = 'any';



		//User address details
		$p            = Place::find($data['location']);
		$resultHeader = 'Results for '.$data['search_for'].' nearby '.place_min($p,true).($data['search_string'] ? ' matching skills '.$data['search_string'].' and in '.$levelText.' level.' : '');
		$skills       = explode(',', $data['search_string']);

		//People who can teach
		$teachQuery     = '';
		$results        = null;
		$i              = 0;
		$search_for     = $data['search_for'];
		if ($search_for == 'people') {
			$distance = 2;
			do {
				$result = '';
				$Query =
					"SELECT `place`.*, `user_tags`.`tag`, ( 3959 * acos( cos( radians(" . $p->lat . ") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(" . $p->lng . ") ) + sin( radians(" . $p->lat . ") ) * sin( radians( lat ) ) ) ) AS distance FROM place" . '
					 LEFT JOIN `user_tags` on `place`.`for_id` = `user_tags`.`user_id`
					 LEFT JOIN `users` on `place`.`for_id` = `users`.`id`
					 WHERE '.( $data['search_string'] ? '`user_tags`.`tag` IN (\'' . implode($skills, "','") . '\') AND `user_tags`.`level` >= ' . $data['level'] . ' AND `user_tags`.`tag_type` IN ("skill","teach") AND' : '').' `for` = "user" AND `for_id` != ' . $uid . '
					 HAVING distance < ' . $distance . '
					 Order BY distance ASC
					 LIMIT 0, 20';
				$Query = "SELECT * FROM (".$Query.") a GROUP BY for_id";

//				var_dump($Query);
				$result = DB::select(DB::raw($Query));
				$distance++;
			} while (!$result && $distance < 10);

			if ($result) {
				$results[$i] = $result;
			} else {
				$results[$i] = null;
			}
			$i++;

			?>
			<h4 class="grey-bottom"><?=$resultHeader?></h4>
			<?php 
			$i = 0;
			?>
			<h4><span class="glyphicon glyphicon-map-marker"></span> <?= place_min($p, true) ?></h4>
			<?php if ($results[$i] != null) {
				?>
				<?php  foreach ($results[$i] as $ts) {
					$user = User::find($ts->user_id);
					?>
					<a href="/user/<?= $user->username ?>" class="row user_list">
						<img src="<?= user_avatar($user->image_name) ?>" class="user_image">

						<div class="content">
							<span class="user_name"><?= $user->first_name . ' ' . $user->last_name ?></span>
                                <span class="skills"><?php  $skill = '';
									foreach ($user->skills() as $s) {
										$skill .= $s->tag . ', ';
									}
									echo rtrim($skill, ', '); ?></span>
						</div>
						<span class="distance"><?= round($ts->distance, 2) ?> miles</span>
					</a>
				<?php 
				} ?>
			<?php 
			} else {
				?>
				No one found
			<?php 
			}
			$i++;
		} elseif ($search_for == 'issue') {
			$distance = 2;
			do {
				$result = '';
				$Query =
					"SELECT `place`.*, `issue_tags`.`tag`, ( 3959 * acos( cos( radians(" . $p->lat . ") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(" . $p->lng . ") ) + sin( radians(" . $p->lat . ") ) * sin( radians( lat ) ) ) ) AS distance FROM place" . '
					 LEFT JOIN `issue_tags` on `place`.`for_id` = `issue_tags`.`issue_id`
					 LEFT JOIN `issues` on `place`.`for_id` = `issues`.`id`
					 WHERE '.( $data['search_string'] ? '`issue_tags`.`tag` IN (\'' . implode($skills, "','") . '\') AND `issue_tags`.`level` >= ' . $data['level'] . ' AND `issue_tags`.`tag_type` = "skill" AND ' : '').' `place`.`for` = "issue"
					 HAVING distance < ' . $distance . '
					 LIMIT 0, 20';
				$Query = "SELECT * FROM (".$Query.") a GROUP BY for_id";

				$result = DB::select(DB::raw($Query));
				$distance++;
			} while (!$result && $distance < 10);

			if ($result) {
				$results[$i] = $result;
			} else {
				$results[$i] = null;
			}

			$i++;
			?>
			<h4 class="grey-bottom"><?=$resultHeader?></h4>
			<?php 
			$i = 0;
			?>
			<h4><span class="glyphicon glyphicon-map-marker"></span> <?= place_min($p, true) ?></h4>
			<?php if ($results[$i] != null) {
				?>

				<?php  foreach ($results[$i] as $ts) {
					$issue = Issue::find($ts->issue_id);

					?>
					<a href="/issue/<?= $issue->slug ?>" class="row user_list">
						<img src="<?= user_avatar($issue->user->image_name) ?>" class="user_image">

						<div class="content">
							<span class="user_name"><?= $issue->title ?></span>
                                <span class="skills"><?php  $skill = '';
									//                                    var_dump($issue->tags());
									foreach ($issue->tags() as $s) {
										$skill .= $s->tag . ', ';
									}
									echo rtrim($skill, ', '); ?></span>
						</div>
						<span class="distance"><?= round($ts->distance, 2) ?> miles</span>
					</a>
				<?php 
				} ?>
			<?php 
			} else {
				?>
				No issues found
			<?php 
			}
			$i++;

		} elseif ($search_for == 'workshop') {
			$distance = 2;
			do {
				$result = '';
				$Query = "SELECT `place`.*, `workshop_tags`.`tag`, ( 3959 * acos( cos( radians(" . $p->lat . ") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(" . $p->lng . ") ) + sin( radians(" . $p->lat . ") ) * sin( radians( lat ) ) ) ) AS distance FROM place" . '
                     LEFT JOIN `workshop_tags` on `place`.`for_id` = `workshop_tags`.`workshop_id`
                     LEFT JOIN `workshops` on `place`.`for_id` = `workshops`.`id`
                     WHERE '.( $data['search_string'] ? '`workshop_tags`.`tag` IN (\'' . implode($skills, "','") . '\') AND `workshop_tags`.`level` >= ' . $data['level'] . ' AND `workshop_tags`.`tag_type` = "skill"  AND' : '').'  `place`.`for` = "workshop"
                     HAVING distance < ' . $distance . '
                     LIMIT 0, 20';
				$Query = "SELECT * FROM (".$Query.") a GROUP BY for_id";

				$result = DB::select(DB::raw($Query));
				$distance++;
//				dd(count($skills).$Query);
			} while (!$result && $distance < 2);

			if ($result) {
				$results[$i] = $result;
			} else {
				$results[$i] = null;
			}
			$i++;

			?>
			<h4 class="grey-bottom"><?=$resultHeader?></h4>
			<?php 
			$i = 0;
			?>
			<h4><span class="glyphicon glyphicon-map-marker"></span> <?= place_min($p, true) ?></h4>
			<?php if ($results[$i] != null) {
				?>

				<?php  foreach ($results[$i] as $ts) {
					$workshop = Workshop::find($ts->workshop_id);
					if($workshop->main_image == ''){
			            $workshop->main_image = 'default.jpeg';
			        }
					?>

					<a href="/program/<?= $workshop->slug ?>" class="row user_list">
						<img src="/uploads/programs/main/thumbnail/<?=$workshop->main_image ?>" class="user_image">

						<div class="content">
							<span class="user_name"><?= $workshop->title ?></span>
                                <span class="skills"><?php  $skill = '';
									//var_dump($issue->tags());
									foreach ($workshop->tags() as $s) {
										$skill .= $s->tag . ', ';
									}
									echo rtrim($skill, ', '); ?></span>
						</div>
						<span class="distance"><?= round($ts->distance, 2) ?> miles</span>
					</a>
				<?php 
				} ?>
			<?php 
			} else {
				?>
				No programs found
			<?php 
			}
			$i++;

		}
	}


	public function notifications(){
		$uid           = $this->uid;
		$user          = User::find($uid);
		$notifications = $user->notifications;

		foreach($notifications as $n){
			if(!$n->read){
				$n->read = 1;
				$n->save();
			}
		}
		return view('home.notifications',compact('notifications'));
	}


}
