<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");
// -----------------------
// Test
// -----------------------
Route::get ('/test', function() {
    
    
    dd(User::find(1));
    
});



// -----------------------
// API - V2 App
// -----------------------
// -----------------------
// Autofillcontroller
// -----------------------
Route::post('skills'             , 'AutofillController@skills');

// -----------------------
// Auth routes
// -----------------------
Route::post('auth/google'             , 'Auth\AuthController@google');
Route::post('auth/facebook'             , 'Auth\AuthController@facebook');
Route::post('auth/twitter'             , 'Auth\AuthController@twitter');
Route::post('auth/register'             , 'Auth\AuthController@register');
Route::post('auth/register2'             , 'Auth\AuthController@register2');

// -----------------------
// Messages Routes
// -----------------------
Route::group(['prefix' => 'api'], function()
{
    Route::post ('/messages',                    'APIv1\messageAPIController@messages');
    Route::post('/send_message',                'APIv1\messageAPIController@post_message');
    Route::post('/load_messages',               'APIv1\messageAPIController@load_messages');
    Route::post('/ajax_post_message',           'APIv1\messageAPIController@ajax_post_message');
    Route::post('/post_new_message',                'APIv1\messageAPIController@post_new_message');


});
// -----------------------
// Search Routes
Route::group(['prefix' => 'api'], function()
{
    Route::post('/search_near_me'             , 'APIv1\searchAPIController@search_near_me');
    Route::post('/search_post_near_me'             , 'APIv1\searchAPIController@search_post_near_me');
    Route::post('/post_search'             , 'APIv1\searchAPIController@postSearch');
    
});

// -----------------------
// Home Routes
// -----------------------
Route::group(['prefix' => 'api'], function()
{
    Route::get('/user_home'      , 'APIv1\homeAPIController@user_home');
    Route::post('/user_wall_data' , 'APIv1\homeAPIController@user_wall_data');
    Route::get('/user_homepage_data'       , 'APIv1\homeAPIController@user_homepage_data');
    Route::get('/dashboard_search' , 'APIv1\homeAPIController@dashboard_search');
    Route::get('/notifications'  , 'APIv1\notificationAPIController@notifications');
    Route::post('/notification_mark_read'  , 'APIv1\notificationAPIController@notification_mark_read');
    
});

// -----------------------
// User Homepage routes
// -----------------------
Route::group(['prefix' => 'api'], function()
{
    //Route::resource('authenticate'       , 'PlacerangeAPI', ['only' => ['index']]);
    Route::post('authenticate'             , 'APIv1\PlacerangeAPI@authenticate');
    Route::get('authenticate/user'             , 'APIv1\PlacerangeAPI@get_authenticated_user');
    Route::post('/register'             , 'APIv1\PlacerangeAPI@register');
    Route::get('/index_page_data'          , 'APIv1\PlacerangeAPI@index_page_data');
    
    Route::post('/user_update_profile'     , 'APIv1\PlacerangeAPI@user_update_profile');
    Route::post('/user_update_skill_level' , 'APIv1\PlacerangeAPI@user_update_skill_level');
    Route::post('/user_update_language'    , 'APIv1\PlacerangeAPI@user_update_language');
    Route::post('/user_add_skill'    	   , 'APIv1\PlacerangeAPI@user_add_skill');
    Route::post('/user_remove_skill'       , 'APIv1\PlacerangeAPI@user_remove_skill');
    Route::post('/user_update_place'       , 'APIv1\PlacerangeAPI@user_update_place');
    Route::post('/user_remove_place'       , 'APIv1\PlacerangeAPI@user_remove_place');
    Route::post('/user_add_place'          , 'APIv1\PlacerangeAPI@user_add_place');
    Route::post('/user_upload_image'       , 'APIv1\PlacerangeAPI@user_upload_image');
    Route::get('/landing_search'           , 'APIv1\PlacerangeAPI@landing_search');
    Route::post('/post_feedback'           , 'APIv1\PlacerangeAPI@post_feedback');
});

// -----------------------
// User Public Page Routes
// -----------------------
Route::group(['prefix' => 'api'], function()
{
    Route::post('/user_publicpage_data'    , 'APIv1\publicProfileAPIController@user_publicpage_data');
    Route::post('/send_connection_request' , 'APIv1\publicProfileAPIController@send_connection_request');
    Route::post('/user_respond_connect'    , 'APIv1\publicProfileAPIController@user_respond_connect');
    Route::post('/user_post_rating'        , 'APIv1\publicProfileAPIController@user_post_rating');
    Route::post('/user_send_message'       , 'APIv1\publicProfileAPIController@user_send_message');
});

// -----------------------
// Task Public Page Routes
// -----------------------
Route::group(['prefix' => 'api'], function()
{
    Route::post('/task_publicpage_data'                , 'APIv1\publicTaskAPIController@task_publicpage_data');
    Route::post('/task_reply_to_comment'               , 'APIv1\publicTaskAPIController@task_reply_to_comment');
    Route::post('/task_add_new_comment'                , 'APIv1\publicTaskAPIController@task_add_new_comment');
    Route::post('/task_edit_reply'                     , 'APIv1\publicTaskAPIController@task_edit_reply');
    Route::post('/task_edit_comment'                   , 'APIv1\publicTaskAPIController@task_edit_comment');
    Route::post('/task_remove_comment'                 , 'APIv1\publicTaskAPIController@task_remove_comment');
    Route::post('/task_remove_reply'                   , 'APIv1\publicTaskAPIController@task_remove_reply');
    Route::post('/task_comment_helpful'                , 'APIv1\publicTaskAPIController@task_comment_helpful');
    Route::post('/task_send_handle_request'            , 'APIv1\publicTaskAPIController@task_send_handle_request');
    Route::post('/task_cancel_awaiting_handle_request' , 'APIv1\publicTaskAPIController@task_cancel_awaiting_handle_request');
    Route::post('/task_cancel_handle'                  , 'APIv1\publicTaskAPIController@task_cancel_handle');
    Route::post('/task_accept_handle_request'          , 'APIv1\publicTaskAPIController@task_accept_handle_request');
    Route::post('/task_reject_handle_request'          , 'APIv1\publicTaskAPIController@task_reject_handle_request');
    Route::post('/task_owner_cancel_handle_request'    , 'APIv1\publicTaskAPIController@task_owner_cancel_handle_request');
});

// -----------------------
// Edit Task routes
// -----------------------
Route::group(['prefix' => 'api'], function()
{
    Route::post('/task_data'               , 'APIv1\taskAPIController@task_data');
    Route::post('/task_update'             , 'APIv1\taskAPIController@task_update');
    Route::post('/task_update_skill_level' , 'APIv1\taskAPIController@task_update_skill_level');
    Route::post('/task_add_skill'          , 'APIv1\taskAPIController@task_add_skill');
    Route::post('/task_remove_skill'       , 'APIv1\taskAPIController@task_remove_skill');
    Route::post('/task_add_place'          , 'APIv1\taskAPIController@task_add_place');
    Route::post('/task_update_place'       , 'APIv1\taskAPIController@task_update_place');
    Route::post('/task_remove_place'       , 'APIv1\taskAPIController@task_remove_place');
    Route::post('/task_create_data'        , 'APIv1\taskAPIController@task_create_data');
    Route::post('/task_create'             , 'APIv1\taskAPIController@task_create');
    Route::post('/task_delete'             , 'APIv1\taskAPIController@task_delete');
});


// -----------------------
// Group Routes
// -----------------------
Route::group(['prefix' => 'api'], function()
{
    Route::post('/group_publicpage_data'          , 'APIv1\groupAPIController@group_publicpage_data');
    Route::get('/groups_home'                    , 'APIv1\groupAPIController@groups_home');
    
});

// -----------------------
// Email Subscription
// -----------------------
Route::group(['prefix' => 'api'], function()
{   
    Route::post('/get-subscribtion' , 'EmailController@get_subscribtion');
    Route::post('/update-subscription' , 'EmailController@update_subscription');
    
});

// -----------------------
// Sitemap
// -----------------------
Route::get('/generate_sitemap', 'SitemapController@generate_sitemap');

// -----------------------
// Home
// -----------------------
Route::get('/', 'UserController@index');

// -----------------------
Route::post('/invite/send-invite',   'ContactController@send_invite');

// -----------------------
//Import contacts
// -----------------------
Route::get ('/invite-friends', 		 'ContactController@inviteFriends');
Route::get ('/contact/import/google', ['as'=>'google.import', 'uses'=>'ContactController@importGoogleContact']);
Route::post('/invite/send-connect',  'ContactController@send_connect');
Route::post('/invite/send-invite',   'ContactController@send_invite');
// -----------------------


// -----------------------
//Not found page 404
// -----------------------
Route::get('/404','DefaultController@not_found');
// -----------------------

// -----------------------
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
// -----------------------

// -----------------------
// Admin Controller
// -----------------------
Route::get('/UASDFmin','AdminController@index');
Route::get('/UASDFmin/tags','AdminController@tags');
// -----------------------


