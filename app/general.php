<?php
/**
 * Created by PhpStorm.
 * User: msmanojpandian
 * Date: 25/02/15
 * Time: 20:24
 */

    function user_avatar($uimg, $size = 'icon')
    {
        if(!$uimg)  return '/uploads/users/'.$size.'/default.png';
        $rfp = '/uploads/users/'.$size.'/'.$uimg;
        $fp = public_path().'/uploads/users/'.$size.'/'.$uimg;
        $fn = false;
        if(file_exists($fp)) $fn = $fp;

        if($fn){
            return $rfp;
        }else{
            return '/uploads/users/'.$size.'/default.png';

        }

    }

    function program_image($uimg, $type, $size = 'icon')
    {
        if(!$uimg)  return '/uploads/programs/'. $type .'/'.$size.'/default.jpeg';
        $rfp = '/uploads/programs/'. $type .'/'.$size.'/'.$uimg;
        $fp = public_path().'/uploads/programs/'. $type .'/'.$size.'/'.$uimg;
        $fn = false;
        if(file_exists($fp)) $fn = $fp;

        if($fn){
            return $rfp;
        }else{
            return '/uploads/programs/'. $type .'/'.$size.'/default.jpeg';

        }

    }

    function place_min($place, $min = false){
        if($min) {
            $place_keys = ['establishment', 'postal_code', 'sublocality_level_2', 'sublocality_level_1', 'sublocality', 'locality', 'administrative_area_level_1', 'country'];
            foreach ($place_keys as $key) {
                if (isset($place[$key])) {
                    if ($place[$key]) {
                        return $place[$key];
                    }
                }
            }
        }
        return $place->formatted_address;
    }


    function unread_message_count($uid)
    {
        $Query = "Select COUNT(*) as um_count from messages where `to_user` = $uid and `read` IS NULL ";
        $count = DB::select(DB::raw($Query));
        $count = $count[0]->um_count;
        return $count;
    }

    function unread_notifications_count($uid)
    {
        $Query = "Select COUNT(*) as n_count from notifications where `user_id` = $uid and `read` IS NULL ";
        $count = DB::select(DB::raw($Query));
        $count = $count[0]->n_count;
        return $count;
    }

    function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    function get_user_connections($uid){
        $Query = "Select * from connections where `user1` = $uid or `user2` = $uid";
        $allConnections = DB::select(DB::raw($Query));
        $connections = [];
        foreach($allConnections as $con){
            if($con->user1 == $uid){
                $connections[] = User::find($con->user2);
            }else{
                $connections[] = User::find($con->user1);
            }
        }

        return $connections;

    }

    // Get the pending request for the current user
    function get_user_awating_connections($uid){
        $Query = "SELECT * FROM connection_requests where to_user = $uid AND status = 1";
        $allConnections = DB::select(DB::raw($Query));
        $connection_requests = [];
        $current_connection = [];

        foreach($allConnections as $con){

            $con->user    = User::find($con->from_user);
            if(!$con->user->image_name) $con->user->image_name = 'default.png';
            $con->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($con->user->image_name, 'thumbnail');
            $connection_requests[] = $con;
        }

        return $connection_requests;

    }

    // Reversible Encryption
    function encrypt($sData, $sKey='AllowtheminPR1'){ 
        $sResult = ''; 
        for($i=0;$i<strlen($sData);$i++){ 
            $sChar    = substr($sData, $i, 1); 
            $sKeyChar = substr($sKey, ($i % strlen($sKey)) - 1, 1); 
            $sChar    = chr(ord($sChar) + ord($sKeyChar)); 
            $sResult .= $sChar; 
        } 
        return encode_base64($sResult); 
    } 

    function decrypt($sData, $sKey='AllowtheminPR1'){ 
        $sResult = ''; 
        $sData   = decode_base64($sData); 
        for($i=0;$i<strlen($sData);$i++){ 
            $sChar    = substr($sData, $i, 1); 
            $sKeyChar = substr($sKey, ($i % strlen($sKey)) - 1, 1); 
            $sChar    = chr(ord($sChar) - ord($sKeyChar)); 
            $sResult .= $sChar; 
        } 
        return $sResult; 
    } 


    function encode_base64($sData){ 
        $sBase64 = base64_encode($sData); 
        return strtr($sBase64, '+/', '-_'); 
    } 

    function decode_base64($sData){ 
        $sBase64 = strtr($sData, '-_', '+/'); 
        return base64_decode($sBase64); 
    }  
