<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskTags extends Model {

	protected $table = 'task_tags';

    protected $fillable = ['tag'];

}
