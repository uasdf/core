<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRating extends Model {

	protected $table = 'user_rating';

	protected $fillable=["comment"];

	public function reviewed_to(){
        return $this->belongsTo('App\User','to_user', 'id');
    }

    public function reviewed_by(){
        return $this->belongsTo('App\User','from_user', 'id');
    }

}
