<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskComments extends Model {

	protected $table="task_comments";

    protected $fillable=["comment"];


    public function user(){
        return $this->belongsTo('App\User');
    }

    public function task(){
        return $this->belongsTo('App\Task');
    }

    public function helpful(){
        return $this->hasMany('App\TaskCommentHelpful', 'comment_id', 'id');
    }

    public function replies(){
        return $this->hasMany('App\TaskCommentReply','comment_id','id');
    }

}
