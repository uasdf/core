<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskCommentReply extends Model {

	protected $table = 'task_comment_reply';

    protected $fillable = ['comment'];

    public function comment(){
        return $this->belongsTo('App\TaskComments','id','comment_id');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

}
