<?php
namespace App\Helpers;

use Guard;
use Geocoder;
use Illuminate\Support\Facades\DB;
use Pusher;
use App\User;
use App\Task;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Routing\Route;
use Illuminate\Http\Request;

class Helper {

    private $req,$helper,$uid,$auth;

    public function __construct(Guard $auth, Route $route, Request $req){
        $this->auth   = $auth;
        $this->req    = $req;
        $this->user   = $auth;
        $token = JWTAuth::getToken();

        if($token){
            $this->uid = JWTAuth::parseToken()->authenticate()->id;
        } else {
            $this->uid = null;
        }

    }

    public function user_logged_in(){
        
        $token = JWTAuth::getToken();

        if($token){
            return JWTAuth::parseToken()->authenticate()->id;
        } else {
            return false;
        }
    }

    public function push_notification( $notification ){

        $notification->user        = User::find($notification->user_id);
        if(!$notification->user->image_name) $notification->user->image_name = 'default.png';
        $notification->user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($notification->user->image_name, 'thumbnail');

        if($notification->target_user){
            $notification->target_user = User::find($notification->target_user);
            if(!$notification->target_user->image_name) $notification->target_user->image_name = 'default.png';
            $notification->target_user->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($notification->target_user->image_name, 'thumbnail');
        }
        
        $notification->target_task = Task::find($notification->target_task);

        $pusher = new Pusher( '3cf39c63f99ff76f1249','1118e34b7e164f6bb7ea','196448' );
        $pusher_data['notification'] = $notification;
        $pusher->trigger('notification_'.$notification->user_id , 'notification', $pusher_data);

    }

    public function connections(){
        $uid            = $this->user_logged_in();
        $Query          = "Select * from connections where `user1` = $uid or `user2` = $uid ";
        $allConnections = DB::select(DB::raw($Query));
        $connections    = array();
        foreach($allConnections as $con){
            if($con->user1 == $uid){
                $connections[] = $con->user2;
            }else{
                $connections[] = $con->user1;
            }
        }
        return $connections;
    }


    // Tasks near current user
    function get_nearby_tasks( $uid ){

        //User details
        $user          = User::find($uid);
        //User address details
        $place         = $user->primary_place();
        
        $distance     = env('DISTANCE', 5);
        $current_date = date('Y-m-d G:i:s');

        $tasks = [];
        $Query = "SELECT `users`.first_name, `users`.last_name, `users`.username, `users`.image_name,`place`.*,`tasks`.title,`tasks`.slug,`tasks`.created_at, `tasks`.updated_at, `tasks`.`id` as wID,".
                        $this->location_query($place['lat'], $place['lng']). '
                         JOIN `tasks` on `place`.`for_id` = `tasks`.`id`
                         JOIN `users` on `tasks`.`user_id` = `users`.`id`
                         WHERE `place`.`for` = "task" AND `tasks`.`user_id` != ' . $uid. '
                         HAVING distance < ' . $distance . "
                         LIMIT 0, 20 ";
            $Query = "SELECT * FROM (" . $Query . ") a GROUP BY task_id ORDER BY task_id DESC";

            $nearbytasks = DB::select(DB::raw($Query));

            foreach ($nearbytasks as $r) {
                $task = Task::find($r->task_id);
                $task_data['title']     = $task->title;
                $task_data['slug']      = $task->slug;
                $task_data['summary']   = $task->summary;
                $task_data['date']      = $task->created_at;
                $task_data['skills']    = $task->tags()->get()->toArray();
                $task_data['comments']  = $task->comments->toArray(); 
                $task_data['days_past'] = 'Raised ' . ((isset($task->created_at)) ? abs(floor((strtotime($task->created_at) - time())/60/60/24)) : 0) . ' days ago.' ;
                $tasks[] = $task_data;
            }

        return $tasks;

    }

    // Tasks near current user
    function get_nearby_tasks_for_group( $group_place, $group_tag, $task_status = null ){

        $uid          = $this->user_logged_in();

        $distance     = env('DISTANCE', 5);
        $current_date = date('Y-m-d G:i:s');

        $tasks = [];
        $Query = "SELECT `users`.first_name, `users`.last_name, `users`.username, `users`.image_name,`place`.*,`tasks`.title,`tasks`.slug,`tasks`.created_at, `tasks`.updated_at, `tasks`.`id` as wID,".
                        $this->location_query($group_place['lat'], $group_place['lng']). '
                         JOIN `tasks` on `place`.`for_id` = `tasks`.`id`
                         JOIN `task_tags` on `task_tags`.`task_id` = `tasks`.`id`
                         JOIN `users` on `tasks`.`user_id` = `users`.`id`
                         WHERE `place`.`for` = "task" AND `task_tags`.`tag` = "' . $group_tag->tag .'"'. ($task_status ? (' AND `task`.`status` = "'. $task_status .'"') : '') . '
                         HAVING distance < ' . $distance . "
                         LIMIT 0, 20 ";
        $Query = "SELECT * FROM (" . $Query . ") a GROUP BY task_id ORDER BY task_id DESC";

        $nearbytasks = DB::select(DB::raw($Query));

        foreach ($nearbytasks as $r) {
            $task = Task::find($r->task_id);
            $task_data['title']     = $task->title;
            $task_data['slug']      = $task->slug;
            $task_data['summary']   = $task->summary;
            $task_data['date']      = $task->created_at;
            $task_data['skills']    = $task->tags()->get()->toArray();
            $task_data['comments']  = $task->comments->toArray(); 
            $task_data['days_past'] = 'Raised ' . ((isset($task->created_at)) ? abs(floor((strtotime($task->created_at) - time())/60/60/24)) : 0) . ' days ago.' ;
            $tasks[] = $task_data;
        }

        return $tasks;

    }

    // Total number of tasks near current user
    function get_nearby_tasks_for_group_count( $group_place, $group_tag, $task_status = null ){

        $distance     = env('DISTANCE', 5);

        $Query = 'SELECT COUNT(1) AS `total` , `tasks`.`id` AS `task_id`, '. $this->location_query($group_place->lat, $group_place->lng) . '
                         JOIN `tasks` on `place`.`for_id` = `tasks`.`id`
                         JOIN `task_tags` on `task_tags`.`task_id` = `tasks`.`id`
                         JOIN `users` on `tasks`.`user_id` = `users`.`id`
                         WHERE `place`.`for` = "task" AND `task_tags`.`tag` = "' . $group_tag->tag .'"'. ($task_status ? (' AND `task`.`status` = "'. $task_status .'"') : '') . '
                         HAVING distance < ' . $distance . "
                         LIMIT 0, 20 ";
        $Query = "SELECT * FROM (" . $Query . ") a GROUP BY task_id ORDER BY task_id DESC";

        $total_tasks = DB::select(DB::raw($Query));

        return $total_tasks;

    }


    function get_nearby_members( $group_place ){

        $uid             = $this->user_logged_in();
        $connections     = get_user_connections($uid);
        $connections_ids = [];
        foreach ($connections as $friend) {
            if(!$friend->image_name) $friend->image_name = 'default.png';
            $friend->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($friend->image_name, 'medium');
            $connections_ids[] = $friend->id;
        }

        $connections_ids[] = $uid;

        // Nearby members
        $distance = env('DISTANCE', 5);
        do {
            $result = '';
            $Query =
                "SELECT `place`.*, `user_tags`.`tag`,`users`.`id` as uID," . $this->location_query($group_place->lat, $group_place->lng) . '
                     LEFT JOIN `user_tags` on `place`.`for_id` = `user_tags`.`user_id`
                     LEFT JOIN `users` on `place`.`for_id` = `users`.`id`
                     WHERE `for` = "user" AND `place`.`user_id` NOT IN (\'' . implode($connections_ids, "','") . '\')
                     HAVING distance < ' . $distance . '
                     Order BY distance ASC
                     LIMIT 0, 10';
            $Query = "SELECT * FROM (".$Query.") a GROUP BY for_id";

            $result = DB::select(DB::raw($Query));
            $distance = $distance + 10;
        } while (!$result);

        $members_nearby = array();

        foreach($result as $mn){
            $skill = '';
            $member = User::find($mn->uID);
            if(!$member->image_name) $member->image_name = 'default.png';
            $member->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($member->image_name, 'icon');

            foreach ($member->skills() as $s) {
                $skill .= $s->tag . ', ';
            }
            $member->skill_set = rtrim($skill, ', ');
            $members_nearby[] = $member;
        }

        return $members_nearby;
    }

    // Get the nerby members for a group
    function get_nearby_members_for_group( $group_place, $group_tag ){

        // Nearby members
        $distance = env('DISTANCE', 5);

        $result = '';
        $Query = "SELECT `place`.*, `user_tags`.`tag`,`users`.`id` as uID," . $this->location_query($group_place->lat, $group_place->lng) . '
                 LEFT JOIN `user_tags` on `place`.`for_id` = `user_tags`.`user_id`
                 LEFT JOIN `users` on `place`.`for_id` = `users`.`id`
                 WHERE `for` = "user" AND `user_tags`.`tag` = "' . $group_tag->tag . '"
                 HAVING distance < ' . $distance . '
                 Order BY distance ASC
                 LIMIT 0, 10';

        $Query = "SELECT * FROM (".$Query.") a GROUP BY for_id";

        $result = DB::select(DB::raw($Query));

        $members_nearby = array();

        foreach($result as $mn){
            $skill = '';
            $member = User::find($mn->uID);
            if(!$member->image_name) $member->image_name = 'default.png';
            $member->image_url = 'http://'.$_SERVER['HTTP_HOST'].user_avatar($member->image_name, 'icon');

            $member->skills   = $member->skills();
            $member->learns   = $member->learn();
            $members_nearby[] = $member;
        }

        return $members_nearby;
    }

    // Get the nerby members for a group
    function get_nearby_members_for_group_count( $group_place, $group_tag ){

        // Nearby members
        $distance = env('DISTANCE', 5);

        $result = '';
        $Query = 'SELECT COUNT(1) AS `total` , `place`.`for_id`, '. $this->location_query($group_place->lat, $group_place->lng) . '
                 LEFT JOIN `user_tags` on `place`.`for_id` = `user_tags`.`user_id`
                 LEFT JOIN `users` on `place`.`for_id` = `users`.`id`
                 WHERE `for` = "user" AND `user_tags`.`tag` = "' . $group_tag->tag . '"
                 HAVING distance < ' . $distance . '
                 Order BY distance ASC
                 LIMIT 0, 10';

        $Query = "SELECT * FROM (".$Query.") a GROUP BY for_id";

        $members_nearby_count = DB::select(DB::raw($Query));

        return $members_nearby_count;
    }

    public function location_query($latitude, $longitude ){

        return "( 3959 * acos( cos( radians(" . $latitude . ") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(" . $longitude . ") ) + sin( radians(" . $latitude . ") ) * sin( radians( lat ) ) ) ) AS distance FROM place" ;
    }

    public function place($address){
        $country = null;
        $city    = null;
        $place   = null;
        if($address) {
            $param           = array("address" => $address);
            $reponse         = Geocoder::geocode('json', $param);
            $geo             = json_decode($reponse);
            if($geo->status != 'OK'){
                return '';
            }

            foreach($geo->results[0]->address_components as $ac){
                if($ac->types[0] == 'country'){
                    $place['country'] =  $ac->long_name;
                }
                if($ac->types[0] == 'locality'){
                    $place['locality'] =  $ac->long_name;
                }
                if($ac->types[0] == 'sublocality'){
                    $place['sublocality'] =  $ac->long_name;
                }
                if($ac->types[0] == 'sublocality_level_2'){
                    $place['sublocality_level_2'] =  $ac->long_name;
                }
                if($ac->types[0] == 'sublocality_level_1'){
                    $place['sublocality_level_1'] =  $ac->long_name;
                }
                if($ac->types[0] == 'administrative_area_level_1'){
                    $place['administrative_area_level_1'] =  $ac->long_name;
                }
                if($ac->types[0] == 'postal_code'){
                    $place['postal_code'] =  $ac->long_name;
                }
                if($ac->types[0] == 'establishment'){
                    $place['establishment'] =  $ac->long_name;
                }
                if($ac->types[0] == 'route'){
                    $place['route'] =  $ac->long_name;
                }


            }
            $location                   = $geo->results[0]->geometry->location;
            $place['lat']               = $location->lat;
            $place['lng']               = $location->lng;
            $place['formatted_address'] = $geo->results[0]->formatted_address;

        }
        if(!$place)
        {
            return [];
        }else{
            return $place;
        }

    }

}