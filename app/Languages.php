<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Languages extends Model {

	// Set the table
	protected $table = 'languages';
	protected $guarded = []; 
	protected $fillable = ['alpha2', 'alpha3', 'english_name', 'created_at', 'updated_at'];
}
