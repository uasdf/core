<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageThreads extends Model {

	protected $table = 'message_threads';

	public function messages(){
		return $this->hasMany('App\Messages','thread_id')->orderBy('id','asc');
	}

	public function UnreadMessages(){
		return $this->hasMany('App\Messages','thread_id')->where('read', null);
	}

}
