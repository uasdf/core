<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLanguages extends Model {

	// Set the table
	protected $table = 'user_languages';

	protected $fillable = ['language'];
	protected $guarded = []; 

	public function user(){
        return $this->belongsTo('App\User');
    }
}
