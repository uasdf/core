<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ConnectionRequests extends Model {

    protected $table = 'connection_requests';

    protected $fillable = ['message'];


    public function sender(){
        return $this->belongsTo('App\User','from_user');
    }

    public function receiver(){
        return $this->belongsTo('App\User','to_user');
    }
}
