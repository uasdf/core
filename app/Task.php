<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;


class Task extends Model implements SluggableInterface{

	protected $table = 'tasks';

    public function places(){
        return $this->hasMany('App\Place','for_id')->where(['for' =>'task'])->orderBy('id','asc')->get();
    }

    public function primary_place(){
        return $this->hasMany('App\Place','for_id')->where(['for' =>'task','primary' => 1])->first();
    }

    //Relation - Has one creator(user_id)
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    //Relation - Solved by(user_id)
    public function solved_by_user()
    {
        return $this->belongsTo('App\User','solved_by','id');
    }

    //Relation - Has many tags
    public function tags()
    {
        return $this->hasMany('App\TaskTags')->where('tag_type','=','skill')->orderBy('level','desc');
    }

    //Relation - Has many comments
    public function comments()
    {
        return $this->hasMany('App\TaskComments')->orderBy('solution','desc');
    }

    //Relation - Has many requests
    public function handle_requests()
    {
        return $this->hasMany('App\TaskHandleRequest');
    }

    //To generate slugs
    use SluggableTrait;

    protected $sluggable = array(
        'build_from' => 'title',
        'save_to'    => 'slug',
    );

    protected $fillable = ['title','summary'];

}
