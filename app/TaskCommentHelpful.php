<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskCommentHelpful extends Model {

	protected $table= 'task_comment_helpful';

	public function user(){
		return $this->belongsTo('App\User');
	}

}
