<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskHandleRequest extends Model {

	protected $table = 'task_handle_request';

	public function user(){
		return $this->belongsTo('App\User');
	}

	public function task(){
		return $this->belongsTo('App\Task');
	}

	public function current(){
		return $this->hasMany('App\Task')->where('status','processing')->get();
	}


}
