<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IssueHandlingCancelMessage extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('issue_handle_request', function ($table) {
		    $table->string('cancel_reason', 500)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('issue_handle_request', function ($table) {
		    $table->dropColumn('cancel_reason');
		});
	}

}
