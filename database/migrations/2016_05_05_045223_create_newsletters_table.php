<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewslettersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('newsletters', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unique();
		    $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
		    // Weekly newsletter
		    $table->tinyInteger('weekly_newsletter');

		    // Messages
		    $table->tinyInteger('new_message');

		    // Tasks
		    $table->tinyInteger('task_comment');
		    $table->tinyInteger('task_handle_request');

		    // Profile
		    $table->tinyInteger('profile_connection');
		    $table->tinyInteger('profile_rating');


			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('newsletters');
	}

}
