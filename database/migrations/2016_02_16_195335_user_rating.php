<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserRating extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Creates the user_rating table
		Schema::create('user_rating', function ($table) {
		    $table->increments('id');
		    $table->integer('from_user');
		    $table->foreign('from_user')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
		    $table->integer('to_user');
		    $table->foreign('to_user')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
		    
		    $table->tinyInteger('rating');
		    $table->string('comment');
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Drop the user_rating table
		Schema::dropIfExists('user_rating');
	}

}
