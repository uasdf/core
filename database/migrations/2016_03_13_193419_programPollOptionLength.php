<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProgramPollOptionLength extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('workshop_poll', function ($table) {
		    $table->string('option1_1', 50)->change();
		    $table->string('option1_2', 50)->change();
		    $table->string('option1_3', 50)->change();
		    $table->string('option2_1', 50)->change();
		    $table->string('option2_2', 50)->change();
		    $table->string('option2_3', 50)->change();
		    $table->string('option3_1', 50)->change();
		    $table->string('option3_2', 50)->change();
		    $table->string('option3_3', 50)->change();
		    $table->string('option4_1', 50)->change();
		    $table->string('option4_2', 50)->change();
		    $table->string('option4_3', 50)->change();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
