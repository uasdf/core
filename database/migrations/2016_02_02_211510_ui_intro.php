<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
class UiIntro extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Creates the ui_introduction table
		Schema::create('ui_introduction', function ($table) {
		    $table->increments('id');
		    $table->integer('user_id')->unique();
		    $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');;
		    $table->tinyInteger('dashboard')->nullable();
		    $table->tinyInteger('issue_page')->nullable();
		    $table->tinyInteger('create_issue')->nullable();
		    $table->tinyInteger('program_page')->nullable();
		    $table->tinyInteger('create_program')->nullable();
		    $table->tinyInteger('near_me')->nullable();
		    $table->tinyInteger('search')->nullable();
		    $table->timestamps();
		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Drop the UI_introduction table
		Schema::dropIfExists('ui_introduction');
	}

}
