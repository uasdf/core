<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NoWorkshop extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		
		Schema::dropIfExists('workshop_attendees');
		Schema::dropIfExists('workshop_comment_reply');
		Schema::dropIfExists('workshop_comments');
		
		
		Schema::dropIfExists('workshop_poll_selections');
		Schema::dropIfExists('workshop_poll');
		
		Schema::dropIfExists('workshop_tags');
		Schema::dropIfExists('workshop_organizers');
		// Schema::table('place', function($table)
		// {
		//     $table->dropColumn('workshop_id');
		//     $table->enum('for', array('user', 'task'));
		// });
		Schema::dropIfExists('workshops');

		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
