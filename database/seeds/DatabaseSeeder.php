<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Languages;
use App\UserLanguages;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		// Seed the Languages
		$this->call('LanguagesTableSeeder');	

		// seed the user languages
		$this->call('UserLanguagesTableSeeder');
	}

}


class LanguagesTableSeeder extends Seeder {
 
   public function run()
   {
     //delete users table records
     DB::table('languages')->delete();
     // Defined the languages objects
     $languages_json = '[
     	{"alpha3":"aar","alpha2":"aa","English":"Afar"},
     	{"alpha3":"abk","alpha2":"ab","English":"Abkhazian"},{"alpha3":"afr","alpha2":"af","English":"Afrikaans"},{"alpha3":"aka","alpha2":"ak","English":"Akan"},
     	{"alpha3":"alb","alpha2":"sq","English":"Albanian"},{"alpha3":"amh","alpha2":"am","English":"Amharic"},{"alpha3":"ara","alpha2":"ar","English":"Arabic"},{"alpha3":"arg","alpha2":"an","English":"Aragonese"},{"alpha3":"arm","alpha2":"hy","English":"Armenian"},{"alpha3":"asm","alpha2":"as","English":"Assamese"},
     	{"alpha3":"ava","alpha2":"av","English":"Avaric"},{"alpha3":"ave","alpha2":"ae","English":"Avestan"},{"alpha3":"aym","alpha2":"ay","English":"Aymara"},{"alpha3":"aze","alpha2":"az","English":"Azerbaijani"},{"alpha3":"bak","alpha2":"ba","English":"Bashkir"},{"alpha3":"bam","alpha2":"bm","English":"Bambara"},{"alpha3":"baq","alpha2":"eu","English":"Basque"},{"alpha3":"bel","alpha2":"be","English":"Belarusian"},{"alpha3":"ben","alpha2":"bn","English":"Bengali"},{"alpha3":"bih","alpha2":"bh","English":"Bihari languages"},{"alpha3":"bis","alpha2":"bi","English":"Bislama"},{"alpha3":"bos","alpha2":"bs","English":"Bosnian"},{"alpha3":"bre","alpha2":"br","English":"Breton"},{"alpha3":"bul","alpha2":"bg","English":"Bulgarian"},{"alpha3":"bur","alpha2":"my","English":"Burmese"},{"alpha3":"cat","alpha2":"ca","English":"Catalan; Valencian"},{"alpha3":"cha","alpha2":"ch","English":"Chamorro"},{"alpha3":"che","alpha2":"ce","English":"Chechen"},{"alpha3":"chi","alpha2":"zh","English":"Chinese"},{"alpha3":"chu","alpha2":"cu","English":"Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic"},{"alpha3":"chv","alpha2":"cv","English":"Chuvash"},{"alpha3":"cor","alpha2":"kw","English":"Cornish"},{"alpha3":"cos","alpha2":"co","English":"Corsican"},{"alpha3":"cre","alpha2":"cr","English":"Cree"},{"alpha3":"cze","alpha2":"cs","English":"Czech"},{"alpha3":"dan","alpha2":"da","English":"Danish"},{"alpha3":"div","alpha2":"dv","English":"Divehi; Dhivehi; Maldivian"},{"alpha3":"dut","alpha2":"nl","English":"Dutch; Flemish"},{"alpha3":"dzo","alpha2":"dz","English":"Dzongkha"},{"alpha3":"eng","alpha2":"en","English":"English"},{"alpha3":"epo","alpha2":"eo","English":"Esperanto"},{"alpha3":"est","alpha2":"et","English":"Estonian"},{"alpha3":"ewe","alpha2":"ee","English":"Ewe"},{"alpha3":"fao","alpha2":"fo","English":"Faroese"},{"alpha3":"fij","alpha2":"fj","English":"Fijian"},{"alpha3":"fin","alpha2":"fi","English":"Finnish"},{"alpha3":"fre","alpha2":"fr","English":"French"},{"alpha3":"fry","alpha2":"fy","English":"Western Frisian"},{"alpha3":"ful","alpha2":"ff","English":"Fulah"},{"alpha3":"geo","alpha2":"ka","English":"Georgian"},{"alpha3":"ger","alpha2":"de","English":"German"},{"alpha3":"gla","alpha2":"gd","English":"Gaelic; Scottish Gaelic"},{"alpha3":"gle","alpha2":"ga","English":"Irish"},{"alpha3":"glg","alpha2":"gl","English":"Galician"},{"alpha3":"glv","alpha2":"gv","English":"Manx"},{"alpha3":"gre","alpha2":"el","English":"Greek, Modern (1453-)"},{"alpha3":"grn","alpha2":"gn","English":"Guarani"},{"alpha3":"guj","alpha2":"gu","English":"Gujarati"},{"alpha3":"hat","alpha2":"ht","English":"Haitian; Haitian Creole"},{"alpha3":"hau","alpha2":"ha","English":"Hausa"},{"alpha3":"heb","alpha2":"he","English":"Hebrew"},{"alpha3":"her","alpha2":"hz","English":"Herero"},{"alpha3":"hin","alpha2":"hi","English":"Hindi"},{"alpha3":"hmo","alpha2":"ho","English":"Hiri Motu"},{"alpha3":"hrv","alpha2":"hr","English":"Croatian"},{"alpha3":"hun","alpha2":"hu","English":"Hungarian"},{"alpha3":"ibo","alpha2":"ig","English":"Igbo"},{"alpha3":"ice","alpha2":"is","English":"Icelandic"},{"alpha3":"ido","alpha2":"io","English":"Ido"},{"alpha3":"iii","alpha2":"ii","English":"Sichuan Yi; Nuosu"},{"alpha3":"iku","alpha2":"iu","English":"Inuktitut"},{"alpha3":"ile","alpha2":"ie","English":"Interlingue; Occidental"},{"alpha3":"ina","alpha2":"ia","English":"Interlingua (International Auxiliary Language Association)"},{"alpha3":"ind","alpha2":"id","English":"Indonesian"},{"alpha3":"ipk","alpha2":"ik","English":"Inupiaq"},{"alpha3":"ita","alpha2":"it","English":"Italian"},{"alpha3":"jav","alpha2":"jv","English":"Javanese"},{"alpha3":"jpn","alpha2":"ja","English":"Japanese"},{"alpha3":"kal","alpha2":"kl","English":"Kalaallisut; Greenlandic"},{"alpha3":"kan","alpha2":"kn","English":"Kannada"},{"alpha3":"kas","alpha2":"ks","English":"Kashmiri"},{"alpha3":"kau","alpha2":"kr","English":"Kanuri"},{"alpha3":"kaz","alpha2":"kk","English":"Kazakh"},{"alpha3":"khm","alpha2":"km","English":"Central Khmer"},{"alpha3":"kik","alpha2":"ki","English":"Kikuyu; Gikuyu"},{"alpha3":"kin","alpha2":"rw","English":"Kinyarwanda"},{"alpha3":"kir","alpha2":"ky","English":"Kirghiz; Kyrgyz"},{"alpha3":"kom","alpha2":"kv","English":"Komi"},{"alpha3":"kon","alpha2":"kg","English":"Kongo"},{"alpha3":"kor","alpha2":"ko","English":"Korean"},{"alpha3":"kua","alpha2":"kj","English":"Kuanyama; Kwanyama"},{"alpha3":"kur","alpha2":"ku","English":"Kurdish"},{"alpha3":"lao","alpha2":"lo","English":"Lao"},{"alpha3":"lat","alpha2":"la","English":"Latin"},{"alpha3":"lav","alpha2":"lv","English":"Latvian"},{"alpha3":"lim","alpha2":"li","English":"Limburgan; Limburger; Limburgish"},{"alpha3":"lin","alpha2":"ln","English":"Lingala"},{"alpha3":"lit","alpha2":"lt","English":"Lithuanian"},{"alpha3":"ltz","alpha2":"lb","English":"Luxembourgish; Letzeburgesch"},{"alpha3":"lub","alpha2":"lu","English":"Luba-Katanga"},{"alpha3":"lug","alpha2":"lg","English":"Ganda"},{"alpha3":"mac","alpha2":"mk","English":"Macedonian"},{"alpha3":"mah","alpha2":"mh","English":"Marshallese"},{"alpha3":"mal","alpha2":"ml","English":"Malayalam"},{"alpha3":"mao","alpha2":"mi","English":"Maori"},{"alpha3":"mar","alpha2":"mr","English":"Marathi"},{"alpha3":"may","alpha2":"ms","English":"Malay"},{"alpha3":"mlg","alpha2":"mg","English":"Malagasy"},{"alpha3":"mlt","alpha2":"mt","English":"Maltese"},{"alpha3":"mon","alpha2":"mn","English":"Mongolian"},{"alpha3":"nau","alpha2":"na","English":"Nauru"},{"alpha3":"nav","alpha2":"nv","English":"Navajo; Navaho"},{"alpha3":"nbl","alpha2":"nr","English":"Ndebele, South; South Ndebele"},{"alpha3":"nde","alpha2":"nd","English":"Ndebele, North; North Ndebele"},{"alpha3":"ndo","alpha2":"ng","English":"Ndonga"},{"alpha3":"nep","alpha2":"ne","English":"Nepali"},{"alpha3":"nno","alpha2":"nn","English":"Norwegian Nynorsk; Nynorsk, Norwegian"},{"alpha3":"nob","alpha2":"nb","English":"Bokmål, Norwegian; Norwegian Bokmål"},{"alpha3":"nor","alpha2":"no","English":"Norwegian"},{"alpha3":"nya","alpha2":"ny","English":"Chichewa; Chewa; Nyanja"},{"alpha3":"oci","alpha2":"oc","English":"Occitan (post 1500); Provençal"},{"alpha3":"oji","alpha2":"oj","English":"Ojibwa"},{"alpha3":"ori","alpha2":"or","English":"Oriya"},{"alpha3":"orm","alpha2":"om","English":"Oromo"},{"alpha3":"oss","alpha2":"os","English":"Ossetian; Ossetic"},{"alpha3":"pan","alpha2":"pa","English":"Panjabi; Punjabi"},{"alpha3":"per","alpha2":"fa","English":"Persian"},{"alpha3":"pli","alpha2":"pi","English":"Pali"},{"alpha3":"pol","alpha2":"pl","English":"Polish"},{"alpha3":"por","alpha2":"pt","English":"Portuguese"},{"alpha3":"pus","alpha2":"ps","English":"Pushto; Pashto"},{"alpha3":"que","alpha2":"qu","English":"Quechua"},{"alpha3":"roh","alpha2":"rm","English":"Romansh"},{"alpha3":"rum","alpha2":"ro","English":"Romanian; Moldavian; Moldovan"},{"alpha3":"run","alpha2":"rn","English":"Rundi"},{"alpha3":"rus","alpha2":"ru","English":"Russian"},{"alpha3":"sag","alpha2":"sg","English":"Sango"},{"alpha3":"san","alpha2":"sa","English":"Sanskrit"},{"alpha3":"sin","alpha2":"si","English":"Sinhala; Sinhalese"},{"alpha3":"slo","alpha2":"sk","English":"Slovak"},{"alpha3":"slv","alpha2":"sl","English":"Slovenian"},{"alpha3":"sme","alpha2":"se","English":"Northern Sami"},{"alpha3":"smo","alpha2":"sm","English":"Samoan"},{"alpha3":"sna","alpha2":"sn","English":"Shona"},{"alpha3":"snd","alpha2":"sd","English":"Sindhi"},{"alpha3":"som","alpha2":"so","English":"Somali"},{"alpha3":"sot","alpha2":"st","English":"Sotho, Southern"},{"alpha3":"spa","alpha2":"es","English":"Spanish; Castilian"},{"alpha3":"srd","alpha2":"sc","English":"Sardinian"},{"alpha3":"srp","alpha2":"sr","English":"Serbian"},{"alpha3":"ssw","alpha2":"ss","English":"Swati"},{"alpha3":"sun","alpha2":"su","English":"Sundanese"},{"alpha3":"swa","alpha2":"sw","English":"Swahili"},{"alpha3":"swe","alpha2":"sv","English":"Swedish"},{"alpha3":"tah","alpha2":"ty","English":"Tahitian"},{"alpha3":"tam","alpha2":"ta","English":"Tamil"},{"alpha3":"tat","alpha2":"tt","English":"Tatar"},{"alpha3":"tel","alpha2":"te","English":"Telugu"},{"alpha3":"tgk","alpha2":"tg","English":"Tajik"},{"alpha3":"tgl","alpha2":"tl","English":"Tagalog"},{"alpha3":"tha","alpha2":"th","English":"Thai"},{"alpha3":"tib","alpha2":"bo","English":"Tibetan"},{"alpha3":"tir","alpha2":"ti","English":"Tigrinya"},{"alpha3":"ton","alpha2":"to","English":"Tonga (Tonga Islands)"},{"alpha3":"tsn","alpha2":"tn","English":"Tswana"},{"alpha3":"tso","alpha2":"ts","English":"Tsonga"},{"alpha3":"tuk","alpha2":"tk","English":"Turkmen"},{"alpha3":"tur","alpha2":"tr","English":"Turkish"},{"alpha3":"twi","alpha2":"tw","English":"Twi"},{"alpha3":"uig","alpha2":"ug","English":"Uighur; Uyghur"},{"alpha3":"ukr","alpha2":"uk","English":"Ukrainian"},{"alpha3":"urd","alpha2":"ur","English":"Urdu"},{"alpha3":"uzb","alpha2":"uz","English":"Uzbek"},{"alpha3":"ven","alpha2":"ve","English":"Venda"},{"alpha3":"vie","alpha2":"vi","English":"Vietnamese"},{"alpha3":"vol","alpha2":"vo","English":"Volapük"},{"alpha3":"wel","alpha2":"cy","English":"Welsh"},{"alpha3":"wln","alpha2":"wa","English":"Walloon"},{"alpha3":"wol","alpha2":"wo","English":"Wolof"},{"alpha3":"xho","alpha2":"xh","English":"Xhosa"},{"alpha3":"yid","alpha2":"yi","English":"Yiddish"},{"alpha3":"yor","alpha2":"yo","English":"Yoruba"},{"alpha3":"zha","alpha2":"za","English":"Zhuang; Chuang"},{"alpha3":"zul","alpha2":"zu","English":"Zulu"}]';

 	$languages = json_decode($languages_json);
     // Seed the database
	 foreach ($languages as $lng) {
		DB::table('languages')->insert(array('alpha2' => $lng->alpha2, 'alpha3' => $lng->alpha3, 'english_name' => $lng->English, 'created_at' => getdate(), 'updated_at' => getdate() ));
   	 }
   }

}


class UserLanguagesTableSeeder extends Seeder {
 
   public function run()
   {
     //delete users table records
     DB::table('user_languages')->delete();

     // Get all the users
	 $users = User::all();     	

     // Seed the language for each users
     // Default - english
	 foreach ($users as $user) {
		DB::table('user_languages')->insert(array('language' => 'English', 'user_id' => $user->id, 'created_at' => getdate(), 'updated_at' => getdate() ));
   	 }
   }

}