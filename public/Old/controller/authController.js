(function() {

    'use strict';
    angular
        .module('Placerange')
        .controller('AuthController', AuthController);

    function AuthController($scope, $auth, $state, $http, $rootScope, $location) {

        $scope.current_url    = $location.absUrl();
        $scope.arr            = $scope.current_url.split("/");
        $rootScope.domain_url = $scope.arr[0] + "//" + $scope.arr[2];

        if ($auth.isAuthenticated()) {
            $rootScope.authenticated = true;
            $rootScope.currentUser   = JSON.parse(localStorage.getItem('user'));
            $scope.currentUser       = JSON.parse(localStorage.getItem('user'));
          }

        var vm = this;
        
        vm.login = function(post_register) {
            var credentials = {
                email: vm.email,
                password: vm.password
            }

            if(post_register === true){
                credentials = {
                    email: $scope.register.email,
                    password: $scope.register.password
                }
            }
            $scope.has_error = false;
            
            // Use Satellizer's $auth service to login
            $auth.login(credentials).then(function(response) {

                // Return an $http request for the now authenticated
                // user so that we can flatten the promise chain
                return $http.get($rootScope.domain_url + '/api/authenticate/user');
            }, function(error) {

                $scope.has_error = true;
                if(error.data.error == 'invalid_credentials'){
                    $scope.error_message = 'Invalid Credentials.';
                } else if(error.data.error == 'could_not_create_token'){
                    $scope.error_message = 'Could not create a session. Please try again later.';
                } else{
                    $scope.error_message = 'Some error has occured.'; 
                }
                
            // Because we returned the $http.get request in the $auth.login
            // promise, we can chain the next promise to the end here
            })
            .then(function (response){

                // The user's authenticated state gets flipped to
                // true so we can now show parts of the UI that rely
                // on the user being logged in
                $rootScope.authenticated = true;

                // Putting the user's data on $rootScope allows
                // us to access it anywhere across the app
                $rootScope.currentUser = response.data.user;
                $scope.currentUser = response.data.user;

                // Stringify the returned data to prepare it
                // to go into local storage
                var user = JSON.stringify(response.data.user);

                // Set the stringified user data into local storage
                localStorage.setItem('user', user);

                // If login is successful, redirect to the users state
                $state.go('home', {});

            });
        }


        $scope.register = {
            first_name:            '',
            last_name:             '',
            email:                 '',
            place:                 '',
            password:              '',
            password_confirmation: '',

        };
        $scope.registerUser = function(){

            var register_data = {
                first_name:            $scope.register.first_name,
                last_name:             $scope.register.last_name,
                email:                 $scope.register.email,
                place:                 $scope.register.place,
                password:              $scope.register.password,
                password_confirmation: $scope.register.password_confirmation
            }

            $http.post($rootScope.domain_url + '/api/register', register_data)
            .then(function mySucces(response) {
                // Data obtained from the database
                // User counts
                // Map user data
                if(response.data.success == 'success'){
                    vm.login(true);
                    return;
                }

                if(response.data.error == 'error'){
                    $scope.errors = response.data.errors;
                }

            },

            function myError(response) {
                alert('Some error has occured.');
            }

            );
        }

    }

    


})();