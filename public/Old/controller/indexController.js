(function() {

    'use strict';

    angular
        .module('Placerange')
        .controller('indexController', function($scope, $http, $rootScope, $location) {
        
        $scope.current_url    = $location.absUrl();
        $scope.arr            = $scope.current_url.split("/");
        $rootScope.domain_url = $scope.arr[0] + "//" + $scope.arr[2];

        $http({
        	method	:'get',
        	url		: $rootScope.domain_url + '/api/index_page_data'
        })
        .then(function mySucces(response) {
        	// Data obtained from the database
        	// User counts
        	// Map user data
            $scope.index_page_data = response.data;

            // todays date for footer copyright
            $scope.today = new Date();
        },

        function myError(response) {
            $scope.index_page_data = 0;
        }

        );
    });

})();