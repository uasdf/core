(function() {

'use strict';

angular
    .module('Placerange')

    .controller('notificationController', function($scope, $http, $filter, $timeout, $stateParams, ngDialog, $state, $location, $rootScope) {


        $scope.current_url    = $location.absUrl();
        $scope.arr            = $scope.current_url.split("/");
        $rootScope.domain_url = $scope.arr[0] + "//" + $scope.arr[2];

        $http({
            method  :'post',
            url     :$rootScope.domain_url + '/api/notifications'
            })
            .then(function mySucces(response) {
                // Data obtained from the database
                // User counts
                // Map user data

                $scope.notification_data = response.data;

            },

            function myError(response) {
                alert('Some error has occured.');
            }

        );


        $scope.navigateTo = function(target_location){
          $location.path(target_location);

        }


    });
})();