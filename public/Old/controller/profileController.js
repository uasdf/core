(function() {

    'use strict';

    angular
        .module('Placerange')

        .controller('profileController', function($scope, $http, $filter, Upload, $timeout, $stateParams, $auth, $rootScope, $location) {
            

            $scope.current_url    = $location.absUrl();
            $scope.arr            = $scope.current_url.split("/");
            $rootScope.domain_url = $scope.arr[0] + "//" + $scope.arr[2];

            $http({
                method  :'get',
                url     : $rootScope.domain_url + '/api/user_homepage_data'
            })
            .then(function mySucces(response) {
                // Data obtained from the database
                // User counts
                // Map user data
                $scope.user_homepage_data = response.data;

                if( $scope.user_homepage_data.user.status == undefined){
                    $scope.user_homepage_data.user.status = 'What are you upto?';
                }

                $scope.current_user = $scope.user_homepage_data.user;

                // todays date for footer copyright
                $scope.today = new Date();
            },

            function myError(response) {
                $scope.index_page_data = 0;
            }

            );

            // Skill level function
              $scope.skill_levels = [
                {value: 2, text: 'Novice'},
                {value: 3, text: 'Intermediate'},
                {value: 4, text: 'Expert'}
              ]; 

              $scope.learn_levels = [
                {value: 1, text: 'Beginner'},
                {value: 2, text: 'Novice'},
                {value: 3, text: 'Intermediate'}
              ]; 

              $scope.showSkillLevel = function($level) {
                var selected = $filter('filter')($scope.skill_levels, {value: $level});
                return ($level && selected.length) ? selected[0].text : 'Not set';
              };
              $scope.showLearnLevel = function($level) {
                var selected = $filter('filter')($scope.learn_levels, {value: $level});
                return ($level && selected.length) ? selected[0].text : 'Not set';
              };

            // Update user function
            $scope.updateUser = function() {
                $http.post($rootScope.domain_url + '/api/user_update_profile', $scope.user_homepage_data.user);
            };

            // Update Skill function
            $scope.updateSkill = function( $skill_id, $skill_level) {
                
                var skill_data = {
                    pk:    $skill_id,
                    skill: $skill_level
                };
                $http.post($rootScope.domain_url + '/api/user_update_skill_level', skill_data);
            };

            // Update Skill function
            $scope.updateLanguage = function( $language_id, $language) {
                
                var language_data = {
                    pk:       $language_id,
                    language: $language
                };
                $http.post($rootScope.domain_url + '/api/user_update_language', language_data);
            };

            $scope.new_skill_level = 2;

            // Add Skill function
            $scope.addSkill = function() {
                
                if($scope.new_skill === undefined || $scope.new_skill === ''){
                    return;
                }
                var already_exist = false;
                angular.forEach($scope.user_homepage_data.skills, function(value, key) {
                  if(value.tag == $scope.new_skill){
                       alert('You have already added this skill.'); 
                       already_exist = true;
                  }
                });
                if(already_exist === false){
                    var new_skill_data = {
                        skill: $scope.new_skill,
                        level: $scope.new_skill_level,
                        type: 'skill'
                    };
                    $http.post($rootScope.domain_url + '/api/user_add_skill', new_skill_data)
                    .then(function addSkillSuccess(response){
                        
                            // Reset the levels
                            $scope.new_skill       = '';
                            $scope.new_skill_level = 2;

                            // Hide the add skill form
                            $scope.show_add_skill_form = !$scope.show_add_skill_form;

                            // Reset the 
                            $scope.user_homepage_data.skills = response.data;                        
                    });
                }
            };

            $scope.new_learn_level = 1;
            // Add Learn function
            $scope.addLearn = function() {
                
                if($scope.new_learn === undefined || $scope.new_learn === ''){
                    return;
                }
                var already_exist = false;
                angular.forEach($scope.user_homepage_data.learn, function(value, key) {
                  if(value.tag == $scope.new_learn){
                       alert('You have already added this skill.'); 
                       already_exist = true;
                  }
                });
                if(already_exist === false){
                    var new_learn_data = {
                        skill: $scope.new_learn,
                        level: $scope.new_learn_level,
                        type: 'learn'
                    };
                    $http.post($rootScope.domain_url + '/api/user_add_skill', new_learn_data)
                    .then(function addLearnSuccess(response){
                        
                            // Reset the levels
                            $scope.new_learn       = '';
                            $scope.new_learn_level = 2;

                            // Hide the add skill form
                            $scope.show_add_learn_form = !$scope.show_add_learn_form;

                            // Reset the skills
                            $scope.user_homepage_data.learn = response.data;                        
                    });
                }
            };

            // Remove Skill function
            $scope.removeSkill = function( $skill_id, $tag_type) {
                    
                var skill_data = {
                    skill_id: $skill_id
                };
                $http.post($rootScope.domain_url + '/api/user_remove_skill', skill_data)
                .then(function removeSkillSuccess(response){
                    
                    if($tag_type == 'skill'){
                        // Reset the skills
                        $scope.user_homepage_data.skills = response.data;  
                    } else{
                        // Reset the learn
                        $scope.user_homepage_data.learn = response.data;  
                    }
                });
            };

            // Add place function
            $scope.addPlace = function() {
                    
                var place_data = {
                    place: $scope.new_place
                };
                $http.post($rootScope.domain_url + '/api/user_add_place', place_data)
                .then(function addPlaceSuccess(response){
                    
                    if(response.data.error == 'invalid_place'){
                        alert('Please enter a proper place.');
                    }

                    // Reset the new place textbox value
                    $scope.new_place = '';
                    // hide the form
                    $scope.show_add_place_form = !$scope.show_add_place_form;
                    // Update the places
                    $scope.user_homepage_data.places        = response.data.places;
                });
            };

            // Update place function
            $scope.updatePlace = function( $place_id, $address) {
                    
                var place_data = {
                    place_id: $place_id,
                    place: $address
                };
                $http.post($rootScope.domain_url + '/api/user_update_place', place_data)
                .then(function updatePlaceSuccess(response){
                    
                    if(response.data.error == 'invalid_place'){
                        alert('Please enter a proper place.');
                        return;
                    }

                    // Update the places
                    $scope.user_homepage_data.places        = response.data.places;
                    $scope.user_homepage_data.primary_place = response.data.primary_place;
                });
            };

            // Update place function
            $scope.removePlace = function( $place_id) {
                    
                var place_data = {
                    place_id: $place_id
                };
                $http.post($rootScope.domain_url + '/api/user_remove_place', place_data)
                .then(function removePlaceSuccess(response){
                    // Update the places
                    $scope.user_homepage_data.places        = response.data.places;
                });
            };

            // upload on file select or drop
            $scope.uploadUserImage = function (file, errFiles) {
                $scope.f = file;
                $scope.errFile = errFiles && errFiles[0];
                if (file) {
                    file.upload = Upload.upload({
                        url: $rootScope.domain_url + '/api/user_upload_image',
                        data: {user_image: file}
                    });

                    file.upload.then(function (response) {
                        $timeout(function () {
                            file.result = response.data;
                            $scope.user_homepage_data.user.image_url = response.data.image_url;
                        });
                    }, function (response) {
                        if (response.status > 0)
                            $scope.errorMsg = response.status + ': ' + response.data;
                    }, function (evt) {
                        file.progress = Math.min(100, parseInt(100.0 * 
                                                 evt.loaded / evt.total));
                    });
                }  
            };




        });

})();