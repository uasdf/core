(function() {

    'use strict';

    angular
        .module('Placerange')

        .controller('publicProgramController', function($scope, $http, $filter, Upload, $timeout, $stateParams, ngDialog, $rootScope, $location) {


            $scope.current_url    = $location.absUrl();
            $scope.arr            = $scope.current_url.split("/");
            $rootScope.domain_url = $scope.arr[0] + "//" + $scope.arr[2];


            var slug = {
                slug : $stateParams.slug
            };
            $http.post($rootScope.domain_url + '/api/program_publicpage_data', slug)
            .then(function mySucces(response) {
                // Data obtained from the database
                // User counts
                // Map user data

                $scope.program_data = response.data;

            },

            function myError(response) {
                alert('Some error has occured.');
            }

            );


            // Skill level function
              $scope.skill_levels = [
                {value: 1, text: 'Beginner'},
                {value: 2, text: 'Novice'},
                {value: 3, text: 'Intermediate'},
                {value: 4, text: 'Expert'}
              ]; 

              $scope.showSkillLevel = function($level) {
                var selected = $filter('filter')($scope.skill_levels, {value: $level});
                return ($level && selected.length) ? selected[0].text : 'Not set';
              };

            // Post comment function
            $scope.addComment = function ($comment) {

                if($comment == ''){
                    return;
                }

                var comment_data = {
                    comment: $comment,
                    program_id: $scope.program_data.program.id
                };

                $http.post($rootScope.domain_url + '/api/program_add_new_comment', comment_data)
                .then(function mySucces(response) {
                    // Data obtained from the database
                    $scope.program_data.comments = response.data.comments;
                    $scope.new_comment = '';
                });
            };

            // Show the reply box when reply button is clicked
            $scope.showEditCommentBox = function ( $comment, $comment_id) {
                $scope.current_comment_id = $comment_id;
                $scope.current_edit_comment = $comment;
                $scope.edit_comment_box = ngDialog.open({ 
                    template: 'template_edit_comment', 
                    className: 'ngdialog-theme-default',
                    scope: $scope });
                return false;
            };

            // Edit comment function
            $scope.postEditComment = function ($comment, $comment_id) {

                if($comment == ''){
                    return;
                }
                var comment_data = {
                    comment: $comment,
                    comment_id: $comment_id
                };

                $http.post($rootScope.domain_url + '/api/program_edit_comment', comment_data)
                .then(function mySucces(response) {
                    // Data obtained from the database
                    $scope.program_data.comments = response.data.comments;
                    ngDialog.close($scope.edit_comment_box.id);

                    
                });
            };

            // Remove comment function
            $scope.removeComment = function ($comment_id) {

                var comment_data = {
                    comment_id: $comment_id
                };

                $http.post($rootScope.domain_url + '/api/program_remove_comment', comment_data)
                .then(function mySucces(response) {
                    // Data obtained from the database
                    $scope.program_data.comments = response.data.comments;                    
                });
            };

            // Show the reply box when reply button is clicked
            $scope.showReplyBox = function ( $comment_id) {
                $scope.reply_comment_id = $comment_id;
                $scope.reply_box = ngDialog.open({ 
                    template: 'template_reply', 
                    className: 'ngdialog-theme-default',
                    scope: $scope });
            };

            // Post connect function
            $scope.postReply = function ($reply, $comment_id) {

                if($reply == ''){
                    return;
                }

                var reply_data = {
                    reply: $reply,
                    comment_id: $comment_id
                };

                $http.post($rootScope.domain_url + '/api/program_reply_to_comment', reply_data)
                .then(function mySucces(response) {
                    // Data obtained from the database
                    $scope.program_data.comments = response.data.comments;

                    ngDialog.close($scope.reply_box.id);

                    
                });
        };

            // Show the reply box when reply button is clicked
            $scope.showEditReplyBox = function ( $reply, $reply_id) {
                $scope.current_reply_id = $reply_id;
                $scope.current_edit_reply = $reply;
                $scope.edit_reply_box = ngDialog.open({ 
                    template: 'template_edit_reply', 
                    className: 'ngdialog-theme-default',
                    scope: $scope });
            };

            // Post connect function
            $scope.postEditReply = function ($reply, $reply_id) {

                if($reply == ''){
                    return;
                }
                var reply_data = {
                    reply: $reply,
                    reply_id: $reply_id
                };

                $http.post($rootScope.domain_url + '/api/program_edit_reply', reply_data)
                .then(function mySucces(response) {
                    // Data obtained from the database
                    $scope.program_data.comments = response.data.comments;
                    ngDialog.close($scope.edit_reply_box.id);

                    
                });
            };

            // Remove comment function
            $scope.removeReply = function ($reply_id) {

                var reply_data = {
                    reply_id: $reply_id
                };

                $http.post($rootScope.domain_url + '/api/program_remove_reply', reply_data)
                .then(function mySucces(response) {
                    // Data obtained from the database
                    $scope.program_data.comments = response.data.comments;
                });
            };


            // Remove comment function
            $scope.attendProgram = function () {

                var program_data = {
                    program_id: $scope.program_data.program.id
                };

                $http.post($rootScope.domain_url + '/api/program_attend', program_data)
                .then(function mySucces(response) {
                    // Data obtained from the database

                    $scope.program_data.attendees                 = response.data.attendees;
                    $scope.program_data.future_program            = response.data.future_program;
                    $scope.program_data.formatted_program_date    = response.data.formatted_program_date;
                    $scope.program_data.current_user_is_attending = response.data.current_user_is_attending;
                    $scope.program_data.seatsLeft                 = response.data.seatsLeft;
                });
            };

            // Take poll function
            $scope.takePoll = function () {

                var poll_data = {
                    poll_id: $scope.program_data.poll.id,
                    option1: $scope.poll_option1,
                    option2: $scope.poll_option2,
                    option3: $scope.poll_option3,
                    option4: $scope.poll_option4
                };

                $http.post($rootScope.domain_url + '/api/program_post_poll', poll_data)
                .then(function mySucces(response) {
                    // Data obtained from the database

                    if(response.data.error == 'not_selected'){
                        alert('Please make a selection.');
                    } else{
                        $scope.program_data.poll        = response.data.poll;
                        $scope.program_data.currentVote = response.data.currentVote;
                        $scope.program_data.option      = response.data.option;
                    }
                });
            };

            // Finalize timings function
            $scope.finalizePoll = function () {

                var poll_data = {
                    poll_id: $scope.program_data.poll.id,
                    final_option: $scope.final_option
                };

                $http.post($rootScope.domain_url + '/api/program_finalize_date', poll_data)
                .then(function mySucces(response) {
                    // Data obtained from the database
                    $scope.program_data.program                = response.data.program;
                    $scope.program_data.formatted_program_date = response.data.formatted_program_date;
                });
            };





        });

})();