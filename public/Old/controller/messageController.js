(function() {

'use strict';

angular
    .module('Placerange')

    .controller('messageController', function($scope, $http, $filter, $timeout, $stateParams, ngDialog, $state, $location, $rootScope) {

        $scope.current_url    = $location.absUrl();
        $scope.arr            = $scope.current_url.split("/");
        $rootScope.domain_url = $scope.arr[0] + "//" + $scope.arr[2];

        $http({
            method  :'post',
            url     :$rootScope.domain_url + '/api/messages'
            })
            .then(function mySucces(response) {
                // Data obtained from the database
                // User counts
                // Map user data

                $scope.message_data = response.data;
                var first_thread_user = $scope.message_data.threads[0].user;
                $scope.selectThread(first_thread_user);


            },

            function myError(response) {
                alert('Some error has occured.');
            }

        );

        $scope.search = "";

        $scope.selectThread = function( $user ) {

            ngDialog.close($scope.new_message_data);

            $scope.new_user_message      = false;
            $scope.selected_user = $user;

            $scope.messageAutoload($user);
        }

        $scope.messageAutoload = function( $user ) {

            $http.post($rootScope.domain_url + '/api/load_messages', {for_user : $user.id})
            .then(function mySucces(response) {
                $scope.message_data.messages = response.data.messages;
                $scope.message_data.thread   = response.data.thread;
            },

            function myError(response) {
                alert('Some error has occured.');
            }

            );
        }

        $scope.postMessage = function() {

            if($scope.new_message == ''){
                return;
            }

            if($scope.new_user_message === true){
                $scope.postNewMessage();
                return;
            }
            $scope.new_user_message = false;

            var new_message_data = {
                to_user : $scope.selected_user.id,
                'message' : $scope.new_message
            };
            $http.post($rootScope.domain_url + '/api/ajax_post_message', new_message_data)
            .then(function mySucces(response) {
                $scope.message_data.messages = response.data.messages;
                $scope.message_data.thread   = response.data.thread;

                $scope.new_message = '';

            },

            function myError(response) {
                alert('Some error has occured.');
            }

            );
        }

        // Show the reply box when reply button is clicked
        $scope.showNewMessageBox = function () {
            $scope.new_message_data = ngDialog.open({ 
                template: 'template_new_message', 
                className: 'ngdialog-theme-default',
                scope: $scope });
            return false;
        };


        // Show the reply box when reply button is clicked
        $scope.showSearchMessageBox = function () {
            $scope.search_message_data = ngDialog.open({ 
                template: 'template_select_thread', 
                className: 'ngdialog-theme-default',
                scope: $scope });
            return false;
        };

        $scope.postNewMessage = function() {

            var new_message_data = {
                to_user : $scope.selected_user.id,
                'message' : $scope.new_message
            };
            $http.post($rootScope.domain_url + '/api/post_new_message', new_message_data)
            .then(function mySucces(response) {
                $scope.message_data = response.data;
                var first_thread_user = $scope.message_data.threads[0].user;
                $scope.selectThread(first_thread_user);

            },

            function myError(response) {
                alert('Some error has occured.');
            }

            );
        }

        $scope.newThread = function( $user ) {

            $scope.selected_user         = $user;
            $scope.new_user_message      = true;
            $scope.message_data.messages = {};
            ngDialog.close($scope.new_message_data);
        }


    });
})();