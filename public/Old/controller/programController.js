(function() {

    'use strict';

    angular
        .module('Placerange')

        .controller('programController', function($scope, $http, $filter, Upload, $timeout, $stateParams, ngDialog, $state, $location, $rootScope) {

            $scope.current_url    = $location.absUrl();
            $scope.arr            = $scope.current_url.split("/");
            $rootScope.domain_url = $scope.arr[0] + "//" + $scope.arr[2];

            $scope.format = 'd MMM yyyy';
            $scope.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1,
            type: 'date'
            };
            $scope.update_date1_1 = false;

            $scope.create_date1_1 = false;
            $scope.create_date2_1 = false;
            $scope.create_date3_1 = false;
            $scope.create_date4_1 = false;

            $scope.get_sql_datetime = function(date) {

                if(date === null || date ==='' || date === undefined){
                    return '';
                }
                return date.getFullYear() + '-' +
                      ('00' + (date.getMonth()+1)).slice(-2) + '-' +
                      ('00' + date.getDate()).slice(-2) + ' ' + 
                      ('00' + date.getHours()).slice(-2) + ':' + 
                      ('00' + date.getMinutes()).slice(-2) + ':' + 
                      ('00' + date.getSeconds()).slice(-2);
            }

            // Get the program slug
            var slug = {
                slug : $stateParams.slug
            };

            if($stateParams.slug !== undefined && $state.current.name != 'program_create'){

                $http.post($rootScope.domain_url + '/api/program_data', slug)
                .then(function mySucces(response) {
                    // Data obtained from the database
                    $scope.program_data = response.data;
                    
                    $scope.program_data.program.date       = new Date($scope.program_data.program.date);
                    $scope.program_data.program.time_start = new Date($scope.program_data.program.time_start);
                    $scope.program_data.program.time_end   = new Date($scope.program_data.program.time_end);
                    
                },

                function myError(response) {
                    alert('Some error has occured.');
                }

                );
            }

            // check if the state is create new program
            if($state.current.name == 'program_create'){

                $http.post($rootScope.domain_url + '/api/program_create_data')
                .then(function mySucces(response) {
                    // Data obtained from the database
                    // User counts
                    // Map user data

                    $scope.new_program               = response.data;
                    $scope.new_program.title         = '';
                    $scope.new_program.summary       = '';
                    $scope.new_program.max_attendees = '';
                    $scope.new_program.venue         = '';
                    $scope.new_program.poll          = false;
                    $scope.new_program.new_contact   = true;
                    $scope.new_program.fee           = '';
                    $scope.new_program.fee           = '';

                    $scope.new_program.option1_1 = null;
                    $scope.new_program.option1_2 = null;
                    $scope.new_program.option1_3 = null;

                    $scope.new_program.date = new Date();
                    $scope.new_program.date.setDate($scope.new_program.date.getDate() + 7);
                    $scope.new_program.time_start = '2016-03-10T09:00:00.000Z';
                    $scope.new_program.time_end   = '2016-03-10T12:00:00.000Z';
                    
                },

                function myError(response) {
                    alert('Some error has occured.');
                }

                );
            }

             // Create program function
            $scope.createProgram = function() {

                var create_data = angular.copy($scope.new_program);
                create_data.date = $scope.get_sql_datetime(create_data.date);
                if($scope.new_program.poll){
                    create_data.date = $scope.get_sql_datetime(create_data.option1_1);
                    create_data.date = $scope.get_sql_datetime(create_data.option2_1);
                    create_data.date = $scope.get_sql_datetime(create_data.option3_1);
                    create_data.date = $scope.get_sql_datetime(create_data.option4_1);
                }

                $http.post($rootScope.domain_url + '/api/program_create', $scope.new_program)
                .then(function programCreated(response){
                    if(response.data.success == 'success'){

                        $location.path('program/' + response.data.slug); 
                        return;
                    }

                    // Some error has occured
                    if(response.data.error == 'error'){
                        $scope.errors = response.data.errors;
                    }
                });
            };

              // Skill level function
              $scope.skill_levels = [
                {value: 2, text: 'Novice'},
                {value: 3, text: 'Intermediate'},
                {value: 4, text: 'Expert'}
              ]; 

              $scope.showSkillLevel = function($level) {
                var selected = $filter('filter')($scope.skill_levels, {value: $level});
                return ($level && selected.length) ? selected[0].text : 'Not set';
              };
              
              // Update user function
            $scope.updateProgram = function() {

                var update_data = angular.copy($scope.program_data.program);
                update_data.date = $scope.get_sql_datetime(update_data.date);

                //return;
                $http.post($rootScope.domain_url + '/api/program_update', update_data)
                .then(function programSaved(response){
                    if(response.data == 'success'){

                        alert('Changes saved.');
                        return;
                    }

                    // Some error has occured
                    if(response.data.error == 'error'){
                        $scope.errors = response.data.errors;
                    }
                });
            };

            // Update Skill function
            $scope.updateSkill = function( $skill_id, $skill_level) {
                
                var skill_data = {
                    id:          $skill_id,
                    skill_level: $skill_level
                };
                $http.post($rootScope.domain_url + '/api/program_update_skill_level', skill_data);
            };

            $scope.new_skill_level = 2;

            // Add Skill function
            $scope.addSkill = function() {
                
                if($scope.new_skill === undefined || $scope.new_skill === ''){
                    return;
                }
                var already_exist = false;
                angular.forEach($scope.program_data.skills, function(value, key) {
                  if(value.tag == $scope.new_skill){
                       alert('You have already added this skill.'); 
                       already_exist = true;
                  }
                });
                if(already_exist === false){
                    var new_skill_data = {
                        skill:      $scope.new_skill,
                        level:      $scope.new_skill_level,
                        type:       'skill',
                        program_id: $scope.program_data.program.id
                    };
                    $http.post($rootScope.domain_url + '/api/program_add_skill', new_skill_data)
                    .then(function addSkillSuccess(response){
                        
                            // Reset the levels
                            $scope.new_skill       = '';
                            $scope.new_skill_level = 2;

                            // Hide the add skill form
                            $scope.show_add_skill_form = !$scope.show_add_skill_form;

                            // Reset the 
                            $scope.program_data.skills = response.data;                        
                    });
                }
            };

            // Remove Skill function
            $scope.removeSkill = function( $skill_id) {
                    
                var skill_data = {
                    skill_id:   $skill_id,
                    program_id: $scope.program_data.program.id
                };
                $http.post($rootScope.domain_url + '/api/program_remove_skill', skill_data)
                .then(function removeSkillSuccess(response){
                
                    // Reset the skills
                    $scope.program_data.skills = response.data;  
                    
                });
            };

            // Add place function
            $scope.addPlace = function() {
                    
                var place_data = {
                    place:      $scope.new_place,
                    program_id: $scope.program_data.program.id
                };
                $http.post($rootScope.domain_url + '/api/program_add_place', place_data)
                .then(function addPlaceSuccess(response){
                    
                    if(response.data.error == 'invalid_place'){
                        alert('Please enter a proper place.');
                    }

                    // Reset the new place textbox value
                    $scope.new_place = '';
                    // hide the form
                    $scope.show_add_place_form = !$scope.show_add_place_form;
                    // Update the places
                    $scope.program_data.places        = response.data.places;
                });
            };

            // Update place function
            $scope.updatePlace = function( $place_id, $address) {
                    
                var place_data = {
                    place_id:   $place_id,
                    place:      $address,
                    program_id: $scope.program_data.program.id
                };
                $http.post($rootScope.domain_url + '/api/program_update_place', place_data)
                .then(function updatePlaceSuccess(response){
                    
                    if(response.data.error == 'invalid_place'){
                        alert('Please enter a proper place.');
                        return;
                    }

                    // Update the places
                    $scope.program_data.places        = response.data.places;
                    $scope.program_data.primary_place = response.data.primary_place;
                });
            };

            // Update place function
            $scope.removePlace = function( $place_id) {
                    
                var place_data = {
                    place_id:   $place_id,
                    program_id: $scope.program_data.program.id
                };
                $http.post($rootScope.domain_url + '/api/program_remove_place', place_data)
                .then(function removePlaceSuccess(response){
                    // Update the places
                    $scope.program_data.places        = response.data.places;
                });
            };


        });

})();