(function() {

    'use strict';

    angular
        .module('Placerange')

        .controller('publicIssueController', function($scope, $http, $filter, Upload, $timeout, $stateParams, ngDialog, $rootScope, $location) {

            $scope.current_url    = $location.absUrl();
            $scope.arr            = $scope.current_url.split("/");
            $rootScope.domain_url = $scope.arr[0] + "//" + $scope.arr[2];

            var slug = {
                slug : $stateParams.slug
            };
            $http.post($rootScope.domain_url + '/api/issue_publicpage_data', slug)
            .then(function mySucces(response) {
                // Data obtained from the database
                // User counts
                // Map user data

                $scope.issue_data = response.data;
                
                // todays date for footer copyright
                $scope.today = new Date();
            },

            function myError(response) {
                alert('Some error has occured.');
            }

            );


            // Post comment function
            $scope.addComment = function ($comment) {

                if($comment == ''){
                    return;
                }

                var comment_data = {
                    comment: $comment,
                    issue_id: $scope.issue_data.issue.id
                };

                $http.post($rootScope.domain_url + '/api/issue_add_new_comment', comment_data)
                .then(function mySucces(response) {
                    // Data obtained from the database
                    $scope.issue_data.comments = response.data.comments;
                    $scope.new_comment = '';
                });
            };

            // Show the reply box when reply button is clicked
            $scope.showReplyBox = function ( $comment_id) {
                $scope.reply_comment_id = $comment_id;
                $scope.reply_box = ngDialog.open({ 
                    template: 'template_reply', 
                    className: 'ngdialog-theme-default',
                    scope: $scope });
            };

            // Post connect function
            $scope.postReply = function ($reply, $comment_id) {

                if($reply == ''){
                    return;
                }

                var reply_data = {
                    reply: $reply,
                    comment_id: $comment_id
                };

                $http.post($rootScope.domain_url + '/api/issue_reply_to_comment', reply_data)
                .then(function mySucces(response) {
                    // Data obtained from the database
                    $scope.issue_data.comments = response.data.comments;

                    ngDialog.close($scope.reply_box.id);

                    
                });
            };

            // Show the reply box when reply button is clicked
            $scope.showEditReplyBox = function ( $reply, $reply_id) {
                $scope.current_reply_id = $reply_id;
                $scope.current_edit_reply = $reply;
                $scope.edit_reply_box = ngDialog.open({ 
                    template: 'template_edit_reply', 
                    className: 'ngdialog-theme-default',
                    scope: $scope });
            };

            // Post connect function
            $scope.postEditReply = function ($reply, $reply_id) {

                if($reply == ''){
                    return;
                }
                var reply_data = {
                    reply: $reply,
                    reply_id: $reply_id
                };

                $http.post($rootScope.domain_url + '/api/issue_edit_reply', reply_data)
                .then(function mySucces(response) {
                    // Data obtained from the database
                    $scope.issue_data.comments = response.data.comments;
                    ngDialog.close($scope.edit_reply_box.id);

                    
                });
            };

            // Show the reply box when reply button is clicked
            $scope.showEditCommentBox = function ( $comment, $comment_id) {
                $scope.current_comment_id = $comment_id;
                $scope.current_edit_comment = $comment;
                $scope.edit_comment_box = ngDialog.open({ 
                    template: 'template_edit_comment', 
                    className: 'ngdialog-theme-default',
                    scope: $scope });
            };

            // Edit comment function
            $scope.postEditComment = function ($comment, $comment_id) {

                if($comment == ''){
                    return;
                }
                var comment_data = {
                    comment: $comment,
                    comment_id: $comment_id
                };

                $http.post($rootScope.domain_url + '/api/issue_edit_comment', comment_data)
                .then(function mySucces(response) {
                    // Data obtained from the database
                    $scope.issue_data.comments = response.data.comments;
                    ngDialog.close($scope.edit_comment_box.id);

                    
                });
            };

            // Comment mark helpful function
            $scope.markHelpful = function ($comment_id) {

                var comment_data = {
                    comment_id: $comment_id
                };

                $http.post($rootScope.domain_url + '/api/issue_comment_helpful', comment_data)
                .then(function mySucces(response) {
                    // Data obtained from the database
                    $scope.issue_data.comments = response.data.comments;                    
                });
            };

            // Remove comment function
            $scope.removeComment = function ($comment_id) {

                var comment_data = {
                    comment_id: $comment_id
                };

                $http.post($rootScope.domain_url + '/api/issue_remove_comment', comment_data)
                .then(function mySucces(response) {
                    // Data obtained from the database
                    $scope.issue_data.comments = response.data.comments;                    
                });
            };

            // Remove comment function
            $scope.removeReply = function ($reply_id) {

                var reply_data = {
                    reply_id: $reply_id
                };

                $http.post($rootScope.domain_url + '/api/issue_remove_reply', reply_data)
                .then(function mySucces(response) {
                    // Data obtained from the database
                    $scope.issue_data.comments = response.data.comments;
                });
            };

            // Hide haldle request form by default
            $scope.show_accept_handle_form = false;
            $scope.issue_handle_message = 'I would like to help you with this issue.';
            // Send issue handle request
            $scope.sendHandleRequest = function ($message) {

                var issue_handle_data = {
                    message: $message,
                    issue_id: $scope.issue_data.issue.id
                };

                $http.post($rootScope.domain_url + '/api/issue_send_handle_request', issue_handle_data)
                .then(function mySucces(response) {
                    // Data obtained from the database
                    $scope.issue_data.handle_requests  = response.data.handle_requests;
                    $scope.issue_data.awating_approval = response.data.awating_approval;
                });
            };

            // Hide haldle request form by default
            $scope.show_cancel_handle_form = false;
            $scope.issue_handle_cancel_message = 'I\'m sorry to infom that I will not be able to handle this issue.';
            // Send issue handle request
            $scope.cancelIssueHandling = function () {

                var issue_handle_data = {
                    cancel_reason: $scope.issue_handle_cancel_message,
                    issue_id: $scope.issue_data.issue.id
                };

                $http.post($rootScope.domain_url + '/api/issue_cancel_handle', issue_handle_data)
                .then(function mySucces(response) {
                    // Data obtained from the database
                    $scope.issue_data.handle_cancelled = true;
                    $scope.issue_data.handle_approved  = false;
                    $scope.issue_data.handle_requests  = response.data.handle_requests;
                });
            };

            // Send issue handle request
            $scope.cancelAwaitingApproval = function () {

                var issue_handle_data = {
                    issue_id: $scope.issue_data.issue.id
                };

                $http.post($rootScope.domain_url + '/api/issue_cancel_awaiting_handle_request', issue_handle_data)
                .then(function mySucces(response) {
                    // Data obtained from the database
                    $scope.issue_data.handle_requests  = response.data.handle_requests;
                    $scope.issue_data.handle_cancelled = true;
                });
            };

            // Accept handling request
            $scope.acceptHandleRequest = function ($handle_id) {

                var issue_handle_data = {
                    issue_id: $scope.issue_data.issue.id,
                    handle_id: $handle_id
                };

                $http.post($rootScope.domain_url + '/api/issue_accept_handle_request', issue_handle_data)
                .then(function mySucces(response) {
                    // Data obtained from the database
                    $scope.issue_data.handle_requests  = response.data.handle_requests;
                });
            };

            // Reject handling request
            $scope.rejectHandleRequest = function ($handle_id) {

                var issue_handle_data = {
                    handle_id: $handle_id
                };

                $http.post($rootScope.domain_url + '/api/issue_reject_handle_request', issue_handle_data)
                .then(function mySucces(response) {
                    // Data obtained from the database
                    $scope.issue_data.handle_requests  = response.data.handle_requests;
                });
            };

            // Reject handling request
            $scope.ownerCancelHandleRequest = function ($handle_id) {

                var issue_handle_data = {
                    handle_id: $handle_id
                };

                $http.post($rootScope.domain_url + '/api/issue_owner_cancel_handle_request', issue_handle_data)
                .then(function mySucces(response) {
                    // Data obtained from the database
                    $scope.issue_data.handle_requests  = response.data.handle_requests;
                });
            };




        });

})();