(function() {

    'use strict';

    angular
        .module('Placerange')

        .controller('issueController', function($scope, $http, $filter, Upload, $timeout, $stateParams, ngDialog, $state, $location, $rootScope) {

            $scope.current_url    = $location.absUrl();
            $scope.arr            = $scope.current_url.split("/");
            $rootScope.domain_url = $scope.arr[0] + "//" + $scope.arr[2];
            // Get the issue slug
            var slug = {
                slug : $stateParams.slug
            };

            if($stateParams.slug !== undefined && $state.current.name != 'issue_create'){

                $http.post($rootScope.domain_url + '/api/issue_data', slug)
                .then(function mySucces(response) {
                    // Data obtained from the database
                    $scope.issue_data = response.data;
                },

                function myError(response) {
                    alert('Some error has occured.');
                }

                );
            }

            // check if the state is create new issue
            if($state.current.name == 'issue_create'){

                $http.post($rootScope.domain_url + '/api/issue_create_data')
                .then(function mySucces(response) {
                    // Data obtained from the database
                    // User counts
                    // Map user data

                    $scope.new_issue = response.data;
                    $scope.new_issue.title = '';
                    $scope.new_issue.summary = '';
                    $scope.new_issue.assistance = true;
                    
                },

                function myError(response) {
                    alert('Some error has occured.');
                }

                );
            }

             // Create issue function
            $scope.createIssue = function() {
                $http.post($rootScope.domain_url + '/api/issue_create', $scope.new_issue)
                .then(function issueCreated(response){
                    if(response.data.success == 'success'){

                        $location.path('issue/' + response.data.slug); 
                        return;
                    }

                    // Some error has occured
                    if(response.data.error == 'error'){
                        $scope.errors = response.data.errors;
                    }
                });
            };

              // Skill level function
              $scope.skill_levels = [
                {value: 2, text: 'Novice'},
                {value: 3, text: 'Intermediate'},
                {value: 4, text: 'Expert'}
              ]; 

              $scope.showSkillLevel = function($level) {
                var selected = $filter('filter')($scope.skill_levels, {value: $level});
                return ($level && selected.length) ? selected[0].text : 'Not set';
              };
              
              // Update user function
            $scope.updateIssue = function() {
                $http.post($rootScope.domain_url + '/api/issue_update', $scope.issue_data.issue)
                .then(function issueSaved(response){
                    if(response.data == 'success'){

                        alert('Changes saved!');
                    }
                });
            };

            // Update Skill function
            $scope.updateSkill = function( $skill_id, $skill_level) {
                
                var skill_data = {
                    id:          $skill_id,
                    skill_level: $skill_level
                };
                $http.post($rootScope.domain_url + '/api/issue_update_skill_level', skill_data);
            };

            $scope.new_skill_level = 2;

            // Add Skill function
            $scope.addSkill = function() {
                
                if($scope.new_skill === undefined || $scope.new_skill === ''){
                    return;
                }
                var already_exist = false;
                angular.forEach($scope.issue_data.skills, function(value, key) {
                  if(value.tag == $scope.new_skill){
                       alert('You have already added this skill.'); 
                       already_exist = true;
                  }
                });
                if(already_exist === false){
                    var new_skill_data = {
                        skill: $scope.new_skill,
                        level: $scope.new_skill_level,
                        type: 'skill',
                        issue_id: $scope.issue_data.issue.id
                    };
                    $http.post($rootScope.domain_url + '/api/issue_add_skill', new_skill_data)
                    .then(function addSkillSuccess(response){
                        
                            // Reset the levels
                            $scope.new_skill       = '';
                            $scope.new_skill_level = 2;

                            // Hide the add skill form
                            $scope.show_add_skill_form = !$scope.show_add_skill_form;

                            // Reset the 
                            $scope.issue_data.skills = response.data;                        
                    });
                }
            };

            // Remove Skill function
            $scope.removeSkill = function( $skill_id) {
                    
                var skill_data = {
                    skill_id: $skill_id,
                    issue_id: $scope.issue_data.issue.id
                };
                $http.post($rootScope.domain_url + '/api/issue_remove_skill', skill_data)
                .then(function removeSkillSuccess(response){
                
                    // Reset the skills
                    $scope.issue_data.skills = response.data;  
                    
                });
            };

            // Add place function
            $scope.addPlace = function() {
                    
                var place_data = {
                    place: $scope.new_place,
                    issue_id: $scope.issue_data.issue.id
                };
                $http.post($rootScope.domain_url + '/api/issue_add_place', place_data)
                .then(function addPlaceSuccess(response){
                    
                    if(response.data.error == 'invalid_place'){
                        alert('Please enter a proper place.');
                    }

                    // Reset the new place textbox value
                    $scope.new_place = '';
                    // hide the form
                    $scope.show_add_place_form = !$scope.show_add_place_form;
                    // Update the places
                    $scope.issue_data.places        = response.data.places;
                });
            };

            // Update place function
            $scope.updatePlace = function( $place_id, $address) {
                    
                var place_data = {
                    place_id: $place_id,
                    place: $address,
                    issue_id: $scope.issue_data.issue.id
                };
                $http.post($rootScope.domain_url + '/api/issue_update_place', place_data)
                .then(function updatePlaceSuccess(response){
                    
                    if(response.data.error == 'invalid_place'){
                        alert('Please enter a proper place.');
                        return;
                    }

                    // Update the places
                    $scope.issue_data.places        = response.data.places;
                    $scope.issue_data.primary_place = response.data.primary_place;
                });
            };

            // Update place function
            $scope.removePlace = function( $place_id) {
                    
                var place_data = {
                    place_id: $place_id,
                    issue_id: $scope.issue_data.issue.id
                };
                $http.post($rootScope.domain_url + '/api/issue_remove_place', place_data)
                .then(function removePlaceSuccess(response){
                    // Update the places
                    $scope.issue_data.places        = response.data.places;
                });
            };


        });

})();