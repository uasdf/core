(function() {

'use strict';

angular
    .module('Placerange')

    .controller('homeController', function($scope, $http, $filter, $timeout, $stateParams, ngDialog, $state, $location, $rootScope) {

        $scope.current_url    = $location.absUrl();
        $scope.arr            = $scope.current_url.split("/");
        $rootScope.domain_url = $scope.arr[0] + "//" + $scope.arr[2];

        
        $http.get($rootScope.domain_url + '/api/authenticate/user')
        .then(function (response){

            // Stringify the returned data to prepare it
            // to go into local storage
            var user = JSON.stringify(response.data.user);

            // Set the stringified user data into local storage
            localStorage.setItem('user', user);

            // The user's authenticated state gets flipped to
            // true so we can now show parts of the UI that rely
            // on the user being logged in
            $rootScope.authenticated = true;

            // Putting the user's data on $rootScope allows
            // us to access it anywhere across the app
            $rootScope.currentUser = response.data.user;

        });

        $http({
            method  :'post',
            url     :$rootScope.domain_url + '/api/user_home'
            })
            .then(function mySucces(response) {
                // Data obtained from the database
                // User counts
                // Map user data

                $scope.home_data = response.data;

                $scope.loadWall();

            },

            function myError(response) {
                alert('Some error has occured.');
            }

        );

        $scope.wall_data = {no_feed: false, feeds : [], loaded: true};

        $scope.wall_request_data = {start_date : ''};

        $scope.loadWallTimeout = function(){

        }

        $scope.loadWall = function(){
          if ($scope.wall_data.no_feed === true || $scope.wall_data.loaded == false){
            return;
          }
          $scope.wall_data.loaded = false;
          $http.post($rootScope.domain_url + '/api/user_wall_data', $scope.wall_request_data)
            .then(function wallSuccess(response) {

             
              $scope.wall_data.feeds = $scope.wall_data.feeds.concat(response.data.feeds);

              $scope.wall_data.no_feed = response.data.no_feed;
              
              $scope.wall_request_data.start_date = response.data.start_date;
              $scope.wall_data.loaded = true;
              
          });
        }


    });
})();