(function() {

'use strict';

angular
    .module('Placerange')

    .controller('searchController', function($scope, $http, $filter, $timeout, $stateParams, ngDialog, $state, $location, $rootScope) {


        $scope.current_url    = $location.absUrl();
        $scope.arr            = $scope.current_url.split("/");
        $rootScope.domain_url = $scope.arr[0] + "//" + $scope.arr[2];

        
        // Set the default values
        $scope.search              = {};
        $scope.search.distance     = 1;
        $scope.search.selected_for = 'users';
        $scope.search.skills       = '';
        $http({
            method  :'post',
            url     :$rootScope.domain_url + '/api/search_near_me'
        })
        .then(function mySucces(response) {
            // Data obtained from the database
            // User counts
            // Map user data

            $scope.search_data = response.data;
            $scope.search.selected_place = $scope.search_data.primary_place.id;

        },

        function myError(response) {
            alert('Some error has occured.');
        }

        );

        

        $scope.postSearch = function (){

            if($scope.search.skills == ''){
                return;
            }
            $http.post($rootScope.domain_url + '/api/search_post_near_me', $scope.search)
                .then(function searchSuccess(response){
                        console.log(response.data);
                        $scope.search_result = response.data;



                },
                function searchError(){
                    alert('Some error has occured');
                }


                );
        }

    });
})();