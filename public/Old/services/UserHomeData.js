angular
	.module('Placerange')
	.factory('UserHomeData',['$http', function($http){
		return {
			get: function(){
				return $http.get('http://localhost:8000/api/user_homepage_data').then(function (response){
					return response.data;
				});
			}
		}

	}]
	);