(function() {

    'use strict';

    angular
        .module('Placerange')
        .config(function( $stateProvider, $urlRouterProvider, $authProvider) {

            // Satellizer configuration that specifies which API
            // route the JWT should be retrieved from
            $authProvider.loginUrl = '/api/authenticate';

            // Redirect to the auth state if any other states
            // are requested other than users
            $urlRouterProvider.otherwise('/index');
            
            $stateProvider
                // Landing Page
                .state('/', {
                    url: '',
                    templateUrl: 'views/welcome.html',
                    controller: 'indexController'
                })

                // Landing page
                .state('index', {
                    url: '/index',
                    templateUrl: 'views/welcome.html',
                    controller: 'indexController'
                })

                // Login page
                .state('auth', {
                    url: '/auth',
                    templateUrl: 'views/auth.html',
                    controller: 'AuthController as auth'
                })

                // Registeration page
                .state('register', {
                    url: '/register',
                    templateUrl: 'views/register.html',
                    controller: 'AuthController as auth'
                })

                // User profile
                .state('profile', {
                    url: '/profile',
                    templateUrl: 'views/user/profile.html',
                    controller: 'profileController',
                    data: {requiredLogin: true}
                })

                // User public profile
                .state('user_public_profile', {
                    url: '/user/{username}',
                    templateUrl: 'views/user/public_profile.html',
                    controller: 'publicProfileController'
                })

                // Issue public profile
                .state('issue_public_profile', {
                    url: '/issue/{slug}',
                    templateUrl: 'views/issue/public_issue.html',
                    controller: 'publicIssueController'
                })

                // Issue Edit 
                .state('issue_edit', {
                    url: '/issue/edit/{slug}',
                    templateUrl: 'views/issue/edit_issue.html',
                    controller: 'issueController',
                    data: {requiredLogin: true}
                })
                // Issue Create new 
                .state('issue_create', {
                    url: '/new-issue',
                    templateUrl: 'views/issue/create_issue.html',
                    controller: 'issueController',
                    data: {requiredLogin: true}
                })
                // Search
                .state('search_near_me', {
                    url: '/near-me',
                    templateUrl: 'views/search/search_near_me.html',
                    controller: 'searchController',
                    data: {requiredLogin: true}
                })

                // Home Wall
                .state('home', {
                    url: '/home',
                    templateUrl: 'views/home/home.html',
                    controller: 'homeController',
                    data: {requiredLogin: true}
                })

                // Notifications
                .state('notifications', {
                    url: '/notifications',
                    templateUrl: 'views/home/notifications.html',
                    controller: 'notificationController',
                    data: {requiredLogin: true}
                })

                // Messages
                .state('messages', {
                    url: '/messages',
                    templateUrl: 'views/home/messages.html',
                    controller: 'messageController',
                    data: {requiredLogin: true}
                });
        });
})();