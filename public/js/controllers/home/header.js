'use strict';

// signup controller
app.controller('headerCtrl',  ['$scope', '$filter', '$modal', '$http', '$state', '$location', '$rootScope', '$pusher', 'toaster', '$auth', 
    function($scope, $filter, $modal, $http, $state, $location, $rootScope, $pusher, toaster, $auth) {
 	
 	$scope.current_url    = $location.absUrl();
    $scope.arr            = $scope.current_url.split("/");
    $rootScope.domain_url = '';

    if(!$auth.isAuthenticated()){
      return;
    }
    $http({
        method  :'get',
        url     :$rootScope.domain_url + '/api/notifications'
        })
        .then(function mySucces(response) {

            $scope.notifications     = response.data.notifications;
            $scope.new_notifications = response.data.unread_count;

        },

        function myError(response) {
            console.log('Some error has occured while trying to retrieve the notifications.');
        }

    );

    $scope.navigateTo = function(target_location){
      $location.path(target_location);

    }


    $scope.pusher_client  = new Pusher('3cf39c63f99ff76f1249');
    $scope.pusher         = $pusher($scope.pusher_client);
    $scope.pusher_channel = $scope.pusher.subscribe('notification_' + $rootScope.current_user.id);
    $scope.pusher_channel.bind('notification', function(data) {

        $scope.notifications.unshift(data.notification);

    });


    $scope.markNotificationRead = function (notification){
        if(notification.read == 1){
            return;
        }
        var notification_data = {
            id : notification.id
        };
        $http.post($rootScope.domain_url + '/api/notification_mark_read', notification_data);

    }

    $scope.selected = undefined;
    $scope.states = [];
    // Any function returning a promise object can be used to load values asynchronously
    $scope.getLocation1 = function(val) {
      return $http.get('http://maps.googleapis.com/maps/api/geocode/json', {
        params: {
          address: val,
          sensor: false
        }
      }).then(function(res){
        var addresses = [];
        angular.forEach(res.data.results, function(item){
          addresses.push(item.formatted_address);
        });
        return addresses;
      });
    };

    // Any function returning a promise object can be used to load values asynchronously
  $scope.getLocation = function(val) {
    return $http.get('//maps.googleapis.com/maps/api/geocode/json', {
      params: {
        address: val,
        sensor: false
      }
    }).then(function(response){
      var states = [];
        angular.forEach(res.data.results, function(item){
          states.push(item);
        });
        return states;
    });
  };


    $scope.search_results;
    $scope.request_received = true;
    $scope.headerSearch = function(search_string) {
        if($scope.request_received === false || search_string.length <= 2){
            return;
        }
        $scope.request_received = false;
        var params = {
          search_string: search_string
        };
      return $http.post($rootScope.domain_url + '/api/post_search', params)
      .then(function(response){
        var states = [];
        angular.forEach(response.data.results, function(item){
          states.push(item);
          $scope.request_received = true;
        });
        
        $scope.states = states;
      });
    };




}
]);
