'use strict';

// signup controller
app.controller('messageCtrl', ['$scope', '$filter', '$modal', '$http', '$state', '$location', '$rootScope', '$pusher', '$auth',  function($scope, $filter, $modal, $http, $state, $location, $rootScope, $pusher, $auth) {
    
    if(!$auth.isAuthenticated()){
      $state.go('access.signin');
      return;
    }

    $scope.current_url    = $location.absUrl();
    $scope.arr            = $scope.current_url.split("/");
    $rootScope.domain_url = $scope.arr[0] + "//" + $scope.arr[2];

    $http({
        method  :'post',
        url     :$rootScope.domain_url + '/api/messages'
        })
        .then(function mySucces(response) {
            // Data obtained from the database
            // User counts
            // Map user data
            $scope.message_data = response.data;
            if($scope.message_data.threads.length > 0){
                var first_thread_user = $scope.message_data.threads[0].user;
                $scope.selectThread(first_thread_user);
            } else {

                $scope.no_threads = true;

            }

            if($scope.message_data.connections.length === 0){

                $scope.no_connections = true;
            }

        },

        function myError(response) {
            alert('Some error has occured.');
        }

    );

    $scope.pusher_client = new Pusher('3cf39c63f99ff76f1249');
    $scope.pusher = $pusher($scope.pusher_client);

    $scope.search = "";

    // Show the reply box when reply button is clicked
    $scope.showSearchMessageBox = function () {
        $scope.search_message_data = $modal.open({ 
            templateUrl: 'template_select_thread', 
            className: 'ngdialog-theme-default',
            scope: $scope });
        return false;
    };
    
    $scope.selectThread = function( $user ) {

        if($scope.search_message_data !== undefined){
            $scope.search_message_data.close();
        }
        $scope.new_user_message      = false;
        $scope.selected_user = $user;

        $scope.messageAutoload($user);
    }

    

    $scope.messageAutoload = function( $user ) {

        $http.post($rootScope.domain_url + '/api/load_messages', {for_user : $user.id})
        .then(function mySucces(response) {
            $scope.message_data.messages = response.data.messages;
            $scope.message_data.thread   = response.data.thread;
            $scope.selected_thread_id    = $scope.message_data.thread.id;
            if($scope.pusher_thread != $scope.selected_thread_id){
                $scope.pusher_channel        = $scope.pusher.subscribe('thread_' + $scope.selected_thread_id);
                $scope.pusher_channel.bind('messages', function(data) {

                  $scope.pusher_thread = $scope.selected_thread_id;
                  $scope.message_data.messages.push(data.message);
                });
            }
        },

        function myError(response) {
            alert('Some error has occured.');
        }

        );
    }

    $scope.postMessage = function( $message ) {

        if($message === '' || $message === undefined){
            return;
        } 
        $scope.new_message = $message;

        if($scope.new_user_message === true){
            $scope.postNewMessage();
            return;
        }
        $scope.new_user_message = false;

        var new_message_data = {
            to_user : $scope.selected_user.id,
            'message' : $scope.new_message
        };
        $scope.new_message = '';
        $http.post($rootScope.domain_url + '/api/ajax_post_message', new_message_data)
        .then(function mySucces(response) {
            $scope.new_message = '';
        },

        function myError(response) {
            alert('Some error has occured.');
        }

        );
    }


    $scope.postNewMessage = function() {

        var new_message_data = {
            to_user : $scope.selected_user.id,
            'message' : $scope.new_message
        };
        $scope.new_message = '';
        $http.post($rootScope.domain_url + '/api/post_new_message', new_message_data)
        .then(function mySucces(response) {
            $scope.message_data = response.data;
            var first_thread_user = $scope.message_data.threads[0].user;
            $scope.selectThread(first_thread_user);

        },

        function myError(response) {
            alert('Some error has occured.');
        }

        );
    }

    

    // Show the reply box when reply button is clicked
    $scope.showNewMessageBox = function () {
        $scope.new_message_dialog = $modal.open({ 
            templateUrl: 'template_new_message', 
            className: 'ngdialog-theme-default',
            scope: $scope });
        return false;
    };

    $scope.newThread = function( $user ) {

        $scope.selected_user         = $user;
        $scope.new_user_message      = true;
        $scope.message_data.messages = {};
		$scope.new_message_dialog.close();
    }




}
]);


// signup controller
app.controller('messageMobileCtrl', ['$scope', '$filter', '$modal', '$http', '$state', '$location', '$rootScope', '$pusher',  function($scope, $filter, $modal, $http, $state, $location, $rootScope, $pusher) {
    
    $scope.current_url    = $location.absUrl();
        $scope.arr            = $scope.current_url.split("/");
        $rootScope.domain_url = $scope.arr[0] + "//" + $scope.arr[2];

        $http({
            method  :'post',
            url     :$rootScope.domain_url + '/api/messages'
            })
            .then(function mySucces(response) {
                // Data obtained from the database
                // User counts
                // Map user data
                $scope.message_data_mobile = response.data;


            },

            function myError(response) {
                alert('Some error has occured.');
            }

        );

        $scope.pusher_client = new Pusher('3cf39c63f99ff76f1249');
        $scope.pusher = $pusher($scope.pusher_client);

        $scope.search = "";

        // Show the reply box when reply button is clicked
        $scope.showSearchMessageBox = function () {
            $scope.search_message_data = $modal.open({ 
                templateUrl: 'template_select_thread', 
                className: 'ngdialog-theme-default',
                scope: $scope });
            return false;
        };
      
        $scope.postMessage = function( $message ) {

            if($message === '' || $message === undefined){
                return;
            } 

            $scope.new_message = $message;
            $scope.new_message = '';
            if($scope.new_user_message === true){
                $scope.postNewMessage();
                return;
            }
            $scope.new_user_message = false;

            var new_message_data = {
                to_user : $scope.selected_user.id,
                'message' : $scope.new_message
            };
            $http.post($rootScope.domain_url + '/api/ajax_post_message', new_message_data)
            .then(function mySucces(response) {

            },

            function myError(response) {
                alert('Some error has occured.');
            }

            );
        }


        $scope.postNewMessage = function() {

            var new_message_data = {
                to_user : $scope.selected_user.id,
                'message' : $scope.new_message
            };
            $scope.new_message = '';
            $http.post($rootScope.domain_url + '/api/post_new_message', new_message_data)
            .then(function mySucces(response) {
                $scope.message_data_mobile = response.data;
                var first_thread_user = $scope.message_data_mobile.threads[0].user;
                $scope.selectThread(first_thread_user);

            },

            function myError(response) {
                alert('Some error has occured.');
            }

            );
        }

        

        // Show the reply box when reply button is clicked
        $scope.showNewMessageBox = function () {
            $scope.new_message_dialog = $modal.open({ 
                templateUrl: 'template_new_message', 
                className: 'ngdialog-theme-default',
                scope: $scope });
            return false;
        };

        $scope.selectThreadMobile = function( $user ) {
             
            $scope.mobile_conversation = $modal.open({ 
                templateUrl: 'mobile_conversation_panel', 
                className: 'ngdialog-theme-default',
                scope: $scope });
            $scope.new_user_message      = false;
            $scope.selected_user = $user;

            $scope.messageAutoloadMobile($user);
        }

        $scope.messageAutoloadMobile = function( $user ) {

            $http.post($rootScope.domain_url + '/api/load_messages', {for_user : $user.id})
            .then(function mySucces(response) {
                $scope.message_data_mobile.messages = response.data.messages;
                $scope.message_data_mobile.thread   = response.data.thread;
                $scope.selected_thread_id    = $scope.message_data_mobile.thread.id;
                if($scope.pusher_thread != $scope.selected_thread_id){
                    $scope.pusher_channel        = $scope.pusher.subscribe('thread_' + $scope.selected_thread_id);
                    $scope.pusher_channel.bind('messages', function(data) {
                      $scope.pusher_thread = $scope.selected_thread_id;
                      $scope.message_data_mobile.messages.push(data.message);
                    });
                }
            },

            function myError(response) {
                alert('Some error has occured.');
            }

            );
        }

        $scope.closeMobileConversation = function( ) {

            $scope.mobile_conversation.close();
        }

        // Show the reply box when reply button is clicked
        $scope.showNewMessageBoxMobile = function () {
            $scope.new_message_dialog = $modal.open({ 
                templateUrl: 'template_new_message_mobile', 
                className: 'ngdialog-theme-default',
                scope: $scope });
            return false;
        };

        $scope.newThreadMobile = function( $user ) {

            $scope.selected_user         = $user;
            $scope.new_user_message      = true;
            $scope.message_data_mobile.messages = {};
            $scope.mobile_conversation = $modal.open({ 
                templateUrl: 'mobile_conversation_panel', 
                className: 'ngdialog-theme-default',
                scope: $scope });
        }

        $scope.closeNewMessageBox = function() {
            $scope.new_message_dialog.close();
        }

}
]);
