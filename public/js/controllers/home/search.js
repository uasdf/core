// this is a lazy load controller, 
// so start with "app." to register this controller

app.filter('propsFilter', function() {
    return function(items, props) {
        var out = [];

        if (angular.isArray(items)) {
          items.forEach(function(item) {
            var itemMatches = false;

            var keys = Object.keys(props);
            for (var i = 0; i < keys.length; i++) {
              var prop = keys[i];
              var text = props[prop].toLowerCase();
              if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                itemMatches = true;
                break;
              }
            }

            if (itemMatches) {
              out.push(item);
            }
          });
        } else {
          // Let the output be the input untouched
          out = items;
        }

        return out;
    };
})
app.controller('searchCtrl', function($scope, $http, $timeout, $rootScope, $state, $auth) {

        if(!$auth.isAuthenticated()){
          $state.go('access.signin');
          return;
        }
        // Set the address object
        $scope.address = {};
        $scope.address.selected = {};
        $scope.search_text = '';
        var current_user_place = $rootScope.current_user_place;
        $scope.address.selected.formatted_address = current_user_place.formatted_address;
        $scope.distance = { value: '5',      text: '5 Miles'};
        $scope.available_distance = [
            { value: '1',      text: '1 Mile'},
            { value: '2',      text: '2 Miles'},
            { value: '5',      text: '5 Miles'},
            { value: '10',      text: '10 Miles'},
            { value: '50',      text: '50 Miles'}
        ];
        

        // Get the dashboad data
        $http.get( 'api/user_home')
        .then(function(response) {
            $scope.members_nearby = response.data.members_nearby;
        });


        $scope.refreshAddresses = function(address) {
          if(address == '' || address == null){
            return;
          }
          
          return $http({
            method: 'GET',
            url: 'http://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&sensor=true',
            skipAuthorization: true

          }).then(function(response) {
            $scope.addresses = response.data.results;
          });
        };
        // Dashboard search
        $scope.dashboardSearch = function() {

          var params = {search_text: $scope.search_text, place: $scope.address.selected.formatted_address, distance: $scope.distance.value};
          return $http.get( 'api/dashboard_search',
            {params: params}
          ).then(function(response) {
            $scope.tasks = response.data.tasks;
            $scope.users = response.data.users;
          });
        };
        



        
});