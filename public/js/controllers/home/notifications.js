'use strict';

// signup controller
app.controller('notificationCtrl', ['$scope', '$modal', '$log', '$http', '$state', '$location', '$rootScope', '$auth',
	function($scope, $modal, $log, $http, $state, $location, $rootScope, $auth) {
 	
    if(!$auth.isAuthenticated()){
      $state.go('access.signin');
      return;
    }
 	$scope.current_url    = $location.absUrl();
    $scope.arr            = $scope.current_url.split("/");
    $rootScope.domain_url = '';

    $http({
        method  :'get',
        url     :$rootScope.domain_url + '/api/notifications'
        })
        .then(function mySucces(response) {
            // Data obtained from the database
            // User counts
            // Map user data

            $scope.notifications = response.data.notifications;

        },

        function myError(response) {
            alert('Some error has occured.');
        }

    );


    $scope.navigateTo = function(target_location){
      $location.path(target_location);

    }

}
]);
