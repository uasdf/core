// this is a lazy load controller, 
// so start with "app." to register this controller

app.filter('propsFilter', function() {
    return function(items, props) {
        var out = [];

        if (angular.isArray(items)) {
          items.forEach(function(item) {
            var itemMatches = false;

            var keys = Object.keys(props);
            for (var i = 0; i < keys.length; i++) {
              var prop = keys[i];
              var text = props[prop].toLowerCase();
              if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                itemMatches = true;
                break;
              }
            }

            if (itemMatches) {
              out.push(item);
            }
          });
        } else {
          // Let the output be the input untouched
          out = items;
        }

        return out;
    };
})
app.controller('dashboardCtrl', function($scope, $http, $timeout, $rootScope, $state, $auth, $filter) {

        if(!$auth.isAuthenticated()){
          $state.go('access.signin');
          return;
        }
        // Set the address object
        $scope.address = {};
        $scope.address.selected = {};
        $scope.search_text = '';
        var current_user_place = $rootScope.current_user_place;
        $scope.address.selected.formatted_address = current_user_place.formatted_address;
        $scope.distance = { value: '5',      text: '5 Miles'};
        $scope.available_distance = [
            { value: '1',      text: '1 Mile'},
            { value: '2',      text: '2 Miles'},
            { value: '5',      text: '5 Miles'},
            { value: '10',      text: '10 Miles'},
            { value: '50',      text: '50 Miles'}
        ];
        

        // Get the dashboad data
        $http.get( 'api/user_home')
        .then(function(response) {
            $scope.members_nearby = response.data.members_nearby;
            $scope.user_address   = response.data.places[0];
            $scope.skills         = response.data.skills;
        });

        $scope.start_date = '';
        $scope.feeds_source = {
          get : function(index, count, success) {
            
            if($scope.no_feed === true){
              return success([]);
            }
            // If the no feed flag is set, just return
            return $timeout(function() {
                // Get the dashboad data
                $http.post( 'api/user_wall_data', {start_date : $scope.start_date})
                .then(function(response) {

                    
                    $scope.start_date = response.data.start_date;
                    $scope.no_feed    = response.data.no_feed;
                    var feeds = [];
                    for(var i = 0; i < response.data.feeds.length; i = i + 1){
                      feeds.push(response.data.feeds[i]);

                    }

                    return success(feeds);
                });
            }, 500);
         }
        };
        

        
});