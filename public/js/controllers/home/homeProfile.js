
app.controller('homeProfileCtrl', function($scope, $http, $timeout, $rootScope, FileUploader, $auth, $localStorage, $state) {

    if(!$auth.isAuthenticated()){
      $state.go('access.signin');
      return;
    }

    $http.get( 'api/user_homepage_data')
      .then(function(response) {

        $scope.user = response.data.user;

        $scope.primary_place = response.data.primary_place;

        $scope.average_rating = response.data.average_rating;
        $scope.members_nearby = response.data.members_nearby;

        $scope.ratings = response.data.ratings;
        $scope.my_tasks = response.data.my_tasks;

        $scope.connections = response.data.connections;

        $scope.skills = response.data.skills;

        $scope.learns = response.data.learns;

        $scope.connection_requests = response.data.connection_requests;

    });


    $scope.clickUpload = function(){

         $timeout(function(){angular.element('#new_user_image').trigger('click')});
    };

    var uploader = $scope.uploader = new FileUploader({
        url: '/api/user_upload_image',
        autoUpload : true,
        name: 'user_image',
        withCredentials: true,
        formData : [{token: $auth.getToken()}]
    });

    uploader.onAfterAddingFile = function(fileItem) {
        
        if (fileItem) {
            fileItem.upload();
            
        }  

        fileItem.onSuccess = function(response, status, headers) {

            $scope.user.image_url = response.image_url;
            $rootScope.current_user.image_url = response.image_url;
            $localStorage.current_user = JSON.stringify($rootScope.current_user);
        }; 

        
    };

    

        
        

        
});