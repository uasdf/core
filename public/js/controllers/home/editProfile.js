
app.controller('editProfileCtrl', function($scope, $http, $timeout, $rootScope, toaster, $state, $auth) {


        if(!$auth.isAuthenticated()){
          $state.go('access.signin');
          return;
        }

        $http.get( 'api/user_homepage_data')
          .then(function(response) {

            $scope.user_data = response.data;


        });

          // Add Skill function
        $scope.addSkill = function() {
            
            if($scope.new_skill === undefined || $scope.new_skill === ''){
                return;
            }
            var already_exist = false;
            angular.forEach($scope.user_data.skills, function(value, key) {
              if(value.tag == $scope.new_skill){
                   alert('You have already added this skill.'); 
                   already_exist = true;
              }
            });
            if(already_exist === false){
                var new_skill_data = {
                    skill: $scope.new_skill,
                    type: 'skill'
                };
                $http.post($rootScope.domain_url + '/api/user_add_skill', new_skill_data)
                .then(function addSkillSuccess(response){
                    
                        // Reset the levels
                        $scope.new_skill       = '';
                        $scope.new_skill_level = 2;

                        // Hide the add skill form
                        $scope.show_add_skill_form = !$scope.show_add_skill_form;

                        // Reset the 
                        $scope.user_data.skills = response.data;                        
                });
            }
        };

        // Add Learn function
        $scope.addLearn = function() {
            
            if($scope.new_learn === undefined || $scope.new_learn === ''){
                return;
            }
            var already_exist = false;
            angular.forEach($scope.user_data.learns, function(value, key) {
              if(value.tag == $scope.new_learn){
                   alert('You have already added this skill.'); 
                   already_exist = true;
              }
            });
            if(already_exist === false){
                var new_learn_data = {
                    skill: $scope.new_learn,
                    type: 'learn'
                };
                $http.post($rootScope.domain_url + '/api/user_add_skill', new_learn_data)
                .then(function addLearnSuccess(response){
                    
                        // Reset the levels
                        $scope.new_learn       = '';
                        $scope.new_learn_level = 2;

                        // Hide the add skill form
                        $scope.show_add_learn_form = !$scope.show_add_learn_form;

                        // Reset the skills
                        $scope.user_data.learns = response.data;                        
                });
            }
        };

        // Remove Skill function
        $scope.removeSkill = function( $skill_id, $tag_type) {
                
            var skill_data = {
                skill_id: $skill_id
            };
            $http.post($rootScope.domain_url + '/api/user_remove_skill', skill_data)
            .then(function removeSkillSuccess(response){
                if($tag_type == 'skill'){
                    // Reset the skills
                    $scope.user_data.skills = response.data;  
                } else{
                    // Reset the learn
                    $scope.user_data.learns = response.data;  
                }
            });
        };

        $scope.errors = {};
        $scope.errors.place = false;
        // Update place function
        $scope.updatePlace = function( $place_id, $address) {
                
            var place_data = {
                place_id: $place_id,
                place: $address
            };
            $http.post($rootScope.domain_url + '/api/user_update_place', place_data)
            .then(function updatePlaceSuccess(response){
                
                if(response.data.error == 'invalid_place'){
                    $scope.errors.place = true;
                    toaster.pop('error', 'Error', 'Please enter a proper place.');
                    return;
                }
                $scope.errors.place = false;

                toaster.pop('success', 'Success', 'You have successfully updated your place.');
                // Update the places
                $scope.user_data.primary_place = response.data.primary_place;
            });
        };

        // Update user function
        $scope.updateUser = function() {
            $scope.errors = {};
            $http.post($rootScope.domain_url + '/api/user_update_profile', $scope.user_data.user)
            .then(function addLearnSuccess(response){


                if(response.data.error == 'error'){ 
                    $scope.errors = response.data.errors;
                    toaster.pop('error', 'Error', 'Please correct the errors.');
                    return;
                }

                toaster.pop('success', 'Success', 'Your changes have been saved.');

            });
        };

        
});