// this is a lazy load controller, 
// so start with "app." to register this controller

app.filter('propsFilter', function() {
    return function(items, props) {
        var out = [];

        if (angular.isArray(items)) {
          items.forEach(function(item) {
            var itemMatches = false;

            var keys = Object.keys(props);
            for (var i = 0; i < keys.length; i++) {
              var prop = keys[i];
              var text = props[prop].toLowerCase();
              if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                itemMatches = true;
                break;
              }
            }

            if (itemMatches) {
              out.push(item);
            }
          });
        } else {
          // Let the output be the input untouched
          out = items;
        }

        return out;
    };
})
app.controller('landingCtrl', function($scope, $http, $timeout, $state, $localStorage) {

        // Set the address object
        $scope.address = {};
        $scope.search_text = '';
        $scope.address.selected = {};
        $scope.address.selected.formatted_address = 'Leicester';
        // Check if the user is logged in
        if($localStorage.current_user){
          $state.go('app.dashboard');
        }

        // Get the ip location
        $.getJSON('//ip-api.com/json?callback=?', function(data) {
          $scope.address.selected.formatted_address = data.city; 
        });
        $scope.disabled = undefined;
        $scope.searchEnabled = undefined;

        $scope.enable = function() {
        $scope.disabled = false;
        };

        $scope.disable = function() {
        $scope.disabled = true;
        };

        $scope.enableSearch = function() {
        $scope.searchEnabled = true;
        }

        $scope.disableSearch = function() {
        $scope.searchEnabled = false;
        }

        $scope.clear = function() {
        $scope.address.selected = undefined;
        };




        $scope.distance = { value: '1',      text: '1 Mile'};
        $scope.available_distance = [
            { value: '1',      text: '1 Mile'},
            { value: '2',      text: '2 Miles'},
            { value: '5',      text: '5 Miles'},
            { value: '10',      text: '10 Miles'},
            { value: '50',      text: '50 Miles'}
        ];

        
        $scope.refreshAddresses = function(address) {
          var params = {address: address, sensor: true};
          return $http.get(
            'http://maps.googleapis.com/maps/api/geocode/json',
            {params: params}
          ).then(function(response) {
            $scope.addresses = response.data.results;
          });
        };


        $scope.landingSearch = function() {
          var params = {search_text: $scope.search_text, place: $scope.address.selected.formatted_address, distance: $scope.distance.value};
          return $http.get( 'api/landing_search',
            {params: params}
          ).then(function(response) {
            $scope.tasks = response.data.tasks;
            $scope.users = response.data.users;
          });
        };



        
});