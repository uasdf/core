'use strict';

// feedbackCtrl controller
app.controller('feedbackCtrl', ['$scope', '$modal', '$log', '$http', '$state', '$rootScope', '$location', 'toaster', function($scope, $modal, $log, $http, $state, $rootScope, $location, toaster) {

	$scope.current_url    = $location.absUrl();
    $scope.arr            = $scope.current_url.split("/");
    $rootScope.domain_url = '';
	$scope.feedback = '';

	
	$scope.sendFeedback = function(){

		if($scope.feedback == ''){
			return;
		}
		var params = {
			content : $scope.feedback
		};
		$http.post($rootScope.domain_url + '/api/post_feedback', params)
		.then(function(response){
			toaster.success('Thank you for your feedback!');
			$scope.feedback = '';
		});
	};

}
]);
