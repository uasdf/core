app.controller('manageTasksCtrl', function($scope, $http, $stateParams, $modal, $rootScope, toaster, $location, $state, $auth) {


	if(!$auth.isAuthenticated()){
      $state.go('access.signin');
      return;
    }

    $http.get( 'api/user_homepage_data')
      .then(function(response) {

        $scope.user = response.data.user;

        $scope.ratings        = response.data.ratings;
        $scope.my_tasks       = response.data.my_tasks;
        $scope.resolved_tasks = response.data.resolved_tasks;
        $scope.handling_tasks = response.data.handling_tasks;
        $scope.tasks_nearby   = response.data.tasks_nearby; 

        console.log($scope.tasks_nearby);

    });


});