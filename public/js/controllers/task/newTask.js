app.controller('newTaskCtrl', function($scope, $http, $stateParams, $modal, $rootScope, toaster, $location, $state, $auth) {

    if(!$auth.isAuthenticated()){
      $state.go('access.signin');
      return;
    }
    $scope.new_task = {};
    $scope.new_task.title = '';
    $scope.new_task.summary = '';
    $scope.new_task.assistance = true;
    // Create task function
    $scope.createTask = function() {
        $http.post($rootScope.domain_url + '/api/task_create', $scope.new_task)
        .then(function taskCreated(response){
            if(response.data.success == 'success'){
                $rootScope.task_created = true;
                $location.path('/app/task/' + response.data.slug); 
                return;
            } else if(response.data.error == 'error'){
                $scope.errors = response.data.errors;
                toaster.pop('error', 'Error', 'Please correct the errors.');
            }
        });
    };     
});