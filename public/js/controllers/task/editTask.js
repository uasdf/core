app.controller('editTaskCtrl', function($scope, $http, $stateParams, $modal, $rootScope, toaster, $location, $state, $auth) {
    
    if(!$auth.isAuthenticated()){
      $state.go('access.signin');
      return;
    }

    
    // Get the task slug
    var slug = {
        slug : $stateParams.slug
    };
    $http.post($rootScope.domain_url + '/api/task_data', slug)
        .then(function mySucces(response) {
            // Data obtained from the database
            $scope.task_data = response.data;
        },

        function myError(response) {
            toaster.pop('error', 'Error', 'Some error has occured.');
        }

    );  

    // Update user function
    $scope.updateTask = function() {
        $http.post($rootScope.domain_url + '/api/task_update', $scope.task_data.task)
        .then(function taskSaved(response){
            if(response.data == 'success'){

                toaster.pop('success', 'Success', 'Your changes have been saved.');
            }
        });
    };  

    // Delete
    $scope.deleteTask = function() {
        if(confirm('Are you sure you want to delete this task?')){
            $http.post($rootScope.domain_url + '/api/task_delete', $scope.task_data.task)
            .then(function taskSaved(response){
                if(response.data == 'success'){

                    toaster.pop('success', 'Success', 'Successfully deleted.');
                    $state.go('app.tasks');
                } else {
                    toaster.pop('error', 'Error', 'Unable to delete the task.');

                }
            });
        }
    }; 

    $scope.loadSkills = function(query) {
      return $http.post('/skills' , {query: query})
      .then(function(response) {
        return response.data;
      });
    };

    $scope.new_skill       = [];

    // Add Skill function
    $scope.addSkill = function() {
        
        if($scope.new_skill[0]['text'] === undefined || $scope.new_skill[0]['text'] === ''){
            return;
        }
        $scope.already_exist = false;
        angular.forEach($scope.task_data.skills, function(value, key) {
          if(value.tag == $scope.new_skill[0]['text']){
               $scope.already_exist = true;
          }
        });

        if($scope.already_exist == true){
            $scope.new_skill       = [];
            toaster.pop('Info', 'Already Added', 'You have already added this skill.'); 
            return;
        }

        if($scope.already_exist == false){
            var new_skill_data = {
                skill: $scope.new_skill[0]['text'],
                type: 'skill',
                task_id: $scope.task_data.task.id
            };
            $http.post($rootScope.domain_url + '/api/task_add_skill', new_skill_data)
            .then(function addSkillSuccess(response){
                
                    toaster.pop('success', 'Success', 'Successfully added ' + $scope.new_skill[0]['text'] + 'to the skill.');
                    // Reset the levels
                    $scope.new_skill       = [];
                    // Reset the 
                    $scope.task_data.skills = response.data;                        
            });
        }

    };

    // Remove Skill function
    $scope.removeSkill = function( $skill_id) {
            
        var skill_data = {
            skill_id: $skill_id,
            task_id: $scope.task_data.task.id
        };
        $http.post($rootScope.domain_url + '/api/task_remove_skill', skill_data)
        .then(function removeSkillSuccess(response){
            
            toaster.pop('success', 'Success', 'Successfully removed the skill.');

            // Reset the skills
            $scope.task_data.skills = response.data;  
            
        });
    };
});