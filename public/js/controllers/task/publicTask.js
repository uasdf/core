app.controller('taskCtrl', function($scope, $http, $filter, $timeout, $stateParams, $modal, $rootScope, $location, toaster) {

        $scope.current_url    = $location.absUrl();
        $scope.arr            = $scope.current_url.split("/");
        $rootScope.domain_url = $scope.arr[0] + "//" + $scope.arr[2];
        
        var slug = {
            slug : $stateParams.slug
        };
        $http.post($rootScope.domain_url + '/api/task_publicpage_data', slug)
        .then(function mySucces(response) {
            // Data obtained from the database
            // User counts
            // Map user data

            $scope.task_data = response.data;

            if($rootScope.task_created == true){
                toaster.pop('success', 'Success', 'You have successfully created a task.');
                $rootScope.task_created = false;
            }
        },

        function myError(response) {
            alert('Some error has occured.');
        }

        );


        // Post comment function
        $scope.addComment = function ($comment) {

            if($comment == ''){
                return;
            }

            var comment_data = {
                comment: $comment,
                task_id: $scope.task_data.task.id
            };

            $http.post($rootScope.domain_url + '/api/task_add_new_comment', comment_data)
            .then(function mySucces(response) {
                // Data obtained from the database
                $scope.task_data.comments = response.data.comments;
                $scope.new_comment = '';
            });
        };

        // Show the reply box when reply button is clicked
        $scope.showReplyBox = function ( $comment_id) {
            $scope.reply_comment_id = $comment_id;
            $scope.reply_box = $modal.open({ 
                templateUrl: 'template_reply', 
                className: 'ngdialog-theme-default',
                scope: $scope });
        };

        // Post connect function
        $scope.postReply = function ($reply, $comment_id) {

            if($reply == ''){
                return;
            }

            var reply_data = {
                reply: $reply,
                comment_id: $comment_id
            };

            $http.post($rootScope.domain_url + '/api/task_reply_to_comment', reply_data)
            .then(function mySucces(response) {
                // Data obtained from the database
                $scope.task_data.comments = response.data.comments;
                $scope.reply_box.close();
                toaster.pop('success', 'Success', 'Thanks for your reply.');
                    
            });
        };

        // Show the reply box when reply button is clicked
        $scope.showEditReplyBox = function ( $reply, $reply_id) {
            $scope.current_reply_id = $reply_id;
            $scope.current_edit_reply = $reply;
            $scope.edit_reply_box = $modal.open({ 
                templateUrl: 'template_edit_reply', 
                className: 'ngdialog-theme-default',
                scope: $scope });
        };

        // Post connect function
        $scope.postEditReply = function ($reply, $reply_id) {

            if($reply == ''){
                return;
            }
            var reply_data = {
                reply: $reply,
                reply_id: $reply_id
            };

            $http.post($rootScope.domain_url + '/api/task_edit_reply', reply_data)
            .then(function mySucces(response) {
                // Data obtained from the database
                $scope.task_data.comments = response.data.comments;
                $scope.edit_reply_box.close();
                toaster.pop('success', 'Success', 'Your reply has been updated.');
                
            });
        };

        // Show the reply box when reply button is clicked
        $scope.showEditCommentBox = function ( $comment, $comment_id) {
            $scope.current_comment_id = $comment_id;
            $scope.current_edit_comment = $comment;
            $scope.edit_comment_box = $modal.open({ 
                templateUrl: 'template_edit_comment', 
                className: 'ngdialog-theme-default',
                scope: $scope });
        };

        // Edit comment function
        $scope.postEditComment = function ($comment, $comment_id) {

            if($comment == ''){
                return;
            }
            var comment_data = {
                comment: $comment,
                comment_id: $comment_id
            };

            $http.post($rootScope.domain_url + '/api/task_edit_comment', comment_data)
            .then(function mySucces(response) {
                // Data obtained from the database
                $scope.task_data.comments = response.data.comments;
                $scope.edit_comment_box.close(); 
                toaster.pop('success', 'Success', 'Your comment has been saved.');
                
            });
        };

        // Comment mark helpful function
        $scope.markHelpful = function ($comment_id) {

            var comment_data = {
                comment_id: $comment_id
            };

            $http.post($rootScope.domain_url + '/api/task_comment_helpful', comment_data)
            .then(function mySucces(response) {
                // Data obtained from the database
                $scope.task_data.comments = response.data.comments;         
                toaster.pop('success', 'Success', 'Thank you for your feedback.');           
            });
        };

        // Remove comment function
        $scope.removeComment = function ($comment_id) {

            var comment_data = {
                comment_id: $comment_id
            };

            $http.post($rootScope.domain_url + '/api/task_remove_comment', comment_data)
            .then(function mySucces(response) {
                // Data obtained from the database
                $scope.task_data.comments = response.data.comments;      
                toaster.pop('info', 'Success', 'Your comment has been removed.');              
            });
        };

        // Remove comment function
        $scope.removeReply = function ($reply_id) {

            var reply_data = {
                reply_id: $reply_id
            };

            $http.post($rootScope.domain_url + '/api/task_remove_reply', reply_data)
            .then(function mySucces(response) {
                // Data obtained from the database
                $scope.task_data.comments = response.data.comments;
                toaster.pop('info', 'Success', 'Your reply has been removed.');
            });
        };

        // Hide haldle request form by default
        $scope.task_handle_message = 'I would like to help you with this task.';
        
        // Send task handle request
        $scope.showHandleForm = function () {

            $scope.handle_task_form = $modal.open({ 
                templateUrl: 'template_send_handle_request', 
                className:   'ngdialog-theme-default',
                scope:       $scope });
        };

        // Send task handle request
        $scope.sendHandleRequest = function ($message) {

            var task_handle_data = {
                message: $message,
                task_id: $scope.task_data.task.id
            };

            $http.post($rootScope.domain_url + '/api/task_send_handle_request', task_handle_data)
            .then(function mySucces(response) {
                // Data obtained from the database
                $scope.task_data.handle_requests  = response.data.handle_requests;
                $scope.task_data.awating_approval = response.data.awating_approval;

                // close the handle form
                $scope.handle_task_form.close();
                toaster.pop('success', 'Success', 'Your handle request has been sent.');
            });
        };

        // Hide haldle request form by default
        $scope.show_cancel_handle_form = false;
        $scope.task_handle_cancel_message = 'I\'m sorry to infom that I will not be able to handle this task.';

        // show cancel handling form
        $scope.showCancelHandleForm = function () {

            $scope.cancel_task_handle_form = $modal.open({ 
                templateUrl: 'template_cancel_task_handling_form', 
                className:   'ngdialog-theme-default',
                scope:       $scope });
        };

        // Send cancel handling
        $scope.cancelTaskHandling = function () {

            var task_handle_data = {
                cancel_reason: $scope.task_handle_cancel_message,
                task_id:       $scope.task_data.task.id
            };

            $http.post($rootScope.domain_url + '/api/task_cancel_handle', task_handle_data)
            .then(function mySucces(response) {
                // Data obtained from the database
                $scope.task_data.handle_cancelled = true;
                $scope.task_data.handle_approved  = false;
                $scope.task_data.handle_requests  = response.data.handle_requests;

                // close thr form
                $scope.cancel_task_handle_form.close();
                toaster.pop('info', 'Handle Canclled', 'Your handle request has been now canclled.');
            });
        };

        // Send task handle request
        $scope.cancelAwaitingApproval = function () {

            var task_handle_data = {
                task_id: $scope.task_data.task.id
            };

            $http.post($rootScope.domain_url + '/api/task_cancel_awaiting_handle_request', task_handle_data)
            .then(function mySucces(response) {
                // Data obtained from the database
                $scope.task_data.handle_requests  = response.data.handle_requests;
                $scope.task_data.handle_cancelled = true;
            });
        };

        // Accept handling request
        $scope.acceptHandleRequest = function ($handle_id) {

            var task_handle_data = {
                task_id: $scope.task_data.task.id,
                handle_id: $handle_id
            };

            $http.post($rootScope.domain_url + '/api/task_accept_handle_request', task_handle_data)
            .then(function mySucces(response) {
                // Data obtained from the database
                $scope.task_data.handle_requests  = response.data.handle_requests;
            });
        };

        // Reject handling request
        $scope.rejectHandleRequest = function ($handle_id) {

            var task_handle_data = {
                handle_id: $handle_id
            };

            $http.post($rootScope.domain_url + '/api/task_reject_handle_request', task_handle_data)
            .then(function mySucces(response) {
                // Data obtained from the database
                $scope.task_data.handle_requests  = response.data.handle_requests;
            });
        };

        // Reject handling request
        $scope.ownerCancelHandleRequest = function ($handle_id) {

            var task_handle_data = {
                handle_id: $handle_id
            };

            $http.post($rootScope.domain_url + '/api/task_owner_cancel_handle_request', task_handle_data)
            .then(function mySucces(response) {
                // Data obtained from the database
                $scope.task_data.handle_requests  = response.data.handle_requests;
            });
        };
});