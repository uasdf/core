
app.controller('publicProfileCtrl', function($scope, $http, $timeout, $rootScope, $stateParams, $modal, toaster) {


    $http.post( 'api/user_publicpage_data', {'username' : $stateParams.username})
      .then(function(response) {

        $scope.user = response.data.user;

        $scope.primary_place = response.data.primary_place;

        $scope.average_rating = response.data.average_rating;
        $scope.members_nearby = response.data.members_nearby;

        $scope.ratings = response.data.ratings;
        $scope.my_tasks = response.data.my_tasks;

        $scope.connections           = response.data.connections;
        $scope.connection            = response.data.connection;
        $scope.to_sent               = response.data.to_sent;
        $scope.current_sent          = response.data.current_sent;
        $scope.connectionStatus      = response.data.connectionStatus;
        $scope.current_user_reviewed = response.data.current_user_reviewed;

        $scope.skills = response.data.skills;

        $scope.learns = response.data.learns;

    });

      // Show the connection box when connect button is clicked
        $scope.showConnectBox = function () {
            $scope.connect_box = $modal.open({ 
                templateUrl: 'template_connect', 
                className: 'ngdialog-theme-default',
                scope: $scope });
        };

        // Post connect function
        $scope.postConnect = function ($message, $purpose) {
            var connect_data = {
                message: $message,
                purpose: $purpose,
                to_user: $scope.user.id
            };
            $http.post($rootScope.domain_url + '/api/send_connection_request', connect_data)
            .then(function mySucces(response) {
                // Data obtained from the database
                $scope.connectionStatus = 1;
                $scope.current_sent = true;

                $scope.connect_box.close();

                
            });
        };

        // Respond to connection function
        $scope.responseConnect = function ($status, $request_id) {
            
            // Status 1 - Initial
            // Status 2 - Accepted
            // Status 3 - Rejected 
            var reponse_data = {
                status: $status,
                request_id: $request_id
                
            };
            $http.post($rootScope.domain_url + '/api/user_respond_connect', reponse_data)
            .then(function mySucces(response) {
                // Data obtained from the database
                $scope.connectionStatus = response.data;
                
            });
        };

        // Respond to connection function
        $scope.addReview = function ($rating, $comment) {
            var review_data = {
                rating: $rating,
                comment: $comment,
                to_user: $scope.user.id
                
            };
            $http.post( '/api/user_post_rating', review_data)
            .then(function mySucces(response) {
                // Data obtained from the database
                $scope.ratings               = response.data.ratings;
                $scope.average_rating        = response.data.average_rating;
                $scope.current_user_reviewed = true;
                
            });
        };

        // Show MEssage box
        // Show the connection box when connect button is clicked
        $scope.showMessageBox = function () {
            $scope.message_box = $modal.open({ 
                templateUrl: 'template_message', 
                className: 'ngdialog-theme-default',
                scope: $scope });
        };

        // Post connect function
        $scope.postMessage = function ($message) {
            var message_data = {
                message: $message,
                to_user: $scope.user.id
            };
            $http.post($rootScope.domain_url + '/api/user_send_message', message_data)
            .then(function mySucces(response) {
                // Data obtained from the database
                $scope.message_box.close();

                if(response.data.success == 'success'){
                    toaster.pop('success', 'Success', 'Message sent!');
                }
            });
        };





        
});