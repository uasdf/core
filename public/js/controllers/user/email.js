app.controller('emailController', function($scope, $http, $stateParams, $modal, $rootScope, toaster, $location) {
    
    
    $http.post($rootScope.domain_url + '/api/get-subscribtion', {token: $stateParams.token})
        .then(function mySucces(response) {

            $scope.weekly_newsletter   = response.data.weekly_newsletter;
            $scope.new_message         = response.data.new_message;
            $scope.task_comment        = response.data.task_comment;
            $scope.task_handle_request = response.data.task_handle_request;
            $scope.profile_connection  = response.data.profile_connection;
            $scope.profile_rating      = response.data.profile_rating;
        },

        function myError(response) {
            toaster.pop('error', 'Error', 'Some error has occured.');
        }

    );  

    $scope.updateSubscribtion = function(){

        var subscribtion = {
            token:               $stateParams.token,
            weekly_newsletter:   $scope.weekly_newsletter,
            new_message:         $scope.new_message,
            task_comment:        $scope.task_comment,
            task_handle_request: $scope.task_handle_request,
            profile_connection:  $scope.profile_connection,
            profile_rating:      $scope.profile_rating,
        };

        $http.post($rootScope.domain_url + '/api/update-subscription', subscribtion)
            .then(function mySucces(response) {
                // if success is returned
                if(response.data == 'success'){
                    toaster.pop('success', 'Success', 'Your email settings has been updated.');
                }
            },

            function myError(response) {
                toaster.pop('error', 'Error', 'Some error has occured.');
            }

        );  
    }

});