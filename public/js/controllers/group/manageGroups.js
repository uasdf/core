app.controller('manageGroupsCtrl', function($scope, $http, $stateParams, $modal, $rootScope, toaster, $location, $state, $auth) {


	if(!$auth.isAuthenticated()){
      $state.go('access.signin');
      return;
    }

  // Get the dashboad data
  $http.get( 'api/groups_home')
  .then(function(response) {

      console.log(response.data);
      $scope.groups = response.data.groups;
      $scope.place  = response.data.place;
  });


});