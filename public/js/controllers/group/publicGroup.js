app.controller('groupCtrl', function($scope, $http, $filter, $timeout, $stateParams, $modal, $rootScope, $location, toaster) {

        $scope.current_url    = $location.absUrl();
        $scope.arr            = $scope.current_url.split("/");
        $rootScope.domain_url = $scope.arr[0] + "//" + $scope.arr[2];
        
        var slug = {
            slug : $stateParams.slug
        };
        $http.post($rootScope.domain_url + '/api/group_publicpage_data', slug)
            .then(function mySucces(response) {
                // Data obtained from the database
                // User counts
                // Map user data

                $scope.group_data = response.data;
                $scope.tasks      = response.data.tasks;
                $scope.members    = response.data.members;

                console.log(response.data);

            },

            function myError(response) {
                alert('Some error has occured.');
            }

        );


});