'use strict';

/* Controllers */
  // signin controller
app.controller('SigninFormController', ['$scope', '$http', '$state','$location', '$auth', '$localStorage', '$rootScope', '$stateParams', function($scope, $http, $state, $location, $auth, $localStorage, $rootScope, $stateParams) {
    $scope.user = {};
    $scope.authError = null;

    $scope.current_url    = $location.absUrl();
    $scope.arr            = $scope.current_url.split("/");
    $rootScope.domain_url = $scope.arr[0] + "//" + $scope.arr[2];

    $auth.logout();

    // Logout the user
    $localStorage.current_user = null;
    $rootScope.current_user = null;

    $scope.login = function() {
            var credentials = {
                email: $scope.user.email,
                password: $scope.user.password
            }

            
            $scope.has_error = false;
            
            // Use Satellizer's $auth service to login
            $auth.login(credentials).then(function(response) {

                if(response.data.new_user === true){
                    $state.go('access.signup2');
                    return;
                }
                // Return an $http request for the now authenticated
                // user so that we can flatten the promise chain
                return $http.get($scope.domain_url + '/api/authenticate/user');
            }, function(error) {

                $scope.has_error = true;
                if(error.data.error == 'invalid_credentials'){
                    $scope.authError = 'Invalid Credentials.';
                } else if(error.data.error == 'could_not_create_token'){
                    $scope.authError = 'Could not create a session. Please try again later.';
                } else{
                    $scope.authError = 'Some error has occured.'; 
                }
                
            // Because we returned the $http.get request in the $auth.login
            // promise, we can chain the next promise to the end here
            })
            .then(function (response){

                // The user's authenticated state gets flipped to
                // true so we can now show parts of the UI that rely
                // on the user being logged in
                // $rootScope.authenticated = true;

                // Putting the user's data on $rootScope allows
                // us to access it anywhere across the app
                $rootScope.current_user = response.data.user;
                $scope.current_user = response.data.user;
                $rootScope.current_user_place = response.data.place;
                // Stringify the returned data to prepare it
                // to go into local storage
                var user = JSON.stringify(response.data.user);

                // Set the stringified user data into local storage
                $localStorage.current_user = user;
                $localStorage.current_user_place = JSON.stringify(response.data.place);

                // If login is successful, redirect to the users state
                $state.go('app.dashboard');

            });

        }

        $scope.signinSocial = function(site){
                // Use Satellizer's $auth service to login
                $auth.authenticate(site).then(function(response) {

                    if(response.data.new_user === true){
                        $state.go('access.signup2');
                        return 'new_user';
                    }
                    // Return an $http request for the now authenticated
                    // user so that we can flatten the promise chain
                    return $http.get($scope.domain_url + '/api/authenticate/user');
                }, function(error) {

                    $scope.has_error = true;
                    if(error.data.error == 'invalid_credentials'){
                        $scope.authError = 'Invalid Credentials.';
                    } else if(error.data.error == 'could_not_create_token'){
                        $scope.authError = 'Could not create a session. Please try again later.';
                    } else{
                        $scope.authError = 'Some error has occured.'; 
                    }
                    
                // Because we returned the $http.get request in the $auth.login
                // promise, we can chain the next promise to the end here
                })
                .then(function (response){

                    if(response == 'new_user'){
                        return;
                    }
                    // The user's authenticated state gets flipped to
                    // true so we can now show parts of the UI that rely
                    // on the user being logged in
                    // $rootScope.authenticated = true;

                    // Putting the user's data on $rootScope allows
                    // us to access it anywhere across the app
                    $rootScope.current_user = response.data.user;
                    $scope.current_user = response.data.user;
                    $rootScope.current_user_place = response.data.place;

                    if($rootScope.current_user_place === null){
                        if ( response.data.token != null ){
                            $auth.setToken(response.data.token);
                            $state.go('access.signup2');
                        }
                    }
                    // Stringify the returned data to prepare it
                    // to go into local storage
                    var user = JSON.stringify(response.data.user);

                    // Set the stringified user data into local storage
                    $localStorage.current_user = user;
                    $localStorage.current_user_place = JSON.stringify(response.data.place);

                    // If login is successful, redirect to the users state
                    $state.go('app.dashboard');

                });
            }

        $scope.reset_email = '';
        $scope.sendResetLink = function (){
            $scope.linkSent = true;
            $scope.emailError = true;
            var params = {
                email: $scope.reset_email
            };
            $http.post($rootScope.domain_url + '/password/email', params)
                .then(function mySucces(response) {
                    if(response.data == 'RESET_LINK_SENT'){
                        $scope.linkSent = false;
                    } else{
                        $scope.emailError = !$scope.emailError;

                    }
                }
            ); 
        }

        // Get the reset token
        $scope.reset_token = $stateParams.token;
        $scope.submitReset = function (){
            $scope.linkSent = true;
            $scope.emailError = true;
            var params = {
                token:                 $scope.reset_token,
                email:                 $scope.reset_email,
                password:              $scope.reset_password,
                password_confirmation: $scope.reset_password,

            };
            $http.post($rootScope.domain_url + '/password/reset', params)
                .then(function mySucces(response) {
                    if(response.data == 'RESET_SUCCESS'){
                        $scope.linkSent = false;
                    } else{
                        $scope.emailError = !$scope.emailError;

                    }
                }
            ); 
        }

  }])
;