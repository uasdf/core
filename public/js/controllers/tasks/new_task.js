'use strict';


app.filter('propsFilter', function() {
    return function(items, props) {
        var out = [];

        if (angular.isArray(items)) {
          items.forEach(function(item) {
            var itemMatches = false;

            var keys = Object.keys(props);
            for (var i = 0; i < keys.length; i++) {
              var prop = keys[i];
              var text = props[prop].toLowerCase();
              if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                itemMatches = true;
                break;
              }
            }

            if (itemMatches) {
              out.push(item);
            }
          });
        } else {
          // Let the output be the input untouched
          out = items;
        }

        return out;
    };
})

// signup controller
app.controller('newTaskCtrl', ['$scope', '$http', '$state', function($scope, $http, $state) {
    $scope.user = {};
   
    
    $scope.skills = [];
    $scope.learn = [];
    $scope.place = '';
    $scope.show_done = false;
    $scope.show_step_2 = false;
    $scope.show_step_3 = false;

    $scope.loadSkills = function(query) {
      
      return [
        {text : 'asdasdf'},
        {text : 'asdsdasdf'},
        {text : 'asasddasdf'},
        {text : 'asdawqesdf'},
      ];

      //return $http.get('/skills?query=' + query);
    };


    $scope.checkSteps = function(){

      //  If skills are cles, show done
      if($scope.skills.length > 0 && $scope.show_step_2 !== true){
        $scope.show_done = true;
        return;
      } else if ($scope.skills.length == 0){
        $scope.show_done = false;
        $scope.show_step_2 = false;
        $scope.show_step_3 = false;
        return
      } else if ($scope.show_step_2 == true){
        $scope.show_done = false;
      }

      //  If learns are cles, show done
      if($scope.learn.length > 0 && $scope.show_step_3 !== true){
        $scope.show_done = true;
        return;
      } else if ($scope.learn.length == 0){
        $scope.show_done = false;
        $scope.show_step_2 = true;
        $scope.show_step_3 = false;
        return
      }else if ($scope.show_step_3 == true){
        $scope.show_done = false;
      }
      $scope.show_done = false;
      //  If place entered, show final done
      if($scope.place.length > 0){
        $scope.show_done = false;
        $scope.show_final_done = true;
        return;
      } 
    }


    $scope.checkDone = function(){

      //  If skills are clear, show done
      if($scope.skills.length > 0 && $scope.show_step_2 == false){
        $scope.show_done   = false;
        $scope.show_step_2 = true;
      } 

      //  If learns are clear, show done
      if($scope.learn.length > 0 && $scope.show_step_3 == false){
        $scope.show_done   = false;
        $scope.show_step_3 = true;
      } 

    }


    $scope.address = {};
      $scope.refreshAddresses = function(address) {
      var params = {address: address, sensor: true};
      return $http.get(
        'http://maps.googleapis.com/maps/api/geocode/json',
        {params: params}
      ).then(function(response) {
        $scope.addresses = response.data.results;
      });
    };
  }]);
