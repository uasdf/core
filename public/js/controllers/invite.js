'use strict';

// signup controller
app.controller('inviteCtrl', ['$scope', '$modal', '$log', '$http', '$state', 'googleContacts', '$rootScope', '$location', 'toaster', function($scope, $modal, $log, $http, $state, googleContacts, $rootScope, $location, toaster) {


	// get the invitations data
	$http.get($rootScope.domain_url + '/invite-friends')
	    .then(function mySucces(response) {
	        // Data obtained from the database
	        $scope.pending_invites = response.data.previousInvites;
	    },

	    function myError(response) {
	        toaster.pop('error', 'Error', 'Some error has occured.');
	    }

	); 

     $scope.sendInvite = function (){

     	var params = {
     		email: $scope.email
     	};
     	$http.post($rootScope.domain_url + '/invite/send-invite', params)
	        .then(function mySucces(response) {
	            // Data obtained from the database
	            if(response.data == 'success') {
                    toaster.pop('success', 'Success', 'Successfully sent!');
                    $scope.pending_inviations = response.data.pending_inviations;
                    $scope.email = '';
                }else if(response.data == 'already'){
                    toaster.pop('error', 'Error', 'You have already invited ' + $scope.email);
                }else{
                    // $location.path('#/app/user/' + response.data);
                }
	        },

	        function myError(response) {
	            toaster.pop('error', 'Error', 'Some error has occured.');
	        }

	    ); 

     };  
	
}
]);
