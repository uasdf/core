'use strict';


app.filter('propsFilter', function() {
    return function(items, props) {
        var out = [];

        if (angular.isArray(items)) {
          items.forEach(function(item) {
            var itemMatches = false;

            var keys = Object.keys(props);
            for (var i = 0; i < keys.length; i++) {
              var prop = keys[i];
              var text = props[prop].toLowerCase();
              if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                itemMatches = true;
                break;
              }
            }

            if (itemMatches) {
              out.push(item);
            }
          });
        } else {
          // Let the output be the input untouched
          out = items;
        }

        return out;
    };
})

// signup controller
app.controller('SignupFormController', ['$scope', '$http', '$state','$auth','$localStorage', '$rootScope', 
                                function($scope,   $http,   $state, $auth,  $localStorage,    $rootScope) {
    


    $scope.user = {};
    $scope.authError = null;
    $localStorage.current_user = null;
    $rootScope.current_user = null;

    // Satellizer configuration that specifies which API
    // route the JWT should be retrieved from
    

    $scope.signupSocial = function(site){

      $auth.authenticate(site)
        .then(function(response) {
          // Signed in with Google.
          $auth.setToken(response.data.token);
          $state.go('access.signup2');
        })
        .catch(function(response) {
          // Something went wrong.
          alert('Some error has occured. Please try again later.');
        });
    }

    $scope.signup = function() {
      $scope.authError = null;

      // Try to create
      $http.post('auth/register', {name: $scope.user.name, email: $scope.user.email, password: $scope.user.password})
      .then(function(response) {

        if ( response.data.token != null ){
          $auth.setToken(response.data.token);
          $state.go('access.signup2');
        }
      }, function(x) {
        $scope.authError = 'Server Error';
      });


    };
    
    $scope.skills = [];
    $scope.learn = [];
    $scope.place = '';
    $scope.show_done = false;
    $scope.show_step_2 = false;
    $scope.show_step_3 = false;

    $scope.loadSkills = function(query) {
      return $http.post('/skills' , {query: query})
      .then(function(response) {
        return response.data;
      });
    };


    $scope.checkSteps = function(){

      //  If skills are cles, show done
      if($scope.skills.length > 0 && $scope.show_step_2 !== true){
        $scope.show_done = true;
        return;
      } else if ($scope.skills.length == 0){
        $scope.show_done = false;
        $scope.show_step_2 = false;
        $scope.show_step_3 = false;
        return
      } else if ($scope.show_step_2 == true){
        $scope.show_done = false;
      }

      //  If learns are cles, show done
      if($scope.learn.length > 0 && $scope.show_step_3 !== true){
        $scope.show_done = true;
        return;
      } else if ($scope.learn.length == 0){
        $scope.show_done = false;
        $scope.show_step_2 = true;
        $scope.show_step_3 = false;
        return
      }
      $scope.show_done = false;
      //  If place entered, show final done
      if($scope.address.formatted_address != ''){
        $scope.show_done = false;
        $scope.show_final_done = true;
        return;
      } 
    }


    $scope.checkDone = function(){

      //  If skills are clear, show done
      if($scope.skills.length > 0 && $scope.show_step_2 == false){
        $scope.show_done   = false;
        $scope.show_step_2 = true;
      } 

      //  If learns are clear, show done
      if($scope.learn.length > 0 && $scope.show_step_3 == false){
        $scope.show_done   = false;
        $scope.show_step_3 = true;
        //  If place entered, show final done
        if($scope.address.formatted_address != ''){
          $scope.show_final_done = true;
        } 
      } 

    }


    $scope.address = {};
    $scope.address.selected = {};
    $.getJSON('//ip-api.com/json?callback=?', function(data) {
      $scope.address.selected.formatted_address = data.city; 

    });
    $scope.refreshAddresses = function(address) {
      if(address == '' || address == null){
        return;
      }
      
      return $http({
        method: 'GET',
        url: 'http://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&sensor=true',
        skipAuthorization: true

      }).then(function(response) {
        $scope.addresses = response.data.results;
      });
    };


    $scope.signup2 = function() {


      $scope.authError = null;
      // Try to create
      $http.post('auth/register2', {skills: $scope.skills, learn: $scope.learn, place: $scope.address.selected})
      .then(function(response) {
        if ( response.data.success == 'success' ){
          // Set the user data
          // Putting the user's data on $rootScope allows
          // us to access it anywhere across the app
          $rootScope.current_user = response.data.user;
          $scope.current_user = response.data.user;
          $rootScope.current_user_place = response.data.place;

          // Stringify the returned data to prepare it
          // to go into local storage
          var user = JSON.stringify(response.data.user);

          // Set the stringified user data into local storage
          $localStorage.current_user = user;
          $localStorage.current_user_place = JSON.stringify(response.data.place);

          $state.go('app.dashboard');
        }
      }, function(x) {
        $scope.authError = 'Server Error';
      });
    };

  }]);
