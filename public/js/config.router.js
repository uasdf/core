'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .run(
    [          '$rootScope', '$state', '$stateParams', '$localStorage',
      function ($rootScope,   $state,   $stateParams,  $localStorage) {
          $rootScope.$state = $state;
          $rootScope.$stateParams = $stateParams;   

      }
    ]
  )
  .config(
    [          '$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 'MODULE_CONFIG',
      function ($stateProvider,   $urlRouterProvider, JQ_CONFIG, MODULE_CONFIG) {
          var layout = "tpl/app.html";
          $urlRouterProvider.otherwise('/');

          $stateProvider
              .state('/', {
                  url: '/',
                  templateUrl: 'tpl/welcome.html',
                  resolve: load( ['ui.select',  'js/controllers/landing/landingCtrl.js'] )
              })
              .state('app', {
                  abstract: true,
                  url: '/app',
                  templateUrl: layout,
                  resolve: load( ['js/controllers/home/header.js',
                    'angular-toArrayFilter', 
                      'pusher-angular', 
                      'toaster'] )
              })
              
              .state('app.dashboard', {
                  url: '/dashboard',
                  templateUrl: 'tpl/dashboard.html',
                  controller: 'dashboardCtrl',
                  resolve: load(['ui.select','js/controllers/home/dashboard.js'])
              })
              .state('app.search', {
                  url: '/search',
                  templateUrl: 'tpl/search.html',
                  controller: 'searchCtrl',
                  resolve: load(['ui.select','js/controllers/home/search.js'])
              })
              .state('app.messages', {
                  url: '/messages',
                  controller: 'messageCtrl',
                  templateUrl: 'tpl/home/messages.html',
                  resolve: load(['angular-toArrayFilter', 
                      'pusher-angular',
                      'js/controllers/home/messages.js',
                      'js/directives/scrollToBottom.js',
                      'js/directives/ng-enter.js'
                      ])
              })
              .state('app.messages-xs', {
                  url: '/messages-xs',
                  controller: 'messageMobileCtrl',
                  templateUrl: 'tpl/home/messages-xs.html',
                  resolve: load(['angular-toArrayFilter', 
                      'pusher-angular',
                      'js/controllers/home/messages.js',
                      'js/directives/scrollToBottom.js',
                      'js/directives/ng-enter.js'
                      ])
              })
              
              .state('app.invite', {
                  url: '/invite',
                  templateUrl: 'tpl/invite.html',
                  controller: 'inviteCtrl',
                  resolve: load(['js/controllers/invite.js', 
                    'https://apis.google.com/js/client.js',
                    'js/services/google-contacts.js', 
                    'googleContacts',
                    'toaster'
                    
                    ])
              })

              .state('app.feedback', {
                  url: '/feedback',
                  templateUrl: 'tpl/feedback.html',
                  controller: 'feedbackCtrl',
                  resolve: load(['js/controllers/feedback.js', 'toaster', 'textAngular'])
              })
              .state('app.form.xeditable', {
                  url: '/xeditable',
                  templateUrl: 'tpl/form_xeditable.html',
                  controller: 'XeditableCtrl',
                  resolve: load(['xeditable','js/controllers/xeditable.js'])
              })
              
              // home
              .state('app.home', {
                  url: '/home',
                  template: '<div ui-view class="fade-in-down"></div>'
              })
              .state('app.home.profile', {
                  url: '/profile',
                  templateUrl: 'tpl/home/profile.html',
                  controller: 'homeProfileCtrl',
                  resolve: load(['ui.select','angularFileUpload', 'js/controllers/home/homeProfile.js'])
              })
              .state('app.home.edit-profile', {
                  url: '/edit-profile',
                  templateUrl: 'tpl/home/edit-profile.html',
                  controller: 'editProfileCtrl',
                  resolve: load(['ui.select','js/controllers/home/editProfile.js',
                                 'toaster'])
              })

              // user
              .state('app.user', {
                  url: '/user',
                  template: '<div ui-view class="fade-in-down"></div>'
              })
              .state('app.user.profile', {
                  url: '/{username}',
                  templateUrl: 'tpl/user/profile.html',
                  controller: 'publicProfileCtrl',
                  resolve: load(['js/controllers/user/publicProfile.js', 'toaster'])
              })
              // Group routes
              .state('app.group', {
                  url: '/group',
                  template: '<div ui-view class="fade-in-down"></div>'
              })
              // Group routes
              .state('app.groups', {
                  url: '/groups',
                  templateUrl: 'tpl/group/groups.html',
                  controller: 'manageGroupsCtrl',
                  resolve: load(['toaster', 'js/controllers/group/manageGroups.js'])
              })
              // Group routes
              .state('app.group.page', {
                  url: '/{slug}',
                  templateUrl: 'tpl/group/group.html',
                  controller: 'groupCtrl',
                  resolve: load(['toaster', 'js/controllers/group/publicGroup.js', 'textAngular'])
              })

              // task routes
              .state('app.task', {
                  url: '/task',
                  template: '<div ui-view class="fade-in-down"></div>'
              })
              // task routes
              .state('app.tasks', {
                  url: '/tasks',
                  templateUrl: 'tpl/task/manage-tasks.html',
                  controller: 'manageTasksCtrl',
                  resolve: load(['toaster', 'js/controllers/task/manageTasks.js'])
              })
              // task routes
              .state('app.task.page', {
                  url: '/{slug}',
                  templateUrl: 'tpl/task/task.html',
                  controller: 'taskCtrl',
                  resolve: load(['toaster', 'js/controllers/task/publicTask.js', 'textAngular'])
              })


              // task routes
              .state('app.new-task', {
                  url: '/new-task',
                  templateUrl: 'tpl/task/new-task.html',
                  controller: 'newTaskCtrl',
                  resolve: load(['toaster', 'js/controllers/task/newTask.js', 'textAngular'])
              })


              // task routes
              .state('app.edit-task', {
                  url: '/edit-task',
                  template: '<div ui-view class="fade-in-down"></div>'
              })
              // task routes
              .state('app.edit-task.task', {
                  url: '/{slug}',
                  templateUrl: 'tpl/task/edit-task.html',
                  controller: 'editTaskCtrl',
                  resolve: load(['toaster', 'ngTagsInput', 'js/controllers/task/editTask.js', 'textAngular'])
              })

              // Notifications route
              // task routes
              .state('app.notifications', {
                  url: '/notifications',
                  //template: '<div ui-view class="fade-in-down"></div>',
                  templateUrl: 'tpl/home/notifications.html',
                  resolve: load(['js/controllers/home/notifications.js'])
              })

              .state('subscribtion', {
                  url: '/subscribtion/{token}',
                  templateUrl: 'tpl/user/unsubscribe.html',
                  resolve: load(['toaster', 'js/controllers/user/email.js'])
              })

              // pages
              .state('app.page', {
                  url: '/page',
                  template: '<div ui-view class="fade-in-down"></div>'
              })
              .state('app.page.post', {
                  url: '/post',
                  templateUrl: 'tpl/page_post.html'
              })
              .state('app.page.search', {
                  url: '/search',
                  templateUrl: 'tpl/page_search.html'
              })
              // others
              .state('lockme', {
                  url: '/lockme',
                  templateUrl: 'tpl/page_lockme.html'
              })
              .state('access', {
                  url: '/access',
                  template: '<div ui-view class="fade-in-right-big smooth"></div>'
              })
              .state('access.signin', {
                  url: '/signin',
                  templateUrl: 'tpl/page_signin.html',
                  resolve: load( ['js/controllers/signin.js'] )
              })
              .state('access.signup', {
                  url: '/signup',
                  templateUrl: 'tpl/page_signup.html',
                  resolve: load( ['js/controllers/signup.js'] )
              })
              .state('access.signup2', {
                  url: '/signup2',
                  templateUrl: 'tpl/signup2.html',
                  resolve: load( ['ngTagsInput', 'ui.select','js/controllers/signup.js']
                      )
              })
              .state('access.forgotpwd', {
                  url: '/forgot-password',
                  templateUrl: 'tpl/forgot-password.html',
                  resolve: load( ['js/controllers/signin.js'])
              })
              .state('access.reset-password', {
                  url: '/reset-password/{token}',
                  templateUrl: 'tpl/reset-password.html',
                  resolve: load( ['js/controllers/signin.js'])
              })
              .state('access.404', {
                  url: '/404',
                  templateUrl: 'tpl/page_404.html'
              })

              // fullCalendar
              .state('app.calendar', {
                  url: '/calendar',
                  templateUrl: 'tpl/app_calendar.html',
                  // use resolve to load other dependences
                  resolve: load(['moment','fullcalendar','ui.calendar','js/app/calendar/calendar.js'])
              })

              // mail
              .state('app.mail', {
                  abstract: true,
                  url: '/mail',
                  templateUrl: 'tpl/mail.html',
                  // use resolve to load other dependences
                  resolve: load( ['js/app/mail/mail.js','js/app/mail/mail-service.js','moment'] )
              })
              .state('app.mail.list', {
                  url: '/inbox/{fold}',
                  templateUrl: 'tpl/mail.list.html'
              })
              .state('app.mail.detail', {
                  url: '/{mailId:[0-9]{1,4}}',
                  templateUrl: 'tpl/mail.detail.html'
              })
              .state('app.mail.compose', {
                  url: '/compose',
                  templateUrl: 'tpl/mail.new.html'
              })
              .state('app.todo.list', {
                  url: '/{fold}'
              })
              .state('app.note', {
                  url: '/note',
                  templateUrl: 'tpl/apps_note_material.html',
                  resolve: load(['js/app/note/note.js', 'moment'])
              });

          function load(srcs, callback) {
            return {
                deps: ['$ocLazyLoad', '$q',
                  function( $ocLazyLoad, $q ){
                    var deferred = $q.defer();
                    var promise  = false;
                    srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
                    if(!promise){
                      promise = deferred.promise;
                    }
                    angular.forEach(srcs, function(src) {
                      promise = promise.then( function(){
                        if(JQ_CONFIG[src]){
                          return $ocLazyLoad.load(JQ_CONFIG[src]);
                        }
                        angular.forEach(MODULE_CONFIG, function(module) {
                          if( module.name == src){
                            name = module.name;
                          }else{
                            name = src;
                          }
                        });
                        return $ocLazyLoad.load(name);
                      } );
                    });
                    deferred.resolve();
                    return callback ? promise.then(function(){ return callback(); }) : promise;
                }]
            }
          }


      }
    ]
  );
