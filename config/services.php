<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => 'sandbox92eae8adcf474648853108b052d330e2.mailgun.org',
		'secret' => 'key-63e3c13d060461466116a47b74e2e652',
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'User',
		'secret' => '',
	],
	'Google' => [
		'client_id'     => '411075580164-8la246ggvu0eq5odm64hcj3824mb36tb.apps.googleusercontent.com',
		'client_secret' => 'N71leel6zPNUdcRzDexIICrv',
		'scope'         => ['userinfo_email', 'userinfo_profile', 'https://www.google.com/m8/feeds/'],
	]

];
