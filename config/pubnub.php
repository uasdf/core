<?php

/*
 * This file is part of Laravel PubNub.
 *
 * (c) Graham Campbell <graham@mineuk.com>
 * (c) Vincent Klaiber <hello@vinkla.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Default Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the connections below you wish to use as
    | your default connection for all work. Of course, you may use many
    | connections at once using the manager class.
    |
    */

    'default' => 'main',

    /*
    |--------------------------------------------------------------------------
    | PubNub Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the connections setup for your application. Example
    | configuration has been included, but you may add as many connections as
    | you would like.
    |
    */

    'connections' => [

        'main' => [
            'publish_key' => 'pub-c-3ac39b60-86da-4ed2-a9c2-86a392e061f4',
            'subscribe_key' => 'sub-c-bd0ca6de-f494-11e4-8763-0619f8945a4f'
        ],

        'alternative' => [
            'publish_key' => 'your-publish-key',
            'subscribe_key' => 'your-subscribe-key'
        ],

    ],

];
