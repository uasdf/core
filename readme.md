UASDF / Placerange
===================

How set it up on your local machine
-----------------------------------------

### Requirements

 1. MAMP or XAMPP or LAMP
 2. Composer

### Steps

 1. Run the MAMP app 
 2. Use PHPMyAdmin to create the Database using the SQL.
 3. Go to the home directory and run 'PHP artisan serve'. This
    should load the app on localhost:8000

### Troubleshoot

 1. If you get a blank white screen, after running 'PHP artisan serve', 
    run 'Composer Install'. 
 2. If you still get a blank screen, you will   
    have to make all app files accessible by Apache user.

How to set up on vagrant machine
-----------------------------------------

### Requirements

 1. Vagrant
 2. Virtual box

### Steps

 1. Run <code> vagrant up </code> from your terminal
 2. You can access the site on 192.168.2.233 

### Troubleshoot

 1. Can I reset my server?
	 Run <code>vagrant provision </code> if you want to reset the server with its content.
	 
 2. Where can I get vagrant from? 
	Please click <a href="https://www.vagrantup.com/downloads.html">here</a> to download vagrant.
	
 3. How to install vagrant?
   Please visit vagrant at https://docs.vagrantup.com/v2/installation/index.html 

