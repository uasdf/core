@extends('app')

@section('title')
    <title>Invite Friends - Placerange</title>
@endsection


@section('content')
    <div class="col-md-10 col-md-offset-1 white-bg">
        <div class="col-md-12 error-holder"></div>
        @if (count($errors) > 0)
            <div class="panel panel-default">
                <div class="panel-body ">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your action.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1>Invite friends</h1>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-4">
                    <h3>Existing users</h3>
                    <?php if($existing_users){?>
                        <div class="row white-bg">

                        <?php foreach($existing_users as $user){?>
                                <div class="col-md-12 pad-10" id="connect_{{$user->id}}">
                                    <a href="/user/<?= $user->username ?>" class="col-md-12 user_list">
                                        <img src="<?= user_avatar($user->image_name) ?>" class="user_image">

                                        <div class="content">
                                            <span class="user_name"><?= $user->first_name . ' ' . $user->last_name ?></span>
                                            <span class="skills"><?php  $skill = '';
                                                foreach ($user->skills() as $s) {
                                                    $skill .= $s->tag . ', ';
                                                }
                                                echo rtrim($skill, ', '); ?>
                                            </span>
                                            </span>
                                        </div>
                                        <button class="btn btn-xs btn-success pull-right before_send" onclick="send_connect({{$user->id}}); return false;">Connect</button>
                                        <button class="btn btn-xs btn-success pull-right after_send" style="display:none;">Request sent</button>
                                    </a>
                                </div>
                        <?php }?>
                        </div>

                    <?php }else{?>
                    <p>No one found.</p>
                    <?php }?>

                </div>

                <div class="col-md-4">
                    <?php if($to_invite){?>
                    <h3>Invite to UASDF</h3>
                    <?php $i = 0;
                        foreach($to_invite as $ti){
                        $i++;
                        $out = strlen($ti) > 30 ? substr($ti,0,30)."..." : $ti;
                        ?>
                    <div class="row white-bg" id="invite_{{$i}}">
                        <div class="col-md-12 pad-10">
                            <span class="glyphicon glyphicon-envelope"></span> {{$out}} <a class="btn btn-xs btn-warning pull-right before_send" href="#" onclick="send_invite({{"'".$ti."'"}},{{$i}}); return false;">Invite</a> <a class="btn btn-xs btn-info pull-right after_send" style="display:none;" href="#">Invite sent</a>
                        </div>
                    </div>
                    <?php }?>
                    <?php }?>
                </div>


                <div class="col-md-4">
                    <h3>Connected/Pending approval</h3>
                    <?php if($already_connected){?>
                        <div class="row white-bg ">

                            <?php foreach($already_connected as $user){?>
                            <div class="col-md-12  pad-10">
                                <a href="/user/<?= $user->username ?>" class="col-md-12 user_list">
                                    <img src="<?= user_avatar($user->image_name) ?>" class="user_image">

                                    <div class="content">
                                        <span class="user_name"><?= $user->first_name . ' ' . $user->last_name ?></span>
                                                <span class="skills"><?php  $skill = '';
                                                    foreach ($user->skills() as $s) {
                                                        $skill .= $s->tag . ', ';
                                                    }
                                                    echo rtrim($skill, ', '); ?></span>
                                    </div>
                                    <?php if(isset($ts->distance)){?><span class="distance"><?= round($ts->distance, 2) ?> miles</span><?php }?>
                                </a>

                            </div>
                            <?php }?>
                        </div>

                    <?php }else{?>
                    <p>No one found.</p>

                    <?php }?>
                </div>

            </div>
        </div>
    </div>


@endsection


@section('styles')
@endsection


@section('scripts')

    <script type="text/javascript">


    function send_connect(id){
        $.ajax({
        url: '/invite/send-connect',
        type: 'post',
        data: 'to_user='+id,

        success: function(data) {
        if(data == 'success') {
        console.log("Successfully sent!");
        $('#connect_' + id +' .before_send').remove();
        $('#connect_' + id +' .after_send').toggle(100);

        }else{
        console.log(data);
        }
        },
        error: function(xhr, textStatus, thrownError) {
            alert(xhr + textStatus + thrownError);
        }
        });
    }


    function send_invite(email,id){
        $.ajax({
            url: '/invite/send-invite',
            type: 'post',
            data: 'email='+email,

            success: function(data) {
                if(data == 'success') {
                    console.log("Successfully sent!");
                    $('#invite_' + id +' .before_send').remove();
                    $('#invite_' + id +' .after_send').toggle(100);
                }else{
                    console.log(data);
                }
            },
            error: function(xhr, textStatus, thrownError) {
//                console.log( JSON.stringify(xhr) + textStatus + thrownError);
                $('.error-holder').html(JSON.stringify(xhr));
            }
        });
    }

    </script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        });
    </script>


@endsection