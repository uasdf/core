@extends('app')

@section('title')
    <title>Invite friends - Placerange</title>
@endsection


@section('content')
    <div class="col-md-10 col-md-offset-1 white-bg">
        <div class="col-md-12 error-holder"></div>
        @if (count($errors) > 0)
            <div class="panel panel-default">
                <div class="panel-body ">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your action.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1>Invite friends <a class="small" href="/contact/import/google">Invite friends from google +</a></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-12">
                        <form class="form-horizontal" id="search_form" role="form" method="POST" action="/search">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">


                            <div class="form-group">
                                <div class="col-md-10">
                                    <input type="email" class="form-control input-lg skill_fill" id="search_string" name="search_string" placeholder="Email ID">

                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-lg btn-default btn-block" onclick="send_invite();return false;">
                                        Invite
                                    </button>
                                </div>
                            </div>

                            <div class="form-group">

                            </div>
                        </form>
                    </div>
                    <div class="col-md-12" id="search_result">
                        <?php if($pendinaInvites){?>

                        <h3>Pending Invitations</h3>
                        <div class="col-md-4" id="pending_invites">
                            <?php $i = 0;
                            foreach($pendinaInvites as $ti){
                            $i++;
                            $out = strlen($ti) > 30 ? substr($ti,0,30)."..." : $ti;
                            ?>
                            <div class="row">
                                <div class="col-md-12 pad-10">
                                    <span class="glyphicon glyphicon-time"></span> {{$out}} <a class="btn btn-xs btn-info pull-right after_send" style="display:none;" href="#">Invite sent</a>
                                </div>
                            </div>
                            <?php }?>
                        </div>
                        <?php }else{?>
                            <div class="col-md-4" id="pending_invites">
                            </div>
                        <?php }?>

                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection


@section('styles')
@endsection


@section('scripts')

    <script type="text/javascript">

        function send_invite(){
            var email = $('#search_string').val();
            $('#search_string').val('');
            $.ajax({
                url: '/invite/send-invite',
                type: 'post',
                data: 'email='+email,

                success: function(data) {
                    if(data == 'success') {
                        console.log("Successfully sent!");
                        $('#pending_invites').prepend('<div class="row"> <div class="col-md-12 pad-10"> <span class="glyphicon glyphicon-time"></span> '+ email +' <a class="btn btn-xs btn-info pull-right after_send" style="display:none;" href="#">Invite sent</a> </div> </div>');
                    }else if(data == 'already'){
                        alert('You have already invited '+ email);
                    }else{
                        window.location.href = '/user/'+data;

                    }
                },
                error: function(xhr, textStatus, thrownError) {
//                console.log( JSON.stringify(xhr) + textStatus + thrownError);
                    $('.error-holder').html(JSON.stringify(xhr));
                }
            });
        }

    </script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        });
    </script>


@endsection