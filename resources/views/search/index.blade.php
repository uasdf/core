@extends('app')

@section('title')
    <title>Search - Placerange</title>
@endsection


@section('content')
    <div class="col-md-10 col-md-offset-1 white-bg">
        @if (count($errors) > 0)
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your action.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1>Search</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                        <div class="col-md-12">
                            <form class="form-horizontal" id="search_form" role="form" method="POST" action="/search">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">


                                <div class="form-group">
                                    <div class="col-md-10">
                                        <input type="text" class="form-control input-lg skill_fill" id="search_string" name="search_string" placeholder="search for people, issues and programs">

                                    </div>
                                    <div class="col-md-2">
                                        <button type="submit" class="btn btn-lg btn-default btn-block" onclick="searchall();return false;">
                                            Find
                                        </button>
                                    </div>
                                </div>

                                <div class="form-group">

                                </div>
                            </form>
                        </div>
                        <div class="col-md-12" id="search_result">

                        </div>
                </div>
            </div>

        </div>
    </div>


@endsection


@section('styles')
    <link rel="stylesheet" href="/css/bootstrap-tagsinput.css">
@endsection


@section('scripts')
    <script src="/js/bootstrap-tagsinput.min.js"></script>

    <script type="text/javascript">
        function searchall(){

            $('#search_result').html("<img style='margin:0 auto; display:block;' src='img/bar-loader.gif'>");
            $.ajax({
                url: '/post-search',
                type: 'post',
                data: $('form#search_form').serialize(),
                success: function(data) {
//                    setTimeout(function(){
                        $('#search_result').html(data);
//                    }, 1000);
                },
                error: function(xhr, textStatus, thrownError) {
                    alert('Something went to wrong.Please Try again later...');
                }

            });
        }



    </script>

    <script src="/js/bootstrap3-typeahead.js"></script>

    <script type="text/javascript">
//        $('.skill_fill').typeahead({
//            source: function(query, process) {
//                // `query` is the text in the field
//                // `process` is a function to call back with the array
//                $.ajax({
//                    url: "/autofill/skills/"+$('.skill_fill').val(),
//                    success: process
//                });
//            }
//        });

    </script>





@endsection