@extends('app')

@section('title')
    <title>Issues - Placerange</title>
@endsection


@section('content')
    <div class="col-md-10 col-md-offset-1 white-bg">
        @if (count($errors) > 0)
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your action.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            
            <div class="col-md-12 grey-left grey-right" style="height: 100%;">
                <div class="col-md-12 ">

                    <div class="row">
                        <h2 class="grey-bottom text-center">Issues - Currently Handling <a class="small" href="/new-issue">Raise +</a></h2>

                        <div class="col-md-12">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="search" id="search2" value="" class="form-control" placeholder="Search">
                            </div>
                        </div>

                        <div class="col-md-12 grey-bottom" style="margin-top:20px;">
                            <?php if(count($issues) > 0 ){?>
                            <div class="col-md-12" id="searchable-container2">
                                <?php foreach($issues as $issue){?>
                                    <div class="list-group">
                                        <a href="/issue/{{$issue['slug']}}" class="list-group-item">
                                            <h4 class="list-group-item-heading" ><strong>{{$issue['title']}}</strong></h4>
                                            <div class="list-group-item-text grey-bottom">{!!$issue['summary']!!}</div>
                                            <span class='small'>{{date("F jS, Y",strtotime($issue['created_at'] ))}} | {{count($issue['comments'])}} Comments | {{count($issue['handle_requests'])}} Handle requests</span>
                                        </a>    
                                    </div>

                                <?php }?>
                            </div>
                            <?php }else{?>
                            You aren't handling any issues.
                            <?php }?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection


@section('styles')

@endsection


@section('scripts')
    <script src="//rawgithub.com/stidges/jquery-searchable/master/dist/jquery.searchable-1.0.0.min.js"></script>
    <script type="text/javascript">
    $(function () {
    $( '#searchable-container2' ).searchable({
    searchField: '#search2',
    selector: '.list-group',
    childSelector: '.list-group-item-heading',
    show: function( elem ) {
    elem.slideDown(100);
    },
    hide: function( elem ) {
    elem.slideUp( 100 );
    }
    })
    });

    </script>



@endsection