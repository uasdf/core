@extends('app')
@section('title')
    <title>New issue - Placerange</title>
@endsection

@section('scripts')
    {{--<script src="/js/bootstrap-wysihtml5.js"></script>--}}
    <script src="/js/plugins/bootstrap3-wysiwyg/bootstrap3-wysihtml5.all.min.js"></script>
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>--}}
    <script type="text/javascript">
        $('#summary').wysihtml5({
            toolbar: {
                "font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
                "emphasis": true, //Italics, bold, etc. Default true
                "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": true, //Button to insert a link. Default true
                "image": true, //Button to insert an image. Default true,
                "color": false, //Button to change color of font
                "blockquote": true, //Blockquote
                "size": 'sm' //default: none, other options are xs, sm, lg
        }
        });
    </script>
    <link rel="stylesheet" href="/js/plugins/bootstrap3-wysiwyg/bootstrap3-wysihtml5.css">
    @endsection
@section('content')
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h2 class="grey-bottom">Raise new issue</h2>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="/new-issue">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group required @if ($errors->has('title')) has-error @endif">
                                <label class="col-md-3 control-label">Title</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="title" value="" placeholder="Eg: My computer is not working">
                                    @if ($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif

                                </div>
                            </div>

                            <div class="form-group required @if ($errors->has('summary')) has-error @endif ">
                                <label class="col-md-3 control-label">Summary</label>
                                <div class="col-md-9">
                                        <textarea class="form-control" style="overflow:scroll; max-height:300px" id="summary" name="summary" value=""></textarea>
                                    @if ($errors->has('summary')) <p class="help-block">{{ $errors->first('summary') }}</p> @endif

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Do you want someone to assist personally?</label>
                                <div class="col-md-9">
                                    <label class="radio-inline" >
                                        <input type="radio" name="assistance" checked="checked" value="1"> Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="assistance" value="0"> No
                                    </label>
                                </div>
                            </div>
                            <div class="form-group required @if ($errors->has('place')) has-error @endif">
                                <label class="col-md-3 control-label">Place</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="place" value="<?=$place->formatted_address?>">
                                    @if ($errors->has('place')) <p class="help-block">{{ $errors->first('place') }}</p> @endif

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" class="btn btn-success btn-block">
                                        Raise
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
@endsection
