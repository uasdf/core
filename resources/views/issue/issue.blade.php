@extends('app')


@section('title')
    <title> {{$issue->title}} </title>
    <link rel="canonical" href="http://{{$_SERVER['HTTP_HOST']}}{{$_SERVER['REQUEST_URI']}}" />
    <meta property="og:url"                content="http://{{$_SERVER['HTTP_HOST']}}{{$_SERVER['REQUEST_URI']}}" />
    <meta property="og:type"               content="Issue" />
    <meta property="og:title"              content="{{$issue->title}}" />
@endsection


@section('styles')

<link rel="stylesheet" href="/js/plugins/bootstrap3-wysiwyg/bootstrap3-wysihtml5.css">
<style type="text/css">
.issue-header{
    margin-bottom: 0;
    padding-bottom: 20px;
    border:none;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0; 
}
</style>
@endsection


@section('content')
            <div id="fb-root"></div>
            <script>(function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=448530131848352";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
            <div class="col-md-10 col-md-offset-1">
                <div class="row">
                    <div class=" issue-header panel panel-{{($issue->status =='resolved' ? 'success':'danger')}}">
                        <div class="panel-heading"><h3>{{$issue->title}}</h3></div>
                        <div class="panel-body">
                            {!!$issue->summary!!}
                        </div>
                    </div>

                </div>
                <div class="row white-bg">
                    <div class="col-md-3 ">
                    <h3 class="grey-bottom "><span class="glyphicon glyphicon-king c_gray"></span> &nbsp;Posted by <?php if($uid == $issue->user_id){?><a class="small pull-right sin" style="margin-top: 4px;" href="/update-issue/{{$issue->id}}"><span class='glyphicon glyphicon-edit'></span>  &nbsp;Edit issue</a><?php }?></h3>
                        <div class="media">
                            <div class="media-left">
                                <a href="/user/{{$issue->user->username}}">
                                    <img class="media-object" src="<?=user_avatar($issue->user->image_name)?>" alt="...">
                                </a>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">{{$user->first_name.' '.$user->last_name}}</h4>
                                {{date("d M - Y",strtotime($issue->created_at))}}
                            </div>
                        </div>

                        <h3 class="grey-bottom"><span class="glyphicon glyphicon-tags sin"></span> &nbsp;Required skills <?php if($uid == $issue->user_id){?><a href="#" style="margin-top: 4px;" class="pull-right small" onclick="$('#skill_form').toggle();return false;">Add skill &nbsp;+</a> <?php }?></h3>
                        <?php foreach($skills as $s){?>
                        <h5>{{$s->tag}} <span class="small"><?php if($s->level == 1){echo 'Beginner';}elseif($s->level == 2){echo 'Novice';}elseif($s->level == 3){echo 'Intermediate';}else{echo 'Expert';}?></span> <?php if($issue->user_id == $uid){?>&nbsp;&nbsp;&nbsp;<a class="small pomo" href="/issue/remove-skill/{{$issue->slug}}/{{$s->tag}}">&times;</a><?php }?></h5>
                        <?php }
                        if($skills->isEmpty()){?>
                        <p>No skills added</p>

                        <?php }if($uid == $issue->user_id){?>

                        <form class="form-horizontal" id="skill_form" style="display:none;" role="form"
                              method="POST" action="/issue/add-skill/{{$issue->slug}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="type" value="skill">

                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="text" class="form-control skill_fill"  data-provide="typeahead" autocomplete="off" placeholder="skill" name="skill" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12" style="padding-left: 40px;">
                                    <label class="radio">
                                        <input type="radio" name="level" value="1" checked="checked"> Beginner
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="level" value="2"> Novice
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="level" value="3"> Intermediate
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="level" value="4"> Expert
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 ">
                                    <button type="submit" class="btn btn-success btn-block">
                                        Add
                                    </button>
                                </div>
                            </div>
                        </form>
                        <?php }?>
                        <h3 class="grey-bottom"><span class="glyphicon glyphicon-home vista"></span> &nbsp;Personal support</h3>
                        <p>
                        <?php if($issue->assistance && $issue->status == 'unresolved' && $uid && $uid != $issue->user_id){
                            $handle_approved = false;
                            $awating_approval = false;
                            $handle_cancelled = false;
                            if(!$handle_requests->isEmpty()){
                                foreach($handle_requests as $hr){
                                    if($uid == $hr->user_id){
                                        if($hr->status == null){
                                            $awating_approval = true;
                                        }elseif($hr->status == 1){
                                            $handle_approved = true;
                                        }elseif($hr->status > 1){
                                            $handle_cancelled = true;
                                        }
                                    }
                                }
                            }
                            ?>
                            <?php if($handle_approved){?>
                                    <a href="#" class="btn btn-sm btn-success">Currently handling&nbsp;&nbsp;<span class="glyphicon glyphicon-thumbs-up"></span></a>
                                    <a href="/issue/cancel-handle/{{$hr->id}}" class="btn btn-sm btn-warning">Cancel</a>

                            <?php }elseif($awating_approval){?>
                                    <a href="#" class="btn btn-sm btn-warning">Awaiting approval&nbsp;&nbsp;<span class="glyphicon glyphicon-thumbs-up"></span></a>
                            <?php }elseif($handle_cancelled){
                                    echo 'Your previous handle has been cancelled';
                            }else{?>
                                    <a href="#" onclick="$('#handle_form').toggle();return false;" class="btn btn-sm btn-default">Accept request&nbsp;&nbsp;<span class="glyphicon glyphicon-thumbs-up"></span></a>
                                    <form class="form-horizontal" id="handle_form" style="display:none;" role="form"
                                          method="POST" action="/issue/handle/{{$issue->slug}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <textarea class="form-control" placeholder="Message" name="message" >I would like to help you with this issue</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-success btn-block">
                                                    Send
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                            <?php }?>
                        <?php }elseif($issue->status == 'resolved'){?>
                            Solved by {{$solved_by->first_name.' '.$solved_by->last_name}}
                        <?php }elseif($uid == $issue->user_id && $issue->assistance){?>

                            <?php  $response_flag = false;

                            if(!$handle_requests->isEmpty()){
                                foreach($handle_requests as $hr){
                                if($hr->status > 1) continue;
                                $response_flag = true;
                                ?>
                                    <div class="col-md-12 grey-bottom">
                                        <div class="col-md-2 " style="padding-left:0px;">
                                            <img src="<?=user_avatar($hr->user->image_name)?>" style="height:25px:width:25px;">
                                        </div>
                                        <div class="col-md-10" style="padding-left:0px;">
                                            <h5 style="margin-top:0"><?=$hr->user->first_name.' '.$hr->user->last_name?></h5>
                                            <?=$hr->message?>
                                            <div class="col-md-12" style="padding:10px 0 10px 0;">
                                                <?php if(!$hr->status){?>
                                                    <a href="/issue/accept-handle/{{$hr->id}}" class="btn btn-xs btn-success">Accept</a>
                                                    <a href="/issue/reject-handle/{{$hr->id}}" class="btn btn-xs btn-danger">Reject</a>
                                                <?php }?>
                                                <?php if($hr->status == 1){?>
                                                    <a href="/issue/owner-cancel-handle/{{$hr->id}}" class="btn btn-xs btn-warning">Cancel</a>
                                                <?php }?>
                                            </div>
                                        </div>

                                    </div>
                                <?php }
                            }
                            if(!$response_flag){?>
                            Waiting for response
                            <?php }?>
                        <?php }else{?>
                        Not required
                        <?php }?>
                        <h3><span class="glyphicon glyphicon-map-marker pomo"></span> &nbsp;Places<?php if($uid == $issue->user_id){?> &nbsp;&nbsp;&nbsp;<a class="small pull-right" href="#" onclick="$('#add_place').toggle();add_place();return false;" >Add place +</a><?php }?></h3>
                        <div id="add_place" style="display:none"></div>
                        <div class="col-md-12" id="place_data">
                            <?php foreach($places as $p){?>
                                <p id="place_<?=$p->id?>"><?=place_min($p);?>
                                    <?php if($uid == $issue->user_id){?>

                                    <a class="small sin" href="#" onclick="edit_place(<?=$p->id?>); return false;">(Edit)</a>&nbsp;
                                    <?php if(!$p->primary){?>
                                    <a class="small pomo" href="#" onclick="remove_place(<?=$p->id?>); return false;">(Remove)</a>
                                    <?php }?>
                                    <?php }?>

                                </p>
                            <?php }?>
                        </div>
                        </p>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="fb-share-button" data-href="http://{{$_SERVER['SERVER_NAME']}}{{$_SERVER['REQUEST_URI']}}" data-layout="button"></div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-9 grey-left">
                        <div class="row">
                            <div class="col-md-12">
                                <h3 class="grey-bottom"><span class="glyphicon glyphicon-comment sin"></span> &nbsp;Solutions</h3>
                            </div>
                            <div class="col-md-12">

                                </section>

                            </div>

                            <div class="col-md-12" style="min-height: 150px;">
                                <section class="comment-list">

                                <?php if($comments->isEmpty()){?>
                                    <p>No solutions yet</p>
                                    <?php }else{?>
                                    <?php foreach($comments as $c){
                                    $helpful_count = 0;
                                    $current_user_set_helpful = false;
                                    ?>
                                    <article class="row" id="comment_{{$c->id}}">
                                        <div class="col-md-2 col-sm-2 hidden-xs">
                                            <figure class="thumbnail">
                                                <a href="/user/{{$c->user->username}}">
                                                <img class="img-responsive" src="<?=user_avatar($c->user->image_name,'thumbnail')?>" />
                                                <figcaption class="text-center">{{$c->user->username}}</figcaption>
                                                </a>
                                            </figure>
                                        </div>
                                        <div class="col-md-10 col-sm-10">
                                            <?php if($c->solution){?>
                                            <span class="glyphicon glyphicon-star" style="position:absolute;left:-0px;top:3px;color:gold;"></span>
                                            <?php }?>
                                            <div class="panel panel-default arrow left">
                                                <div class="panel-body">
                                                    <header class="text-left">
                                                        <div class="comment-user"><i class="fa fa-user"></i> {{$c->user->first_name.' '.$c->user->last_name}}</div>
                                                        <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i> {{date("d M - Y",strtotime($c->created_at))}}</time>
                                                    </header>
                                                    <div class="comment-post">
                                                        <p>
                                                            {!! $c->comment !!}
                                                        </p>
                                                    </div>
                                                    <p class="text-right"><?php if($c->user_id == $uid && $uid){?>
                                                        <a class="btn btn-default btn-xs" href="#" onclick="$('form#edit_reply{{$c->id}}').toggle();return false;">Edit </a>
                                                        <a class="btn btn-danger btn-xs" href="/issue/remove-comment/{{$c->id}}">Delete </a>

                                                    <?php }?>
                                                        <?php if($uid){?>
                                                        <a class="btn btn-default btn-xs" href="#" onclick="$('form#reply_comment{{$c->id}}').toggle();return false;"><span class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true"></span>&nbsp;Reply </a>
                                                        <?php }?>
                                                        <?php if($c->user_id != $uid && $issue->user_id == $uid && !$issue->solved_by && $uid){?>
                                                        <a class="btn btn-success btn-xs" href="/issue/mark-solved/{{$c->id}}">Mark solved</a>
                                                        <?php }?>
                                                        <?php 
                                                        foreach($c->helpful as $ch){
                                                            $helpful_count++;
                                                            if($uid == $ch->user_id){
                                                                $current_user_set_helpful = true;
                                                            }

                                                        }?>
                                                        <?php if($c->user_id != $uid && !$current_user_set_helpful && $uid){?>
                                                        <a class="btn btn-info btn-xs" href="/issue/comment-helpful/{{$c->id}}"><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>&nbsp;Helpful</a>
                                                        <?php }?>
                                                        <?php if($current_user_set_helpful && $helpful_count == 1 && $uid){echo '<p class="pull-right">You have marked this as helpful</p>';}
                                                        elseif($current_user_set_helpful && $helpful_count == 2 && $uid){echo '<p class="pull-right">You and '.$helpful_count.' other person found this comment helpful</p>';}
                                                        elseif($current_user_set_helpful && $helpful_count > 2 && $uid){echo '<p class="pull-right">You and '.$helpful_count.' others found this comment helpful</p>';}?>

                                                        <?php if(!$current_user_set_helpful && $helpful_count == 1){echo '<p class="pull-right">'.$helpful_count.' person found this comment helpful</p>';}
                                                        elseif(!$current_user_set_helpful && $helpful_count > 1){echo '<p class="pull-right">'.$helpful_count.' people found this comment helpful</p>';}?></p>
                                                </div>
                                            </div>
                                            <?php if($uid){?>
                                                <div class="col-md-12">
                                                    <form class="form-horizontal" class="edit_reply" id="reply_comment{{$c->id}}"role="form"
                                                          method="POST" action="/issue/reply-to-comment/{{$c->id}}" style="display:none;">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <textarea class="form-control wsyi" style="overflow:scroll; max-height:300px" id="reply" name="reply" placeholder="Post your reply..."></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <button type="submit" class="btn btn-success btn-block">
                                                                    Send reply
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            <?php }?>
                                        </div>


                                    </article>

                                    <div class="col-md-12 grey-bottom " style="padding-bottom: 50px;margin-bottom: 10px;">
                                        <?php if($c->user_id == $uid){?>
                                        <div class="col-md-12">
                                            <form class="form-horizontal" class="edit_reply" id="edit_reply{{$c->id}}"role="form"
                                                  method="POST" action="/issue/edit-reply/{{$c->id}}" style="display:none;">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <textarea class="form-control wsyi" style="overflow:scroll; max-height:300px" id="reply" name="reply" placeholder="Post your solution...">{!! $c->comment !!}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-success btn-block">
                                                            Update
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <?php }?>

                                        <?php if(!$c->replies->isEmpty()){?>
                                        <div class="col-md-10 col-md-offset-2 grey-left grey-bottom ">
                                            <span class="glyphicon glyphicon-menu-up" style="position:absolute; top:-14px;left:-7px;color:#eee;"></span>
                                        <h3 class="">Replies</h3>
                                        <?php foreach($c->replies as $cr){?>
                                            <div class="row grey-top " id="comment_{{$c->id}}" style="padding-bottom: 10px;">
                                                <div class="col-md-8  ">
                                                {!! $cr->comment !!}
                                                </div>
                                                <div class="col-md-4 grey-left" style="margin-top: 10px;">
                                                    <div class="media issue_reply">
                                                        <div class="media-left">
                                                            <a href="/user/{{$cr->user->username}}">
                                                                <img class="media-object" src="<?=user_avatar($cr->user->image_name)?>" alt="...">
                                                            </a>
                                                        </div>
                                                        <div class="media-body">
                                                            <h5 class="media-heading">{{$cr->user->first_name.' '.$cr->user->last_name}}</h5>
                                                            {{date("d M - Y",strtotime($cr->created_at))}}
                                                            <?php if($uid == $cr->user_id){?>
                                                            <a class="btn btn-danger btn-xs" href="/issue/remove-reply/{{$cr->id}}">Delete </a>
                                                            <?php }?>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php }?>
                                        </div>
                                        <?php }?>

                                    </div>
                                    <?php }?>
                                <?php }?>
                                </section>
                            </div>
                            <?php if($uid){?>
                            <div class="col-md-12">
                                <form class="form-horizontal" id="reply_form"role="form"
                                      method="POST" action="/issue/reply/{{$issue->id}}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <textarea class="form-control wsyi" style="overflow:scroll; max-height:300px" id="reply" name="reply" placeholder="Post your solution..."></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-success ">
                                                Add
                                            </button>
                                        </div>

                                    </div>
                                </form>
                            </div>
                            <?php }?>

                        </div>
                    </div>
                </div>
            </div>
    @endsection


@section('scripts')

    <script src="/js/bootstrap3-typeahead.js"></script>

    <script type="text/javascript">
        $('.skill_fill').typeahead({
            source: function(query, process) {
                // `query` is the text in the field
                // `process` is a function to call back with the array
                $.ajax({
                    url: "/autofill/skills/"+$('.skill_fill').val(),
                    success: process
                });
            }
        });

    </script>

    <script type="text/javascript">

        function add_place(){
            $.ajax({
                url: '/ajax/get/add-place',
                type: 'get',
                data: 'for=issue',
                success: function(data) {
                    console.log("Success!");
                    $('#add_place').html(data);
                },
                error: function(xhr, textStatus, thrownError) {
                    alert('Something went to wrong.Please Try again later...');
                }
            });
        }

        function post_add_place(){
            $.ajax({
                url: '/ajax/post/add-place',
                type: 'post',
                data: $('#form_place').serialize()+'&related_id=<?=$issue->id?>',
                success: function(data) {
//            console.log("Success!");
                    $('#add_place').hide();
                    $('#place_data').append(data);
//            location.reload();
                },
                error: function(xhr, textStatus, thrownError) {
                    alert(thrownError);
                }
            });
        }

        function edit_place(id){
            $.ajax({
                url: '/ajax/get/edit-place',
                type: 'get',
                data:'id='+id,
                success: function(data) {
                    console.log("Success!");
                    $('#place_'+id).append(data);
                },
                error: function(xhr, textStatus, thrownError) {
                    alert('Something went to wrong.Please Try again later...');
                }
            });
        }

        function post_edit_place(id){
            $.ajax({
                url: '/ajax/post/edit-place',
                type: 'post',
                data: $('#edit_place_'+id).serialize()+'&id='+id,
                success: function(data) {
                    $('edit_place_'+id).hide();
                    $('#place_'+id).html(data);
                },
                error: function(xhr, textStatus, thrownError) {
                    alert(thrownError);
                }
            });
        }

        function remove_place(id){
            $.ajax({
                url: '/ajax/post/remove-place/',
                type: 'post',
                data: 'id='+id,

                success: function(data) {
                    if(data == 'success') {
                        console.log("Successfully removed!");
                        $('#place_' + id).remove();
                    }else{
                        console.log(data);
                    }
                },
                error: function(xhr, textStatus, thrownError) {
                    alert(xhr + textStatus + thrownError);
                }
            });
        }



    </script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script src="/js/plugins/bootstrap3-wysiwyg/bootstrap3-wysihtml5.all.min.js"></script>
    <script type="text/javascript">
        $("textarea.wsyi").each(function(){$(this).wysihtml5({
            toolbar: {
                "font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
                "emphasis": true, //Italics, bold, etc. Default true
                "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": true, //Button to insert a link. Default true
                "image": true, //Button to insert an image. Default true,
                "color": false, //Button to change color of font
                "blockquote": true, //Blockquote
                "size": 'sm' //default: none, other options are xs, sm, lg
            }
        });
        });

    </script>

@endsection