@extends('app')

@section('title')
<title>{{$user->first_name.' '.$user->last_name}} - Placerange</title>
<link rel="canonical" href="http://{{$_SERVER['HTTP_HOST']}}{{$_SERVER['REQUEST_URI']}}" />
<meta property="og:url"                content="http://{{$_SERVER['HTTP_HOST']}}{{$_SERVER['REQUEST_URI']}}" />
<meta property="og:type"               content="User" />
<meta property="og:title"              content="{{$user->first_name.' '.$user->last_name}} - Placerange" />
<meta property="og:image"              content="http://www.placerange.com/uploads/users/thumbnail/{{$user->image_name}}" />

<link href="/js/plugins/rateit/rateit.css" media="all" rel="stylesheet" type="text/css" />
@endsection



@section('content')
<div class="col-md-10 col-md-offset-1 white-bg" style="padding-top: 10px;">
@if (count($errors) > 0)
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your action.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif
<div class="row">
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-12 user_change_image">
                <?php if(file_exists(public_path()."/uploads/users/thumbnail/" .$user->image_name)) { ?>
                    <img style="height: 100%;" src="/uploads/users/thumbnail/{{$user->image_name}}">
                <?php } else { ?>
                    <img style="height: 100%;" src="/uploads/users/thumbnail/default.png">
                <?php } ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 grey-bottom">
                <h4> Places </h4>
                <div class="col-md-12" id="place_data">
                    <?php if(!empty($places)) { ?>
                    <?php foreach($places as $p) { ?>
                        <p id="place_<?=$p->id?>"><span class="glyphicon glyphicon-map-marker"></span> <?=place_min($p,true);?></p>
                    <?php } } ?>
                </div>
            </div>
            <div class="col-md-12">
                @if (Auth::guest())

                @else 
                    @if(Auth::user()->role == 'admin')
                        <a href="/become/{{$user->id}}">Become</a>
                    @endif
                @endif
            </div>

        </div>
    </div>
    <div class="col-md-6 grey-left grey-right" style="padding-left: 0;padding-right: 0;">
        <div class="col-md-12">
            <h2>{{$user->first_name .' '. $user->last_name}} </h2>
            <span> Member since {{date("F jS, Y",strtotime($user->created_at ))}}</span>
            <div class="clear"></div>
            <a id='rating_count' href='#user_reviews' style=' height:40px;max-width:100px; font-size:30px;'>{{$average_rating}} <span style='font-size: 25px;' class="glyphicon glyphicon-star sin"> </span></a> &nbsp; Based on {{count($ratings)}} review(s)
            <div class="row">
                <div class="col-md-12 user_page_recents">
                    <h3 class="grey-bottom" style="margin-bottom: 20px;">Recent Issues</h3>

                    <div class="row">
                        <div class="col-md-12 user_page_recents">

                            <span class="recent_contents_icon opacity_highlight" >
                                <img src="/img/user_page/issues_raised.svg" style="height: 100%;">
                            </span>
                            <div class="row">
                                <span class="recent_contents">
                                    <?php if(!$my_issues->isEmpty()) { ?>
                                    <div class="col-md-12">
                                        <div class="list-group">
                                            <?php foreach($my_issues as $issue) { ?>
                                            <div class="list-group">
                                                <a href="/issue/{{$issue->slug}}" class="list-group-item list-group-item-danger">
                                                    <h4 class="list-group-item-heading"><strong>{{$issue->title}}</strong></h4>
                                                    <div class="list-group-item-text">{!!$issue->summary!!}</div>
                                                </a>
                                            </div>

                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } else { ?>
                                    <div class="col-md-12">
                                        Haven't raised any issues.
                                    </div>
                                    <?php } ?>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-12 user_page_recents">
                            <span class="recent_contents_icon opacity_highlight">
                                <img src="/img/user_page/issues_handling.svg" style="height: 100%;">
                            </span>
                            <div class="row">
                                <span class="recent_contents">
                                    <?php if(!$handling_issues->isEmpty()) { ?>
                                    <div class="col-md-12">
                                        <div class="list-group">
                                            <?php foreach($handling_issues as $hand){ ?>
                                            <div class="list-group">
                                                <a href="/issue/{{$hand->slug}}" class="list-group-item list-group-item-info">
                                                    <h4 class="list-group-item-heading"><strong>{{$hand->title}}</strong></h4>
                                                    <div class="list-group-item-text">{!!$hand->summary!!}</div>
                                                </a>
                                            </div>

                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } else { ?>
                                    <div class="col-md-12">
                                        Not handling any issues. <!-- <a href='#'>Send a request?</a>-->
                                    </div>
                                    <?php } ?>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-12 user_page_recents">
                            <span class="recent_contents_icon opacity_highlight">
                                <img src="/img/user_page/issues_fixing.svg" >
                            </span>
                            <div class="row">
                                <span class="recent_contents">
                                    <?php if(!$resolved_issues->isEmpty()){ ?>
                                    <div class="col-md-12">
                                        <div class="list-group">
                                            <?php foreach($resolved_issues as $issue){ ?>
                                            <div class="list-group">
                                                <a href="/issue/{{$issue->slug}}" class="list-group-item list-group-item-success">
                                                    <h4 class="list-group-item-heading"><strong>{{$issue->title}}</strong></h4>
                                                    <div class="list-group-item-text">{!!$issue->summary!!}</div>
                                                </a>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } else { ?>
                                    <div class="col-md-12">
                                        Haven't solved any issues.
                                    </div>
                                    <?php } ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 user_page_recents">
                    <h3 class="grey-bottom" style="margin-bottom: 20px;">Programs</h3>

                    <div class="row ">

                        <div class="col-md-12 user_page_recents">
                            <span class="recent_contents_icon opacity_highlight">
                                <img src="/img/user_page/program_organising.svg" style="height: 100%;">
                            </span>
                        <div class="row">
                                <span class="recent_contents">
                                    <?php  if(!$organising_workshops->isEmpty()) { ?>
                                    <div class="col-md-12">
                                        <div class="list-group">
                                            <?php foreach($organising_workshops as $odw) { ?>
                                            <div class="list-group">
                                                <a href="/workshop/{{$odw->slug}}" class="list-group-item list-group-item-info">
                                                    <h4 class="list-group-item-heading"><strong>{{$odw->title}}</strong></h4>
                                                    <div class="list-group-item-text">{!!$odw->summary!!}</div>
                                                </a>
                                            </div>

                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } else { ?>
                                    <div class="col-md-12">
                                        Not organising any programs.
                                    </div>
                                    <?php } ?>
                                </span>
                        </div>
                        </div>

                        <div class="col-md-12 user_page_recents">
                            <span class="recent_contents_icon opacity_highlight">
                                <img src="/img/user_page/program_attending.svg" style="height: 100%;">
                            </span>
                            <div class="row">
                                <span class="recent_contents">
                                    <?php if(!$attending_workshops->isEmpty()){ ?>
                                    <div class="col-md-12">
                                        <div class="list-group">
                                            <?php foreach($attending_workshops as $agw) { ?>
                                            <div class="list-group">
                                                <a href="/program/{{$agw->workshop->slug}}" class="list-group-item list-group-item-info">
                                                    <h4 class="list-group-item-heading"><strong>{{$agw->workshop->title}}</strong></h4>
                                                    <div class="list-group-item-text">{!!$agw->workshop->summary!!}</div>
                                                </a>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php }else{ ?>
                                    <div class="col-md-12">
                                        Not attending any program.
                                    </div>
                                    <?php } ?>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-12 user_page_recents">
                            <span class="recent_contents_icon opacity_highlight">
                                <img src="/img/user_page/program_organised.svg" style="height: 100%;">
                            </span>
                            <div class="row">
                                <span class="recent_contents">
                                    <?php if(!$organised_workshops->isEmpty()) { ?>
                                    <div class="col-md-12">
                                        <div class="list-group">
                                            <?php  foreach($organised_workshops as $odw) { ?>
                                            <div class="list-group">
                                                <a href="/program/{{$odw->slug}}" class="list-group-item list-group-item-success">
                                                    <h4 class="list-group-item-heading"><strong>{{$odw->title}}</strong></h4>
                                                    <div class="list-group-item-text">{!!$odw->summary!!}</div>
                                                </a>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } else { ?>
                                    <div class="col-md-12">
                                        Havn't organised any programs.
                                    </div>
                                    <?php } ?>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-12 user_page_recents">
                            <span class="recent_contents_icon opacity_highlight">
                                <img src="/img/user_page/program_attended.svg" style="height: 100%;">
                            </span>
                            <div class="row">
                                <span class="recent_contents">
                                <?php if(!$attended_workshops->isEmpty()) { ?>
                                <div class="col-md-12">
                                    <div class="list-group">
                                        <?php foreach($attended_workshops as $adw) { ?>
                                        <div class="list-group">
                                            <a href="/program/{{$adw->workshop->slug}}" class="list-group-item list-group-item-success">
                                                <h4 class="list-group-item-heading"><strong>{{$adw->workshop->title}}</strong></h4>
                                                <div class="list-group-item-text">{!!$adw->workshop->summary!!}</div>
                                            </a>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php } else { ?>
                                <div class="col-md-12">
                                    Haven't attended to any programs.
                                </div>
                                <?php } ?>
                                </span>
                            </div>
                        </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 user_page_recents">
                        <h3 class="grey-bottom" id='user_reviews'>Reviews </h3>
                        <section class="comment-list">
                            <?php 
                                $current_user_reviewed  = false;
                                if(count($ratings) > 0){
                                
                                foreach ($ratings as $rating) {
                                    if($currentUid  == $rating->reviewed_by->id){
                                        $current_user_reviewed = true;
                                    }
                                    ?>
                                <article class="row" id="comment_{{$rating->id}}">
                                        <div class="col-md-2 col-sm-2 hidden-xs">
                                            <figure class="thumbnail">
                                                <a href="/user/{{$rating->reviewed_by->username}}">
                                                <img class="img-responsive" src="<?=user_avatar($rating->reviewed_by->image_name,'thumbnail')?>" />
                                                
                                                </a>
                                            </figure>
                                        </div>
                                        <div class="col-md-10 col-sm-10">
                                            <div class="panel panel-default arrow left">
                                                <div class="panel-body">
                                                    <header class="text-left">
                                                        <div class="comment-user"><i class="fa fa-user"></i> <a href='/user/{{$rating->reviewed_by->username}}'>{{$rating->reviewed_by->first_name.' '.$rating->reviewed_by->last_name}}</a>
                                                        <span class='pull-right'><?php for($i=1; $i <= $rating->rating; $i++){?> <span class="glyphicon glyphicon-star sin"></span><?php }?></span>
                                                        </div>
                                                        <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i> {{date("d M - Y",strtotime($rating->created_at))}}</time>
                                                    </header>
                                                    <div class="comment-post">
                                                        <p>
                                                            {!! $rating->comment !!}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </article>

                                <?php }
                               
                            }else{ ?>

                                No reviews yet.
                            <?php }?>
                        </section>
                        <?php if($current_user_reviewed !== true && $currentUid != $user->id && $connectionStatus == 2){ ?>
                            <span id='rate_user' style="display:block;margin-top: 20px;">
                                <span class="sin" href="#" style="font-size: 16px;">Add a review</span>
                                <form class="form-horizontal" id="rating_form" role="form" method="POST" action="/user/post-rating">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="to_user" value="{{$user->id}}">
                                    <input type='text' name='rating' id= 'rating' value='1'>
                                    <div style=" margin:10px 0;" id="user_rating_star" type="text" class="rateit" data-rateit-backingfld="#rating" data-rateit-resetable="false"></div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <textarea id="comment" rows='8' class="form-control" autocomplete="off" placeholder="Write your review here" name="comment" required></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button type="submit" onclick="if($('#review_text').val() ==''){return false;}" class="btn btn-info pull-right">
                                                Send Review
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </span>

                        <?php }?>
                    </div>
                </div>

            </div>
        </div>


    <div class="col-md-3">
        
        <div class="row">
                
            @if (!Auth::guest())
            <?php if($currentUid != $user->id) { ?>

                <div class="col-md-12">
                    <img src='/img/user_page/mailbox.svg' class="user_page_mailbox opacity_highlight" title='Send Message' alt='Send Message' onclick="$('#message_form').toggle(500); return false;" >
            
                    <form class="form-horizontal" id="message_form" role="form"
                          method="POST" action="/send-message" style="display:none;margin-top: 20px;">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="to_user" value="{{$user->id}}">
                        <div class="form-group">
                            <div class="col-md-12">
                                <textarea id="send_message" class="form-control" autofocus autocomplete="off" placeholder="Message" name="message" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" onclick="if($('#send_message').val() ==''){return false;}" class="btn btn-info">
                                    Send message
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            <?php } ?>
                <div class="col-md-12">
                    <?php if($currentUid != $user->id) {
                        if( $connectionStatus == null ) { ?>
                            <h3>Connection</h3>
                            Get connected with {{$user->first_name}} to get updates, ask for support and more.
                            <a href="#"  class="btn btn-default btn-block" onclick="$('#connect_form').toggle(300);return false;">Connect</a>
                            <form class="form-horizontal" id="connect_form" role="form"
                                  method="POST" action="/connect" style="display:none;margin-top: 20px;">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="to_user" value="{{$user->id}}">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <textarea class="form-control" autofocus autocomplete="off" placeholder="An optional message" name="message">I'd like to get connected</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12" style="padding-left: 40px;">
                                        <label class="radio">
                                            <input type="radio" name="purpose" checked="checked" value="friend"> Friend
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="purpose" value="support"> Support
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn  btn-info">
                                            Send request
                                        </button>
                                    </div>
                                </div>
                            </form>
                        <?php } elseif($connectionStatus == 1) { ?>
                            <?php if($to_sent) { ?>
                            <a href="#" onclick="return false;" class="btn btn-block btn-warning">Request sent</a>
                            <?php } else { ?>
                                <span style="display: block;" class='well '>
                                    <span style="display:block;margin-bottom:10px;"> {{$user->first_name}} has sent you a connection request.</span>
                                    <a onclick="response_connect(2,{{$connection->id}});return false;" class="btn btn-md btn-warning" >Accept</a>
                                    <a onclick="response_connect(3,{{$connection->id}});return false;" class="btn  btn-md btn-danger" >Reject</a>
                                <span>
                            <?php } ?>
                        <?php } elseif($connectionStatus == 2) { ?>

                            {{--<a href="#" onclick="return false;" class="btn btn-block btn-success">Connected</a>--}}


                        <?php } ?>
                    <?php } ?>
                </div>

             @endif  
            <div class="col-md-12 ">
                <h2 class="grey-bottom">Skilled in</h2>
                <?php if(!$skills->isEmpty()){ ?>
                <div class="list-group">
                    <?php foreach($skills as $s){ ?>
                    <h4 class="list-group-item list-group-item-default" id="skill_{{$s->id}}">{{$s->tag}} <span class="small">(<?php if($s->level == 1){echo 'Beginner';}elseif($s->level == 2){echo 'Novice';}elseif($s->level == 3){echo 'Intermediate';}else{echo 'Expert';}?>)</span>
                    </h4>
                    <?php } ?>
                </div>
                <?php }else{ ?>
                <p>No skills added.</p>
                <?php } ?>
            </div>

            <div class="col-md-12">
                <h2 class="grey-bottom">Interested to learn</h2>
                <?php if(!$learn->isEmpty()){ ?>
                <div class="list-group">
                    <?php foreach($learn as $s){ ?>
                    <h4 class="list-group-item list-group-item-default">{{$s->tag}} <span class="small">(<?php if($s->level == 1){echo 'Beginner';}elseif($s->level == 2){echo 'Novice';}elseif($s->level == 3){echo 'Intermediate';}else{echo 'Expert';}?>)</span></h4>
                    <?php } ?>
                </div>
                <?php } else { ?>
                <p>No skills added.</p>
                <?php } ?>
            </div>

        </div>
    </div>

</div>
</div>
@endsection


@section('styles')

@endsection


@section('scripts')
    
    <script type="text/javascript">
            // User Star Rating initializer
            // initialize with defaults
            $("#user_rating_star").rateit();

            // with plugin options
            $("#user_rating_star").rateit({min:1, max:5, step:1, size:'l'});
        
        function response_connect(status, cr_id){

            $.ajax({
                url: '/post-connection',
                type: 'post',
                data: 'cr_id='+cr_id+'&status='+status,
                success: function(data) {
                    console.log(data);
                    location.reload();
                    },
                error: function(xhr, textStatus, thrownError) {
                    console.log(thrownError);
                    alert('Something went to wrong.Please Try again later...');

                }
            });
        }

        function show_review_dialog(){
            BootstrapDialog.show({
                title: 'Add a Review',
                message: $('<div></div>').load('/rate_user'),
                buttons: [{
                    label: 'Send',
                    action: function(dialogRef) {
                        dialogRef.close();
                    }
                }, 
                {
                    label: 'Cancel',
                    action: function(dialogRef) {
                        dialogRef.close();
                    }
                }
                ]
            });
        }
        </script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        });
    </script>


@endsection