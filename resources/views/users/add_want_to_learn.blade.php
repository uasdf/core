@extends('app')
@section('title')
<title>Add Skills - Placerange</title>
@endsection
@section('content')
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h2 class="grey-bottom"><span class="glyphicon glyphicon-tags sin"></span> &nbsp; Want to learn something?</h2>
                        <div class='row'>
                        	<div class="col-md-12 ">
                        		<p style="font-size: 20px;text-align: center;">
                            		Wanna learn to play piano? code Java? Badminton? 
                                    <br>or something else? 
                            		<br><br>Add all the skills you want to learn 
                                    <br>You can find people near you who can teach you
                                    <br>or
                                    <br>You can also find and attend 
                                    <br>programs or workshops 
                                    <br>near you that are based on what you want to learn
                                    <br><br>
                        		</p>
                                <form class="form-horizontal" id="learn_form_desktop"  role="form" method="POST" action="/profile/add-skill">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <input type="hidden" name="type" value="learn">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input type="text" class="form-control learn_fill" data-provide="typeahead" autocomplete="off" placeholder="skill" name="skill" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label class="radio-inline">
                                                <input type="radio" name="level" value="1" checked="checked"> Beginner
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="level" value="2"> Novice
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="level" value="3"> Intermediate
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-default" onclick="add_skill('learn');return false;">Add</button>
                                            <a type="submit" href='/profile' class="btn btn-success pull-right">Next</a>
                                        </div>
                                    </div>
                                </form>
                                <span id="skills_desktop">
                                    <div class="list-group" id="learn_list_md"></div>
                                </span>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection

@section('scripts')
	<script src="/js/bootstrap3-typeahead.js"></script>
	<script type="text/javascript">

        // dismiss alerts
        function dismiss_alerts(){
            $(".alert-danger").fadeTo(2000, 500).slideUp(500, function(){
                $(".alert-danger").alert('close');
            });
                
        }

        $('.learn_fill').typeahead({
            source: function(query, process) {
                // `query` is the text in the field
                // `process` is a function to call back with the array
                $.ajax({
                    url: "/autofill/skills/"+$('.learn_fill').val(),
                    success: process
                });
            }
        });

		function add_skill(type){
            // Dissmiss all the previous alerts
            dismiss_alerts();
            if(type == 'learn') {
                $.ajax({
                    url: '/ajax/post/add-skill',
                    type: 'post',
                    data: $('#learn_form_desktop').serialize(),
                    success: function (data) {
                        if (data) {
                            console.log("Successfully added!");
                            $('#learn_list_md').append(data);
                            $('.learn_fill').val('');
                        } else {
                            console.log(data);
                        }
                    },
                    error: function (xhr, textStatus, thrownError) {
                        JSON.parse(JSON.stringify(xhr))
                    }
                });
            }else{
                $.ajax({
                    url: '/ajax/post/add-skill',
                    type: 'post',
                    data: $('#skill_form_desktop').serialize(),
                    success: function (data) {
                        if (data) {
                            console.log("Successfully added!");
                            $('#teach_list_md').append(data);
                            $('.skill_fill').val('');
                        } else {
                            // console.log(data);
                        }
                    },
                    error: function (xhr, textStatus, thrownError) {
                        JSON.parse(JSON.stringify(xhr))
                    }
                });
            }
        }

    function remove_skill(tag,type){
        $.ajax({
            url: '/ajax/post/remove-skill',
            type: 'post',
            data: 'tag='+tag+'&type='+type,
            success: function(data) {
                if(data == 'removed') {
                    // console.log("Successfully removed!");
                }else{
                    // console.log(data);
                }
            },
            error: function(xhr, textStatus, thrownError) {
                JSON.parse(JSON.stringify(xhr + textStatus + thrownError))
            }
        });

    }
	</script>
@endsection
