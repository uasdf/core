@extends('app')
@section('title')
    <title>Change image - Placerange</title>
@endsection
@section('content')
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Please upload your image</div>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" enctype='multipart/form-data' action="/upload-image">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="col-md-4 control-label">Image</label>
                                <div class="col-md-6">
                                    {!! Form::file('image') !!}
                                </div>
                                <p class="errors">{{$errors->first('image')}}</p>
                                @if(Session::has('error'))
                                    <p class="errors">{{ Session::get('error') }}</p>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-success btn-block">
                                        Upload
                                    </button>
                                    <a href="/profile" class="pull-right">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
@endsection
