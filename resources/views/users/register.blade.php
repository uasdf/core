@extends('app')
@section('title')
<title>Register - Placerange</title>
@endsection
@section('content')
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <?php   if(isset($_COOKIE["user_data"]))$input = unserialize($_COOKIE["user_data"]);  ?>
                            <h2 class="grey-bottom">Register</h2>

                            <form class="form-horizontal" role="form" method="POST" action="/auth/register" enctype='multipart/form-data'>

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group required @if ($errors->has('first_name')) has-error @endif">
                                <label class="col-md-4 control-label">First name</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="first_name" placeholder="First name" value="{{$input['first_name']}}">
                                    @if ($errors->has('first_name')) <p class="help-block">{{ $errors->first('first_name') }}</p> @endif

                                </div>
                            </div>

                            <div class="form-group required @if ($errors->has('last_name')) has-error @endif">
                                <label class="col-md-4 control-label">Last name</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="last_name" placeholder="Last name" value="{{$input['last_name']}}">
                                    @if ($errors->has('last_name')) <p class="help-block">{{ $errors->first('last_name') }}</p> @endif

                                </div>
                            </div>

                            <div class="form-group required @if ($errors->has('email')) has-error @endif">
                                <label class="col-md-4 control-label">E-Mail Address</label>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" placeholder="Email" value="{{$input['email']}}">
                                    @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif

                                </div>
                            </div>

                            <div class="form-group @if ($errors->has('phone')) has-error @endif">
                                <label class="col-md-4 control-label">Phone</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="phone" placeholder="Phone" value="{{$input['phone']}}">
                                    @if ($errors->has('phone')) <p class="help-block">{{ $errors->first('phone') }}</p> @endif
                                </div>
                            </div>


                            <div class="form-group required @if ($errors->has('place')) has-error @endif">
                                <label class="col-md-4 control-label ">Place</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="place" value="{{$input['place']}}" placeholder="Your street, area, postcode, college or university. ">
                                    @if ($errors->has('place')) <p class="help-block">{{ $errors->first('place') }}</p> @endif

                                </div>
                            </div>

                            <div class="form-group @if ($errors->has('image')) has-error @endif">
                                <label class="col-md-4 control-label">Image</label>
                                <div class="col-md-6">
                                    {!! Form::file('image') !!}
                                    <p class="help-block">{{$errors->first('image')}}</p>
                                    @if(Session::has('error'))
                                        <p class="help-block">{{ Session::get('error') }}</p>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group required @if ($errors->has('password')) has-error @endif">
                                <label class="col-md-4 control-label">Password</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" placeholder="Password" name="password">
                                    @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif

                                </div>
                            </div>

                            <div class="form-group required @if ($errors->has('password_confirmation')) has-error @endif">
                                <label class="col-md-4 control-label">Confirm Password</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" placeholder="Confirm password" name="password_confirmation">
                                    @if ($errors->has('password_confirmation')) <p class="help-block">{{ $errors->first('password_confirmation') }}</p> @endif

                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-success btn-block">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
@endsection
