@extends('app')
<title>Feedback - Placerange</title>

@section('content')
    <div class="col-md-8 col-md-offset-2  vertical-center">
        <div class="panel panel-defaul col-md-12">

            <div class="panel-body">
                <h2 class="grey-bottom">Feedback</h2>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <?php if(!isset($success)){?>

                <form class="form-horizontal" role="form" method="POST" action="/feedback">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <div class="col-md-12">
                            <textarea class="form-control" name="content" placeholder="Please enter your feedback here"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary" style="margin-right: 15px;">
                                Send
                            </button>
                        </div>
                    </div>
                </form>

                <?php }else{?>

                <p>Thank you for sending your feedback.</p>
                <?php }?>
            </div>
        </div>
    </div>
@endsection
