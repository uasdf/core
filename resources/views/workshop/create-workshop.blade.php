@extends('app')
@section('title')
    <title>New program - Placerange</title>
@endsection

@section('styles')
    <link rel="stylesheet" href="/js/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css">

@endsection

@section('content')
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h2 class="grey-bottom">Create new program</h2>

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group required @if ($errors->has('title')) has-error @endif">
                                <label class="col-md-3 control-label">Title</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="title" value="{{isset($data['title']) ? $data['title'] : ''}}" placeholder="Eg: Badminton session, Free Keyboard class">
                                    @if ($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif

                                </div>
                            </div>

                            <div class="form-group required @if ($errors->has('summary')) has-error @endif">
                                <label class="col-md-3 control-label">Summary</label>
                                <div class="col-md-9">
                                    <textarea class="form-control" style="overflow:scroll; max-height:300px" id="summary" name="summary" value="">{{isset($data['summary']) ? $data['summary'] : ''}}</textarea>
                                    @if ($errors->has('summary')) <p class="help-block">{{ $errors->first('summary') }}</p> @endif

                                </div>
                            </div>
                            <div class="form-group required @if ($errors->has('max_attendees')) has-error @endif">
                                <label class="col-md-3 control-label">Maximum number of attendees</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="max_attendees" placeholder="100" value="{{isset($data['max_attendees']) ? $data['max_attendees'] : ''}}">
                                    @if ($errors->has('max_attendees')) <p class="help-block">{{ $errors->first('max_attendees') }}</p> @endif

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Fee</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="fee" value="{{isset($data['fee']) ? $data['fee'] : ''}}" placeholder="Free, 10£, 20£, etc.">

                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-3 control-label">Would you like to conduct a poll to decide on the timings ?</label>
                                <div class="col-md-9">
                                    <label class="radio-inline">
                                        <input type="radio" name="poll" value="1" <?=((isset($data['poll']) ? ($data['poll'] ? 'checked="checked"' : '') : '' ))?> onclick="$('#workshop_poll').show();$('#workshop_date').hide();$('#fdate').val('');$('#fstime').val('');$('#fetime').val('');"> Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="poll" value="0"  <?=((isset($data['poll']) ? ($data['poll'] ? '' : 'checked="checked"') : 'checked="checked"' ))?> onclick="$('#workshop_poll').hide();$('#workshop_date').show();"> No
                                    </label>
                                </div>
                            </div>

                            <div id="workshop_poll" <?=((isset($data['poll']) ? ($data['poll'] ? '' : 'style="display:none;"') : 'style="display:none;"' ))?> >
                                <div class="col-md-9 col-md-offset-3">
                                    <h4>You can enter upto 4 different timings</h4>
                                </div>

                                <div class="form-group required @if ($errors->has('option1_1')) has-error @endif">
                                    <label class="col-md-3 control-label ">Date and time option 1</label>
                                    <div class="col-md-3">
                                        <div class='input-group date'>
                                            <input type="text" class="form-control datetime" name="option1_1" placeholder="date" value="{{isset($data['option1_1']) ? $data['option1_1'] : ''}}">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                        @if ($errors->has('option1_1')) <p class="help-block">{{ $errors->first('option1_1') }}</p> @endif

                                    </div>
                                    <div class="col-md-3">
                                        <div class='input-group date'>
                                            <input type="text" class="form-control datetime1_1" name="option1_2" placeholder="start time" value="{{isset($data['option1_2']) ? $data['option1_2'] : ''}}">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class='input-group date'>
                                            <input type="text" class="form-control datetime1_2" name="option1_3" placeholder="end time" value="{{isset($data['option1_2']) ? $data['option1_2'] : ''}}">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Date and time option 2</label>
                                    <div class="col-md-3">
                                        <div class='input-group date'>
                                            <input type="text" class="form-control datetime" name="option2_1" placeholder="date" value="{{isset($data['option2_1']) ? $data['option2_1'] : ''}}">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class='input-group date'>
                                            <input type="text" class="form-control datetime2_1" name="option2_2" placeholder="start time" value="{{isset($data['option2_2']) ? $data['option2_2'] : ''}}">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class='input-group date'>
                                            <input type="text" class="form-control datetime2_2" name="option2_3" placeholder="end time" value="{{isset($data['option2_3']) ? $data['option2_3'] : ''}}">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Date and time option 3</label>
                                    <div class="col-md-3">
                                        <div class='input-group date'>

                                            <input type="text" class="form-control datetime" name="option3_1" placeholder="date" value="{{isset($data['option3_1']) ? $data['option3_1'] : ''}}">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class='input-group date'>

                                            <input type="text" class="form-control datetime3_1" name="option3_2" placeholder="start time" value="{{isset($data['option3_2']) ? $data['option3_2'] : ''}}">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class='input-group date'>

                                            <input type="text" class="form-control datetime3_2" name="option3_3" placeholder="end time" value="{{isset($data['option3_3']) ? $data['option3_3'] : ''}}">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Date and time option 4</label>
                                    <div class="col-md-3">
                                        <div class='input-group date'>
                                            <input type="text" class="form-control datetime" name="option4_1" placeholder="date" value="{{isset($data['option4_1']) ? $data['option4_1'] : ''}}">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class='input-group date'>
                                            <input type="text" class="form-control datetime4_1" name="option4_2" placeholder="start time" value="{{isset($data['option4_2']) ? $data['option4_2'] : ''}}">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class='input-group date'>
                                            <input type="text" class="form-control datetime4_2" name="option4_3" placeholder="end time" value="{{isset($data['option4_3']) ? $data['option4_3'] : ''}}">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div id="workshop_date" <?=((isset($data['poll']) ? ($data['poll'] ? 'style="display:none;"' : '') : '' ))?>>
                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Date and time</label>
                                    <div class="col-md-3">
                                        <div class='input-group date'>
                                            <input type="text" id="fdate" class="form-control datetime"  placeholder="date" name="date" value="{{isset($data['date']) ? $data['date'] : ''}}">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class='input-group date'>
                                            <input type="text" id="fstime" class="form-control datetime0_1"  placeholder="start time" name="time_start" value="{{isset($data['time_start']) ? $data['time_start'] : ''}}">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class='input-group date'>
                                            <input type="text" id="fetime" class="form-control datetime0_2"  placeholder="end time" name="time_end" value="{{isset($data['time_end']) ? $data['time_end'] : ''}}">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Would you like to use your contact details?</label>
                                <div class="col-md-9">
                                    <label class="radio-inline">
                                        <input type="radio" name="new_contact" value="0" checked="checked" onclick="$('#workshop_address').hide();"> Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="new_contact" value="1"  onclick="$('#workshop_address').show();"> No
                                    </label>
                                </div>
                            </div>
                            <div id="workshop_address" style="display:none;">

                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Name</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="contact_name" value="{{isset($data['contact_name']) ? $data['contact_name'] : ''}}" placeholder="Contact name">
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Email</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="email" value="{{isset($data['email']) ? $data['email'] : ''}}" placeholder="Contact email">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Phone</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="phone" value="{{isset($data['phone']) ? $data['phone'] : ''}}" placeholder="Contact phone">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group required @if ($errors->has('venue')) has-error @endif">
                                <label class="col-md-3 control-label">Venue</label>
                                <div class="col-md-9">
                                    <textarea class="form-control" name="venue">{{isset($data['venue']) ? $data['venue'] : ''}}</textarea>
                                    @if ($errors->has('venue')) <p class="help-block">{{ $errors->first('venue') }}</p> @endif

                                </div>
                            </div>

                            <div class="col-md-9 col-md-offset-3">
                                <h4>Associate this program to a place</h4>
                            </div>
                            <div class="form-group required @if ($errors->has('place')) has-error @endif">
                                <label class="col-md-3 control-label">Place</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="place" value="<?php if(isset($place_d))echo place_min($place_d)?>">
                                    @if ($errors->has('place')) <p class="help-block">{{ $errors->first('place') }}</p> @endif

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" class="btn btn-success btn-block">
                                        Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
@endsection




@section('scripts')
    <script src="/js/plugins/bootstrap-datetimepicker/js/moment.js"></script>
    <script src="/js/dateFormat.js"></script>

    <script src="/js/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">

        $(function () {
            
            $('.datetime').datetimepicker({
                format: "D MMM YYYY"
            });
        });

        var dateNow = new Date();
        dateNow.setDate(dateNow.getDate() + 7);
        $('.datetime').val($.format.date(dateNow, "D MMM yyyy"));
        

        $(function () {
            $('.datetime0_1').datetimepicker({
                format: "LT",
                stepping: 15

            });
        });
        $(function () {
            $('.datetime0_2').datetimepicker({
                format: "LT",
                stepping: 15
            });
        });

        $('.datetime0_1').val('9:00');
        $('.datetime0_2').val('10:00');


        $(function () {
            $('.datetime1_1').datetimepicker({
                format: "LT",
                stepping: 15
            });
        });
        $(function () {
            $('.datetime1_2').datetimepicker({
                format: "LT",
                stepping: 15
            });
        });

        $(function () {
            $('.datetime2_1').datetimepicker({
                format: "LT",
                stepping: 15
            });
        });
        $(function () {
            $('.datetime2_2').datetimepicker({
                format: "LT",
                stepping: 15
            });
        });

        $(function () {
            $('.datetime3_1').datetimepicker({
                format: "LT",
                stepping: 15
            });
        });
        $(function () {
            $('.datetime3_2').datetimepicker({
                format: "LT",
                stepping: 15
            });
        });

        $(function () {
            $('.datetime4_1').datetimepicker({
                format: "LT",
                stepping: 15
            });
        });
        $(function () {
            $('.datetime4_2').datetimepicker({
                format: "LT",
                stepping: 15
            });
        });
    </script>


        <script src="/js/plugins/bootstrap3-wysiwyg/bootstrap3-wysihtml5.all.min.js"></script>
        <script type="text/javascript">
        $('#summary').wysihtml5({
            toolbar: {
                "font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
                "emphasis": true, //Italics, bold, etc. Default true
                "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": true, //Button to insert a link. Default true
                "image": true, //Button to insert an image. Default true,
                "color": false, //Button to change color of font
                "blockquote": true, //Blockquote
                "size": 'sm' //default: none, other options are xs, sm, lg
            }
        });
    </script>
    <link rel="stylesheet" href="/js/plugins/bootstrap3-wysiwyg/bootstrap3-wysihtml5.css">

@endsection