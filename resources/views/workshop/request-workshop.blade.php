@extends('app')
@section('title')
    <title>New workshop - Placerange</title>
@endsection

@section('scripts')
    {{--<script src="/js/bootstrap-wysihtml5.js"></script>--}}
    <script src="/js/plugins/bootstrap3-wysiwyg/bootstrap3-wysihtml5.all.min.js"></script>
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>--}}
    <script type="text/javascript">
        $('#summary').wysihtml5({
            toolbar: {
                "font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
                "emphasis": true, //Italics, bold, etc. Default true
                "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": true, //Button to insert a link. Default true
                "image": true, //Button to insert an image. Default true,
                "color": false, //Button to change color of font
                "blockquote": true, //Blockquote
                "size": 'sm' //default: none, other options are xs, sm, lg
            }
        });
    </script>
    <link rel="stylesheet" href="/js/plugins/bootstrap3-wysiwyg/bootstrap3-wysihtml5.css">
@endsection
@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">New workshop</div>
            <div class="panel-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form class="form-horizontal" role="form" method="POST" action="">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group required">
                        <label class="col-md-3 control-label">I want to learn</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="skill" value="" placeholder="Enter a skill">
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-3 control-label">Summary</label>
                        <div class="col-md-9">
                            <textarea class="form-control" style="overflow:scroll; max-height:300px" id="summary" name="summary" value="" placholder="Details like date, time, location, etc. "></textarea>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-md-3 control-label">Postcode</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="postcode" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-success btn-block">
                                Create
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
