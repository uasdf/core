@extends('app')
@section('title')
    <title>Update workshop - Placerange</title>
@endsection

@section('styles')
    <link rel="stylesheet" href="/js/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css">

@endsection

@section('content')
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Update program</div>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group required">
                                <label class="col-md-3 control-label">Title</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="title" value="{{$workshop->title}}">
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-md-3 control-label">Summary</label>
                                <div class="col-md-9">
                                    <textarea class="form-control" style="overflow:scroll; max-height:300px" id="summary" name="summary" value="">{!!$workshop->summary!!}</textarea>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-md-3 control-label">Maximum number of attendees</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="max_attendees" value="{{$workshop->max_attendees}}">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-md-3 control-label">Date and time</label>
                                <div class="col-md-3">
                                    <div class='input-group date'>
                                        <input type="text" id="fdate" class="form-control datetime"  placeholder="date" name="date" value="{{date('d M YYYY',strtotime($workshop->date))}}">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class='input-group date'>
                                        <input type="text" id="fstime" class="form-control datetime1"  placeholder="start time" name="time_start" value="{{$workshop->time_start}}">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class='input-group date'>
                                        <input type="text" id="fetime" class="form-control datetime2"  placeholder="end time" name="time_end" value="{{$workshop->time_end}}">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Fee</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="fee" value="{{$workshop->fee}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Would you like to use your contact details?</label>
                                <div class="col-md-9">
                                    <label class="radio-inline">
                                        <input type="radio" name="new_contact" value="0" <?=($workshop->new_contact ? '' : 'checked="checked"')?> onclick="$('#workshop_address').hide();"> Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="new_contact" value="1"  <?=($workshop->new_contact ? 'checked="checked"' : '')?> onclick="$('#workshop_address').show();"> No
                                    </label>
                                </div>
                            </div>
                            <div id="workshop_address" style="display:none;">
                                <div class="col-md-9 col-md-offset-3">
                                    <h4>Contact details of the organizer.</h4>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Name</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="contact_name" value="{{$workshop->contact_name}}">
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Email</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="email" value="{{$workshop->email}}">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-md-3 control-label">Phone</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="phone" value="{{$workshop->phone}}">
                                    </div>
                                </div>
                            </div>


                            <div class="form-group required">
                                <label class="col-md-3 control-label">Venue</label>
                                <div class="col-md-9">
                                    <textarea class="form-control" name="venue">{{$workshop->venue}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" class="btn btn-success btn-block">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
@endsection


@section('scripts')
    <script src="/js/plugins/bootstrap-datetimepicker/js/moment.js"></script>

    <script src="/js/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('.datetime').datetimepicker({
                format: "D MMM YYYY"
            });
        });
        $(function () {
            $('.datetime1').datetimepicker({
                format: "LT"
            });
        });
        $(function () {
            $('.datetime2').datetimepicker({
                format: "LT"
            });
        });
    </script>


    <script src="/js/plugins/bootstrap3-wysiwyg/bootstrap3-wysihtml5.all.min.js"></script>
    <script type="text/javascript">
        $('#summary').wysihtml5({
            toolbar: {
                "font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
                "emphasis": true, //Italics, bold, etc. Default true
                "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": true, //Button to insert a link. Default true
                "image": true, //Button to insert an image. Default true,
                "color": false, //Button to change color of font
                "blockquote": true, //Blockquote
                "size": 'sm' //default: none, other options are xs, sm, lg
            }
        });
    </script>
    <link rel="stylesheet" href="/js/plugins/bootstrap3-wysiwyg/bootstrap3-wysihtml5.css">
@endsection