@extends('app')

@section('title')
    <title> {{$workshop->title}} </title>
    <link rel="canonical" href="http://{{$_SERVER['HTTP_HOST']}}{{$_SERVER['REQUEST_URI']}}" />
    <meta property="og:url"                content="http://{{$_SERVER['HTTP_HOST']}}{{$_SERVER['REQUEST_URI']}}" />
    <meta property="og:type"               content="Program" />
    <meta property="og:title"              content="{{$workshop->title}}" />
    <meta property="og:image"              content="http://www.placerange.com/uploads/programs/main/thumbnail/{{$workshop->main_image}}" />
@endsection

@section('styles')
<style>
#main_image{
    display:block;
    height:100%; 
    width:100%; 
    background-image: url(/uploads/programs/main/thumbnail/{{$workshop->main_image}});
    background-repeat:no-repeat;
    background-position: center center;
    background-size: 100% 100%;
}

#upload_main_image_div{
    height:50px; 
    width:100%;
    margin-top: -50px; 
    position:relative; 

    bottom:0;
    background-color: rgba(0,0,0,.5);
    display:none;
    color:#fff;
    text-align: center;
    padding-top:15px;
    cursor: pointer;
}

#main_image_holder{
    height: 200px;
    position: absolute;
    bottom: 70px;
    left: 20px;
    width: 200px;
}

.title_holder{
    position: absolute;
    bottom: 220px;
    left: 250px;
    color:#fff;
}

.title_holder h1{
    font-size: 30px;
    text-shadow: 2px 2px 5px #000;
}

#main_image_holder:hover #upload_main_image_div{
    display: block;
}

#cover_image{
    display:block;
    height:100%; 
    width:100%; 
    background-color:#000;    
    overflow:hidden;
}

#upload_cover_image_button{
    position:absolute;
    right:20px;
    bottom:20px;
}

#cover_image_holder:hover #upload_cover_image_button{
    display: block;
}


</style>

@endsection


@section('header_fullwidth')
    
@endsection

@section('content')
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=448530131848352";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <div class="row ">

            <div class="col-md-10 col-md-offset-1 white-bg">
                
                <div class="row" id='cover_image_holder' style="height:341px;margin-bottom: 10px; position:relative;">
                    <div class='image-gloss' style="display:block; height:100%; width:100%;">
                        <span id='cover_image' style='filter: blur(5px);'>
                            <img class="image-blur" src='/uploads/programs/cover/medium/{{$workshop->cover_image}}' height="100%" width="100%">
                        </span>
                    </div>

                    <div class="grey-all" id='main_image_holder'>
                        <span id='main_image'></span>
                        <?php if($uid == $workshop->user_id){?>
                        <span id='upload_main_image_div' onclick="$('#main_image_file_button').click();"> Change Image </span>
                        <form style="display:none;" id='main_image_upload_form' enctype='multipart/form-data'>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="program_id" value="{{$workshop->id}}">
                            <input id='main_image_file_button' name="main_image" type="file" onchange="upload_main_image();">        
                        </form>
                        <?php }?>
                    </div>

                    <div class="title_holder">
                        <h1>{{$workshop->title}} <small></small></h1>
                    </div>

                    <?php if($uid == $workshop->user_id){?>
                    <button id='upload_cover_image_button'onclick="$('#cover_image_file_button').click();" class="btn btn-default"> Change Cover Image </button>
                    <form style="display:none;" id='cover_image_upload_form' enctype='multipart/form-data'>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="program_id" value="{{$workshop->id}}">
                        <input id='cover_image_file_button' name="cover_image" type="file" onchange="upload_cover_image();">        
                    </form>
                    <?php }?>
                </div>
                                    
                <div class="row">
                    <div class="col-md-3 ">
                        
                        <h3 class="grey-bottom"><span class="glyphicon glyphicon-king c_gray"></span> &nbsp;Created by <?php if($uid == $workshop->user_id){?><a class="sin small pull-right" style="margin-top: 4px;" href="/update-program/{{$workshop->id}}"><span class='glyphicon glyphicon-edit'></span>  &nbsp;Edit Program</a><?php }?></h3>
                        <div class="media">
                            <div class="media-left">
                                <a href="/user/{{$workshop->user->username}}">
                                    <img class="media-object" src="<?=user_avatar($workshop->user->image_name)?>" alt="...">
                                </a>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">{{$workshop->user->first_name.' '.$workshop->user->last_name}}</h4>
                                {{date("d M - Y",strtotime($workshop->created_at))}}
                            </div>
                        </div>

                        <h3 class="grey-bottom"> <span class='glyphicon glyphicon-tags vista'></span> &nbsp; Skills <?php if($uid === $workshop->user_id){?> <a class="small pull-right " style="margin-top: 6px;" href="#" onclick="$('#skill_form').toggle(300);return false;">Add skill +</a> <?php }?></h3>
                        <?php foreach($skills as $s){?>
                        <h5>{{$s->tag}} <span class="small"><?php if($s->level == 1){echo 'Beginner';}elseif($s->level == 2){echo 'Novice';}elseif($s->level == 3){echo 'Intermediate';}else{echo 'Expert';}?></span><?php if($uid == $workshop->user_id){?>&nbsp;&nbsp;&nbsp;<a class="small pomo" href="/program/remove-skill/{{$workshop->slug}}/{{$s->tag}}">&times;</a><?php }?></h5>
                        <?php }
                        if($skills->isEmpty()){?>
                        <p>No skills added</p>

                        <?php }if($uid == $workshop->user_id){?>

                        <form class="form-horizontal" id="skill_form" style="display:none;" role="form"
                              method="POST" action="/program/add-skill/{{$workshop->slug}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="type" value="skill">

                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="text" class="form-control skill_fill"  data-provide="typeahead" autocomplete="off"  placeholder="skill" name="skill" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12" style="padding-left: 40px;">
                                    <label class="radio">
                                        <input type="radio" name="level" value="1" checked="checked"> Beginner
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="level" value="2"> Novice
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="level" value="3"> Intermediate
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="level" value="4"> Expert
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-success btn-block">
                                        Add
                                    </button>
                                </div>
                            </div>
                        </form>
                        <?php }?>

                        <h3 class="grey-bottom"> <span class='glyphicon glyphicon-home sin'></span> &nbsp;Venue </h3>
                        <p>
                        <?=($workshop->venue ? nl2br($workshop->venue) : '')?>
                        </p>

                        <h3 class="grey-bottom"><span class="glyphicon glyphicon glyphicon-user c_cal"></span> &nbsp;Organizer</h3>
                        <?php if($workshop->new_contact && $uid){?>){
                        ?>
                        <p>
                        <?=($workshop->name ?  $workshop->name.'<br>' : '')?>
                        <?=($workshop->phone ? $workshop->phone.'<br>' : '')?>
                        <?=($workshop->email ? $workshop->email.'<br>' : '')?>
                        <?php }elseif($uid){?>
                        <?=$workshop->user->first_name.' '.$workshop->user->last_name?><br>
                        <?=($workshop->user->phone ? $workshop->user->phone.'<br>' : '')?>
                        <?=($workshop->user->email ? $workshop->user->email.'<br>' : '')?>
                        <?php }else{?>
                            Login to view the organizer details.
                        <?php }?>
                        </p>

                        <h3 class="grey-bottom"><span class="glyphicon glyphicon-map-marker pomo"></span> &nbsp;Places<?php if($uid == $workshop->user_id){?> &nbsp;&nbsp;&nbsp;<a class="small pull-right" style="margin-top: 6px;" href="#" onclick="$('#add_place').toggle(300);add_place();return false;" >Add place +</a><?php }?></h3>
                        <div id="add_place" style="display:none"></div>
                        <div id="place_data">
                            <?php foreach($places as $p){?>
                            <p id="place_<?=$p->id?>"><?=place_min($p);?>
                                <?php if($uid == $workshop->user_id){?>

                                <a class="small sin" href="#" onclick="edit_place(<?=$p->id?>); return false;">(Edit)</a>&nbsp;
                                <?php if(!$p->primary){?>
                                <a class="small pomo" href="#" onclick="remove_place(<?=$p->id?>); return false;">(Remove)</a>
                                <?php }?>
                                <?php }?>

                            </p>

                            <?php }?>
                        </div>

                    </div>
                    <div class="col-md-6 grey-left grey-right">
                        <div class="row">

                            <div class="col-md-12">
                                <h2 class="grey-bottom"><span class="glyphicon glyphicon-align-justify"></span> &nbsp;Summary</h2>
                                {!!$workshop->summary!!}

                            </div>
                            
                            <div class="col-md-12">
                                <h3 class="grey-bottom"><span class="glyphicon glyphicon-comment sin"></span> &nbsp;Comments</h3>
                            </div>
                            <div class="col-md-12 grey-bottom" style="margin-bottom: 20px;" >
                                <section class="comment-list">
                                <?php if($comments->isEmpty()){?>
                                <p>No comments yet</p>
                                <?php }else{?>
                                <?php foreach($comments as $c){
                                ?>


                                        <article class="row" style="padding-bottom: 50px;margin-bottom: 10px;">
                                            <div class="col-md-2 col-sm-2 hidden-xs">
                                                <figure class="thumbnail">
                                                    <a href="/user/{{$c->user->username}}">
                                                        <img class="img-responsive" src="<?=user_avatar($c->user->image_name,'thumbnail')?>" />
                                                        <figcaption class="text-center">{{$c->user->username}}</figcaption>
                                                    </a>
                                                </figure>
                                            </div>
                                            <div class="col-md-10 col-sm-10">
                                                <div class="panel panel-default arrow left">

                                                    <div class="panel-body">

                                                        <header class="text-left">
                                                            <div class="comment-user"><i class="fa fa-user"></i> {{$c->user->first_name.' '.$c->user->last_name}}</div>
                                                            <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i> {{date("d M - Y",strtotime($c->created_at))}}</time>
                                                        </header>
                                                        <div class="comment-post">
                                                            <p>
                                                                {!! $c->comment !!}
                                                            </p>
                                                        </div>
                                                        <p class="text-right"><?php if($c->user_id == $uid && $uid){?>
                                                            <a class="btn btn-default btn-xs" href="#" onclick="$('form#edit_reply{{$c->id}}').toggle(300);return false;">Edit </a>
                                                            <a class="btn btn-danger btn-xs" href="/issue/remove-comment/{{$c->id}}">Delete </a>

                                                        <?php }?>
                                                            <?php if($uid){?>
                                                            <a class="btn btn-default btn-xs" href="#" onclick="$('form#reply_comment{{$c->id}}').toggle(300);return false;"><span class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true"></span>&nbsp;Reply </a>
                                                            <?php }?>
                                                    </div>
                                                </div>
                                                <?php if($uid){?>
                                                    <div class="col-md-12">
                                                        <form class="form-horizontal" class="reply_comment" id="reply_comment{{$c->id}}"role="form"
                                                              method="POST" action="/program/reply-to-comment/{{$c->id}}" style="display:none;">
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                            <div class="form-group">
                                                                <div class="col-md-12">
                                                                    <textarea class="form-control wsyi" style="overflow:scroll; max-height:300px" rows=5 id="reply" name="reply" placeholder="Post your reply..."></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-md-12">
                                                                    <button type="submit" class="btn btn-success btn-block">
                                                                        Send reply
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                <?php }?>
                                            </div>




                                    <?php if($c->user_id == $uid){?>
                                    <div class="col-md-12">
                                        <form class="form-horizontal" class="edit_reply" id="edit_reply{{$c->id}}"role="form"
                                              method="POST" action="/program/edit-reply/{{$c->id}}" style="display:none;">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <textarea class="form-control wsyi" style="overflow:scroll; max-height:300px" rows=5 id="reply" name="reply" placeholder="Post your solution...">{!! $c->comment !!}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-success btn-block">
                                                        Update
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <?php }?>

                                    <?php if(!$c->replies->isEmpty()){?>
                                    <div class="col-md-9 col-md-offset-3 grey-left grey-bottom">
                                        <span class="glyphicon glyphicon-menu-up" style="position:absolute; top:-14px;left:-7px;color:#eee;"></span>
                                        <h3 class="">Replies</h3>
                                        <?php foreach($c->replies as $cr){?>
                                        <div class="row grey-top" style="padding-bottom: 10px;">
                                            <div class="col-md-8 ">
                                                {!! $cr->comment !!}
                                            </div>
                                            <div class="col-md-4 grey-left" style="margin-top: 10px;">
                                                <p>{{$cr->user->first_name.' '.$cr->user->last_name}} <br> {{date("d M Y - h:i A",strtotime($cr->created_at))}}</p>
                                                <?php if($uid == $cr->user_id){?>
                                                <a class="btn btn-danger btn-xs" href="/program/remove-reply/{{$cr->id}}">Delete </a>
                                                <?php }?>
                                            </div>
                                        </div>
                                        <?php }?>
                                    </div>
                                    <?php }?>
                                    </article>


                                <?php }?>
                            <?php }?>
                            </section>
                            </div>
                            <?php if($uid){?>
                            <div class="col-md-12">
                                <form class="form-horizontal" id="reply_form" role="form" method="POST" action="/program/reply/{{$workshop->id}}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <textarea class="form-control wsyi" style="overflow:scroll; max-height:100px" rows=5 id="reply" name="reply" placeholder="Post your comment..."></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-success btn-block">
                                                Add
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <?php }?>
                        </div>
                    </div>
                    <div class="col-md-3">

                    <?php if($currentVote && !$workshop->date){?>
                        <h3 class="grey-bottom">Timings</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <p>Waiting to finalize </p>

                                <h4>Given timings</h4>
                                <span class="c_pomo glyphicon glyphicon-heart{{($currentVote->option1 ? '' : '-empty c_gray' )}} "></span> {{$workshop->poll->option1_1.' '.$workshop->poll->option1_2.' to '.$workshop->poll->option1_3}} <span class="badge pull-right">{{count($option[0])}}</span> <br>
                                <?php if($workshop->poll->option2_1){?><span class="c_pomo glyphicon glyphicon-heart{{($currentVote->option2 ? '' : '-empty c_gray' )}} "></span> {{$workshop->poll->option2_1.' '.$workshop->poll->option2_2.' to '.$workshop->poll->option2_3}} <span class="badge pull-right">{{count($option[1])}}</span><br><?php }?>
                                <?php if($workshop->poll->option3_1){?><span class="c_pomo glyphicon glyphicon-heart{{($currentVote->option3 ? '' : '-empty c_gray' )}} "></span> {{$workshop->poll->option3_1.' '.$workshop->poll->option3_2.' to '.$workshop->poll->option3_3}} <span class="badge pull-right">{{count($option[2])}}</span><br><?php }?>
                                <?php if($workshop->poll->option4_1){?><span class="c_pomo glyphicon glyphicon-heart{{($currentVote->option4 ? '' : '-empty c_gray' )}} "></span> {{$workshop->poll->option4_1.' '.$workshop->poll->option4_2.' to '.$workshop->poll->option4_3}} <span class="badge pull-right">{{count($option[3])}}</span> <br><?php }?>

                            </div>
                        </div>
                    <?php }elseif(!$workshop->date && $uid == $workshop->user_id){?>
                        <h3 class="grey-bottom">Finalize timings</h3>

                        <div class="row">
                            <form class="form-horizontal" id="reply_form"role="form"
                                  method="POST" action="/program/finalize-date/{{$workshop->poll->id}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <div class="col-md-11 col-md-offset-1">
                                        <p>Please select the timings and submit once you have finalized.</p>

                                        <label class="radio grey-bottom" >
                                            <input type="radio" name="option" value="1" checked onclick="$('#option_1_users').toggle(300);$('#option_2_users').hide();$('#option_3_users').hide();$('#option_4_users').hide();">{{$workshop->poll->option1_1.' '.$workshop->poll->option1_2.' to '.$workshop->poll->option1_3}} <span class="badge pull-right">{{count($option[0])}}</span>
                                        </label>
                                        <?php if($option[0]){?>
                                        <div class="row option-users" id="option_1_users" style="display:none; margin-top: 20px;">

                                        <?php foreach($option[0] as $opt){?>
                                            <div class="media">
                                                <div class="media-left">
                                                    <a href="/user/{{$opt->username}}">
                                                        <img class="media-object" src="<?=user_avatar($opt->image_name)?>" alt="...">
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading">{{$opt->first_name.' '.$opt->last_name}}</h4>
                                                </div>
                                            </div>
                                        <?php }?>
                                        </div>

                                        <?php }?>
                                        <?php if($workshop->poll->option2_1){?>
                                        <label class="radio grey-bottom">
                                            <input type="radio" name="option" value="2"  onclick="$('#option_2_users').toggle(300);$('#option_1_users').hide();$('#option_3_users').hide();$('#option_4_users').hide();">{{$workshop->poll->option2_1.' '.$workshop->poll->option2_2.' to '.$workshop->poll->option2_3}} <span class="badge  pull-right">{{count($option[1])}}</span>
                                        </label>
                                        <?php }?>
                                        <?php if($option[1]){?>
                                        <div class="row option-users" id="option_2_users" style="display:none;margin-top: 20px;">

                                        <?php foreach($option[1] as $opt){?>
                                            <div class="media">
                                                <div class="media-left">
                                                    <a href="/user/{{$opt->username}}">
                                                        <img class="media-object" src="<?=user_avatar($opt->image_name)?>" alt="...">
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading">{{$opt->first_name.' '.$opt->last_name}}</h4>
                                                </div>
                                            </div>
                                        <?php }?>
                                        </div>

                                        <?php }?>

                                        <?php if($workshop->poll->option3_1){?>
                                        <label class="radio grey-bottom">
                                            <input type="radio" name="option" value="3"  onclick="$('#option_3_users').toggle(300);$('#option_2_users').hide();$('#option_1_users').hide();$('#option_4_users').hide();">{{$workshop->poll->option3_1.' '.$workshop->poll->option3_2.' to '.$workshop->poll->option3_3}} <span class="badge  pull-right">{{count($option[2])}}</span>
                                        </label>
                                        <?php }?>
                                        <?php if($option[2]){?>
                                        <div class="row option-users" id="option_3_users" style="display:none;margin-top: 20px;">

                                        <?php foreach($option[2] as $opt){?>
                                            <div class="media">
                                                <div class="media-left">
                                                    <a href="/user/{{$opt->username}}">
                                                        <img class="media-object" src="<?=user_avatar($opt->image_name)?>" alt="...">
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading">{{$opt->first_name.' '.$opt->last_name}}</h4>
                                                </div>
                                            </div>
                                        <?php }?>
                                        </div>

                                        <?php }?>


                                        <?php if($workshop->poll->option4_1){?>
                                        <label class="radio grey-bottom" >
                                            <input type="radio" name="option" value="4" onclick="$('#option_4_users').toggle(300);$('#option_2_users').hide();$('#option_3_users').hide();$('#option_1_users').hide();">{{$workshop->poll->option4_1.' '.$workshop->poll->option4_2.' to '.$workshop->poll->option4_3}} <span class="badge pull-right">{{count($option[3])}}</span>
                                        </label>
                                        <?php }?>
                                        <?php if($option[3]){?>
                                        <div class="row option-users" id="option_4_users" style="display:none;margin-top: 20px;">

                                        <?php foreach($option[3] as $opt){?>
                                            <div class="media">
                                                <div class="media-left">
                                                    <a href="/user/{{$opt->username}}">
                                                        <img class="media-object" src="<?=user_avatar($opt->image_name)?>" alt="...">
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading">{{$opt->first_name.' '.$opt->last_name}}</h4>
                                                </div>
                                            </div>
                                        <?php }?>
                                        </div>

                                        <?php }?>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-11 col-md-offset-1">
                                        <button type="submit" class="btn btn-success btn-block">
                                            Finalize
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    <?php }elseif(!$currentVote && !$workshop->date){?>

                    <h3 class="grey-bottom">Available timings</h3>

                    <div class="row">
                            <form class="form-horizontal" id="reply_form"role="form"
                                  method="POST" action="/program/poll/{{$workshop->poll->id}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                        <div class="col-md-11 col-md-offset-1">
                                            <p>Please select the timings that would be convenient for you?</p>

                                            <label class="checkbox grey-bottom">
                                                <input type="checkbox" name="option1" value="1">{{$workshop->poll->option1_1.' '.$workshop->poll->option1_2.' to '.$workshop->poll->option1_3}} <span class="badge pull-right">{{count($option[0])}}</span>
                                            </label>
                                            <?php if($workshop->poll->option2_1){?>
                                            <label class="checkbox grey-bottom">
                                                <input type="checkbox" name="option2" value="1">{{$workshop->poll->option2_1.' '.$workshop->poll->option2_2.' to '.$workshop->poll->option2_3}} <span class="badge  pull-right">{{count($option[1])}}</span>
                                            </label>
                                            <?php }?>
                                            <?php if($workshop->poll->option3_1){?>
                                            <label class="checkbox grey-bottom">
                                                <input type="checkbox" name="option3" value="1">{{$workshop->poll->option3_1.' '.$workshop->poll->option3_2.' to '.$workshop->poll->option3_3}} <span class="badge  pull-right">{{count($option[2])}}</span>
                                            </label>
                                            <?php }?>
                                            <?php if($workshop->poll->option4_1){?>
                                            <label class="checkbox grey-bottom">
                                                <input type="checkbox" name="option4" value="1">{{$workshop->poll->option4_1.' '.$workshop->poll->option4_2.' to '.$workshop->poll->option4_3}} <span class="badge pull-right">{{count($option[3])}}</span>
                                            </label>
                                            <?php }?>
                                        </div>
                                </div>

                            <div class="form-group">
                                <div class="col-md-11 col-md-offset-1">
                                    <button type="submit" class="btn btn-success btn-block">
                                        Submit
                                    </button>
                                </div>
                            </div>
                            </form>
                    </div>

                   <?php  }elseif($workshop->date && strtotime($workshop->date) >= time()){?>

                    <h3 class="grey-bottom"><span class="glyphicon glyphicon-calendar vista"></span> &nbsp;Date and Time</h3>
                        {{date('d M Y', strtotime($workshop->date)).' '.$workshop->time_start.' to '.$workshop->time_end}}

                        <?php if($attendees){
                            $current_user_is_attending = false;
                            foreach($attendees as $at){
                                if($uid == $at->user_id && !$at->cancelled){
                                    $current_user_is_attending = true;
                                }

                            }
                        }
                        ?>
                        <?php 
                        $seatsLeft = $workshop->max_attendees - count($attendees);
                        ?>
                        <p>{{$seatsLeft}} seats left!</p>
                        <?php if($current_user_is_attending){?>
                        <p>You are attending this workshop <span class="glyphicon glyphicon-thumbs-up"></span></p>
                        <a href="/program/cancel-attend/{{$workshop->slug}}" class="btn btn-danger btn-xs">cancel <span class="glyphicon glyphicon-thumbs-down"></span></a>
                        <?php }elseif($seatsLeft && $uid != $workshop->user_id){?>
                        <a href="/program/attend/{{$workshop->slug}}" class="btn btn-success btn-block">Attend <span class="glyphicon glyphicon-thumbs-up"></span></a>
                        <?php }?>
                        <h3 class="grey-bottom">Participants</h3>
                        <?php if(!$attendees->isEmpty()){?>
                        <?php foreach($attendees as $at){?>
                        <div class="media">
                            <div class="media-left">
                                <a href="/user/{{$at->user->username}}">
                                    <img class="media-object" src="<?=user_avatar($at->user->image_name)?>" alt="...">
                                </a>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">{{$at->user->first_name.' '.$at->user->last_name}}</h4>
                            </div>
                        </div>
                        <?php }?>
                        <?php }else{?>
                        <p>No attendees yet.</p>
                        <?php }?>
                    <?php }elseif($workshop->date && strtotime($workshop->date) < time()){?>

                        <h3 class="grey-bottom"><span class="glyphicon glyphicon-calendar vista"></span> &nbsp;Date and Time</h3>
                        {{date('d M Y', strtotime($workshop->date)).' '.$workshop->time_start.' to '.$workshop->time_end}}

                        <?php if($attendees){
                            $current_user_is_attending = false;
                            foreach($attendees as $at){
                                if($uid == $at->user_id && !$at->cancelled){
                                    $current_user_is_attending = true;
                                }
                            }
                        }
                        ?>
                        <h3 class="grey-bottom"><span class="glyphicon glyphicon-list c_cal"></span> &nbsp;Attendees</h3>
                        <?php if(!$attendees->isEmpty()){?>
                        <?php foreach($attendees as $at){?>
                        <div class="media">
                            <div class="media-left">
                                <a href="/user/{{$at->user->username}}">
                                    <img class="media-object" src="<?=user_avatar($at->user->image_name)?>" alt="...">
                                </a>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">{{$at->user->first_name.' '.$at->user->last_name}}</h4>
                            </div>
                        </div>
                        <?php }?>
                        <?php }else{?>
                        <p>No attendees.</p>
                        <?php }?>
                    <?php }?>
                            <div class="row" style="margin-top:20px;">
                                <div class="col-md-12">
                                    <div class="fb-share-button" data-href="http://{{$_SERVER['SERVER_NAME']}}{{$_SERVER['REQUEST_URI']}}" data-layout="button"></div>
                                </div>
                            </div>
                    </div>

                </div>
            </div>
        </div>
@endsection

@section('scripts')

    <script src="/js/bootstrap3-typeahead.js"></script>
    <script src="/js/bootstrap-dialog.js"></script>


    <script type="text/javascript">
        $('.skill_fill').typeahead({
            source: function(query, process) {
                // `query` is the text in the field
                // `process` is a function to call back with the array
                $.ajax({
                    url: "/autofill/skills/"+$('.skill_fill').val(),
                    success: process
                });
            }
        });

    </script>

    <script src="/js/plugins/bootstrap3-wysiwyg/bootstrap3-wysihtml5.all.min.js"></script>
    <script type="text/javascript">
        $("textarea.wsyi").each(function(){$(this).wysihtml5({
            toolbar: {
                "font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
                "emphasis": true, //Italics, bold, etc. Default true
                "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": true, //Button to insert a link. Default true
                "image": true, //Button to insert an image. Default true,
                "color": false, //Button to change color of font
                "blockquote": true, //Blockquote
                "size": 'sm' //default: none, other options are xs, sm, lg
            }
        });
        });

    </script>
    <link rel="stylesheet" href="/js/plugins/bootstrap3-wysiwyg/bootstrap3-wysihtml5.css">

    <script type="text/javascript">

        function add_place(){
            $.ajax({
                url: '/ajax/get/add-place',
                type: 'get',
                data: 'for=workshop',
                success: function(data) {
                    console.log("Success!");
                    $('#add_place').html(data);
                },
                error: function(xhr, textStatus, thrownError) {
                    alert('Something went to wrong.Please Try again later...');
                }
            });
        }

        function post_add_place(){
            $.ajax({
                url: '/ajax/post/add-place',
                type: 'post',
                data: $('#form_place').serialize()+'&related_id=<?=$workshop->id?>',
                success: function(data) {
                    $('#add_place').hide();
                    $('#place_data').append(data);
                },
                error: function(xhr, textStatus, thrownError) {
                    alert(thrownError);
                }
            });
        }

        function edit_place(id){
            $.ajax({
                url: '/ajax/get/edit-place',
                type: 'get',
                data:'id='+id,
                success: function(data) {
                    console.log("Success!");
                    $('#place_'+id).append(data);
                },
                error: function(xhr, textStatus, thrownError) {
                    alert('Something went to wrong.Please Try again later...');
                }
            });
        }

        function post_edit_place(id){
            $.ajax({
                url: '/ajax/post/edit-place',
                type: 'post',
                data: $('#edit_place_'+id).serialize()+'&id='+id,
                success: function(data) {
                    $('edit_place_'+id).hide();
                    $('#place_'+id).html(data);
                },
                error: function(xhr, textStatus, thrownError) {
                    alert(thrownError);
                }
            });
        }

        function remove_place(id){
            $.ajax({
                url: '/ajax/post/remove-place/',
                type: 'post',
                data: 'id='+id,

                success: function(data) {
                    if(data == 'success') {
                        console.log("Successfully removed!");
                        $('#place_' + id).remove();
                    }else{
                        console.log(data);
                    }
                },
                error: function(xhr, textStatus, thrownError) {
                    alert(xhr + textStatus + thrownError);
                }
            });
        }

        function upload_main_image(){
            $.ajax({
                url:         "/program/main_image", // Url to which the request is send
                type:        "POST",             // Type of request to be send, called as method
                data:        new FormData($('#main_image_upload_form')[0]), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache:       false,             // To unable request pages to be cached
                processData: false,        // To send DOMDocument or non processed data file it is set to false
            success:     function(data)   // A function to be called if request succeeds
            {   
                if(data != 'error'){
                    $('#main_image_upload_loading').hide();
                    $("#main_image").css('background-image','url(/uploads/programs/main/medium/' + data + ')');
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) { 
                BootstrapDialog.show({title: 'Unable to upload your image.', message: errorThrown});
            } 
            });
        }

        function upload_cover_image(){
            $.ajax({
                url:         "/program/cover_image", // Url to which the request is send
                type:        "POST",             // Type of request to be send, called as method
                data:        new FormData($('#cover_image_upload_form')[0]), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache:       false,             // To unable request pages to be cached
                processData: false,        // To send DOMDocument or non processed data file it is set to false
            success:     function(data)   // A function to be called if request succeeds
            {   
                if(data != 'error'){
                    $('#cover_image_upload_loading').hide();
                    $("#cover_image img").attr('src','/uploads/programs/cover/medium/' + data);
                    console.log(data);
                }
            }
            });
        }

    </script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    @endsection