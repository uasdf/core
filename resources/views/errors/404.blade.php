@extends('app')
@section('scripts')
    <link href='http://fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

    <style>
        body {
            height: 100vh;
        }

        .container {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            color: #B0BEC5;
            display: block;
            font-weight: 100;
            font-family: 'Lato';
            text-align: center;
            vertical-align: middle;
        }

        .content {
            margin-top: 200px;
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 72px;
            margin-bottom: 40px;
        }
    </style>
@endsection
@section('content')
<div class="container">
    <div class="content">
        <div class="title">Not found :(</div>
    </div>
</div>
@endsection
