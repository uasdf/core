
        @if (count($errors) > 0)
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your action.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif

        <div class="row" >
            <div class="col-md-12" id="user_list_new">
                <?php if($connections){?>
                <div class="row">
                    <div class="col-md-12">
                        <input type="search" id="search3" value="" class="form-control" placeholder="Search users">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="margin-top: 20px;height:310px;overflow: scroll;" id="searchable-container3">
                        <?php 
                        foreach($connections  as $co){
                            ?>
                            <div class="media user_list " onclick="$('form#message_form_new #to_user').val({{$co->id}});$('#to_name_new').html('{{$co->first_name." ".$co->last_name}}');$('#message_frame2').show();$('#user_list_new').hide();">
                                <div class="media-left">
                                    <a href="/user/{{$co->username}}">
                                        <img class="media-object small" src="<?=user_avatar($co->image_name)?>" alt="...">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">{{$co->first_name.' '.$co->last_name}}</h4>
                                </div>
                            </div>
                            <?php 
                        }?>
                    </div>
                </div>
                <?php }else{?>
                    You have no connections yet.
                    <?php }?>
            </div>


            <div class="col-md-12 grey-left"  id="message_frame2" style="display: none;">
                <div class="col-md-12">
                    <h3>To:<span id="to_name_new"></span></h3>
                    <form class="form-horizontal" id="message_form_new" role="form"
                          method="POST" action="/post-message" style="margin-top: 20px;">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id="to_user" name="to_user" value="">
                        <div class="form-group">
                            <div class="col-md-12">
                                <textarea  class="form-control" autofocus autocomplete="off" placeholder="An optional message" name="message" id="message_text_new" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-info pull-right">
                                    Send message
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <script>
            $(function () {
                $( '#searchable-container3' ).searchable({
                    searchField: '#search3',
                    selector: '.media',
                    childSelector: '.media-heading',
                    show: function( elem ) {
                        elem.slideDown(100);
                    },
                    hide: function( elem ) {
                        elem.slideUp( 100 );
                    }
                })
            });
        </script>