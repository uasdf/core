@extends('app')

@section('title')
    <title>Notifications - Placerange</title>
@endsection


@section('content')
    <div class="col-md-10 col-md-offset-1 white-bg">
        @if (count($errors) > 0)
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your action.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1>Notifications</h1>
                </div>
            </div>
            <div class="col-md-12">
                    <?php if($notifications){?>
                    <div class="row bottom-20">
                        <div class="col-md-12  pad-10">
                            <div class="notifications">
                                <?php foreach($notifications as $not){?>
                                <a href="{{$not->target_url}}">
                                    <div class="notification-item">
                                        {!! $not->content !!}
                                    </div>
                                </a>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                    <?php }else{?>

                    <p>You have no notifications yet.</p>

                    <?php }?>

            </div>

        </div>
    </div>


@endsection


@section('styles')
@endsection


@section('scripts')

@endsection