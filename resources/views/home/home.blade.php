@extends('app')

@section('title')
	<title>Home - Placerange</title>
@endsection


@section('content')
			<div class="col-md-10 col-md-offset-1">
				@if (count($errors) > 0)
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="alert alert-danger">
								<strong>Whoops!</strong> There were some problems with your action.<br><br>
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
				@endif
				<div class="row white-bg">
					<div class="col-md-3" style="padding-top: 12px;">
						<div class="media">
							<div class="media-left">
								<a href="/user/{{$user->username}}">
									<img class="media-object" src="<?=user_avatar($user->image_name)?>" alt="...">
								</a>
							</div>
							<div class="media-body">
								<h4 class="media-heading">{{$user->first_name.' '.$user->last_name}}</h4>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 ">
                                    <h3 class="grey-bottom">Issues <a class="small pull-right " href="/new-issue">Raise +</a></h3>

                                    <div class="row">
                                          <div class="col-md-12">
                                              <div class="list-group">
                                                  <a href="/raised-issues" class="list-group-item list-group-item-danger  ">Raised <span class="badge">{{count($my_issues)}}</span></a>
                                                  <a href="/handling-issues" class="list-group-item list-group-item-info">Currently Handling<span class="badge">{{count($handling_issues)}}</span></a>
                                                  <a href="/solved-by-me" class="list-group-item list-group-item-success ">Solved by me <span class="badge">{{count($resolved_issues)}}</span></a>

                                              </div>
                                          </div>
                                    </div>
                                </div>

                            <div class="col-md-12">
                                <h3 class="grey-bottom">Programs <a class="small pull-right " href="/create-program">create +</a></h3>

                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="list-group">
                                            <a href="/programs-organising" class="list-group-item list-group-item-danger  ">Organizing <span class="badge">{{count($organising_workshops)}}</span></a>
                                            <a href="/programs-attending" class="list-group-item list-group-item-warning ">Attending <span class="badge">{{count($attending_workshops)}}</span></a>
                                            <a href="/programs-organised" class="list-group-item list-group-item-success">Organized <span class="badge">{{count($organised_workshops)}}</span></a>
                                            <a href="/programs-attended" class="list-group-item list-group-item-success">Attended <span class="badge">{{count($attended_workshops)}}</span></a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
					</div>
					<div class="col-md-6" >
							<h3 class="grey-bottom">What's new </h3>
							<div id="feed_data" class="infinite-container">
								<input type="hidden" value="" name="start_date" id="start_date">
							</div>

						</div>
					<div class="col-md-3 hidden-xs" style="padding-top: 10px;">

						<div class="row bottom-20">
							<div class="col-md-12  pad-10">

								<h3 class="grey-bottom">Members near you</h3>
								<?php foreach($members_nearby as $mn){?>
								<div class="media">
									<div class="media-left">
										<a href="/user/{{$mn->username}}">
											<img class="media-object" src="<?=user_avatar($mn->image_name)?>" alt="...">
										</a>
									</div>
									<div class="media-body">
										<a href="/user/{{$mn->username}}"><h5 class="media-heading">{{$mn->first_name.' '.$mn->last_name}}</h5></a>
										<p>
											<?php $skill = '';
											foreach ($mn->skills() as $s) {
												$skill .= $s->tag . ', ';
											}
											echo rtrim($skill, ', '); ?>
										</p>

									</div>
								</div>
								<?php } ?>
								<a href="/invite-friends" class="pull-right">Find or Invite friends +</a>
							</div>
						</div>

						<?php if(!$notifications->isEmpty()){ ?>
						<div class="row " >
							<div class="col-md-12  pad-10" >
								<h3 class="grey-bottom">Notifications</h3>
								<div class="notifications">
									<?php foreach($notifications as $not){ ?>
									<a href="{{$not->target_url}}">
										<div class="notification-item">
											{!! $not->content !!}
										</div>
									</a>
									<?php } ?>
								</div>
								<a class="pull-right" href="/notifications">View all +</a>

							</div>
						</div>
						<?php } ?>

					</div>
				</div>
			</div>

			<div class="col-md-12 text-center " id="feed_autoload">
				<a class="infinite-more-link" href="#" onclick="load_feed();return false;">More</a>

			</div>
@endsection


@section('styles')

@endsection


@section('scripts')

	<script src="/js/plugins/waypoint/waypoint.js"></script>
	<script src="/js/plugins/waypoint/adapters/jquery.js"></script>
	<script src="/js/plugins/waypoint/adapters/jquery-zepto-fn-extension.js"></script>
	<script src="/js/plugins/waypoint/adapters/noframework.js"></script>
	{{--<script src="/js/plugins/waypoint/adapters/zepto.js"></script>--}}
	<script src="/js/plugins/waypoint/group.js"></script>
	<script src="/js/plugins/waypoint/context.js"></script>
	<script src="/js/plugins/waypoint/debug.js"></script>
	<script src="/js/plugins/waypoint/shortcuts/inview.js"></script>
	<script src="/js/plugins/waypoint/shortcuts/sticky.js"></script>





	<script>



		$(function() {
			load_initial();

			var waypoint = new Waypoint({
				element: document.getElementById('footer'),
				handler: function(direction) {

					if (direction === 'down') {
						load_feed();
					}
				},
				offset: 'bottom-in-view'

			})

		});


		function load_feed(){
			var start_date = $('#start_date').val();

			if(!start_date){
				return false;
			}
			$('#feed_data').append("<img style='margin:0 auto; display:block;' id='loader_bar' src='img/bar-loader.gif'>");

			$.ajax({
				url: '/get-feeds',
				type: 'get',
				data:'start_date='+start_date,
				success: function(data) {
					console.log('Feed loaded!')

					console.log("Success!");
					$('#start_date').remove();
					$('#loader_bar').remove();
					$('#feed_data').append(data);
					Waypoint.refreshAll();

				},
				error: function(xhr, textStatus, thrownError) {
					alert('Something went to wrong.Please Try again later...');
				}
			});
		}

		function load_initial(){
			var start_date = $('#start_date').val();

			$('#feed_data').append("<img style='margin:0 auto; display:block;' id='loader_bar' src='img/bar-loader.gif'>");

			$.ajax({
				url: '/get-feeds',
				type: 'get',
				data:'start_date='+start_date,
				success: function(data) {
					console.log('Feed loaded!')

					console.log("Success!");
					$('#start_date').remove();
					$('#loader_bar').remove();
					$('#feed_data').append(data);
					Waypoint.refreshAll();

				},
				error: function(xhr, textStatus, thrownError) {
					alert('Something went to wrong.Please Try again later...');
				}
			});
		}

	</script>






@endsection