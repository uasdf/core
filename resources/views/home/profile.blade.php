@extends('app')

@section('title')
    <title>Dashboard - Placerange</title>
@endsection

@section('content')
            <div class="col-md-10 col-md-offset-1 white-bg">
                @if (count($errors) > 0)
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your action.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12" style="padding-top: 12px;">
                                <div class="media">
                                    <div class="media-left">
                                        <a href="/user/{{$user->username}}">
                                            <img id='user_image' class="media-object" src="<?=user_avatar($user->image_name)?>" alt="...">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">{{$user->first_name.' '.$user->last_name}}</h4>
                                        <a class="small" href="#" onclick="$('#upload_image_file_button').click();">(Change image)</a>
                                        <form style="display:none;" id='user_image_upload_form' enctype='multipart/form-data'>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="user_id" value="{{$user->id}}">
                                            <input id='upload_image_file_button' name="user_image" type="file" onchange="upload_user_image();">        
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row hidden-xs" >
                            <div class="col-md-12" data-step="1" data-intro="Here you can manage the skills you have got.">
                                <h3><span class="glyphicon glyphicon-tags sin"></span> &nbsp;Skilled in  <a class="small pull-right" href="#" style="margin-top:6px;" onclick="$('#skill_form_desktop').toggle(300);return false;">Add skill +</a></h3>
                                <form class="form-horizontal skillForm_desktop" id="skill_form_desktop" style="display:none;" role="form"
                                      method="POST" action="/profile/add-skill">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="type" value="skill">

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input type="text" class="form-control skill_fill"  data-provide="typeahead" autocomplete="off" placeholder="skill" name="skill"
                                                   value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12" style="padding-left: 40px;">
                                            <label class="radio">
                                                <input type="radio" name="level" value="2" checked> Novice
                                            </label>
                                            <label class="radio">
                                                <input type="radio" name="level" value="3"> Intermediate
                                            </label>
                                            <label class="radio">
                                                <input type="radio" name="level" value="4"> Expert
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-success btn-block" onclick="add_skill('teach');return false;">
                                                Add
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <span id="skills_desktop">
                                <div class="list-group" id="teach_list_md">
                                    {{--Skill teach desktop--}}
                                <?php foreach($skills as $s){?>
                                 <h4 class="list-group-item list-group-item-default"> {{$s->tag}} <span class="small skill_level " data-name="skill_level" data-type="select" data-pk="{{$s->id}}" data-url="/profile/skill-update">(<?php if($s->level == 1){echo 'Beginner';}elseif($s->level == 2){echo 'Novice';}elseif($s->level == 3){echo 'Intermediate';}else{echo 'Expert';}?>)</span>&nbsp;&nbsp;&nbsp;<a class="small pull-right pomo" onclick="remove_skill('{{$s->tag}}','skill');$(this).parent().fadeOut();return false;" href="/profile/remove-skill/{{$s->tag}}">&times;</a></h4>
                                <?php } ?>
                                <a href="/who-can-learn" data-step="2" data-intro="Once you have added your skill, you can then find people near you who wants to learn what you know." class="pull-right" style="margin-top: 10px;" onclick="if($('#teach_list_md h4').length > 0){}else{alert('Please add a skill to view this page.');return false;}">See who wants to learn</a>
                                </div>

                                </span>
                                



                            </div>

                            <div class="col-md-12" data-step="3" data-intro="Here you can manage what you want to learn.">

                                <h3><span class="glyphicon glyphicon-tags sin"></span> &nbsp;Want to learn  <a href="#" class='small pull-right' style="margin-top: 6px;" onclick="$('#learn_form_desktop').toggle(300);return false;" class="small"> Add skill +</a></h3>
                                <form class="form-horizontal" id="learn_form_desktop" style="display:none;" role="form"
                                      method="POST" action="/profile/add-skill">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <input type="hidden" name="type" value="learn">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input type="text" class="form-control learn_fill" data-provide="typeahead" autocomplete="off" placeholder="skill" name="skill" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label class="radio-inline">
                                                <input type="radio" name="level" value="1" checked="checked"> Beginner
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="level" value="2"> Novice
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="level" value="3"> Intermediate
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-success btn-block" onclick="add_skill('learn');return false;">
                                                Add
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <div class="list-group" id="learn_list_md">
                                    {{--Skill learn desktop--}}
                                    <?php foreach($learn as $s){?>
                                    <h4 class="list-group-item list-group-item-default">{{$s->tag}} <span class="small learn_level" data-name="skill_level" data-type="select" data-pk="{{$s->id}}" data-url="/profile/skill-update">(<?php if($s->level == 1){echo 'Beginner';}elseif($s->level == 2){echo 'Novice';}elseif($s->level == 3){echo 'Intermediate';}else{echo 'Expert';}?>)</span>&nbsp;&nbsp;&nbsp;<a class="small pull-right pomo" href="#" onclick="remove_skill('{{$s->tag}}','learn');$(this).parent().fadeOut();return false;">&times;</a></h4>
                                    <?php }?>
                                <a href="/who-can-teach" data-step="4" data-intro="Once you have added what you want to learn, you can then find people near you who can teach you." class='pull-right' style="margin-top: 10px;" onclick="if($('#learn_list_md h4').length > 0){}else{alert('Please add a skill to view this page.');return false;}">See who can teach </a>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 grey-left grey-right" style="padding-left: 0;padding-right:0;min-height: 80vh;">
                        <div class="col-md-12">
                            <div class="row visible-xs-block">
                                <div class="col-md-12 ">
                                    <h2 class="grey-bottom">Skilled in <a class="small" href="#" onclick="$('#skill_form').toggle(300);return false;">Add skill &nbsp;&nbsp;&nbsp;+</a></h2>
                                    <form class="form-horizontal" id="skill_form" style="display:none;" role="form"
                                          method="POST" action="/profile/add-skill">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                        <input type="hidden" name="type" value="skill">

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control skill_fill" data-provide="typeahead" autocomplete="off" placeholder="skill" name="skill" value="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="radio-inline">
                                                    <input type="radio" name="level" value="2" checked> Novice
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="level" value="3"> Intermediate
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="level" value="4"> Expert
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-success btn-block">
                                                    Add
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="list-group" id="teach_list_xs">
                                    {{--Teach skills mobile--}}
                                    <?php foreach($skills as $s){?>
                                        <h4 class="list-group-item list-group-item-default"> {{$s->tag}} <span class="small skill_level" data-name="skill_level" data-type="select" data-pk="{{$s->id}}" data-url="/profile/skill-update">(<?php if($s->level == 1){echo 'Beginner';}elseif($s->level == 2){echo 'Novice';}elseif($s->level == 3){echo 'Intermediate';}else{echo 'Expert';}?>)</span>&nbsp;&nbsp;&nbsp;<a class="small pull-right pomo" href="/profile/remove-skill/{{$s->tag}}">&times;</a></h4>
                                    <?php }?>
                                    <a href="/who-can-learn" class='pull-right' style="margin-top: 10px;" onclick="if($('#teach_list_xs h4').length > 0){}else{alert('Please add a skill to view this page.');return false;}">See who wants to learn</a>
                                    </div>                                    

                                </div>


                                <div class="col-md-12">

                                    <h3>Want to learn  <a href="#" onclick="$('#learn_form_mobile').toggle(300);return false;" class="small"> Add a skill +</a></h3>
                                    <form class="form-horizontal" id="learn_form_mobile" style="display:none;" role="form"
                                          method="POST" action="/profile/add-skill">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                        <input type="hidden" name="type" value="learn">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control learn_fill" data-provide="typeahead" autocomplete="off" placeholder="skill" name="skill" value="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="radio-inline">
                                                    <input type="radio" name="level" value="1" checked="checked"> Beginner
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="level" value="2"> Novice
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="level" value="3"> Intermediate
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-success btn-block">
                                                    Add
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="list-group" id="learn_list">
                                        {{--Learn skills mobile--}}
                                        <?php foreach($learn as $s){?>
                                        <h4 class="list-group-item list-group-item-default">{{$s->tag}} <span class="small">(<?php if($s->level == 1){echo 'Beginner';}elseif($s->level == 2){echo 'Novice';}elseif($s->level == 3){echo 'Intermediate';}else{echo 'Expert';}?>)</span>&nbsp;&nbsp;&nbsp;<a class="small pull-right pomo" href="/profile/remove-skill/{{$s->tag}}">&times;</a></h4>
                                        <?php }?>
                                    <a href="/who-can-teach" class='pull-right' style="margin-top: 10px;" onclick="if($('#learn_list h4').length > 0){}else{alert('Please add a skill to view this page.');return false;}">See who can teach </a>
                                    </div>
                                    
                                </div>


                            </div>
                            <div class="row">
                                <div class="col-md-12" data-step="5" data-intro="Here you can manage your places. You can add upto 5 places (Work, Home, Education, etc,.).">
                                <h3> <span class="glyphicon glyphicon-map-marker pomo"></span> &nbsp; Places &nbsp;&nbsp;&nbsp;<a class="small" href="#" onclick="$('#add_place').toggle();add_place();return false;" >Add place +</a></h3>
                                    <div id="add_place" style="display:none"></div>

                                    <div class="col-md-12" id="place_data">
                                    <?php if($places){?>
                                        <?php foreach($places as $p){?>

                                            <p id="place_<?=$p->id?>"><span class="glyphicon glyphicon-map-marker pomo"></span> <?=place_min($p, true);?>
                                                <a class="small" href="#" onclick="edit_place(<?=$p->id?>);">(Edit)</a>&nbsp;
                                                <?php if(!$p->primary){?>
                                                <a class="small" href="#" onclick="remove_place(<?=$p->id?>);">(Remove)</a>
                                                <?php }?>
                                            </p>
                                        <?php }?>
                                    <?php }?>
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12" data-step="6" data-intro="These tabs makes it easy for you to view all the Programs and Issues created near you right on your dashboard.">
                                        
                                          <ul class="nav nav-tabs" style="margin-bottom: 30px;">
                                            <li class="active"><a data-toggle="tab" href="#programs_tab">Programs</a></li>
                                            <li><a data-toggle="tab" href="#issues_tab">Issues</a></li>
                                            <li><a data-toggle="tab" href="#learn_tab">Learn</a></li>
                                            <li><a data-toggle="tab" href="#teach_tab">Teach</a></li>
                                          </ul>

                                          <div class="tab-content">
                                            <div id="programs_tab" class="tab-pane fade in active">
                                              <h3>Near you</h3>
                                                    <?php if(count($programs) > 0 ){?>
                                                    <div class="col-md-12" id="searchable-container2">
                                                        <?php foreach($programs as $program){?>
                                                            <div class="list-group">
                                                                <a href="/program/{{$program['slug']}}" class="list-group-item">
                                                                    <h4 class="list-group-item-heading" ><strong>{{$program['title']}}</strong></h4>
                                                                    <div class="list-group-item-text grey-bottom">
                                                                        {!!$program['summary']!!}
                                                                        <br>
                                                                        <h4>{{$program['days_left']}} </h4>
                                                                    </div>
                                                                    
                                                                    <span class='small'>{{date("F jS, Y",strtotime($program['date'] ))}} | {{count($program['comments'])}} Comments | {{count($program['attendees'])}} Attendees</span>
                                                                </a>    
                                                            </div>

                                                        <?php }?>
                                                    </div>
                                                    <?php }else{?>
                                                    You haven't got any programs near you.
                                                    <?php }?>
                                                </div>                                            
                                            <div id="issues_tab" class="tab-pane fade">
                                              <h3>Near you</h3>
                                                    <?php if(count($issues) > 0 ){?>
                                                    <div class="col-md-12" id="searchable-container2">
                                                        <?php foreach($issues as $issue){?>
                                                            <div class="list-group">
                                                                <a href="/issue/{{$issue['slug']}}" class="list-group-item">
                                                                    <h4 class="list-group-item-heading" ><strong>{{$issue['title']}}</strong></h4>
                                                                    <div class="list-group-item-text grey-bottom">
                                                                        {!!$issue['summary']!!}
                                                                        <br>
                                                                        <h4>{{$issue['days_past']}} </h4>
                                                                    </div>
                                                                    
                                                                    <span class='small'>{{date("F jS, Y",strtotime($issue['date'] ))}} | {{count($issue['comments'])}} Comments</span>
                                                                </a>    
                                                            </div>

                                                        <?php }?>
                                                    </div>
                                                    <?php }else{?>
                                                    You haven't got any issues near you.
                                                    <?php }?>
                                            </div>
                                            <div id="learn_tab" class="tab-pane fade">
                                              
                                              <a href="/who-can-teach" style="margin-top: 10px;" onclick="if($('#learn_list_md h4').length > 0){}else{alert('Please add some skills.');return false;}"> 
                                                <span class="glyphicon glyphicon-search" aria-hidden="true" style='font-size: 20px;'></span>
                                                Search for users nearby and find out who can teach what you want to learn.
                                              </a>
                                            </div>
                                            <div id="teach_tab" class="tab-pane fade">

                                              <p>
                                                  <a href="/who-can-learn" style="margin-top: 10px;" onclick="if($('#teach_list_md h4').length > 0){}else{alert('Please add some skills.');return false;}"> 
                                                    <span class="glyphicon glyphicon-search" aria-hidden="true" style='font-size: 20px;'></span>
                                                    Search for users nearby who wants to learn what you know.
                                                  </a>
                                              </p>
                                            </div>
                                          </div>
                                        </div>  
                                    </div>
                                </div>


                            
                            <div class="col-md-12">
                                <div class="row">

                                </div>
                            </div>
                    </div>
                </div>
            </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12" id="cr_holder" class="cr_holder">
                                <?php if(!$connectionRequests->isEmpty()){?>
                                    <h3 id="cr_header" class="grey-bottom">Connection Requests</h3>
                                    <?php foreach($connectionRequests as $cr){?>
                                        <div class="media grey-bottom" style="padding-bottom:20px;" id="cr_{{$cr->id}}">
                                            <div class="media-left">
                                                <a href="/user/{{$cr->sender->username}}">
                                                    <img class="media-object" src="<?=user_avatar($cr->sender->image_name)?>" alt="...">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">{{$cr->sender->first_name.' '.$cr->sender->last_name}}</h4>
                                                <p>{{$cr->message}}</p>
                                                <span class="button_replace">
                                                <a id="crAccept_{{$cr->id}}" onclick="response_connect(2,{{$cr->id}});return false;" class="btn btn-xs btn-info" >Accept</a>
                                                <a id="crReject_{{$cr->id}}" onclick="response_connect(3,{{$cr->id}});return false;" class="btn  btn-xs btn-danger" >Reject</a>
                                                </span>

                                            </div>
                                        </div>
                                    <?php }?>

                                <?php }?>

                            </div>
                            <div class="col-md-12 " data-step="7" data-intro="Here you can manage all the Issues. Unlike StackOverflow, Issues here are mainly handled by someone living near you so that support can be provided in person.">
                                    <h3 class="grey-bottom">Issues <a class="small pull-right " href="/new-issue">Raise +</a></h3>

                                    <div class="row">
                                          <div class="col-md-12">
                                              <div class="list-group">
                                                  <a href="/raised-issues" class="list-group-item list-group-item-danger  ">Raised <span class="badge">{{count($my_issues)}}</span></a>
                                                  <a href="/handling-issues" class="list-group-item list-group-item-info">Currently Handling<span class="badge">{{count($handling_issues)}}</span></a>
                                                  <a href="/solved-by-me" class="list-group-item list-group-item-success ">Solved by me <span class="badge">{{count($resolved_issues)}}</span></a>

                                              </div>
                                          </div>
                                    </div>
                                </div>

                            <div class="col-md-12" data-step="8" data-intro="Here you can manage all the Programs. Programs are activities or workshops that are based on knowledge sharing within your place. ">
                                <h3 class="grey-bottom">Programs <a class="small pull-right " href="/create-program">create +</a></h3>

                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="list-group">
                                            <a href="/programs-organising" class="list-group-item list-group-item-danger  ">Organizing <span class="badge">{{count($organising_workshops)}}</span></a>
                                            <a href="/programs-attending" class="list-group-item list-group-item-warning ">Attending <span class="badge">{{count($attending_workshops)}}</span></a>
                                            <a href="/programs-organised" class="list-group-item list-group-item-success">Organized <span class="badge">{{count($organised_workshops)}}</span></a>
                                            <a href="/programs-attended" class="list-group-item list-group-item-success">Attended <span class="badge">{{count($attended_workshops)}}</span></a>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

@endsection


@section('styles')
    <link rel="stylesheet" href="/js/plugins/datepicker/css/datepicker.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/js/plugins/intro-js/introjs.css">
@endsection


@section('scripts')
    
    <script src="/js/plugins/intro-js/intro.js"></script>
    <?php if(!$dashboard_intro){ ?>
    <script type='text/javascript'>
    $(document).ready(function(){
        introJs().start();

    });
    </script>
    <?php }?>
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
    <script>
        $.fn.editable.defaults.mode = 'inline';
        $(function(){
        $('.skill_level').editable({
//            value: 2,
            source: [
                {value: 2, text: 'Novice'},
                {value: 3, text: 'Intermediate'},
                {value: 4, text: 'Expert'}
            ],
            error: function(response, newValue) {
                if(response.status === 500) {
                    return 'Service unavailable. Please try later.';
                } else {
                     console.log(response.responseText);
                }
            }
        });

        $('.learn_level').editable({
//            value: 1,
            source: [
                {value: 1, text: 'Beginner'},
                {value: 2, text: 'Novice'},
                {value: 3, text: 'Intermediate'}
            ],
            error: function(response, newValue) {
                if(response.status === 500) {
                    return 'Service unavailable. Please try later.';
                } else {
                    console.log(response.responseText);
                }
            }
        });

        });
    </script>

    <script src="/js/bootstrap3-typeahead.js"></script>

    <script type="text/javascript">
        $('.skill_fill').typeahead({
            source: function(query, process) {
                // `query` is the text in the field
                // `process` is a function to call back with the array
                $.ajax({
                    url: "/autofill/skills/"+$('.skill_fill').val(),
                    success: process
                });
            }
        });

    </script>

    <script type="text/javascript">
        $('.learn_fill').typeahead({
            source: function(query, process) {
                // `query` is the text in the field
                // `process` is a function to call back with the array
                $.ajax({
                    url: "/autofill/skills/"+$('.learn_fill').val(),
                    success: process
                });
            }
        });

    </script>

    <script type="text/javascript">
        $('.teach_fill').typeahead({
            source: function(query, process) {
                // `query` is the text in the field
                // `process` is a function to call back with the array
                $.ajax({
                    url: "/autofill/skills/"+$('.teach_fill').val(),
                    success: process
                });
            }
        });


    </script>

    <script src="/js/plugins/datepicker/js/bootstrap-datepicker.js"></script>

    <script type="text/javascript">

    function upload_user_image(){
        $.ajax({
            url:         "/upload-image", // Url to which the request is send
            type:        "POST",             // Type of request to be send, called as method
            data:        new FormData($('#user_image_upload_form')[0]), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache:       false,             // To unable request pages to be cached
            processData: false,        // To send DOMDocument or non processed data file it is set to false
        success:     function(data)   // A function to be called if request succeeds
        {   
            if(data != 'error'){
                $('#user_image_upload_loading').hide();
                $("#user_image").attr('src','/uploads/users/icon/' + data);
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
            BootstrapDialog.show({title: 'Unable to upload your image.', message: errorThrown});
        } 
        });
    }

    function response_connect(status, cr_id){

        $.ajax({
            url: '/post-connection',
            type: 'post',
            data: 'cr_id='+cr_id+'&status='+status,
            success: function(data) {
                console.log(data);
                if(status == 2) {
                    $('#cr_'+cr_id + ' .button_replace').html('Accepted');
                }else {
                    $('#cr_'+cr_id + ' .button_replace').html('Rejected');

                }
            },
            error: function(xhr, textStatus, thrownError) {
                console.log(thrownError);
                alert('Something went to wrong.Please Try again later...');
            }
        });
    }

    function add_place(){
        $.ajax({
            url: '/ajax/get/add-place',
            type: 'get',
            data: 'for=user',
            success: function(data) {
                console.log("Success!");
                $('#add_place').html(data);
            },
            error: function(xhr, textStatus, thrownError) {
                alert('Something went to wrong.Please Try again later...');
            }
        });
    }

    function post_add_place(){
        $.ajax({
            url: '/ajax/post/add-place',
            type: 'post',
            data: $('#form_place').serialize()+'&related_id=<?=$uid?>',
        success: function(data) {
//            console.log("Success!");
            $('#add_place').hide();
            $('#place_data').append(data);
//            location.reload();
        },
        error: function(xhr, textStatus, thrownError) {
            alert(thrownError);
        }
        });
    }

    function edit_place(id){
            $.ajax({
                url: '/ajax/get/edit-place',
                type: 'get',
                data:'id='+id,
                success: function(data) {
                    console.log("Success!");
                    $('#place_'+id).append(data);
                },
                error: function(xhr, textStatus, thrownError) {
                    alert('Something went to wrong.Please Try again later...');
                }
            });
        }

    function post_edit_place(id){
            $.ajax({
                url: '/ajax/post/edit-place',
                type: 'post',
                data: $('#edit_place_'+id).serialize()+'&id='+id,
                success: function(data) {
                    $('edit_place_'+id).hide();
                    $('#place_'+id).html(data);
                },
                error: function(xhr, textStatus, thrownError) {
                    alert(thrownError);
                }
            });
        }

    function remove_place(id){
            $.ajax({
                url: '/ajax/post/remove-place',
                type: 'post',
                data: 'id='+id,

                success: function(data) {
                    if(data == 'success') {
                        console.log("Successfully removed!");
                        $('#place_' + id).fadeOut();
                    }else{
                        console.log(data);
                    }
                },
                error: function(xhr, textStatus, thrownError) {
                    alert(xhr + textStatus + thrownError);
                }
            });
    }

        function add_skill(type){
            if(type == 'learn') {
                $.ajax({
                    url: '/ajax/post/add-skill',
                    type: 'post',
                    data: $('#learn_form_desktop').serialize(),
                    success: function (data) {
                        if (data) {
                            console.log("Successfully added!");
                            $('#learn_list_md').append(data);
                            $('.learn_fill').val('');
                            $('#learn_form_desktop').toggle(300);
                        } else {
                            console.log(data);
                        }
                    },
                    error: function (xhr, textStatus, thrownError) {
                        JSON.parse(JSON.stringify(xhr))
//                    alert(xhr + textStatus + thrownError);
                    }
                });
            }else{
                $.ajax({
                    url: '/ajax/post/add-skill',
                    type: 'post',
                    data: $('#skill_form_desktop').serialize(),
                    success: function (data) {
                        if (data) {
                            console.log("Successfully added!");
                            $('#teach_list_md').append(data);
                            $('.skill_fill').val('');
                            $('#skill_form').toggle(300);
                        } else {
                            console.log(data);
                        }
                    },
                    error: function (xhr, textStatus, thrownError) {
                        JSON.parse(JSON.stringify(xhr))
//                    alert(xhr + textStatus + thrownError);
                    }
                });
            }
        }

    function remove_skill(tag,type){
        $.ajax({
            url: '/ajax/post/remove-skill',
            type: 'post',
            data: 'tag='+tag+'&type='+type,
            success: function(data) {
                if(data == 'removed') {
                    console.log("Successfully removed!");
                }else{
                    console.log(data);
                }
            },
            error: function(xhr, textStatus, thrownError) {
                JSON.parse(JSON.stringify(xhr + textStatus + thrownError))
//                    alert(xhr + textStatus + thrownError);
            }
        });

    }
        


    </script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        });
    </script>

    <script type="text/javascript">
        function load_datepicker(){
            $(".date_picker").each(function(){$(this).datepicker({ format: 'yyyy-mm-dd' });});
        }
    </script>
@endsection