@extends('app')

@section('title')
    <title>Issues - Placerange</title>
@endsection


@section('content')
    <div class="col-md-10 col-md-offset-1 white-bg">
        @if (count($errors) > 0)
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your action.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-3" style="padding-top: 12px;">
                    <div class="media" style="margin-bottom: 20px;">
                        <div class="media-left">
                            <a href="/user/{{$user->username}}">
                                <img class="media-object" src="<?=user_avatar($user->image_name)?>" alt="...">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">{{$user->first_name.' '.$user->last_name}}</h4>
                        </div>
                    </div>
                <div class="row">
                </div>
            </div>
            <div class="col-md-6 grey-left grey-right" style="height: 100%;">
                <div class="col-md-12 ">

                    <div class="row">
                        <h2 class="grey-bottom text-center">Issues <a class="small" href="/new-issue">Raise +</a></h2>

                        <div class="col-md-12 grey-bottom">
                            <h3 id="raised">Raised</h3>
                            <?php if(!$my_issues->isEmpty()){?>
                            <div class="col-md-12">
                                <div class="list-group">
                                <?php foreach($my_issues as $issue){?>
                                    <div class="list-group">
                                        <a href="/issue/{{$issue->slug}}" class="list-group-item list-group-item-danger">
                                            <h4 class="list-group-item-heading"><strong>{{$issue->title}}</strong></h4>
                                            <div class="list-group-item-text">{!!$issue->summary!!}</div>
                                        </a>
                                    </div>

                                <?php }?>
                                </div>
                            </div>
                            <?php }else{?>
                            No issues yet
                            <?php }?>
                        </div>

                        <div class="col-md-12 grey-bottom">
                            <h3 id="currently_handling">Currently Handling</h3>
                            <?php if(!$handling_issues->isEmpty()){?>
                            <div class="col-md-12">
                                <div class="list-group">
                                    <?php foreach($handling_issues as $hand){?>
                                    <div class="list-group">
                                        <a href="/issue/{{$hand->issue->slug}}" class="list-group-item list-group-item-info">
                                            <h4 class="list-group-item-heading"><strong>{{$hand->issue->title}}</strong></h4>
                                            <div class="list-group-item-text">{!!$hand->issue->summary!!}</div>
                                        </a>
                                    </div>

                                    <?php }?>
                                </div>
                            </div>
                            <?php }else{?>
                            No issues yet
                            <?php }?>
                        </div>

                        <div class="col-md-12 grey-bottom">
                            <h3 id="solved_by_me">Solved by me</h3>
                            <?php if(!$solved_issues->isEmpty()){?>
                            <div class="col-md-12">
                                <div class="list-group">
                                <?php foreach($solved_issues as $issue){?>
                                    <div class="list-group">
                                        <a href="/issue/{{$issue->slug}}" class="list-group-item list-group-item-success">
                                            <h4 class="list-group-item-heading"><strong>{{$issue->title}}</strong></h4>
                                            <div class="list-group-item-text">{!!$issue->summary!!}</div>
                                        </a>
                                    </div>
                                <?php }?>
                                </div>
                            </div>
                            <?php }else{?>
                            None yet
                            <?php }?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-3 grey-left" style="position:static;">
                <div class="list-group" style="position: fixed;width: 265px;margin-top:10px">
                    <a href="#raised" class="list-group-item active issues_menu" onclick="$('.issues_menu').removeClass('active');$(this).addClass('active');">Raised <span class="badge">{{count($my_issues)}}</span></a>
                    <a href="#currently_handling" class="list-group-item issues_menu" onclick="$('.issues_menu').removeClass('active');$(this).addClass('active');">Currently Handling <span class="badge">{{count($handling_issues)}}</span></a>
                    <a href="#solved_by_me" class="list-group-item issues_menu" onclick="$('.issues_menu').removeClass('active');$(this).addClass('active');">Solved by me <span class="badge">{{count($solved_issues)}}</span></a>
                </div>

            </div>
        </div>
    </div>


@endsection


@section('styles')

@endsection


@section('scripts')




@endsection