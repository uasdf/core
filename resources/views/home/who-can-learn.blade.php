@extends('app')

@section('title')
    <title>Home - Placerange</title>
@endsection


@section('content')
    <div class="col-md-10 col-md-offset-1 white-bg">
        @if (count($errors) > 0)
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your action.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-12" style="padding-top: 12px;">
                        <div class="media">
                            <div class="media-left">
                                <a href="/user/{{$user->username}}">
                                    <img class="media-object" src="<?=user_avatar($user->image_name)?>" alt="...">
                                </a>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">{{$user->first_name.' '.$user->last_name}}</h4>
                                <a class="small" href="/upload-image">(Change image)</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h3>Select Skill</h3>
                        <form class="form-horizontal" id="learn" role="form" method="POST" onchange="update_suggestions();">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-3">
                                    <?php $i = 0;
                                    foreach($skills as $s){
                                    $i++;?>
                                    <label class="radio"><input type="radio" onclick="$('#level').val({{$s->level}});" name="skill" <?=($i == 1 ? 'checked="checked"' : '')?> value="{{$s->tag}}" > {{$s->tag}}</label>
                                    <?php }?>

                                </div>
                            </div>
                            <input id="level" type="hidden" name="level" value="1">
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6 grey-left" style="padding-left: 0;">
                <div class="col-md-12 ">


                    <div class="row">
                        <div class="col-md-12" id="suggestion">

                        <?php if($teachArray){?>

                            <h4 class="grey-bottom">People who wants to learn <?=$teachArray[0]?> near you</h4>
                            <?php 
                            $i = 0;
                            foreach($places as $p){
                            ?>
                            <h4><span class="glyphicon glyphicon-map-marker"></span> {{place_min($p, true)}}</h4>
                            <?php if($learn_suggestions[$i] != null){?>

                            <?php foreach($learn_suggestions[$i] as $ts){
                            $usr = User::find($ts->uID)?>
                            <a href="/user/{{$usr->username}}" class="col-md-12 user_list">
                                <img src="<?=user_avatar($usr->image_name)?>" class="user_image">
                                <span class="user_name">{{$usr->first_name.' '.$usr->last_name}}</span>
                                <span class="distance">{{round($ts->distance,2)}} miles</span>
                            </a>
                            <?php }?>
                            <?php }else{?>
                            No one found
                            <?php }
                            $i++;
                            }?>
                            <?php }else{?>

                            <p>Please add a skill to your profile to use this feature.</p>
                            <?php }?>
                        </div>
                        <div class="col-md-4">

                        </div>
                        <div class="col-md-4">

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 grey-left" style="padding-left: 0;">
            </div>
        </div>
    </div>


@endsection


@section('styles')

@endsection


@section('scripts')

    <script type="text/javascript">

        function update_suggestions(){

            $('#suggestion').html("<img style='margin:0 auto; display:block;' src='img/bar-loader.gif'>");
            $.ajax({
                url: '/home/learn-suggestions',
                type: 'get',
                data: $('form#learn').serialize(),
                success: function(data) {
                    setTimeout(function(){
                        $('#suggestion').html(data);
                    }, 1000);
                },
                error: function(xhr, textStatus, thrownError) {
                    alert('Something went to wrong.Please Try again later...');
                }

            });
        }
    </script>



@endsection