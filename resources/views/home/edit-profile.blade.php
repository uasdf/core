@extends('app')
@section('title')
    <title>Register - Placerange</title>
@endsection
@section('content')
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit profile</div>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="/profile/edit">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="col-md-4 control-label">First name</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="first_name" value="{{$user->first_name}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Last name</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="last_name" value="{{$user->last_name}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Phone</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="phone" value="{{$user->phone}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Would you like to receive email newsletter?</label>
                                <div class="col-md-6">
                                    <label class="radio-inline" >
                                        <input type="radio" name="newsletter" <?=($user->newsletter ? 'checked="checked"' : '')?> value="1"> Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="newsletter" <?=($user->newsletter ? '' : 'checked="checked"')?> value="0"> No
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-success btn-block">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
@endsection
