@extends('app')
@section('title')
    <title>Address - Placerange</title>
@endsection
@section('content')
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit location</div>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" enctype='multipart/form-data' action="/profile/edit-location/{{$address->id}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="user_id" value="{{$uid }}">
                            <input type="hidden" name="id" value="{{ $address->id }}">
                            <div class="form-group" style="display: <?=($address->type == 'work' ? 'block' : 'none')?>">
                                <label class="col-md-4 control-label">Position</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="position" value="{{ $address->position }}">
                                </div>
                            </div>

                            <div class="form-group" style="display: <?=($address->type == 'education' ? 'block' : 'none')?>">
                                <label class="col-md-4 control-label">Subject</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="subject" value="{{ $address->subject }}">
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-md-4 control-label">Postcode</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="postcode" value="{{ $address->postcode }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Name of the place/building</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" value="{{ $address->name }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-success btn-block">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
@endsection
