@extends('app')

@section('title')
    <title>Messages - Placerange</title>
@endsection


@section('content')
    <div class="col-md-10 col-md-offset-1 white-bg" style="min-height: 80vh">

        @if (count($errors) > 0)
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your action.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1><span class='glyphicon glyphicon-envelope'></span> &nbsp;Messages</h1>
                </div>
            </div>

            <div class="col-md-3 visible-xs-block" id="user_list">
                <div class="panel panel-success">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="margin-bottom: 0;">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <span class="panel-title">
                                <a id="con_toggle" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                   Change Conversations
                                </a>
                            </span>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="search" id="search2" value="" class="form-control" placeholder="Search conversation">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" style="margin-top: 20px;" id="searchable-container2">
                                        <?php 
                                        $flag=0;
                                        foreach($threads  as $th){
                                        $flag ++;
                                        ?>
                                        <div class="media user_list collapsed {{($flag ==1 ? 'well' : '')}} {{$unread[$th->id] ? ' unread' : ''}}" onclick="load_messages({{$users[$th->id]->id}});$('#to_user').val({{$users[$th->id]->id}});$('.user_list').removeClass('well');$(this).addClass('well');" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <div class="media-left">
                                                <a href="/user/{{$users[$th->id]->username}}">
                                                    <img class="media-object small" src="<?=user_avatar($users[$th->id]->image_name)?>" alt="...">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <strong class="media-heading">{{$users[$th->id]->first_name.' '.$users[$th->id]->last_name}}</strong>
                                            </div>
                                        </div>
                                        <?php 
                                        }?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-3 hidden-xs" id="user_list" >
                <div class="row" style="padding-bottom:20px;">
                    <div class="col-md-12">
                        <input type="search" id="search" value="" class="form-control" placeholder="Search conversation">
                    </div>
                </div>
                <div class="row" style="max-height:60vh; overflow: scroll; padding-bottom:20px;">
                    <div class="col-md-12 thread_list" id="searchable-container">
                        <?php 
                        $flag=0;
                        foreach($threads  as $th){
                        $flag ++;
                        ?>
                        <div class="media user_list {{($flag ==1 ? 'well' : '')}} {{$unread[$th->id] ? ' unread' : ''}}" onclick="load_messages({{$users[$th->id]->id}});$('#to_user').val({{$users[$th->id]->id}});$('.user_list').removeClass('well');$(this).addClass('well');$(this).removeClass('unread');">
                            <div class="media-left">
                                <a href="/user/{{$users[$th->id]->username}}">
                                    <img class="media-object small" src="<?=user_avatar($users[$th->id]->image_name)?>" alt="...">
                                </a>
                            </div>
                            <div class="media-body">
                                <strong class="media-heading">{{$users[$th->id]->first_name.' '.$users[$th->id]->last_name}}</strong>

                            </div>
                        </div>
                        <?php 
                        }
                        if(!$flag){
                            echo 'No conversations yet.';
                        }
                        ?>
                    </div>
                </div>
            </div>


            <div class="col-md-9 grey-left"  id="message_frame" style="margin-bottom: 20px;">
                <div class="col-md-12 " style="margin-bottom: 20px;">
                    <a href="#" class="btn btn-default" onclick="new_message();return false;"><span class='glyphicon glyphicon-pencil'></span> &nbsp;Send new message</a>
                    <form class="form-horizontal" id="message_form" role="form"
                          method="POST" action="/asd" style="<?=($flag ? '':'display:none;')?>">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id="to_user" name="to_user" value="{{$start_user}}">
                        <div class="form-group">
                            <div class="col-md-12">
                                
                            </div>
                        </div>
                        <div class="input-group">
                            <textarea style="height:36px;resize:none;" class="form-control" autofocus autocomplete="off" placeholder="Message" name="message" id="message_text" required></textarea>
                            <span class="input-group-btn">
                                <button class="btn btn-info no-rounded" onclick="send_message();return false;" type="button"><span class='glyphicon glyphicon-comment'></span> &nbsp;Send</button>
                            </span>
                        </div>

                    </form>
                </div>
                <div class="col-md-12" id="messages_holder" style="max-height: 400px;overflow: scroll">
                    <?php if(isset($start_messages)){?>
                        <?php foreach($start_messages as $m){
                        if($m->from_user == $uid){
                            $sender = 1;
                        }else{
                            $sender = 0;
                        }
                        ?>
                            <?php if($sender){?>
                            <div id="{{$m->id}}" class="media row clear message-sender pull-right" style="width: 70%;">
                                <div class="media-right pull-right" >
                                    <a href="#">
                                        <img class="media-object small" src="<?=user_avatar($m->from_user_data->image_name)?>" alt="...">
                                    </a>
                                </div>
                                <div class="media-body pull-right">
                                    {{$m->message}}
                                </div>
                            </div>
                            <?php }else{?>
                            <div id="{{$m->id}}" class="media clear row message-receiver pull-left" style="width: 70%;">
                                <div class="media-left ">
                                    <a href="/user/{{$m->from_user_data->username}}">
                                        <img class="media-object small" src="<?=user_avatar($m->from_user_data->image_name)?>" alt="...">
                                    </a>
                                </div>
                                <div class="media-body">
                                    {{$m->message}}
                                </div>
                            </div>
                            <?php }?>
                        <?php }?>
                    <?php }?>

                </div>

            </div>
            <div class="col-md-3 grey-left" style="padding-left: 0;">
            </div>
        </div>
    </div>


@endsection


@section('styles')

@endsection


@section('scripts')


    <script>
        function new_message() {
            BootstrapDialog.show({
                title: 'Send new message',
                message: $('<div></div>').load('/new-message'),
                buttons: [{
                    label: 'Cancel',
                    action: function(dialogRef) {
                        dialogRef.close();
                    }
                }]
            });
        }p
    </script>
    <script src="//rawgithub.com/stidges/jquery-searchable/master/dist/jquery.searchable-1.0.0.min.js"></script>
    <script type="text/javascript">
    $(function () {
    $( '#searchable-container' ).searchable({
    searchField: '#search',
    selector: '.media',
    childSelector: '.media-heading',
    show: function( elem ) {
    elem.slideDown(100);
    },
    hide: function( elem ) {
    elem.slideUp( 100 );
    }
    })
    });

    $(function () {
        $( '#searchable-container2' ).searchable({
            searchField: '#search2',
            selector: '.media',
            childSelector: '.media-heading',
            show: function( elem ) {
                elem.slideDown(100);
            },
            hide: function( elem ) {
                elem.slideUp( 100 );
            }
        })
    });

    $(function () {
        $( '#searchable-container3' ).searchable({
            searchField: '#search3',
            selector: '.media',
            childSelector: '.media-heading',
            show: function( elem ) {
                elem.slideDown(100);
            },
            hide: function( elem ) {
                elem.slideUp( 100 );
            }
        })
    });
    </script>

    <script type="text/javascript">
        var send_message_flag;
        function load_messages(for_user){

            $('#messages_holder').html("<img style='margin:0 auto; display:block;' src='img/bar-loader.gif'>");
            $.ajax({
                url: '/load-messages',
                type: 'post',
                data: 'for_user='+for_user,
                success: function(data) {
                    $('#messages_holder').html(data);
                },
                error: function(xhr, textStatus, thrownError) {
                    alert('Something went to wrong.Please Try again later...');
                }

            });
        }

        function send_message(){
            var message = $('#message_text').val();
            message = $.trim(message);

            if(message.length === 0){
                return;
            }
            $('#messages_holder').prepend("<img id='send_loader' style='margin:0 auto; display:block;' src='img/bar-loader.gif'>");
            if(send_message_flag === true){
                return;
            }
            send_message_flag = true;
            $.ajax({
                url:     '/ajax-post-message',
                type:    'post',
                data:    $('form#message_form').serialize(),
                success: function(data) {
                        $('#messages_holder').prepend(data);
                        $('#send_loader').remove();
                        $('#message_text').val('');
                    send_message_flag = false;
                },
                error: function(xhr, textStatus, thrownError) {
                    alert('Something went to wrong. Please refresh and try again.');
                    send_message_flag = false;
                }

            });
        }

        function send_new_message(){

            var message       = $('#message_text_new').val();
            message           = $.trim(message);
            if(message.length == = 0){
                return;
            }

            $.ajax({
                url:     '/ajax-post-message',
                type:    'post',
                data:    $('form#message_form_new').serialize(),
                success: function(data) {
                },
                error: function(xhr, textStatus, thrownError) {
                    alert('Something went to wrong. Please refresh and try again.');
                }

            });
        }

        // set interval
        var tid = setInterval(message_autoload, 5000);
        function message_autoload() {
            var count = parseInt($('#messages_holder div:first-child').attr('id'));
            $.ajax({
                url: '/message-autoload',
                type: 'post',
                data: $('form#message_form').serialize()+'&count='+count,
                success: function(data) {
                        $('#messages_holder').prepend(data);
                        console.log(data);
                },
                error: function(xhr, textStatus, thrownError) {
                    alert('Something went to wrong. Please refresh and try again.');
                }

            });
        }
        function abortTimer() { // to be called when you want to stop the timer
            clearInterval(tid);
        }
    </script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        });
    </script>




@endsection