@extends('app')

@section('title')
    <title>Programs - Placerange</title>
@endsection


@section('content')
    <div class="col-md-10 col-md-offset-1 white-bg">
        @if (count($errors) > 0)
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your action.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-3" style="padding-top: 12px;">
                    <div class="media" style="margin-bottom: 20px;">
                        <div class="media-left">
                            <a href="/user/{{$user->username}}">
                                <img class="media-object" src="<?=user_avatar($user->image_name)?>" alt="...">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">{{$user->first_name.' '.$user->last_name}}</h4>
                        </div>
                    </div>
                <div class="row">
                </div>
            </div>
            <div class="col-md-6 grey-left grey-right">
                <div class="col-md-12 ">

                    <div class="row">
                        <h2 class="grey-bottom text-center">Programs <a class="small" href="/create-program">Create +</a></h2>

                        <div class="col-md-12 grey-bottom">
                            <h3 id="organizing">Organizing</h3>
                            <?php if(!$organising_workshops->isEmpty()){?>
                            <div class="col-md-12">
                                <div class="list-group">
                                    <?php foreach($organising_workshops as $odw){?>
                                    <div class="list-group">
                                        <a href="/program/{{$odw->slug}}" class="list-group-item list-group-item-info">
                                            <h4 class="list-group-item-heading"><strong>{{$odw->title}}</strong></h4>
                                            <div class="list-group-item-text">{!!$odw->summary!!}</div>
                                        </a>
                                    </div>

                                    <?php }?>
                                </div>
                            </div>
                            <?php }else{?>
                            No issues yet
                            <?php }?>
                        </div>
                        <div class="col-md-12 grey-bottom">
                            <h3 id="attending">Attending</h3>
                            <?php if($attending_workshops){?>
                            <div class="col-md-12">
                                <div class="list-group">
                                    <?php foreach($attending_workshops as $agw){?>
                                    <div class="list-group">
                                        <a href="/program/{{$agw->slug}}" class="list-group-item list-group-item-info">
                                            <h4 class="list-group-item-heading"><strong>{{$agw->title}}</strong></h4>
                                            <div class="list-group-item-text">{!!$agw->summary!!}</div>
                                        </a>
                                    </div>
                                    <?php }?>
                                </div>
                            </div>
                            <?php }else{?>
                            None yet
                            <?php }?>
                        </div>
                        <div class="col-md-12 grey-bottom">
                            <h3 id="organized">Organized</h3>
                            <?php if(!$organised_workshops->isEmpty()){?>
                            <div class="col-md-12">
                                <div class="list-group">
                                    <?php foreach($organised_workshops as $odw){?>
                                    <div class="list-group">
                                        <a href="/program/{{$odw->slug}}" class="list-group-item list-group-item-success">
                                            <h4 class="list-group-item-heading"><strong>{{$odw->title}}</strong></h4>
                                            <div class="list-group-item-text">{!!$odw->summary!!}</div>
                                        </a>
                                    </div>
                                    <?php }?>
                                </div>
                            </div>
                            <?php }else{?>
                            None yet
                            <?php }?>
                        </div>
                        <div class="col-md-12">
                            <h3 id="attended">Attended</h3>
                            <?php if($attended_workshops){?>
                            <div class="col-md-12">
                                <div class="list-group">
                                    <?php foreach($attended_workshops as $adw){?>
                                    <div class="list-group">
                                        <a href="/program/{{$adw->slug}}" class="list-group-item list-group-item-success">
                                            <h4 class="list-group-item-heading"><strong>{{$adw->title}}</strong></h4>
                                            <div class="list-group-item-text">{!!$adw->summary!!}</div>
                                        </a>
                                    </div>
                                    <?php }?>
                                </div>
                            </div>
                            <?php }else{?>
                            None yet
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 grey-left" style="position:static;">
                <div class="list-group" style="position: fixed;width: 265px;margin-top:10px">
                    <a href="#organizing" class="list-group-item active workshops_menu" onclick="$('.workshops_menu').removeClass('active');$(this).addClass('active');">Organizing <span class="badge">{{count($organising_workshops)}}</span></a>
                    <a href="#attending" class="list-group-item workshops_menu" onclick="$('.workshops_menu').removeClass('active');$(this).addClass('active');">Attending <span class="badge">{{count($attending_workshops)}}</span></a>
                    <a href="#organized" class="list-group-item workshops_menu" onclick="$('.workshops_menu').removeClass('active');$(this).addClass('active');">Organized <span class="badge">{{count($organised_workshops)}}</span></a>
                    <a href="#attended" class="list-group-item workshops_menu" onclick="$('.workshops_menu').removeClass('active');$(this).addClass('active');">Attended <span class="badge">{{count($attended_workshops)}}</span></a>

                </div>

            </div>
        </div>
    </div>


@endsection


@section('styles')

@endsection


@section('scripts')




@endsection