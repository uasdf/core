
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body style="width: 90%;margin: 0 5%;max-width: 100%;">

<img src="http://www.uasplacerangedf.com/img/logo-main.png" style="height:70px; width:70px">

<p>Hello <strong>{{$user->first_name.' '.$user->last_name}}</strong></p>

<div>

    <strong>Issues created near you</strong>
    <?php if(isset($issues[0])){?>
        <?php foreach($issues as $ir){?>
            <div style="width:100%;border-bottom:1px solid #eee;display:block;float:left;clear:both;">
                <p>{{$ir->user->first_name.' '.$ir->user->last_name}} has raised an issue <a href="http://www.placerange.com/issue/{{$ir->slug}}">{{$ir->title}}</a></p>
                <p style="clear: both;"><?php  $skill = '';
                    foreach ($ir->tags() as $s) {
                        $skill .= $s->tag . ', ';
                    }
                    echo rtrim($skill, ', '); ?>
                </p>
            </div>
        <?php }?>
    <?php }else{?>
    <p>Currently no issues are created near you.</p>
    <?php }?>

    <strong>Programs created near you</strong>
    <?php if(isset($workshops[0])){?>
    <?php foreach($workshops as $wr){?>
    <div style="width:100%;border-bottom:1px solid #eee;display:block;float:left;clear:both;">
        <p>{{$wr->user->first_name.' '.$wr->user->last_name}} is organising a program<a href="http://www.placerange.com/workshop/{{$wr->slug}}">{{$wr->title}}</a></p>
        <p style="clear: both;"><?php  $skill = '';
            foreach ($wr->tags() as $s) {
                $skill .= $s->tag . ', ';
            }
            echo rtrim($skill, ', '); ?>
        </p>
    </div>
    <?php }?>

    <?php }else{?>
    <p>Currently no programs are organized near you.</p>
    <?php }?>

    <strong>New members near you</strong>
    <?php if(isset($users[0])){?>
        <?php foreach($users as $ur){?>
        <div style="width:100%;border-bottom:1px solid #eee;display:block;float:left;clear:both;margin-bottom: 5px;">
            <p><div style="height: 50px;width:50px;float:left;display:block;margin-right: 10px;"><img src="http://www.placerange.com/uploads/users/icon/{{($ur->image_name ? $ur->image_name : 'default.png')}}"></div> <a href="http://www.placerange.com/user/{{$ur->username}}">{{$ur->first_name .' '.$ur->last_name}}</a><br>
                <?php  $skill = '';
                foreach ($ur->skills() as $s) {
                    $skill .= $s->tag . ', ';
                }
                echo rtrim($skill, ', '); ?>
            </p>
        </div>
        <?php }?>
    <?php }else{?>
    <p>No new users near you.</p>
    <?php }?>


</div>

<div style="text-align: center; display: block; width: 100%;">
    Placerange 2015<br>
    Click here to <a href="http://www.placerange.com/unsubscribe/{{$user->email}}">unsubscribe</a>
</div>

</body>

</html>
