<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
<title>Welcome to Placerange</title>

<style type="text/css">

div, p, a, li, td { -webkit-text-size-adjust:none; }

*{
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale;
}

.ReadMsgBody
{width: 100%; background-color: #ffffff;}
.ExternalClass
{width: 100%; background-color: #ffffff;}
body{width: 100%; height: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;}
html{width: 100%; background-color: #ffffff;}

@font-face {
    font-family: 'proxima_novalight';src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-light-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-light-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-light-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-light-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}

@font-face {
    font-family: 'proxima_nova_rgregular'; src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-regular-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-regular-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-regular-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-regular-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}

@font-face {
    font-family: 'proxima_novasemibold';src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-semibold-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-semibold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-semibold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-semibold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
    
@font-face {
    font-family: 'proxima_nova_rgbold';src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-bold-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-bold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-bold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-bold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
    
@font-face {
    font-family: 'proxima_novablack';src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-black-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-black-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-black-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-black-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
    
@font-face {font-family: 'proxima_novathin';src: url('http://rocketway.net/themebuilder/template/templates/mason/font/proximanova-thin-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/mason/font/proximanova-thin-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/mason/font/proximanova-thin-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/mason/font/proximanova-thin-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}

p {padding: 0!important; margin-top: 0!important; margin-right: 0!important; margin-bottom: 0!important; margin-left: 0!important; }

.hover:hover {opacity:0.85;filter:alpha(opacity=85);}

.image77 img {width: 77px; height: auto;}
.avatar125 img {width: 125px; height: auto;}
.icon61 img {width: 61px; height: auto;}
.logo img {width: 75px; height: auto;}
.icon18 img {width: 18px; height: auto;}

</style>

<!-- @media only screen and (max-width: 640px) 
           {*/
           -->
<style type="text/css"> @media only screen and (max-width: 640px){
        body{width:auto!important;}
        table[class=full2] {width: 100%!important; clear: both; }
        table[class=mobile2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
        table[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
        td[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
        td[class=pad15] {width: 100%!important; padding-left: 15px; padding-right: 15px; clear: both;}
        
} </style>
<!--

@media only screen and (max-width: 479px) 
           {
           -->
<style type="text/css"> @media only screen and (max-width: 479px){
        body{width:auto!important;}
        table[class=full2] {width: 100%!important; clear: both; }
        table[class=mobile2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
        table[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
        td[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
        table[class=full] {width: 100%!important; clear: both; }
        table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
        table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
        td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
        td[class=pad15] {width: 100%!important; padding-left: 15px; padding-right: 15px; clear: both;}
        .erase {display: none;}
                
        }
} </style>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<!-- Notification 2  -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full"  bgcolor="#2a2a2a"style="background-color: rgb(42, 42, 42); position: relative; z-index: 0;">
    <tr>
        <td bgcolor="#2a2a2a"align="center" style="background-color: rgb(42, 42, 42);">
        
            
            <!-- Mobile Wrapper -->
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#2a2a2a"style="background-color: rgb(42, 42, 42);">
                <tr>
                    <td width="100%" align="center">
                    
                        
                        <div class="sortable_inner ui-sortable">
                        <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" object="drag-module-small" bgcolor="#2a2a2a"style="background-color: rgb(42, 42, 42);">
                            <tr>
                                <td align="center" width="352" valign="middle" bgcolor="#2a2a2a"style="background-color: rgb(42, 42, 42);">
                                
                                    <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse: collapse; background-color: rgb(42, 42, 42);" class="fullCenter" bgcolor="#2a2a2a">
                                        <tr>
                                            <td width="100%" height="30"></td>
                                        </tr>
                                    </table>
                                                                    
                                </td>
                            </tr>
                        </table>
                        <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" object="drag-module-small" bgcolor="#2a2a2a"style="background-color: rgb(42, 42, 42);">
                            <tr>
                                <td align="center" width="352" valign="middle">
                                
                                    <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                        <tr>
                                            <td width="100%" height="20"></td>
                                        </tr>
                                    </table>
                                                                    
                                </td>
                            </tr>
                        </table>
                        
                        <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" object="drag-module-small">
                            <tr>
                                <td align="center" width="352" valign="middle">
                                    
                                    <!-- Header Text --> 
                                    <table width="300" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                        <tr>
                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 22px; color: #ffffff; line-height: 32px; font-weight: 100;"class="fullCenter"   cu-identify="element_08751253872178495">
                                                <!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;"><!--<![endif]-->Hello,</span></td>
                                        </tr>
                                    </table>                            
                                </td>
                            </tr>
                        </table>
                        
                        <div style="display: none" id="element_007249019760638475"></div></p></div><table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" object="drag-module-small" style="position: relative; z-index: 0;">
                            <tr>
                                <td align="center" width="352" valign="middle">
                                
                                    <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                        <tr>
                                            <td width="100%" height="20"></td>
                                        </tr>
                                    </table>
                                                                    
                                </td>
                            </tr>
                        </table>
                        
                        </div>
                    </td>
                </tr>
            </table>
            
            <table width="392" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                <tr>
                    <td align="center" width="20" valign="middle" bgcolor="#2a2a2a"style="background-color: rgb(42, 42, 42);"></td>
                    <td align="center" width="352" valign="middle">
            
                        <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" style="border-top-right-radius: 5px; border-top-left-radius: 5px;">
                            <tr>
                                <td align="center" width="352" valign="middle" bgcolor="#22a7f0" style="-webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;">
                                
                                    <div class="sortable_inner ui-sortable">
                        
                                    <div style="display: none;" id="element_0587781980400905"></div></p></div><table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140" style="border-top-right-radius: 5px; border-top-left-radius: 5px; background-color: rgb(232, 81, 64);"object="drag-module-small">
                                        <tr>
                                            <td align="center" width="352" valign="middle">
                                                
                                                <table width="300" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                    <tr>
                                                        <td width="100%" height="50"></td>
                                                    </tr>
                                                </table>                            
                                            </td>
                                        </tr>
                                    </table>
                                    
                                    
                                    
                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140"object="drag-module-small" style="background-color: rgb(232, 81, 64); position: relative; z-index: 0;">
                                        <tr>
                                            <td align="center" width="352" valign="middle">
                                            
                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                    <tr>
                                                        <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 15px; color: #ffffff; line-height: 24px;"class="fullCenter"   cu-identify="element_07202445385046303"><span style="text-align:left;float:left;font-family: proxima_nova_rgregular, Helvetica; font-size: 22px;">{{$from_user->first_name.' '.$from_user->last_name}} has invited you to join Placerange.</span><!--<![endif]--></td>
                                                    </tr>
                                                </table>                            
                                            </td>
                                        </tr>
                                    </table><table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140"object="drag-module-small" style="background-color: rgb(232, 81, 64); position: relative; z-index: 0;">
                                        <tr>
                                            <td align="center" width="352" valign="middle">
                                            
                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                    <tr>
                                                        <td width="100%" height="40"></td>
                                                    </tr>
                                                </table>                            
                                            </td>
                                        </tr>
                                    </table><table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140"object="drag-module-small" style="background-color: rgb(232, 81, 64); position: relative; z-index: 0;">
                                        <tr>
                                            <td align="center" width="352" valign="middle" class="image77">
                                            
                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                    <tr>
                                                        <td width="100%" align="center" style="text-align: center;"><span ><img src="http://www.placerange.com/img/logo-main.jpg" width="77" alt="" border="0"  cu-identify="element_0013173321960493922"></span></td>
                                                    </tr>
                                                </table>                            
                                            </td>
                                        </tr>
                                    </table><div style="display: none;" id="element_02743773739784956"></div></p></div>
                                    
                                    <div style="display: none;" id="element_0611181351589039"></div></p></div><table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140"object="drag-module-small" style="background-color: rgb(232, 81, 64);">
                                        <tr>
                                            <td align="center" width="352" valign="middle">
                                            
                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                    
                                                    <tr>
                                                        <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 48px; color: #ffffff; line-height: 44px; font-weight: bold;"class="fullCenter"   cu-identify="element_09813562023919076"><div style="text-align: center;"><span style="font-family: proxima_nova_rgbold, Helvetica;color:#000;">Placerange</span></div></td>
                                                    </tr>
                                                </table>                            
                                            </td>
                                        </tr>
                                    </table>
                                    
                                    <div style="display: none;" id="element_05025359550490975"></div></p></div>
                                    
                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140"object="drag-module-small" style="background-color: rgb(232, 81, 64); position: relative; z-index: 0;">
                                        <tr>
                                            <td align="center" width="352" valign="middle">
                                            
                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                    <tr>
                                                        <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 15px; color: #ffffff; line-height: 24px;"class="fullCenter"   cu-identify="element_08513456876389682"><span style="color:#000;">Exposing all the skills, programs and issues belonging to a place.</span><!--<![endif]--></td>
                                                    </tr>
                                                </table>                            
                                            </td>
                                        </tr>
                                    </table><table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140"object="drag-module-small" style="background-color: rgb(232, 81, 64); position: relative; z-index: 0;">
                                        <tr>
                                            <td align="center" width="352" valign="middle">
                                            
                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                    <tr>
                                                        <td width="100%" height="40"></td>
                                                    </tr>
                                                </table>                            
                                            </td>
                                        </tr>
                                    </table><div style="display: none;" id="element_023593043303117156"></div></p></div>
                                    
                                    
                                    
                                    <div style="display: none;" id="element_00035157150123268366"></div></p></div><table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140"object="drag-module-small" style="background-color: rgb(232, 81, 64); position: relative; z-index: 0;">
                                        <tr>
                                            <td align="center" width="352" valign="middle">
                                            
                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                    <tr>
                                                        <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 15px; color: #ffffff; line-height: 24px;"class="fullCenter"   cu-identify="element_06200403347611427"><span style="text-align: center; float: left; font-size: large; border-top:4px dashed #fff;border-bottom:4px dashed #fff; padding:10px 0;"><span style="font-family: Verdana;">We believe that there's a 99% chance you can find someone near you who knows what you want to learn!</span></span><!--<![endif]--></td>
                                                    </tr>
                                                </table>                            
                                            </td>
                                        </tr>
                                    </table><table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140"object="drag-module-small" style="background-color: rgb(232, 81, 64); position: relative; z-index: 0;">
                                        <tr>
                                            <td align="center" width="352" valign="middle">
                                            
                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                    <tr>
                                                        <td width="100%" height="40"></td>
                                                    </tr>
                                                </table>                            
                                            </td>
                                        </tr>
                                    </table><table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140"object="drag-module-small" style="background-color: rgb(232, 81, 64); position: relative; z-index: 0;">
                                        <tr>
                                            <td align="center" width="352" valign="middle">
                                            
                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                    <tr>
                                                        <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 15px; color: #ffffff; line-height: 24px;"class="fullCenter"   cu-identify="element_09452243591658771">
                                                        <div style="color:#000;">
                                                        <span style="text-align: center;">Are you good at Programming? Dancing? Cricket? Mathematics?&nbsp;</span>
                                                        <br style="box-sizing: border-box; text-align: center;">
                                                        <span style="text-align: center;">or something else?&nbsp;</span>
                                                        <br style="box-sizing: border-box; text-align: center;">
                                                        <br style="box-sizing: border-box; text-align: center;">
                                                        <span style="text-align: center;">Let people near you know about your skills and contact you if they want to learn what you know&nbsp;</span>
                                                        <br style="box-sizing: border-box; text-align: center;">
                                                        <span style="text-align: center;">or&nbsp;</span>
                                                        <br style="box-sizing: border-box; text-align: center;">
                                                        <span style="text-align: center;">Find people near you with mutual interests&nbsp;</span>
                                                        </div>&nbsp;<br>
<img src="http://www.placerange.com/img/home/issues.jpg" style="max-width: 100%;"></td>
                                                    </tr>
                                                </table>                            
                                            </td>
                                        </tr>
                                    </table><table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140"object="drag-module-small" style="background-color: rgb(232, 81, 64); position: relative; z-index: 0;">
                                        <tr>
                                            <td align="center" width="352" valign="middle">
                                            
                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                    <tr>
                                                        <td width="100%" height="40"></td>
                                                    </tr>
                                                </table>                            
                                            </td>
                                        </tr>
                                    </table><table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140"object="drag-module-small" style="background-color: rgb(232, 81, 64); position: relative; z-index: 0;">
                                        <tr>
                                            <td align="center" width="352" valign="middle">
                                            
                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                    <tr>
                                                        <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 15px; color: #ffffff; line-height: 24px;"class="fullCenter"   cu-identify="element_021911054337397218">
                                                        <div style="color:#000;">
                                                            <span style="text-align: center;">Want to learn piano? code Java? Badminton?&nbsp;</span>
                                                            <span style="text-align: center;">or something else?&nbsp;</span>
                                                            <br style="box-sizing: border-box; text-align: center;">
                                                            <br style="box-sizing: border-box; text-align: center;">
                                                            <span style="text-align: center;">Find people near you</span>
                                                            <br style="box-sizing: border-box; text-align: center;">
                                                            <span style="text-align: center;">who can teach you</span>
                                                            <br style="box-sizing: border-box; text-align: center;">
                                                            <span style="text-align: center;">or&nbsp;</span>
                                                            <br style="box-sizing: border-box; text-align: center;">
                                                            <span style="text-align: center;">Find and attend&nbsp;</span>
                                                            <br style="box-sizing: border-box; text-align: center;">
                                                            <span style="text-align: center;">programs or workshops&nbsp;</span>
                                                            <br style="box-sizing: border-box; text-align: center;">
                                                            <span style="text-align: center;">near you that are based on what you want to learn&nbsp;</span>
                                                        </div>
                                                        <br>
                                                        <img src="http://www.placerange.com/img/home/place.jpg" style="max-width: 100%;"></td>
                                                    </tr>
                                                </table>                            
                                            </td>
                                        </tr>
                                    </table><table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140"object="drag-module-small" style="background-color: rgb(232, 81, 64); position: relative; z-index: 0;">
                                        <tr>
                                            <td align="center" width="352" valign="middle">
                                            
                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                    <tr>
                                                        <td width="100%" height="40"></td>
                                                    </tr>
                                                </table>                            
                                            </td>
                                        </tr>
                                    </table>

                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140"object="drag-module-small" style="background-color: rgb(232, 81, 64); position: relative; z-index: 0;">
                                        <tr>
                                            <td align="center" width="352" valign="middle">
                                            
                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                    <tr>
                                                        <td width="100%" height="40"></td>
                                                    </tr>
                                                </table>                            
                                            </td>
                                        </tr>
                                    </table><table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140"object="drag-module-small" style="background-color: rgb(232, 81, 64);">
                                        <tr>
                                            <td align="center" width="352" valign="middle">
                                            
                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                    <!----------------- Button Center ----------------->
                                                    <tr>
                                                        <td align="center">
                                                            <table border="0" cellpadding="0" cellspacing="0" align="center"> 
                                                                <tr> 
                                                                    <td align="center" height="45"bgcolor="#ffffff" style="border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px; border-bottom-left-radius: 5px; padding-left: 30px; padding-right: 30px; font-weight: bold; font-family: Helvetica, Arial, sans-serif; color: rgb(42, 42, 42); text-transform: uppercase; background-color: rgb(255, 255, 255);">
                                                                        <!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgbold', Helvetica; font-weight: normal;"><!--<![endif]-->
                                                                            <a href="http://{{$_SERVER['HTTP_HOST']}}" style="color: #2a2a2a; font-size:15px; text-decoration: none; line-height:34px; width:100%;" cu-identify="element_08933610711246729">Join Now!</a>
                                                                        <!--[if !mso]><!--></span><!--<![endif]-->
                                                                    </td> 
                                                                </tr> 
                                                            </table> 
                                                        </td>
                                                    </tr>
                                                    <!----------------- End Button Center ----------------->
                                                </table>                            
                                            </td>
                                        </tr>
                                    </table>
                                    
                                    <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140"object="drag-module-small" style="background-color: rgb(232, 81, 64);">
                                        <tr>
                                            <td align="center" width="352" valign="middle">
                                            
                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                    <tr>
                                                        <td width="100%" height="35"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    
                                    <div style="display: none;" id="element_06999863788951188"></div></p></div>
                                    
                                    <div style="display: none;" id="element_05455660654697567"></div></p></div><div style="display: none;" id="element_05617034311871976"></div></p></div>
                                    
                                    </div>
                                    
                                </td>
                            </tr>
                        </table>
                        
                    </td>
                    <td align="center" width="20" valign="middle" bgcolor="#2a2a2a"style="background-color: rgb(42, 42, 42);"></td>
                </tr>
            </table>
            
            <!-- Mobile Wrapper -->
            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#2a2a2a"style="background-color: rgb(42, 42, 42);">
                <tr>
                    <td width="100%" align="center" bgcolor="#2a2a2a"style="background-color: rgb(42, 42, 42);">
                        
                        <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" object="drag-module-small">
                            <tr>
                                <td align="center" width="352" valign="middle">
                                
                                    <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                        <tr>
                                            <td width="100%" height="30"></td>
                                        </tr>
                                    </table>
                                                                    
                                </td>
                            </tr>
                        </table><table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" object="drag-module-small">
                            <tr>
                                <td align="center" width="352" valign="middle">
                                
                                    <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                        <tr>
                                            <td width="100%" height="30"></td>
                                        </tr>
                                    </table>
                                                                    
                                </td>
                            </tr>
                        </table>
                        <div style="display: none" id="element_04515317394398153"></div></p></div>
                        <div style="display: none" id="element_09808721703011543"></div></p></div>
                        
            
                    </td>
                </tr>
            </table>
            
        </div>
        </td>
    </tr>
</table><!-- End Notification 2 -->

</body></html>  <style>body{ background: none !important; } </style>