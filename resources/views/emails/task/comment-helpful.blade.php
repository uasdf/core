		
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
<title>Placerange - New Comment</title>

<style type="text/css">

div, p, a, li, td { -webkit-text-size-adjust:none; }

*{
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale;
}

.ReadMsgBody
{width: 100%; background-color: #fff;}
.ExternalClass
{width: 100%; background-color: #fff;}
body{width: 100%; height: 100%; background-color: #fff; margin:0; padding:0; -webkit-font-smoothing: antialiased;}
html{width: 100%; background-color: #fff;}

@font-face {
    font-family: 'proxima_novalight';src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-light-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-light-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-light-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-light-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}

@font-face {
    font-family: 'proxima_nova_rgregular'; src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-regular-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-regular-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-regular-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-regular-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}

@font-face {
    font-family: 'proxima_novasemibold';src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-semibold-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-semibold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-semibold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-semibold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
    
@font-face {
	font-family: 'proxima_nova_rgbold';src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-bold-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-bold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-bold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-bold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
	
@font-face {
    font-family: 'proxima_novablack';src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-black-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-black-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-black-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/titan/font/proximanova-black-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
    
@font-face {font-family: 'proxima_novathin';src: url('http://rocketway.net/themebuilder/template/templates/mason/font/proximanova-thin-webfont.eot');src: url('http://rocketway.net/themebuilder/template/templates/mason/font/proximanova-thin-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/template/templates/mason/font/proximanova-thin-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/template/templates/mason/font/proximanova-thin-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}

p {padding: 0!important; margin-top: 0!important; margin-right: 0!important; margin-bottom: 0!important; margin-left: 0!important; }

.hover:hover {opacity:0.85;filter:alpha(opacity=85);}

.image77 img {width: 77px; height: auto;}
.avatar125 img {width: 125px; height: auto;}
.icon61 img {width: 61px; height: auto;}
.logo img {width: 75px; height: auto;}
.icon18 img {width: 18px; height: auto;}

@import url("http://weloveiconfonts.com/api/?family=entypo");
* {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  font-family: 'proxima_novathin', Helvetica; 
}

body {
  background: #fff;
}

.wrapper {

  text-align: center;
  display: block;
}

.social {
  height: 30px;
  width:30px;
  float:left;
  display: inline-block;
  margin-right: 7px;
  cursor: pointer;
}

.social img{
	height: 100%;
	width:100%;
}


</style>

<!-- @media only screen and (max-width: 640px) 
		   {*/
		   -->
<style type="text/css"> @media only screen and (max-width: 640px){
		body{width:auto!important;}
		table[class=full2] {width: 100%!important; clear: both; }
		table[class=mobile2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
		table[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=pad15] {width: 100%!important; padding-left: 15px; padding-right: 15px; clear: both;}
		
} </style>
<!--

@media only screen and (max-width: 479px) 
		   {
		   -->
<style type="text/css"> @media only screen and (max-width: 479px){
		body{width:auto!important;}
		table[class=full2] {width: 100%!important; clear: both; }
		table[class=mobile2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
		table[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
		table[class=full] {width: 100%!important; clear: both; }
		table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
		table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=pad15] {width: 100%!important; padding-left: 15px; padding-right: 15px; clear: both;}
		.erase {display: none;}
				
		}
} </style>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background-color: #fff;">

<!-- Notification 4  -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full"  bgcolor="#fff" >
	<tr>
		<td align="center" id="not4">
		
			
			<!-- Mobile Wrapper -->
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2">
				<tr>
					<td width="100%" align="center">
						
						<div class="sortable_inner ui-sortable">
						<!-- Space -->
						<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
							<tr>
								<td width="352" height="40"></td>
							</tr>
						</table><!-- End Space -->
						
						<!-- Space -->
						<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
							<tr>
								<td width="352" height="40"></td>
							</tr>
						</table><!-- End Space -->
						
						<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" object="drag-module-small">
							<tr>
								<td width="352" valign="middle" align="center">
									
									<!-- Text --> 
									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
										<tr>
											<td valign="middle" width="352" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 67px; color: #000; line-height: 72px; font-weight: 100; word-break:break-all;"class="fullCenter"  >
												<!--[if !mso]><!--><span style="font-family: 'proxima_novathin', Helvetica; font-weight: normal;"><!--<![endif]-->Hello!<!--[if !mso]><!--></span><!--<![endif]-->
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						
						
						<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" object="drag-module-small">
							<tr>
								<td width="352" valign="middle" align="center">
								
									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
										<tr>
											<td valign="middle" width="100%" style="text-align: center; font-size: 19px; color: #000; line-height: 26px; font-weight: bold; text-transform: uppercase;"class="fullCenter"   cu-identify="element_005531530873849988">
												<!--[if !mso]><!--><span style="font-family: 'proxima_nova', Helvetica; font-weight: normal;"><!--<![endif]-->Your comment was marked as helpful!<!--[if !mso]><!--></span><!--<![endif]--></td>
										</tr>
										<tr>
											<td width="100%" height="20"></td>
										</tr>
									</table>							
								</td>
							</tr>
						</table>
						
					</div>
					</td>
				</tr>
			</table>
			
			<table width="392" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
				<tr>
					<td align="center" width="20" valign="middle"></td>
					<td align="center" width="352" valign="middle">
			
						<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
							<tr>
								<td align="center" width="352" valign="middle" bgcolor="#fff" style="background-color: #fff;">
								
									<div class="sortable_inner ui-sortable">
									
									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#fff"object="drag-module-small" style="background-color: #fff;">
										<tr>
											<td width="352" valign="middle" align="center">
												 
												<table width="300" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
													<tr>
														<td width="100%" height="30"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									
									<!-- Avatar -->
									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#fff"object="drag-module-small" style="background-color: #fff;">
										<tr>
											<td width="352" valign="middle" class="avatar125" align="center">
											
												<table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
													<tr>
														<td width="100%"><span ><img style="border:3px solid #fff; border-radius:100%;" src="http://{{$_SERVER['HTTP_HOST'].$sender->image_url}}" width="125" alt="" border="0" ></span></td>
													</tr>
												</table>							
											</td>
										</tr>
									</table>
											
									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#fff"object="drag-module-small" style="background-color: #fff;">
										<tr>
											<td width="352" valign="middle" align="center">
											<table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
													<tr>
														<td width="100%" height="30"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									
									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#fff"object="drag-module-small" style="background-color: #fff;">
										<tr>
											<td width="352" valign="middle" align="center">
											
												<table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
													
													<tr>
														<td valign="middle" width="100%" style="text-align: center;  font-size: 34px; color: #000; line-height: 44px; font-weight: bold;"class="fullCenter"  >
															<!--[if !mso]><!--><span><a  style="font-family: 'proxima_novalight', Helvetica; font-weight: normal;text-decoration: none;color:#000;" href="http://{{$_SERVER['HTTP_HOST']}}/#/app/user/{{$sender->username}}"><!--<![endif]-->{{$sender->name}}<!--[if !mso]><!--></a></span><!--<![endif]-->
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									
									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#fff"object="drag-module-small" style="background-color: #fff;">
										<tr>
											<td width="352" valign="middle" align="center">
											
												<table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
													<tr>
														<td width="100%" height="30"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									
									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#fff"object="drag-module-small" style="background-color: #fff;">
										<tr>
											<td width="352" valign="middle" align="center">
											
												<table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
													<tr>
														<td valign="middle" width="100%" style="text-align: center;  font-size: 14px; color: #000; line-height: 24px;"class="fullCenter"  >
															<!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;"><!--<![endif]-->Has marked your comment as helpful on the task "{{$task->title}}".<!--[if !mso]><!--></span><!--<![endif]-->
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									 
									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#fff"object="drag-module-small" style="background-color: #fff;">
										<tr>
											<td width="352" valign="middle" align="center">
											
												<table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
													<tr>
														<td width="100%" height="40"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									

									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#fff"object="drag-module-small" style="background-color: #fff;">
										<tr>
											<td width="352" valign="middle" align="center">
											
												<table width="300" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">

													<tr>
														<td align="center">
															<table border="0" cellpadding="0" cellspacing="0" align="center"> 
																<tr> 
																	<td align="center" height="45" bgcolor="#383838" style="border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px; border-bottom-left-radius: 5px; padding-left: 30px; padding-right: 30px; font-weight: bold;color: rgb(56, 56, 56); text-transform: uppercase; background-color: #383838;">
																		<!--[if !mso]><!--><span style="font-weight: normal;"><!--<![endif]-->
																			<a href="http://{{$_SERVER['HTTP_HOST']}}/#/app/task/{{$task->slug}}" style="font-family: 'proxima_nova_rgbold', Helvetica; color: #fff; font-size:15px; text-decoration: none; line-height:35px; width:100%;" cu-identify="element_05778178584296256">View Comment</a>
																		<!--[if !mso]><!--></span><!--<![endif]-->
																	</td> 
																</tr> 
															</table> 
														</td>
													</tr>

												</table>							
											</td>
										</tr>
									</table>
									
									<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#fff" object="drag-module-small" style="background-color: #fff;">
										<tr>
											<td width="352" valign="middle">
											
												<table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
													<tr>
														<td width="100%" height="40"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									
									</div>
								</td>
							</tr>
						</table>
									
					</td>
					<td align="center" width="20" valign="middle"></td>
				</tr>
			</table>
			
			<!-- Mobile Wrapper -->
			<table width="100%" border="0"cellpadding="0" cellspacing="0" align="center" class="mobile2">
				<tr>
					<td width="100%" align="center">
						
						<div class="sortable_inner ui-sortable">
			
						
						<!-- CopyRight -->
						<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
							<tr>
								<td width="352" height="40"></td>
							</tr>
						</table>
						
						<table width="352" border="0" cellpadding="0"  bgcolor="#fff"  cellspacing="0" align="center" class="full" object="drag-module-small">
							<tr>
								<td valign="middle" width="352" style="text-align: center; font-size: 13px; color: #000; line-height: 24px;" class="fullCenter"  align="center">
									<span style="font-family: 'proxima_nova_rgbold', Helvetica;display:block;text-align:center;"><a href="http://{{$_SERVER['HTTP_HOST'].'#/subscribtion/'.encrypt($receiver->email)}}" style="text-decoration: none;color:#000;">Unsubscribe</a></span>
								</td>
							</tr>
						</table>
						
						
						
						<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
							<tr>
								<td width="352" height="60"></td>
							</tr>
							<tr>
								<td width="352" height="1" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
							</tr>
						</table><!-- End CopyRight -->
						</div>
			
					</td>
				</tr>
			</table>
			
		</div>
		</td>
	</tr>
</table><!-- End Notification 4 -->
</body></html>	<style>body{ background: none !important; } </style>