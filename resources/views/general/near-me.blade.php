@extends('app')

@section('title')
    <title>Near me - Placerange</title>
@endsection


@section('content')
    <div class="col-md-10 col-md-offset-1 white-bg">
        @if (count($errors) > 0)
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your action.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1><span class='glyphicon glyphicon-search' style='font-size: 18px;'></span> &nbsp;Search</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 grey-right">
                        <div class="col-md-12">
                            <h3 class="grey-bottom">For</h3>
                            <ul class="list-group grey-top">
                                <li class="list-group-item near-me-button search_menu active" value="people"><span class="glyphicon glyphicon-user"></span> &nbsp; People</li>
                                <li class="list-group-item near-me-button search_menu " value="issue"><span class="glyphicon glyphicon-wrench"></span> &nbsp; Issues</li>
                                <li class="list-group-item near-me-button search_menu" value="workshop"><span class="glyphicon glyphicon-blackboard"></span> &nbsp; Programs</li>
                            
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <h3 class="grey-bottom">Nearby</h3>
                            <ul class="list-group grey-top">
                                <?php $i = 0;
                                foreach($places as $p){
                                $i++;
                                if($i ==1) $firstPlaceID = $p->id;
                                ?>
                                <li style="overflow: hidden; text-overflow:ellipsis;white-space: nowrap;" class="list-group-item near-me-button search_location <?=($i==1 ? 'active' : '')?>" value="{{$p->id}}"> <span class="glyphicon glyphicon-map-marker pomo"></span> &nbsp; {{place_min($p,true)}}</li>
                                <?php }?>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <h3 class="grey-bottom">Level</h3>
                            <ul class="list-group grey-top">
                                <li class="list-group-item near-me-button search_level active" value="0"><span class='glyphicon glyphicon-asterisk'></span> &nbsp; Any</li>
                                <li class="list-group-item near-me-button search_level" value="2"> <span class='glyphicon glyphicon-pawn'></span> &nbsp; Beginner</li>
                                <li class="list-group-item near-me-button search_level " value="3"><span class='glyphicon glyphicon-knight'></span> &nbsp; Intermediate</li>
                                <li class="list-group-item near-me-button search_level " value="4"><span class='glyphicon glyphicon-queen'></span> &nbsp; Expert</li>
                            </ul>
                        </div>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form-horizontal" id="search_form" role="form" method="POST" action="/search">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" id="search_data_for" name="search_for" value="people">
                                <input type="hidden" id="search_data_level" name="level" value="0">
                                <input type="hidden" id="search_data_location" name="location" value="<?=$firstPlaceID?>">


                                <div class="form-group">
                                    <div class="col-md-10">
                                        <input type="text" class="form-control input-lg skill_fill" data-role="tagsinput" id="search_string" name="search_string" placeholder="Enter a skill">

                                    </div>
                                    <div class="col-md-2">
                                        <button type="submit" class="btn btn-lg btn-default btn-block" onclick="search();return false;">
                                            Find
                                        </button>
                                    </div>
                                </div>

                                <div class="form-group">

                                </div>
                            </form>
                        </div>
                        <div class="col-md-12" id="search_result">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection


@section('styles')
    <link rel="stylesheet" href="/css/bootstrap-tagsinput.css">
@endsection


@section('scripts')
    <script src="/js/bootstrap-tagsinput.min.js"></script>

    <script type="text/javascript">
        function search(){

            $('#search_result').html("<img style='margin:0 auto; display:block;' src='img/bar-loader.gif'>");
            $.ajax({
                url: '/near-me',
                type: 'post',
                data: $('form#search_form').serialize(),
                success: function(data) {
                    setTimeout(function(){
                        $('#search_result').html(data);
                    }, 1000);
                },
                error: function(xhr, textStatus, thrownError) {
                    alert('Something went to wrong.Please Try again later...');
                }

            });
        }

        $('#search_string').tagsinput();

    </script>

    <script src="/js/bootstrap3-typeahead.js"></script>

    <script type="text/javascript">
        $('.skill_fill').typeahead({
            source: function(query, process) {
                // `query` is the text in the field
                // `process` is a function to call back with the array
                $.ajax({
                    url: "/autofill/skills/"+$('.skill_fill').val(),
                    success: process
                });
            }
        });

    </script>


    <script type="text/javascript">
        $('.search_menu').on('click',function(){
            $('.search_menu').removeClass('active');
            $(this).addClass('active');
            var searchFor = $(this).attr('value');
            $('#search_data_for').val(searchFor);
            search();

        });
    </script>

    <script type="text/javascript">
        $('.search_level').on('click',function(){
            $('.search_level').removeClass('active');
            $(this).addClass('active');
            var searchLevel = $(this).attr('value');
            $('#search_data_level').val(searchLevel);
            search();
        });
    </script>


    <script type="text/javascript">
        $('.search_location').on('click',function(){
            $('.search_location').removeClass('active');
            $(this).addClass('active');
            var searchLocation = $(this).attr('value');
            $('#search_data_location').val(searchLocation);
            search();
        });
    </script>




@endsection