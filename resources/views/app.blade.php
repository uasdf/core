<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">

	<link rel="icon" type="image/jpg" href="favicon-pr.ico">
	@yield('title')

	<link href="/css/app.css" rel="stylesheet">
	<link href="/css/style.css" rel="stylesheet">
	<link href="/css/bootstrap-theme.css" rel="stylesheet">
	<link href="/css/bootstrap-dialog.css" rel="stylesheet">




	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	@yield('styles')

	<!-- HTML5 shim and Respond.js for IE8 s AbstractSmtpTransportupport of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','//connect.facebook.net/en_US/fbevents.js');

	fbq('init', '1756808314541413');
	fbq('track', "PageView");</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=1756808314541413&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->

</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" style='padding: 0 10px;'>
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/">
					<img alt="Placerange" src="/img/logo-main.jpg">
				</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				{{--<ul class="nav navbar-nav">--}}
					{{--<li><a href="/home">Home</a></li>--}}
				{{--</ul>--}}
				@if (Auth::user())
				<div class="col-sm-4 col-md-3 col-md-offset-2 text-center">
					
					<form class="navbar-form" role="search">
						
						<div class="input-group" style="width: 82%;">
							<input type="text" class="form-control search " placeholder="Search" name="searchbox" id="searchbox" autocomplete="off">
							<div id="display">
							</div>
						</div>
					</form>
				</div>
				@endif



				<ul class="nav navbar-nav navbar-right ">

					@if (Auth::guest())
						<li><a href="/login"><span class="glyphicon glyphicon-play-circle pomo"></span> &nbsp;Login</a></li>
						<li><a href="/register"><span class="glyphicon glyphicon-check vista"></span> &nbsp;Register</a></li>
						<li><a href="/feedback"><span class="glyphicon glyphicon-comment sin"></span> &nbsp;Give a Feedback</a></li>
					@else
						@if (Auth::user()->role == 'admin')
							<li><a href="/UASDFmin">Admin</a></li>
						@endif
						<li><a href="/near-me"><span class="glyphicon glyphicon-map-marker pomo"></span> &nbsp;Near Me</a></li>
						<li><a href="/messages"><span class="glyphicon glyphicon-comment sin"></span> &nbsp;Messages <?php $unread_message_count = unread_message_count(Auth::user()->id); if($unread_message_count > 0){?><span class="badge" id="msg_count"><?=$unread_message_count?></span><?php } ?></a></li>
						<li><a href="/notifications"><span class="glyphicon glyphicon-th-list c_cal"></span> &nbsp;Notifications <?php $unread_notifications_count = unread_notifications_count(Auth::user()->id); if($unread_notifications_count > 0){?><span class="badge" id="not_count"><?=$unread_message_count?></span><?php } ?></a></li>
						<li><a href="/profile"><span class="glyphicon glyphicon-th-large "></span> &nbsp;Dashboard</a></li>
						<li><a href="/home"><span class="glyphicon glyphicon-home vista"></span> &nbsp;Home</a></li>
						<li class="dropdown">
							<a href="/profile" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="padding-top: 10px;padding-bottom: 10px;">
								<img src='<?=user_avatar(Auth::user()->image_name,'icon')?>' style='height:30px;width:30px;'> 
								<span class="caret"></span>
								</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="/issues">Issues</a></li>
								<li><a href="/programs">Programs</a></li>
								<li><a href="/profile/edit">Edit profile</a></li>
								<li><a href="/invite-friends">Find/Invite friends</a></li>
								<li><a href="/feedback">Give a Feedback</a></li>
								<li><a href="/auth/logout">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>
	@yield('header_fullwidth')
	<div class="container-fluid" style="padding-bottom: 50px;">
		<div class="row" style="margin: 0px;">
			<div class="col-md-10 col-md-offset-1">
				@include('flash::message')
			</div>

			@yield('content')
		</div>
	</div>
	<footer class="footer" id="footer">
		<div class="container">
			<p class="text-muted">All content &copy; <?php print date('Y'); ?> Placerange. All Rights Reserved. </p>
		</div>
	</footer>


	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="/js/bootstrap-dialog.js"></script>
	<script src="/js/debounce.js"></script>
	<script src="/js/plugins/rateit/jquery.rateit.js" type="text/javascript"></script>
	@if (Auth::user())
	<script>

		var tid = setInterval(count_update, 10000);
		function count_update() {
			$.ajax({
				url: '/notifications-count/{{Auth::user()->id}}',
				type: 'post',
				success: function(data) {
					$('#not_count').html(data);
//					alert('Notification Count :' + data);
				},
				error: function(xhr, textStatus, thrownError) {
					// alert('Something went to wrong.Please Try again later...');
				}

			});

			$.ajax({
				url: '/messages-count/{{Auth::user()->id}}',
				type: 'post',
				success: function(data) {
					$('#msg_count').html(data);
//					alert('Message Count :' + data);
				},
				error: function(xhr, textStatus, thrownError) {
					// alert('Something went to wrong.Please Try again later...');
				}

			});
		}
		function abortTimer() { // to be called when you want to stop the timer
		clearInterval(tid);
		}
	</script>

	@endif
	<script>

		$('#searchbox').keyup( $.debounce( 250, search ) ); // This is the line you want!

	function search() {
		var searchbox = $('#searchbox').val();
		var dataString = 'search_string=' + searchbox;
		if (searchbox == '') {
			$("#display").html('').hide();
		}
		else {
			if (dataString.length < 3) {
				return false;
			}
			$.ajax({
				type: "POST",
				url: "/post-search",
				data: dataString,
				cache: false,
				success: function (html) {
					$("#display").html(html).show();
				}
			});
		}
		return false;
	}
	</script>


	<script>
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
		});
	</script>


	<script>
		// --------------------
		// GOOGLE ANALYTICS 
		// --------------------
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-72501448-1', 'auto');
		@if (Auth::guest())
			ga('set', 'userId', ' Visitor'); 
		@else
			ga('set', 'userId', '{{Auth::user()->email}}'); 
		@endif
	
		ga('send', 'pageview');
	</script>



	@yield('scripts')

</body>
</html>
